#  Reviewed
#
#  This is the main Makefile to buid the DCT library and test programs
#
#  Authors:
#     Dany De Cecchis
#     Tony Drummond
################################################################
#                                                              #
#                Created on Jun 22                             #
#   07/03/08 DDC Modified for the new directories structure    #
################################################################
#
################################################################
# Options: Use the variable MACH to select the machine where the
#          code is going to be built.
################################################################
# Path
MACH    = $(shell uname -n)
ROOTLIB = $(PWD)
SOURCE  = $(ROOTLIB)/SRC
FINTER  = $(ROOTLIB)/SRC/FINTERF
TESTDIR = $(ROOTLIB)/TEST
BINDIR  = $(ROOTLIB)/BIN
LIBDIR  = $(ROOTLIB)/LIB
LIBINC  = $(ROOTLIB)/INCLUDE
DCTVER  = 0.9

###########################################################
# This is used to other architectures as generic GNU linux
#  Compiler Definitions
CC           = mpicc
# gcc
F90          = mpif90
# gfortran
MPICC        = mpicc
MPIF90       = mpif90
CLOAD        = mpicc

# Setting the flags
# Flags Definitions
# DCTPROD is defined for production i.e. optimize the code
# otherwise the debugging flag is activated
ifneq ($(DCTPROD),)
  CFLAGS       = -03 $(DEFINE) -Winit-self
  # -std=c99 -pedantic
  FFLAGS       = -O3 -J$(LIBINC)
  CLDFLAGS     = -O3
else
  CFLAGS       = -g3 -ggdb $(DEFINE) -Winit-self # -Wall -Wextra
  # -std=c99 -pedantic
  FFLAGS       = -g3 -ggdb -J$(LIBINC) # -Wall
  CLDFLAGS     = -ggdb # -Wall -Wextra
endif
# Lib and Include
MPIINC = /usr/include
INCLUDE      = -I$(LIBINC)  -I$(MPIINC)
FINCLUDE     = -I$(LIBINC)
LIBPROF      = 

AR           = ar
ARFLAGS      = -ruc

CLIB =  -L$(LIBDIR)
ifeq ($(PROF),IPM) # It is used to profile
   CLIB     += $(IPM)
   DEFINE = -DIPM
endif

LIB         = $(CLIB) -ldct -lm
FLIB        = $(CLIB) -lfdct -lm
# NetCDF present
CNETCDFLIB   = -lnetcdf
FNETCDFLIB   = -lnetcdff

# Source Files
# SOURC = $(wildcard $(SOURCE)/*.c )
# OBJC  = $(SOURC:.c=.o)
# SOURIF = $(wildcard $(FINTER)/*.c )
# OBJIF  = $(SOURC:.c=.o)


EXECUTABLES  = DCT_TEST_SIZE TEST_DCT_PERFORM TEST_DCT_PERFOR_LOOPS \
               TEST_SD_COMM_INTRP TEST_SUBDOM TEST_DCT_PERF_3D # TEST_BUG
              

all: library finterf $(EXECUTABLES)

# library:  build the library
##################################################################################
library:
	@echo "############################################################################"
	@echo ""
	(cd $(SOURCE); make "CC=$(CC)" "CFLAGS=$(CFLAGS)" "ARFLAGS=$(ARFLAGS)" \
        "INCLUDE=$(INCLUDE)" "LIBINC=$(LIBINC)")
	$(AR) $(ARFLAGS) $(LIBDIR)/libdct.a  $(SOURCE)/*.o
	@echo ""
	@echo "############################################################################"
##################################################################################

# finterf:  build the fortran wrapper library
##################################################################################
finterf: library
	@echo "############################################################################"
	@echo ""
	(cd $(FINTER); make "CC=$(MPICC)" "F90=$(F90)" "CFLAGS=$(CFLAGS)" "FFLAGS=$(FFLAGS)"\
        "ARFLAGS=$(ARFLAGS)" "INCLUDE=$(INCLUDE)" "CLIB=$(CLIB)" "LIBINC=$(LIBINC)")
	$(AR) $(ARFLAGS) $(LIBDIR)/libfdct.a  $(SOURCE)/*.o $(FINTER)/dct_fortran.o
	@echo ""
	@echo "############################################################################"
##################################################################################

# release:  Pack a release of the library
##################################################################################
release: cleanall doc
	@echo "############################################################################"
	@echo ""
	( cd ..; tar -zcvf DCT_$(DCTVER).tgz DCT/Makefile DCT/SRC/Makefile \
	        DCT/SRC/dct_*.c DCT/TEST/Makefile DCT/TEST/test*.[cfFi]* DCT/TEST/inter*.[cfF]* \
	        DCT/INCLUDE/*.h DCT/FINTERF/*.[cf]* DCT/FINTERF/Makefile DCT/LIB DCT/BIN)

	@echo ""
	@echo "############################################################################"
##################################################################################

# backup:  Make a backup package just time stamping a release
##################################################################################
backup: release
	@echo "############################################################################"
	@echo ""
	( cd ..; mv DCT_$(DCTVER).tgz DCT_$(DCTVER)_`date +"%Y-%m-%d"`.tgz)

	@echo ""
	@echo "############################################################################"
##################################################################################

clean:
	@echo "Cleaning object files..."
	@echo ""
	rm -f *~ *.o
	(cd $(LIBINC); rm -f *~ *.mod )
	(cd $(SOURCE); make clean)
	(cd $(FINTER); make clean)
	(cd $(TESTDIR); make clean)
	@echo ""
	
cleanex: clean
	@echo "Cleaning exectables files..."
	@echo ""
	(cd $(BINDIR); rm -f $(EXECUTABLES))

cleanall: clean cleanex
	@echo "Cleaning library files..."
	@echo ""
	rm -f $(LIBDIR)/libdct.a $(LIBDIR)/libfdct.a

# TEST_BUG: Test to isolate bug in mixcoalt code
#
##################################################################################
TEST_BUG: library $(TESTDIR)/test_bug_mixcoalt.c \
  $(TESTDIR)/test_bug_resflow.cpp $(TESTDIR)/test_bug_subflow.cpp
	@echo "############################################################################"
	@echo ""
	(cd $(TESTDIR); make test_bug_mixcoalt.o "CC=$(CC)" "CFLAGS=$(CFLAGS)" \
            "ARFLAGS=$(ARFLAGS)" "INCLUDE=$(INCLUDE)" "LIB=$(CLIB)")
	@echo ""
	$(MPICC) $(CLDFLAGS) $(INCLUDE) -o $(BINDIR)/TEST_BUG \
      $(TESTDIR)/test_bug_mixcoalt.o $(TESTDIR)/bug_resflow.o \
      $(TESTDIR)/test_bug_subflow.o $(LIB)
	@echo ""
	@echo "############################################################################"
##################################################################################

# TEST_DCT_PERF_3D: Test for one way coupling using DCT and DCT_3d_Var
#
##################################################################################
TEST_DCT_PERF_3D: library $(TESTDIR)/test_dct_perform_3d.c \
  $(TESTDIR)/test_dct_perform_a_3d.c $(TESTDIR)/test_dct_perform_b_3d.c
	@echo "############################################################################"
	@echo ""
	(cd $(TESTDIR); make test_dct_perform_3d.o "CC=$(CC)" "CFLAGS=$(CFLAGS)" \
            "ARFLAGS=$(ARFLAGS)" "INCLUDE=$(INCLUDE)" "LIB=$(CLIB)")
	@echo ""
	$(MPICC) $(CLDFLAGS) $(INCLUDE) -o $(BINDIR)/TEST_DCT_PERF_3D \
      $(TESTDIR)/interpo_tools_c.o $(TESTDIR)/test_dct_perform_3d.o \
      $(TESTDIR)/test_dct_perform_a_3d.o $(TESTDIR)/test_dct_perform_b_3d.o $(LIB)
	@echo ""
	@echo "############################################################################"
##################################################################################

# TEST_SUBDOM:  Test the communication between 3 models and the resolution transfor.
#               under a FORTRAN 90 program, using the fortran binding
##################################################################################
TEST_SUBDOM: finterf $(TESTDIR)/test_subdom_comm.f90
	@echo "############################################################################"
	@echo ""
	(cd $(TESTDIR); make test_subdom_comm.o "F90=$(F90)" "FFLAGS=$(FFLAGS)" \
            "FINCLUDE=$(FINCLUDE)" )
	@echo ""
	$(F90) $(CLDFLAGS) -o $(BINDIR)/TEST_SUBDOM $(TESTDIR)/test_subdom_comm.o \
            $(TESTDIR)/test_subdom_moda.o $(TESTDIR)/test_subdom_modb.o \
            $(TESTDIR)/test_subdom_modc.o $(TESTDIR)/interp_tools.o $(FLIB)
	@echo ""
	@echo "############################################################################"
##################################################################################

# test_fld_par_interp: Verifies the all registration and communication
#                      for coupling 2 models, using DCT_Fields 
##################################################################################
TEST_SD_COMM_INTRP: library $(TESTDIR)/test_subdom_comm_intp.c
	@echo "############################################################################"
	@echo ""
	(cd $(TESTDIR); make test_subdom_comm_intp.o "CC=$(CC)" "CFLAGS=$(CFLAGS)" \
            "ARFLAGS=$(ARFLAGS)" "INCLUDE=$(INCLUDE)" "LIB=$(CLIB)")
	@echo ""
	$(MPICC) $(CLDFLAGS) $(INCLUDE) -o $(BINDIR)/TEST_SD_COMM_INTRP \
      $(TESTDIR)/test_subdom_comm_intp.o $(TESTDIR)/test_sd_comm_intp_tool.o \
      $(TESTDIR)/test_sd_comm_a.o $(TESTDIR)/test_sd_comm_b.o $(LIB)
	@echo ""
	@echo "############################################################################"
##################################################################################

# TEST_DCT_PERFORM: Test for one time communication using DCT
#
##################################################################################
TEST_DCT_PERFORM: library $(TESTDIR)/test_dct_perform.c $(TESTDIR)/test_dct_perform_a.c $(TESTDIR)/test_dct_perform_b.c
	@echo "############################################################################"
	@echo ""
	(cd $(TESTDIR); make test_dct_perform.o "CC=$(CC)" "CFLAGS=$(CFLAGS)" \
            "ARFLAGS=$(ARFLAGS)" "INCLUDE=$(INCLUDE)" "LIB=$(CLIB)")
	@echo ""
	$(MPICC) $(CLDFLAGS) $(INCLUDE) -o $(BINDIR)/TEST_DCT_PERFORM \
      $(TESTDIR)/interpo_tools_c.o $(TESTDIR)/test_dct_perform.o \
      $(TESTDIR)/test_dct_perform_a.o $(TESTDIR)/test_dct_perform_b.o $(LIB)
	@echo ""
	@echo "############################################################################"
##################################################################################

# TEST_DCT_PERFOR_LOOPS: Test for time looping communication using DCT
#
##################################################################################
TEST_DCT_PERFOR_LOOPS: library $(TESTDIR)/test_dct_perfor_loops.c $(TESTDIR)/test_dct_perfor_loops_a.c $(TESTDIR)/test_dct_perfor_loops_b.c
	@echo "############################################################################"
	@echo ""
	(cd $(TESTDIR); make test_dct_perfor_loops.o "CC=$(CC)" "CFLAGS=$(CFLAGS)" \
            "ARFLAGS=$(ARFLAGS)" "INCLUDE=$(INCLUDE)" "LIB=$(CLIB)")
	@echo ""
	$(MPICC) $(CLDFLAGS) $(INCLUDE) -o $(BINDIR)/TEST_DCT_PERFOR_LOOPS \
      $(TESTDIR)/test_dct_perfor_loops.o $(TESTDIR)/test_dct_perfor_loops_a.o \
      $(TESTDIR)/test_dct_perfor_loops_b.o $(TESTDIR)/test_dct_perf_common.o \
      $(TESTDIR)/interpo_tools_c.o $(LIB)
	@echo ""
	@echo "############################################################################"
##################################################################################

# size_test: Verifies the size in bytes of the different structures defined in 
#             dct_fortran.f90
##################################################################################
DCT_TEST_SIZE: library  $(TESTDIR)/size_test.c
	@echo "############################################################################"
	@echo ""
	(cd $(TESTDIR); make size_test.o "CC=$(CC)" "CFLAGS=$(CFLAGS)" \
            "ARFLAGS=$(ARFLAGS)" "INCLUDE=$(INCLUDE)" "LIB=$(CLIB)")
	@echo ""
	$(CLOAD) $(CLDFLAGS) $(INCLUDE) -o $(BINDIR)/DCT_TEST_SIZE \
      $(TESTDIR)/size_test.o $(LIB)
	@echo ""
	@echo "############################################################################"
##################################################################################

# convertnetcdf: Converts files from convencional to netcdf
# 
##################################################################################
CONV2NETCDF: $(TESTDIR)/convertnetcdf.c
	@echo "############################################################################"
	@echo ""
	(cd $(TESTDIR); make convertnetcdf.o "CC=$(CC)" "CFLAGS=$(CFLAGS)" \
            "ARFLAGS=$(ARFLAGS)" "INCLUDE=$(INCLUDE)" "LIB=$(CLIB)")
	@echo ""
	$(CLOAD) $(CLDFLAGS) $(INCLUDE) -o $(BINDIR)/CONV2NETCDF \
      $(TESTDIR)/convertnetcdf.o $(CLIB) $(CNETCDFLIB)
	@echo ""
	@echo "############################################################################"
##################################################################################

