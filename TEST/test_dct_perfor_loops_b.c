/*******************************************************************/
/*                 Distributed Coupling Toolkit (DCT)              */
/*                                                                 */
/*!
   \file test_dct_perfor_loops_b.c

      \brief This program is to test the DCT performance of all
      the phases and interpoaltion operations with timestepping
      and fully 2 way coupling. Dummy model B.
      
      These tests consist of defining two dummy models with
      different resolutions and perform simulated two-way forcing
      of model variables. In a way that one model forces one
      variable into the other at a given interval, and in turn
      the counterpart model forces back a different variable into
      the model.

      This corresponds the implementation of dummy model
      B, which sends or produces the values of vv_b and
      receives or consumes the values of uu_b.

    \date Created on Sep 30, 2011
    
    \author Dany De Cecchis: dcecchis@gmail.com
    \author Tony Drummond: LADrummond@lbl.gov

    \copyright GNU Public License.

*/
/*******************************************************************/

#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include <mpi.h>
#ifdef MEMTRK
  #include <TAU.h>
#endif
#include "dct.h"
#include "interp_tools.h"
#include "test_dct_perform.h"

/******************************************************************/
/*                     test_dct_perform_b                         */
/*                                                                */
/*   This function implements the dummy model B, which receives   */
/*   or consumes the values                                       */
/*                                                                */
/*  np:         Is a pointer to an arry of two positions,         */
/*              corresponding the number of processes in each     */
/*              direction                                         */
/*  pts:        Is a pointer to an arry of two positions,         */
/*              corresponding the number of points in each        */
/*              direction                                         */
/*  grank:      Process Id respecting the globalcomm              */
/*  gnumtasks:  Number of task in the globalcomm                  */
/*  split:      Global rank that model b processes start          */
/*  dom_comm:   Communicator corresponding to the model           */
/*  globalcomm: Global communicator, including all models         */
/******************************************************************/
int test_dct_perfor_loops_b( int *np, int *pts, int split, MPI_Comm dom_comm,
                                                     MPI_Comm globalcomm )
{
/* ---------------------------------------  Variables Declaration  */
   int          ierr = 0;
   DCT_Error    dcterr = { DCT_SUCCESS, (DCT_String)NULL};
   
//    char        fname[50];
//    char        vname[12];   
   int         rank, numtasks, rc, dest, source, tag;
   int         nbrs[NGBRS]={MPI_PROC_NULL, MPI_PROC_NULL, MPI_PROC_NULL, MPI_PROC_NULL};
   MPI_Request horreqs[NGBRS];
   MPI_Request verreqs[NGBRS];
   MPI_Status  horstats[NGBRS];
   MPI_Status  verstats[NGBRS];
   MPI_Datatype rowtype, coltype;
   double     *inbuf[2][NGBRS], *outbuf[2][NGBRS];
   
   DCT_Couple   couple;
   DCT_Model    model;
   DCT_Field    dctuu, dctvv;

//    DCT_Scalar  *xmarks;
//    DCT_Scalar  *ymarks;
   DCT_Scalar   xmarks[2];
   DCT_Scalar   ymarks[2];
   DCT_Scalar   xVarMarks[2];
   DCT_Scalar   yVarMarks[2];
   DCT_Scalar   deltax, deltay;
   DCT_Integer  xnpts, ynpts, npts;
   DCT_Integer  npx, npy;

   DCT_Integer *iniind;
   DCT_Integer *endind;
   DCT_Integer  nnx, nny, nn;
   DCT_Rank    *mranks;
   DCT_Integer  xintdiv, yintdiv, xintres, yintres;

   double      tini,dt, tend, tt, alpha, aux;
   double     *uu, *vv;
   double     *VV[2], *current;

   int         ii, jj, indi, indj, inew, inix, iniy, endx, endy, lastii, lastjj;
   int         locinix, locnnx, lociniy, locnny;
   int         halox, haloy;

/* -----------------------------  BEGIN( test_dct_perfor_loops_b ) */
// #ifdef IPM
//    MPI_Pcontrol( 1,"model_b");
// #endif

   rc  = MPI_Comm_size(dom_comm, &numtasks);
   rc |= MPI_Comm_rank(dom_comm, &rank);
   if ( rc != MPI_SUCCESS ) {
         fprintf( stderr, "\nERROR: Error asking model rank and comm size.\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( rc );
   }  

   mranks = (DCT_Rank *)malloc( sizeof(DCT_Rank)*(size_t)numtasks );
   if ( mranks == (DCT_Rank *)NULL ) {
         fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }
   iniind = (DCT_Integer *)malloc( sizeof(DCT_Integer)*2*(size_t)numtasks );
   if ( iniind == (DCT_Integer *)NULL ) {
         fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }
   endind = (DCT_Integer *)malloc( sizeof(DCT_Integer)*2*(size_t)numtasks );
   if ( endind == (DCT_Integer *)NULL ) {
         fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }

   /* Generating the model ticks marks */
   xnpts = (DCT_Integer)pts[0];
   ynpts = (DCT_Integer)pts[1];
   /*** Generating the model domain tick marks ***/
   xmarks[0] = INIX;
   xmarks[1] = ENDX;
   ymarks[0] = INIY;
   ymarks[1] = ENDY;
   lastii = xnpts-1;
   lastjj = ynpts-1;
   deltax = 1.0/(DCT_Scalar)lastii;
   deltay = 1.0/(DCT_Scalar)lastjj;
//    dcterr = DCT_Gen_Number_Labels( INIX, ENDX, xnpts-1, &xmarks, &npts);
//    DCTCHKERR( dcterr );
//    if ( npts != xnpts ) {
//          fprintf( stderr, "\nERROR[X-Labels]: Error number of tick marks in x-direction\n" );
//          fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
//          return( 1 );
//    }
//    dcterr = DCT_Gen_Number_Labels( INIY, ENDY, ynpts-1, &ymarks, &npts);
//    DCTCHKERR( dcterr );
//    if ( npts != ynpts ) {
//          fprintf( stderr, "\nERROR[Y-Labels]: Error number of tick marks in y-direction\n" );
//          fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
//          return( 1 );
//    }
   
   /****  Determine the subdomain distribution  ****/
   npx = np[0];
   npy = np[1];
   xintdiv = xnpts/npx;
   xintres = xnpts%npx;
   yintdiv = ynpts/npy;
   yintres = ynpts%npy;

   for ( ii=0; ii < numtasks; ii++ ) {
      jj = 2*ii;
      mranks[ii] = (DCT_Rank)ii + split;
      indi = ii%npx;
      indj = ii/npx;

      /* Calculating correspondig size in X direction for each node */
      /* how many elements a block has in x-direction */
      nnx = xintdiv + (indi < xintres? 1: 0);
      /* Where index each block begins */
      inix = indi*xintdiv + (indi < xintres? indi: xintres);
      iniind[jj] = inix;
      endind[jj] = inix + nnx - 1;

      jj++;
      /* Calculating correspondig size in Y direction for each node */
      /* how many elements a block has in y-direction */
      nny = yintdiv + (indj < yintres? 1: 0);
      /* Where index each block begins */
      iniy = indj*yintdiv + (indj < yintres? indj: yintres);
      iniind[jj] = iniy;
      endind[jj] = iniy + nny - 1;
   }
   /* Determining the local size of process' exclusive model subdomain */
   jj = 2*rank;


   inix = iniind[jj];
   endx = endind[jj];
   nnx = endx - inix + 1;
   if ( inix == 0 ) xVarMarks[0] = INIX;
   else xVarMarks[0] = (INIX*(double)(lastii-inix) + ENDX*(double)inix)*deltax;
   if ( endx == lastii ) xVarMarks[1] = ENDX;
   else xVarMarks[1] = (INIX*(double)(lastii-endx) + ENDX*(double)endx)*deltax;
   jj++;
   iniy = iniind[jj];
   endy = endind[jj];
   nny = endy - iniy + 1;
   if ( iniy == 0 ) yVarMarks[0] = INIY;
   else yVarMarks[0] = (INIY*(double)(lastjj-iniy) + ENDY*(double)iniy)*deltay;
   if ( endy == lastjj ) yVarMarks[1] = ENDY;
   else yVarMarks[1] = (INIY*(double)(lastjj-endy) + ENDY*(double)endy)*deltay;

   /** Creating the coupling model variable **/
   nn = nnx*nny;
   uu = (double *)malloc( sizeof(double)*(size_t)nn );
   if ( uu == (double *)NULL ) {
         fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }  
   vv = (double *)malloc( sizeof(double)*(size_t)nn );
   if ( vv == (double *)NULL ) {
         fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }
   tini =  1.0;
   dt   =  1.0;
//    if ( xnpts <= 49152 ) tend = 8.0;
//    else tend = 16.0;
   tend = 10.0;

/*******************************************************************
 *****************   DCT_BeginRegistration Call   ******************
 *******************************************************************/
   if (rank == 0 ) printf("Registrating...\n" );
   dcterr = DCT_BeginRegistration( globalcomm );
   DCTCHKERR( dcterr );

/*******************************************************************
 *******************   Creating the DCT_Model  *********************
 *******************************************************************/
   dcterr = DCT_Create_Model( &model, "model_b", "Dummy model B", numtasks );
   DCTCHKERR( dcterr );

   dcterr = DCT_Set_Model_Time( &model, tini, dt, DCT_TIME_NO_UNIT );
   DCTCHKERR( dcterr );
   
//    dcterr = DCT_Set_Model_Dom( &model, 2, 1, DCT_RECTILINEAR, xmarks, xnpts );
//    DCTCHKERR( dcterr );
//    dcterr = DCT_Set_Model_Dom( &model, 2, 2, DCT_RECTILINEAR, ymarks, ynpts );
//    DCTCHKERR( dcterr );
   dcterr = DCT_Set_Model_Dom( &model, 2, 1, DCT_CARTESIAN, xmarks, xnpts );
   DCTCHKERR( dcterr );
   dcterr = DCT_Set_Model_Dom( &model, 2, 2, DCT_CARTESIAN, ymarks, ynpts );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Model_ParLayout( &model, DCT_DIST_RECTANGULAR, np);
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Model_SubDom( &model, mranks, iniind, endind );
   DCTCHKERR( dcterr );

/*******************************************************************
 ***********************   Creating the uu  ************************
 *******************************************************************/
   dcterr = DCT_Create_Field( &dctuu, "uu_b", "uu variable from Model B",
                            CELSIUS, DCT_CONSUME);
   DCTCHKERR( dcterr );

   dcterr = DCT_Set_Field_Dims( &dctuu, nnx, nny );
   DCTCHKERR( dcterr );

//    dcterr = DCT_Set_Field_Labels( &dctuu, DCT_RECTILINEAR, (xmarks + inix), nnx,
//                                   DCT_RECTILINEAR, (ymarks + iniy), nny );
//    DCTCHKERR( dcterr );
   dcterr = DCT_Set_Field_Labels( &dctuu, DCT_CARTESIAN, xVarMarks, nnx,
                                  DCT_CARTESIAN, yVarMarks, nny );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Val_Location( &dctuu, DCT_LOC_CORNERS );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Time( &dctuu, DCT_TIME_NO_UNIT, tini );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Freq_Consumption( &dctuu, dt );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Values( &dctuu, DCT_DOUBLE, uu );
   DCTCHKERR( dcterr );

/*******************************************************************
 *********************  Linking uu to Model  ***********************
 *******************************************************************/
   dcterr =  DCT_Set_Model_Var( &model, &dctuu, DCT_FIELD_TYPE );
   DCTCHKERR( dcterr );

/*******************************************************************
 ***********************   Creating the vv  ************************
 *******************************************************************/
   dcterr = DCT_Create_Field( &dctvv, "vv_b", "vv variable from Model B",
                            CELSIUS, DCT_PRODUCE);
   DCTCHKERR( dcterr );

   dcterr = DCT_Set_Field_Dims( &dctvv, nnx, nny );
   DCTCHKERR( dcterr );

//    dcterr = DCT_Set_Field_Labels( &dctvv, DCT_RECTILINEAR, (xmarks + inix), nnx,
//                                   DCT_RECTILINEAR, (ymarks + iniy), nny );
//    DCTCHKERR( dcterr );
   dcterr = DCT_Set_Field_Labels( &dctvv, DCT_CARTESIAN, xVarMarks, nnx,
                                          DCT_CARTESIAN, yVarMarks, nny );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Val_Location( &dctvv, DCT_LOC_CORNERS );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Time( &dctvv, DCT_TIME_NO_UNIT, tini );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Freq_Production( &dctvv, dt );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Values( &dctvv, DCT_DOUBLE, vv );
   DCTCHKERR( dcterr );

/*******************************************************************
 *********************  Linking vv to Model  ***********************
 *******************************************************************/
   dcterr =  DCT_Set_Model_Var( &model, &dctvv, DCT_FIELD_TYPE );
   DCTCHKERR( dcterr );

/*******************************************************************
 **********************   Creating the Couple  *********************
 *******************************************************************/
   dcterr =  DCT_Create_Couple( &couple, "Dummy_clp",
               "Coupling beteween two dummy model", &model, "model_a" );
   DCTCHKERR( dcterr );

/*******************************************************************
 ******************   Linking uu to the Couple  ********************
 *******************************************************************/
   dcterr =  DCT_Set_Coupling_Vars( &couple, &dctuu, "uu_a", DCT_FIELD_TYPE,
                                    DCT_LINEAR_INTERPOLATION );
   DCTCHKERR( dcterr );
   
/*******************************************************************
 ******************   Linking vv to the Couple  ********************
 *******************************************************************/
   dcterr =  DCT_Set_Coupling_Vars( &couple, &dctvv, "vv_a", DCT_FIELD_TYPE,
                                    DCT_LINEAR_INTERPOLATION );
   DCTCHKERR( dcterr );
   
/*******************************************************************
 ******************   DCT_EndRegistration Call   *******************
 *******************************************************************/
   dcterr = DCT_EndRegistration(  );
   DCTCHKERR( dcterr );

   /* Determining process' neighbors and the required halo */
   /* Orientation is given considering the cartesian domain instead
      of matrix orientation. i and j grow RIGHT and UP respectively */
   locinix = inix;
   locnnx = nnx;
   lociniy = iniy;
   locnny = nny;
   indi = rank%npx;
   indj = rank/npx;
   halox = 0;
   haloy = 1;
   if ( indi > 0 ) {
      nbrs[LEFT] = rank-1;
      locinix -= 1;
      locnnx += 1;
      halox = 1;
   }
   if ( indi < npx-1 ) {
      nbrs[RIGHT] = rank+1;
      locnnx += 1;
   }
   if ( indj > 0) {
      nbrs[DOWN] = rank-npx;
      lociniy -= 1;
      locnny += 1;
      haloy = 1;
   }
   if ( indj < npy-1 ) {
      nbrs[UP] = rank+npx;
      locnny += 1;
   }

   /** Creating the model variable **/
   nn = locnnx*locnny;
   VV[0] = (double *)malloc( sizeof(double)*(size_t)nn );
   if ( VV[0] == (double *)NULL ) {
         fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }
   VV[1] = (double *)malloc( sizeof(double)*(size_t)nn );
   if ( VV[1] == (double *)NULL ) {
         fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }

   /** Creating the MPI data type **/
   MPI_Type_contiguous(nny, MPI_DOUBLE, &rowtype);
   MPI_Type_commit(&rowtype);

   MPI_Type_vector(locnnx, 1, locnny, MPI_DOUBLE, &coltype);
   MPI_Type_commit(&coltype);
   
   /*** LEFT and RIGHT starting buffer points ***/
   indj = halox;
   inbuf[0][LEFT] = VV[0] + indj;
   inbuf[1][LEFT] = VV[1] + indj;
   indj = locnny + halox;
   outbuf[0][LEFT] = VV[0] + indj;
   outbuf[1][LEFT] = VV[1] + indj;
   
   indj = (locnnx-1)*locnny + halox;
   inbuf[0][RIGHT] = VV[0] + indj;
   inbuf[1][RIGHT] = VV[1] + indj;
   indj = (locnnx-2)*locnny + halox;
   outbuf[0][RIGHT] = VV[0] + indj;
   outbuf[1][RIGHT] = VV[1] + indj;

   /*** UP and DOWN starting buffer points ***/
   indj = locnny - 1;
   inbuf[0][UP] = VV[0] + indj;
   inbuf[1][UP] = VV[1] + indj;
   indj = locnny - 2;
   outbuf[0][UP] = VV[0] + indj;
   outbuf[1][UP] = VV[1] + indj;
   
   indj = 0;
   inbuf[0][DOWN] = VV[0] + indj;
   inbuf[1][DOWN] = VV[1] + indj;
   indj = 1;
   outbuf[0][DOWN] = VV[0] + indj;
   outbuf[1][DOWN] = VV[1] + indj;   

   tt = tini;
   nn = nnx*nny;
   inew = 0; /* It is used to alternate the old and new values */
/*******************************************************************
 *******************************************************************
 ******************   Model Timesteping begins   *******************
 *******************************************************************/
   while ( tt <=  tend ) {
   
      if (rank == 0 ) printf("\t****** Model running time %g\n\t******\n", tt );
      dcterr =  DCT_Update_Model_Time( &model );
      DCTCHKERR( dcterr );
      
/*******************************************************************
 ******************   Receiving the variable uu   ******************
 *******************************************************************/
      if (rank == 0 ) printf("Receiving uu\n" );
      dcterr =  DCT_Recv_Field( &dctuu );
      DCTCHKERR( dcterr );
      /* Transfering  values from coupling var to the model var */
      indj = 0;
      current = VV[inew];
      if ( tt == tini ) {  /** VV = uu  **/
         for ( ii=0; ii < nnx; ii++ ) {
            indi = (halox + ii)*locnny + haloy;
            for ( jj=0; jj < nny; jj++ ) {
               *(current + indi++ ) = uu[indj++];
            }
         }
//          sprintf( fname, "./laplacian_VV_%d.m", rank );
//          sprintf( vname, "VV%d", rank );
//          rc = dprtdatMatrix( locnnx, locnny, VV[inew], vname, fname );
//          if ( rc ) {
//             fprintf( stderr, "\nERROR: Error writing the matrix Laplacian MM.\n" );
//             fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
//             return( 1 );
//          }
      }
      else {     /** VV = (uu + VV)/2  **/
         for ( ii=0; ii < nnx; ii++ ) {
            indi = (halox + ii)*locnny + haloy;
            for ( jj=0; jj < nny; jj++ ) {
               *(current + indi) = ( uu[indj] + *(current + indi) )/2.0;
               indi++; indj++;
            }
         }
      }

/*******************************************************************
 ******************   Updating boundary values   *******************
 *******************************************************************/
      /** Posting the receiving messages **/
      /** Communicating LEFT and RIGHT **/
      tag = HORTAG;
      for ( ii=0; ii < 2; ii++) {
         source = nbrs[ii];
         MPI_Irecv(inbuf[inew][ii], 1, rowtype, source, tag, dom_comm, (horreqs+ii) );
      }
      /** Communicating UP and DOWN **/
      tag = VERTAG;
      for ( ii=2; ii < 4; ii++) {
         source = nbrs[ii];
         MPI_Irecv(inbuf[inew][ii], 1, coltype, source, tag, dom_comm, (verreqs+ii-2) );
      }
      /** Posting the sending messages **/
      /** Communicating LEFT and RIGHT **/
      tag = HORTAG;
      for ( ii=0; ii < 2; ii++) {
         dest = nbrs[ii];
         MPI_Isend(outbuf[inew][ii], 1, rowtype, dest, tag, dom_comm, (horreqs+ii+2) );
      }
      MPI_Waitall(NGBRS, horreqs, horstats);
      /** Communicating UP and DOWN **/
      tag = VERTAG;
      for ( ii=2; ii < 4; ii++) {
         dest = nbrs[ii];
         MPI_Isend(outbuf[inew][ii], 1, coltype, dest, tag, dom_comm, (verreqs+ii) );
      }
      MPI_Waitall(NGBRS, verreqs, verstats);

/*******************************************************************
 ******************   Compute VV next time step   ******************
 *******************************************************************/
      update( locnnx, locnny, VV[inew], VV[1-inew]);
      inew = 1 - inew; /* Move to inew the new values */

/*******************************************************************
 *******************   Sending the variable vv   *******************
 *******************************************************************/
      if (rank == 0 ) printf("Sending vv\n" );
      /* Transfering values from model var to the coupling var */
      indj = 0;
      current = VV[inew];
      for ( ii=0; ii < nnx; ii++ ) {
         indi = (halox + ii)*locnny + haloy;
         for ( jj=0; jj < nny; jj++ ) {
            vv[indj++] = *(current + indi++);
         }
      }
      dcterr =  DCT_Send_Field( &dctvv );
      DCTCHKERR( dcterr );
/*******************************************************************
 *************   Letting global alpha = ||VV||_inf   ***************
 *******************************************************************/
      aux = (double)0.0;
      for ( ii=0; ii < nn; ii++ ) aux = dmax( aux, fabs (vv[ ii ]) );
      rc = MPI_Allreduce ( &aux, &alpha, 1, MPI_DOUBLE, MPI_MAX, dom_comm );
      if ( rc != MPI_SUCCESS ) {
            fprintf( stderr, "\nERROR: Error calling MPI_Allreduce.\n" );
            fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
            return( rc );
      }  
      /* Transfering values from model var to the coupling var */
      indj = 0;
      current = VV[inew];
      for ( ii=0; ii < nnx; ii++ ) {
         indi = (halox + ii)*locnny + haloy;
         for ( jj=0; jj < nny; jj++ ) {
            *(current + indi++ ) = alpha*vv[indj++];
         }
      }
  
      tt += dt;
   }
   
/*******************************************************************
 ********************   Model Timesteping ends   *******************
 *******************************************************************
 *******************************************************************/

/*******************************************************************
 ****************   Destroying the DCT structures   ****************
 *******************************************************************/
   if (rank == 0 ) printf("Finalizing...\n" );
   dcterr = DCT_Destroy_Couple( &couple );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Destroy_Field( &dctuu );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Destroy_Field( &dctvv );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Destroy_Model( &model );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Finalized(  );
   DCTCHKERR( dcterr );

   MPI_Type_free ( &rowtype );
   MPI_Type_free ( &coltype );

   free ( uu );
   free ( vv );
   free ( VV[0] );
   free ( VV[1] );
   free ( mranks );
   free ( iniind );
   free ( endind );
//    free ( xmarks );
//    free ( ymarks );
   
// #ifdef IPM
//    MPI_Pcontrol( -1,"model_b");
// #endif
   return ( ierr );
/* -------------------------------  END( test_dct_perfor_loops_b ) */

}
