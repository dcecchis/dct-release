/**************************************************************/
/*              Distributed Coupling Toolkit (DCT)            */
/*                                                            */
/*!
   \file test_dct_perform_b.c

      \brief This program is to test the DCT performance of all
      the phases and interpolation operations of one way and
      one variable. Dummy model B.
      
      The file implements a test consists of defining two dummy
      models with different resolutions and perform the forcing
      of one variable from one model into the other.

      This corresponds the implementation of dummy model
      B, which receives or consumes the values

    \date Created on Sep 30, 2011
    
    \author Dany De Cecchis: dcecchis@gmail.com
    \author Tony Drummond: LADrummond@lbl.gov

    \copyright GNU Public License.
*/
/**************************************************************/

#include <stdio.h>
#include <ctype.h>
#include <mpi.h>
#ifdef MEMTRK
  #include <TAU.h>
#endif
#include "dct.h"
#include "interp_tools.h"
#include "test_dct_perform.h"

/******************************************************************/
/*                     test_dct_perform_b                         */
/*                                                                */
/*   This function implements the dummy model B, which receives   */
/*   or consumes the values                                       */
/*                                                                */
/*  np:         Is a pointer to an arry of two positions,         */
/*              corresponding the number of processes in each     */
/*              direction                                         */
/*  pts:        Is a pointer to an arry of two positions,         */
/*              corresponding the number of points in each        */
/*              direction                                         */
/*  grank:      Process Id respecting the globalcomm              */
/*  gnumtasks:  Number of task in the globalcomm                  */
/*  split:      Global rank that model b processes start          */
/*  dom_comm:   Communicator corresponding to the model           */
/*  globalcomm: Global communicator, including all models         */
/******************************************************************/
int test_dct_perform_b( int *np, int *pts, int split, MPI_Comm dom_comm,
                                                     MPI_Comm globalcomm )
{
/* ---------------------------------------  Variables Declaration  */
   int          ierr = 0;
   DCT_Error    dcterr = { DCT_SUCCESS, (DCT_String)NULL};
   
   char         fname[50];
   
   int          rank, numtasks, rc;
   
   DCT_Couple   couple;
   DCT_Model    model;
   DCT_Field    dcttemp;

   DCT_Scalar  *xmarks;
   DCT_Scalar  *ymarks;
   DCT_Integer  xnpts, ynpts, npts;
   DCT_Integer  npx, npy;

   DCT_Integer *iniind;
   DCT_Integer *endind;
   DCT_Integer  nnx, nny;
   DCT_Rank    *mranks;
   DCT_Integer  xintdiv, yintdiv, xintres, yintres;

   double      tini,dt, tend, tt;
   double     *temp;

   int         ii, jj, indi, indj, inix, iniy;

/* ----------------------------------  BEGIN( test_dct_perform_b ) */

   rc  = MPI_Comm_size(dom_comm, &numtasks);
   rc |= MPI_Comm_rank(dom_comm, &rank);
   if ( rc != MPI_SUCCESS ) {
         fprintf( stderr, "\nERROR: Error asking model rank and comm size.\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }  

   mranks = (DCT_Rank *)malloc( sizeof(DCT_Rank)*(size_t)numtasks );
   if ( mranks == (DCT_Rank *)NULL ) {
         fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }
   iniind = (DCT_Integer *)malloc( sizeof(DCT_Integer)*2*(size_t)numtasks );
   if ( iniind == (DCT_Integer *)NULL ) {
         fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }
   endind = (DCT_Integer *)malloc( sizeof(DCT_Integer)*2*(size_t)numtasks );
   if ( endind == (DCT_Integer *)NULL ) {
         fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }

   tini =  0.0;
   dt   =  1.0;
   tend = 3.0;
   
   /* Generating the model ticks marks */
   xnpts = (DCT_Integer)pts[0];
   ynpts = (DCT_Integer)pts[1];
   /*** Generating the model domain tick marks ***/
   dcterr = DCT_Gen_Number_Labels( INIX, ENDX, xnpts-1, &xmarks, &npts);
   DCTCHKERR( dcterr );
   if ( npts != xnpts ) {
         fprintf( stderr, "\nERROR[X-Labels]: Error number of tick marks in x-direction\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }
   dcterr = DCT_Gen_Number_Labels( INIY, ENDY, ynpts-1, &ymarks, &npts);
   DCTCHKERR( dcterr );
   if ( npts != ynpts ) {
         fprintf( stderr, "\nERROR[Y-Labels]: Error number of tick marks in y-direction\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }
   
   /****  Determine the subdomain distribution  ****/
   npx = np[0];
   npy = np[1];
   xintdiv = xnpts/npx;
   xintres = xnpts%npx;
   yintdiv = ynpts/npy;
   yintres = ynpts%npy;

   for ( ii=0; ii < numtasks; ii++ ) {
      jj = 2*ii;
      mranks[ii] = (DCT_Rank)ii + split;
      indi = ii%npx;
      indj = ii/npx;

      /* Calculating correspondig size in X direction for each node */
      /* how many elements a block has in x-direction */
      nnx = xintdiv + (indi < xintres? 1: 0);
      /* Where index each block begins */
      inix = indi*xintdiv + (indi < xintres? indi: xintres);
      iniind[jj] = inix;
      endind[jj] = inix + nnx - 1;

      jj++;
      /* Calculating correspondig size in Y direction for each node */
      /* how many elements a block has in y-direction */
      nny = yintdiv + (indj < yintres? 1: 0);
      /* Where index each block begins */
      iniy = indj*yintdiv + (indj < yintres? indj: yintres);
      iniind[jj] = iniy;
      endind[jj] = iniy + nny - 1;
//       if (rank == 0) printf("iniind[%d] = %d\n", ii, iniind[jj]);
   }
   
   /** Creating the model variable **/
   jj = 2*rank;
   inix = iniind[jj];
   nnx = endind[jj] - inix + 1;
   jj++;
   iniy = iniind[jj];
   nny = endind[jj] - iniy + 1;
   temp = (double *)malloc(sizeof(double)*(size_t)(nnx*nny) );
   if ( temp == (double *)NULL ) {
         fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }  

/*******************************************************************
 *****************   DCT_BeginRegistration Call   ******************
 *******************************************************************/
   if (rank == 0 ) printf("Registrating...\n" );
#ifdef IPM
   MPI_Pcontrol( 1,"Registration");
#endif
   dcterr = DCT_BeginRegistration( globalcomm );
   DCTCHKERR( dcterr );

/*******************************************************************
 *******************   Creating the DCT_Model  *********************
 *******************************************************************/
   dcterr = DCT_Create_Model( &model, "model_b", "Dummy model B", numtasks );
   DCTCHKERR( dcterr );

   dcterr = DCT_Set_Model_Time( &model, tini, dt, DCT_TIME_NO_UNIT );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Model_Dom( &model, 2, 1, DCT_RECTILINEAR, xmarks, xnpts );
   DCTCHKERR( dcterr );
   dcterr = DCT_Set_Model_Dom( &model, 2, 2, DCT_RECTILINEAR, ymarks, ynpts );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Model_ParLayout( &model, DCT_DIST_RECTANGULAR, np);
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Model_SubDom( &model, mranks, iniind, endind );
   DCTCHKERR( dcterr );

/*******************************************************************
 *******************   Creating the Temperature  *******************
 *******************************************************************/
   dcterr = DCT_Create_Field( &dcttemp, "sst_b", "Temperature at Sea Surface",
                            CELSIUS, DCT_CONSUME);
   DCTCHKERR( dcterr );

   dcterr = DCT_Set_Field_Dims( &dcttemp, nnx, nny );
   DCTCHKERR( dcterr );

   dcterr = DCT_Set_Field_Labels( &dcttemp, DCT_RECTILINEAR, (xmarks + inix), nnx,
                                  DCT_RECTILINEAR, (ymarks + iniy), nny );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Val_Location( &dcttemp, DCT_LOC_CORNERS );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Time( &dcttemp, DCT_TIME_NO_UNIT, tini );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Freq_Consumption( &dcttemp, dt );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Values( &dcttemp, DCT_DOUBLE, temp );
   DCTCHKERR( dcterr );

/*******************************************************************
 *****************  Linking Temperature to Model  ******************
 *******************************************************************/
   dcterr =  DCT_Set_Model_Var( &model, &dcttemp, DCT_FIELD_TYPE );
   DCTCHKERR( dcterr );


/*******************************************************************
 **********************   Creating the Couple  *********************
 *******************************************************************/
   dcterr =  DCT_Create_Couple( &couple, "Dummy_clp",
               "Coupling beteween two dummy model", &model, "model_a" );
   DCTCHKERR( dcterr );

/*******************************************************************
 **************   Linking Temperature to the Couple  ***************
 *******************************************************************/
   dcterr =  DCT_Set_Coupling_Vars( &couple, &dcttemp, "sst_a", DCT_FIELD_TYPE,
                                    DCT_LINEAR_INTERPOLATION );
   DCTCHKERR( dcterr );
   
/*******************************************************************
 ******************   DCT_EndRegistration Call   *******************
 *******************************************************************/
   dcterr = DCT_EndRegistration(  );
   DCTCHKERR( dcterr );
#ifdef IPM
   MPI_Pcontrol( -1,"Registration");
#endif
   
/*******************************************************************
 *******************************************************************
 ******************   Model Timesteping begins   *******************
 *******************************************************************/
   tt = tini;
   while ( tt <=  tend ) {
      if (rank == 0 ) printf("\t****** Model running time %g\n\t******\n", tt );
      dcterr =  DCT_Update_Model_Time( &model );
      DCTCHKERR( dcterr );
      
      if (rank == 0 ) printf("Receiving temperature\n" );
      dcterr =  DCT_Recv_Field( &dcttemp );
      DCTCHKERR( dcterr );
      
//       sprintf( fname, "./model_b_sst_%d_%g.m", rank, tt );
//       rc = dprtdat( nnx, nny, (xmarks + inix), (ymarks + iniy), temp, "xx", "yy", "sst", fname );
//       if ( rc ) {
//          fprintf( stderr, "\nERROR: Error writing model model b sst.\n" );
//          fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
//          return( 1 );
//       }
      
      tt += dt;
   }
   
/*******************************************************************
 ********************   Model Timesteping ends   *******************
 *******************************************************************
 *******************************************************************/

/*******************************************************************
 ****************   Destroying the DCT structures   ****************
 *******************************************************************/
   if (rank == 0 ) printf("Finalizing...\n" );
   dcterr = DCT_Destroy_Couple( &couple );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Destroy_Field( &dcttemp );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Destroy_Model( &model );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Finalized(  );
   DCTCHKERR( dcterr );

   free ( temp );
   free ( mranks );
   free ( iniind );
   free ( endind );
   free ( xmarks );
   free ( ymarks );
   
   return ( ierr );
/* ------------------------------------  END( test_dct_perform_b ) */

}