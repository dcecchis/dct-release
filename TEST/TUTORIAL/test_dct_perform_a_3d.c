/*******************************************************************/
/*                 Distributed Coupling Toolkit (DCT)              */
/*                                                                 */
/*!
   \file test_dct_perform_a_3d.c

      \brief This program is to test the DCT performance of all
      the phases and interpolation operations of one way and
      one 3d variable. Dummy model A.
      
      The file implements a test consists of defining two dummy
      models with different resolutions and perform the forcing
      of one variable from one model into the other.

      This corresponds the implementation of dummy model
      A, which sends or produces the values

    \date Created on Apr 12, 2018
    
    \author Dany De Cecchis: dcecchis@gmail.com

    \copyright GNU Public License.

*/
/*******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <mpi.h>
// #include "dct.h"
#include "interp_tools.h"
#include "test_dct_perform.h"

#define ROOT   0
#define PRB_DIM 3 /* Define de dimension of the problem */

/*** Funcition to generate equally spaced tick marks labels ***/
int Generate_Number_Labels( const double, const double, const int,
                            double **, int * );

/*******************************************************************/
/*                      test_dct_perform_a                         */
/*                                                                 */
/*!   \brief This program is used in the tutorial to learn how
      to use the DCT. Dummy model B.

       \param[in]         np  Is a pointer to an arry of three
                              positions, corresponding the number
                              of processes in each direction.
       \param[in]        pts  Is a pointer to an arry of three
                              positions, corresponding the number
                              of points in each direction.
       \param[in]   dom_comm  Communicator corresponding to the
                              model.
       \param[in] globalcomm  Global communicator, including all
                              models.

       \return An integer value with 0, if success and; otherwise,
               if there is an error.

*/
/*******************************************************************/

int main(int  argc, char *argv[])
// int test_dct_perform_a( int *np, int *pts, MPI_Comm dom_comm,
//                                           MPI_Comm globalcomm )
{
/* ---------------------------------------  Variables Declaration  */
   int         ierr = 0;
   int         np[PRB_DIM], pts[PRB_DIM];

   int         rank, numtasks, rc;
   
//    DCT_Error   dcterr = { DCT_SUCCESS, (DCT_String)NULL};
//    DCT_Couple  couple;
//    DCT_Model   model;
//    DCT_3d_Var  dcttemp;
   
   double  *xmarks;
   double  *ymarks;
   double  *zmarks;
   int  xnpts, ynpts, znpts, npts;
   int  npx, npy, npz;

   int *iniind;
   int *endind;
   int  nnx, nny, nnz;
   int    *mranks;
   int  xintdiv, yintdiv, xintres, yintres;
   int  zintdiv, zintres;

   double      tini,dt, tend, tt;
   double     *temp;

   int         ii, jj, kk, indi, indj, indk, inix, iniy, iniz;
   /*int         ntime;*/

/* ----------------------------------  BEGIN( test_dct_perform_a ) */

   rc = MPI_Init(&argc, &argv);
   if (rc != MPI_SUCCESS) {
      printf("Error starting MPI program. Terminating.\n");
      MPI_Abort(MPI_COMM_WORLD,rc);
   }

   rc  = MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
   rc |= MPI_Comm_rank(MPI_COMM_WORLD, &rank);
   if ( rc != MPI_SUCCESS ) {
         fprintf( stderr, "\nERROR: Error asking model rank and comm size.\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }  
 
    /* The number of parameter is checked */
   if (argc != 7) {
      if (rank == ROOT){
         printf("[ERROR %s]: %s npx npy npz nptsx nptsy nptsz\n", argv[0], argv[0] );
         printf("where npx, npy, npz, nptsx and nptsy are the number of processor in,\n");
         printf("x, y and z directions and the corresponding number of points in each\n");
         printf("direction.\n");
         printf("ERROR using the command\n");
      }
      MPI_Abort(MPI_COMM_WORLD,-1);
   }

   /* Discretization and parallel layout Model */
   np[0] = atoi(argv[1]);
   np[1] = atoi(argv[2]);
   np[2] = atoi(argv[3]);
   pts[0] = atoi(argv[4]);
   pts[1] = atoi(argv[5]);
   pts[2] = atoi(argv[6]);

   mranks = (int *)malloc( sizeof(int)*(size_t)numtasks );
   if ( mranks == (int *)NULL ) {
         fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }
   iniind = (int *)malloc( sizeof(int)*PRB_DIM*(size_t)numtasks );
   if ( iniind == (int *)NULL ) {
         fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }
   endind = (int *)malloc( sizeof(int)*PRB_DIM*(size_t)numtasks );
   if ( endind == (int *)NULL ) {
         fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }
    
   tini =  0.0;
   dt   =  1.0;
   tend = 3.0;
   
   /* Generating the model ticks marks */
   xnpts = pts[0];
   ynpts = pts[1];
   znpts = pts[2];
   /*** Generating the model domain tick marks ***/
   Generate_Number_Labels( INIX, ENDX, xnpts-1, &xmarks, &npts);
   if ( npts != xnpts ) {
         fprintf( stderr, "\nERROR[X-Labels]: Error number of tick marks in x-direction\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }
   Generate_Number_Labels( INIY, ENDY, ynpts-1, &ymarks, &npts);
   if ( npts != ynpts ) {
         fprintf( stderr, "\nERROR[Y-Labels]: Error number of tick marks in y-direction\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }
   Generate_Number_Labels( INIZ, ENDZ, znpts-1, &zmarks, &npts);
   if ( npts != znpts ) {
         fprintf( stderr, "\nERROR[Z-Labels]: Error number of tick marks in z-direction\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }
   
   /*------------------------------------------------------
    |          Start the domain decomposition description
    -------------------------------------------------------*/
   /****  Determine the subdomain distribution  ****/
   npx = np[0];
   npy = np[1];
   npz = np[2];
   xintdiv = xnpts/npx;
   xintres = xnpts%npx;
   yintdiv = ynpts/npy;
   yintres = ynpts%npy;
   zintdiv = znpts/npz;
   zintres = znpts%npz;

   for ( ii=0; ii < numtasks; ii++ ) {
      /* Index to jump every 3 times ii in the initial and end indices array */
      jj = 3*ii;
      mranks[ii] = ii;
      /* Calculating the (ii, jj, kk) indices from a
         vectorized indices in the lexicographic order */
      indk = ii/(npx*npy);
      kk   = ii%(npx*npy); /* Residual to calculate (ii, jj) */
      indj = kk/npx;
      indi = kk%npx;

      /* Calculating correspondig size in X direction for each node */
      /* how many elements a block has in x-direction */
      nnx = xintdiv + (indi < xintres? 1: 0);
      /* Where index each block begins */
      inix = indi*xintdiv + (indi < xintres? indi: xintres);
      iniind[jj] = inix;
      endind[jj] = inix + nnx - 1;

      jj++;
      /* Calculating correspondig size in Y direction for each node */
      /* how many elements a block has in y-direction */
      nny = yintdiv + (indj < yintres? 1: 0);
      /* Where index each block begins */
      iniy = indj*yintdiv + (indj < yintres? indj: yintres);
      iniind[jj] = iniy;
      endind[jj] = iniy + nny - 1;

      jj++;
      /* Calculating correspondig size in Z direction for each node */
      /* how many elements a block has in z-direction */
      nnz = zintdiv + (indk < zintres? 1: 0);
      /* Where index each block begins */
      iniz = indk*zintdiv + (indk < zintres? indk: zintres);
      iniind[jj] = iniz;
      endind[jj] = iniz + nnz - 1;
   }
   
   /*------------------------------------------------------
    |          Creating the piece of the variables for a
    |          particular PE
    -------------------------------------------------------*/
   /** Creating the model variable size corresponding the actual process **/
   jj = 3*rank;
   inix = iniind[jj];
   nnx = endind[jj] - inix + 1;
   jj++;
   iniy = iniind[jj];
   nny = endind[jj] - iniy + 1;
   jj++;
   iniz = iniind[jj];
   nnz = endind[jj] - iniz + 1;
   /*** The temperature values is calculated ***/
   temp = (double *)malloc( sizeof(double)*(size_t)(nnx*nny*nnz) );
   if ( temp == (double *)NULL ) {
         fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }  

// ******************************************************************
//  *****************   DCT_BeginRegistration Call   ******************
//  *******************************************************************/
//    if (rank == 0 ) printf("Registrating...\n" );
//    dcterr = DCT_BeginRegistration( globalcomm );
//    DCTCHKERR( dcterr );
// 
// ******************************************************************
//  *******************   Creating the DCT_Model  *********************
//  *******************************************************************/
//    dcterr = DCT_Create_Model( &model, "model_a", "Dummy model A", numtasks );
//    DCTCHKERR( dcterr );
// 
//    dcterr = DCT_Set_Model_Time( &model, tini, dt, DCT_TIME_NO_UNIT );
//    DCTCHKERR( dcterr );
//    
//    dcterr = DCT_Set_Model_Dom( &model, 3, 1, DCT_RECTILINEAR, xmarks, xnpts );
//    DCTCHKERR( dcterr );
//    dcterr = DCT_Set_Model_Dom( &model, 3, 2, DCT_RECTILINEAR, ymarks, ynpts );
//    DCTCHKERR( dcterr );
//    dcterr = DCT_Set_Model_Dom( &model, 3, 3, DCT_RECTILINEAR, zmarks, znpts );
//    DCTCHKERR( dcterr );
//    
//    * Check when in 3D is using only DCT_DIST_RECTANGULA layout **/
//    dcterr = DCT_Set_Model_ParLayout( &model, DCT_DIST_3D_RECTANGULAR, np );
//    DCTCHKERR( dcterr );
//    
//    dcterr = DCT_Set_Model_SubDom( &model, mranks, iniind, endind );
//    DCTCHKERR( dcterr );
// 
// ******************************************************************
//  *******************   Creating the Temperature  *******************
//  *******************************************************************/
//    dcterr = DCT_Create_3d_Var( &dcttemp, "sst_a", "Temperature at Sea Surface",
//                             CELSIUS, DCT_PRODUCE);
//    DCTCHKERR( dcterr );
// 
//    dcterr = DCT_Set_3d_Var_Dims( &dcttemp, nnx, nny, nnz );
//    DCTCHKERR( dcterr );
// 
//    dcterr = DCT_Set_3d_Var_Labels( &dcttemp, DCT_RECTILINEAR, (xmarks + inix), nnx,
//                                   DCT_RECTILINEAR, (ymarks + iniy), nny,
//                                   DCT_RECTILINEAR, (zmarks + iniz), nnz );
//    DCTCHKERR( dcterr );
//    
//    dcterr = DCT_Set_3d_Var_Val_Location( &dcttemp, DCT_LOC_CORNERS );
//    DCTCHKERR( dcterr );
//    
//    dcterr = DCT_Set_3d_Var_Time( &dcttemp, DCT_TIME_NO_UNIT, tini );
//    DCTCHKERR( dcterr );
//    
//    dcterr = DCT_Set_3d_Var_Freq_Production( &dcttemp, dt );
//    DCTCHKERR( dcterr );
//    
//    dcterr = DCT_Set_3d_Var_Values( &dcttemp, DCT_DOUBLE, temp );
//    DCTCHKERR( dcterr );
// 
// ******************************************************************
//  *****************  Linking Temperature to Model  ******************
//  *******************************************************************/
//    dcterr =  DCT_Set_Model_Var( &model, &dcttemp, DCT_3D_VAR_TYPE );
//    DCTCHKERR( dcterr );
// 
// 
// ******************************************************************
//  **********************   Creating the Couple  *********************
//  *******************************************************************/
//    dcterr =  DCT_Create_Couple( &couple, "Dummy_clp",
//                         "Coupling beteween two dummy model", &model, "model_b" );
//    DCTCHKERR( dcterr );
// 
// ******************************************************************
//  **************   Linking Temperature to the Couple  ***************
//  *******************************************************************/
//    dcterr =  DCT_Set_Coupling_Vars( &couple, &dcttemp, "sst_b", DCT_3D_VAR_TYPE,
//                                     DCT_NO_INTERPOLATION );
//    DCTCHKERR( dcterr );
//    
// ******************************************************************
//  ******************   DCT_EndRegistration Call   *******************
//  *******************************************************************/
//    dcterr = DCT_EndRegistration(  );
//    DCTCHKERR( dcterr );
   
/*******************************************************************
 *******************************************************************
 ******************   Model Timesteping begins   *******************
 *******************************************************************/
   tt = tini;
   /*ntime = 0;*/
  while ( tt <=  tend ) {
      if (rank == 0 )  printf("\t****** Model running time %g\n\t******\n", tt );
//       dcterr =  DCT_Update_Model_Time( &model );
//       DCTCHKERR( dcterr );
      
      /*
           And then we do some silly calculations
      */
      /** Some calculation to obtain temperature **/
      // Calculating model temperature */
      ierr = dfuncEval3D( (xmarks + inix), (ymarks + iniy ), (zmarks + iniz ),
                          temp, nnx, nny, nnz );
      if ( ierr ) {
         fflush( NULL );
         fprintf( stderr, "\nERROR[calculating temperature]: Error in function feed_temperature\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         fflush( NULL);
         return ( ierr );
      }

//       if (rank == 0 ) printf("Sending temperature\n" );
//       dcterr =  DCT_Send_3d_Var( &dcttemp );
//       DCTCHKERR( dcterr );
   
      tt += dt;
      /*ntime++;*/
  } /** End time loop **/
   
/*******************************************************************
 ********************   Model Timesteping ends   *******************
 *******************************************************************
 *******************************************************************/

// ******************************************************************
//  ****************   Destroying the DCT structures   ****************
//  *******************************************************************/
//    if (rank == 0 ) printf("Finalizing...\n" );
//    dcterr = DCT_Destroy_Couple( &couple );
//    DCTCHKERR( dcterr );
//    
//    dcterr = DCT_Destroy_3d_Var( &dcttemp );
//    DCTCHKERR( dcterr );
//    
//    dcterr = DCT_Destroy_Model( &model );
//    DCTCHKERR( dcterr );
//    
//    dcterr = DCT_Finalized(  );
//    DCTCHKERR( dcterr );
   
   free ( temp );
   free ( mranks );
   free ( iniind );
   free ( endind );
   free ( xmarks );
   free ( ymarks );
   free ( zmarks );
   
   MPI_Finalize();
   return ( ierr );
/* ------------------------------------  END( test_dct_perform_a ) */

}


/*******************************************************************/
/*                    Generate_Number_Labels                       */
/*                                                                 */
/*   Automatically generates a set of labels (tick marks) to
     be used in a variable or field given the quantity of
     intervals, i.e the interval, from star to end, is divided
     into n subintervals.

     \param[in]  start  first spatial label in a given direction.
     \param[in]    end  last spatial label in a given direction.
     \param[in]      n  number of subintervals.
     \param[out] point  Array of n generated labels.
     \param[out]   dim  number of generated labels.

     \return An integer with 0 if success, otherwise if error.

*/
/*******************************************************************/
int Generate_Number_Labels( const double start, const double end,
                                 const int n, double **point,
                                 int *dim)
{
/* ---------------------------------------------  Local Variables  */
  int i;
  double inc;
  int   ierr=1;

/* ------------------------------  BEGIN( Generate_Number_Labels ) */

  inc= (end - start)/(double)n;
  *dim = n+1;
  *point = (double *) malloc(sizeof(double)*(size_t)(*dim));

  if (point == NULL) {
    return(ierr);
  }
  else {
    for (i=0 ; i < *dim ; i++) {
      *(*point+i)= start + ((inc)*(double)i);
      /*printf("point[%d]= %lf == %lf\n", i, (*point)[i], start + (inc*(DCT_Scalar)i));*/
    }

  }
  return(ierr=0);

/* --------------------------------  END( Generate_Number_Labels ) */

}

