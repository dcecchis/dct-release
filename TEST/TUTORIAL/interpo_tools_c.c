/*******************************************************************/
/*                 Distributed Coupling Toolkit (DCT)              */
/*                                                                 */
/*!
    \file interpo_tools_c.c                      
                                                            
    \brief File of tools to check interpolation results.

    In this file there are different tool subroutines that
    generates the values to be interpolated on the several
    mesh implementation, and output result to be plotted using
    Matlab.                                              

    \date Created on Aug 10, 2011                
                                                            
    \author Dany De Cecchis: dcecchis@gmail.com
    \author Tony Drummond: LADrummond@lbl.gov

    \copyright GNU Public License.

*/
/*******************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include <float.h>

/*******************************************************************/
/*                            fprtdat                              */
/*                                                                 */
/*!    Print values of a function represented by two sets of
       labels and a linearized 2 dimensional matrix with the
       values of the function in float precision. The format
       to print the file is in a way that can be used a Matlab
       script and generate the variables named in the function.

       \param[in]     nx  Number of points along x-direction.
       \param[in]     ny  Number of points along y-direction.
       \param[in]     x1  Array of the tick marks values along
                          x-direction.
       \param[in]     x2  Array of the tick marks values along
                          y-direction.
       \param[in]     u1  Array of nx times ny with the values
                          of the function.
       \param[in] xname1  String for the name given to the tick
                          marks along x-direction.
       \param[in] xname2  String for the name given to the tick
                          marks along y-direction.
       \param[in]  yname  String for the name given to the
                          function values.
       \param[in]   fnam  String for the name given to the file
                          where all the values will be dumped.

       \return An integer value with 0, if success and; otherwise,
               if there is an error.

*/
/*******************************************************************/
int fprtdat(int nx, int ny, double *x1,
             double *x2, float *u1, char *xname1, char *xname2,
             char *yname, char *fnam) {
/* ---------------------------------------  Variables Declaration  */
   register int ix, iy;
   int     endi, offset;
   FILE *fp;
/* ---------------------------------------------  BEGIN( fprtdat ) */

   fp = fopen(fnam, "w");
   if ( fp == (FILE *)NULL ) {
         fprintf( stderr, "\nERROR[open]: Error opening the file %s\n", fnam );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }

   fprintf(fp,"%s = [ ", xname2);
   for (iy =0; iy < ny; iy++)
      fprintf(fp," %.15lE ", x2[iy]);
   fprintf(fp," ];\n\n");

   fprintf(fp,"%s = [ ", xname1);
   for (ix =0; ix < nx; ix++)
      fprintf(fp," %.15lE ", x1[ix]);
   fprintf(fp," ];\n\n");
   
   fprintf(fp,"%s = [ ", yname);
   endi = nx-1;
   for (ix =0; ix < endi; ix++) {
      offset = ix*ny;
      for (iy =0; iy < ny; iy++)
         fprintf(fp," %.15lE ", u1[offset + iy]);
      fprintf(fp, "\n");
   }

   offset = endi*ny;
   for (iy =0; iy < ny; iy++)
      fprintf(fp," %.15lE ", u1[offset + iy]);
   fprintf(fp," ];\n");

   fclose(fp);
   return 0;
/* -----------------------------------------------  END( fprtdat ) */
}

/*******************************************************************/
/*                            dprtdat                              */
/*                                                                 */
/*!    Print values of a function represented by two sets of
       labels and a linearized 2 dimensional matrix with the
       values of the function in double precision. The format
       to print the file is in a way that can be used a Matlab
       script and generate the variables named in the function.

       \param[in]     nx  Number of points along x-direction.
       \param[in]     ny  Number of points along y-direction.
       \param[in]     x1  Array of the tick marks values along
                          x-direction.
       \param[in]     x2  Array of the tick marks values along
                          y-direction.
       \param[in]     u1  Array of nx times ny with the values
                          of the function.
       \param[in] xname1  String for the name given to the tick
                          marks along x-direction.
       \param[in] xname2  String for the name given to the tick
                          marks along y-direction.
       \param[in]  yname  String for the name given to the
                          function values.
       \param[in]   fnam  String for the name given to the file
                          where all the values will be dumped.

       \return An integer value with 0, if success and; otherwise,
               if there is an error.

*/
/*******************************************************************/
int dprtdat(int nx, int ny, double *x1,
             double *x2, double *u1, char *xname1, char *xname2,
             char *yname, char *fnam) {
/* ---------------------------------------  Variables Declaration  */
   register int ix, iy;
   int     endi, offset;
   FILE *fp;
/* ---------------------------------------------  BEGIN( dprtdat ) */

   fp = fopen(fnam, "w");
   if ( fp == (FILE *)NULL ) {
         fprintf( stderr, "\nERROR[open]: Error opening the file %s\n", fnam );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }

   fprintf(fp,"%s = [ ", xname2);
   for (iy =0; iy < ny; iy++)
      fprintf(fp," %.15lE ", x2[iy]);
   fprintf(fp," ];\n\n");

   fprintf(fp,"%s = [ ", xname1);
   for (ix =0; ix < nx; ix++)
      fprintf(fp," %.15lE ", x1[ix]);
   fprintf(fp," ];\n\n");
   
   fprintf(fp,"%s = [ ", yname);
   endi = nx-1;
   for (ix =0; ix < endi; ix++) {
      offset = ix*ny;
      for (iy =0; iy < ny; iy++)
         fprintf(fp," %.15lE ", u1[offset + iy]);
      fprintf(fp, "\n");
   }

   offset = endi*ny;
   for (iy =0; iy < ny; iy++)
      fprintf(fp," %.15lE ", u1[offset + iy]);
   fprintf(fp," ]\';\n");

   fclose(fp);
   return 0;
/* -----------------------------------------------  END( dprtdat ) */
}

/*******************************************************************/
/*                           ldprtdat                              */
/*                                                                 */
/*!    Print values of a function represented by two sets of
       labels and a linearized 2 dimensional matrix with the
       values of the function in long double precision. The format
       to print the file is in a way that can be used a Matlab
       script and generate the variables named in the function.

       \param[in]     nx  Number of points along x-direction.
       \param[in]     ny  Number of points along y-direction.
       \param[in]     x1  Array of the tick marks values along
                          x-direction.
       \param[in]     x2  Array of the tick marks values along
                          y-direction.
       \param[in]     u1  Array of nx times ny with the values
                          of the function.
       \param[in] xname1  String for the name given to the tick
                          marks along x-direction.
       \param[in] xname2  String for the name given to the tick
                          marks along y-direction.
       \param[in]  yname  String for the name given to the
                          function values.
       \param[in]   fnam  String for the name given to the file
                          where all the values will be dumped.

       \return An integer value with 0, if success and; otherwise,
               if there is an error.

*/
/*******************************************************************/
int ldprtdat(int nx, int ny, double *x1,
             double *x2, long double *u1, char *xname1, char *xname2,
             char *yname, char *fnam) {
/* ---------------------------------------  Variables Declaration  */
   register int ix, iy;
   int     endi, offset;
   FILE *fp;
/* --------------------------------------------  BEGIN( ldprtdat ) */

   fp = fopen(fnam, "w");
   if ( fp == (FILE *)NULL ) {
         fprintf( stderr, "\nERROR[open]: Error opening the file %s\n", fnam );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }

   fprintf(fp,"%s = [ ", xname2);
   for (iy =0; iy < ny; iy++)
      fprintf(fp," %.15lE ", x2[iy]);
   fprintf(fp," ];\n\n");

   fprintf(fp,"%s = [ ", xname1);
   for (ix =0; ix < nx; ix++)
      fprintf(fp," %.15lE ", x1[ix]);
   fprintf(fp," ];\n\n");
   
   fprintf(fp,"%s = [ ", yname);
   endi = nx-1;
   for (ix =0; ix < endi; ix++) {
      offset = ix*ny;
      for (iy =0; iy < ny; iy++)
         fprintf(fp," %.15LE ", u1[offset + iy]);
      fprintf(fp, "\n");
   }

   offset = endi*ny;
   for (iy =0; iy < ny; iy++)
      fprintf(fp," %.15LE ", u1[offset + iy]);
   fprintf(fp," ]\';\n");

   fclose(fp);
   return 0;
/* ----------------------------------------------  END( ldprtdat ) */
}

/*******************************************************************/
/*                        dprtdatMatrix                            */
/*                                                                 */
/*!    Print a two-dimensional matrix in double precision. The
       format to print the file is in a way that can be used a
       Matlab script and generate the variables named in the
       function.

       \param[in]      n  Number of points along x-direction.
       \param[in]      m  Number of points along y-direction.
       \param[in]     MM  Array of the values of the matrix.
       \param[in]  Mname  String with the name give to the matrix.
       \param[in]   fnam  String for the name given to the file
                          where all the values will be dumped.

       \return An integer value with 0, if success and; otherwise,
               if there is an error.

*/
/*******************************************************************/
int dprtdatMatrix( int n, int m, double *MM, char *Mname,
                                                              char *fnam ) {
/* ---------------------------------------  Variables Declaration  */
   register int ix, iy;
   int     endi, offset;
   FILE *fp;
/* ---------------------------------------  BEGIN( dprtdatMatrix ) */

   fp = fopen(fnam, "w");
   if ( fp == (FILE *)NULL ) {
         fprintf( stderr, "\nERROR[open]: Error opening the file %s\n", fnam );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }

   fprintf(fp,"%s = [ ", Mname);
   endi = n-1;
   for (ix =0; ix < endi; ix++) {
      offset = ix*m;
      for (iy =0; iy < m; iy++)
         fprintf(fp," %.15lE ", MM[offset + iy]);
      fprintf(fp, "\n");
   }

   offset = endi*m;
   for (iy =0; iy < m; iy++)
      fprintf(fp," %.15lE ", MM[offset + iy]);
   fprintf(fp," ];\n");

   fclose(fp);
   return 0;
/* -----------------------------------------  END( dprtdatMatrix ) */
}

/*******************************************************************/
/*                           ffuncEval                             */
/*                                                                 */
/*!    Set a given array f in float precision, as a function of
       the x1 and x2 tick marks, i.e. f(x, y) = (x + y)*(x - y).

       \param[in]     x1  Array of the tick marks values along
                          x-direction.
       \param[in]     x2  Array of the tick marks values along
                          y-direction.
       \param[out]     f  Array of nx times ny with the values
                          of the function.
       \param[in]      m  Number of points along x-direction.
       \param[in]      n  Number of points along y-direction.

       \return An integer value with 0, if success and; otherwise,
               if there is an error.

*/
/*******************************************************************/
int ffuncEval(double *x1, double *x2, float *f,
              int m, int n )
{
/* ---------------------------------------  Variables Declaration  */
   register int i,j, offset;
   float x,y;

/* -------------------------------------------  BEGIN( ffuncEval ) */
   if ( (n==0) || (m==0) || (x1==(double *)NULL) || 
        (x2==(double *)NULL) || (f == (float *)NULL) ) return 1;
   
   for(i=0; i<m; i++){ 
      x = (float) x1[i];
      offset = i*n;
      for(j=0; j<n; j++){
         y = (float) x2[j];
//          f[offset + j] = (x + y);
         f[offset + j] = (x + y)*(x - y);
//          f[offset + j] = cosh(M_PI*(x + y)/180.0);
      }
   }

   return 0;
/* ---------------------------------------------  END( ffuncEval ) */
}

/*******************************************************************/
/*                           dfuncEval                             */
/*                                                                 */
/*!    Set a given array f in double precision, as a function of
       the x1 and x2 tick marks, i.e. f(x, y) = (x + y)*(x - y).

       \param[in]     x1  Array of the tick marks values along
                          x-direction.
       \param[in]     x2  Array of the tick marks values along
                          y-direction.
       \param[out]     f  Array of nx times ny with the values
                          of the function.
       \param[in]      m  Number of points along x-direction.
       \param[in]      n  Number of points along y-direction.

       \return An integer value with 0, if success and; otherwise,
               if there is an error.

*/
/*******************************************************************/
int dfuncEval(double *x1, double *x2, double *f,
              int m, int n )
{
/* ---------------------------------------  Variables Declaration  */
   register int i,j, offset;
   double x,y;

/* -------------------------------------------  BEGIN( dfuncEval ) */
   if ( (n==0) || (m==0) || (x1==(double *)NULL) || 
        (x2==(double *)NULL) || (f == (double *)NULL) ) return 1;
   
   for(i=0; i<m; i++){ 
      x = (double) x1[i];
      offset = i*n;
      for(j=0; j<n; j++){
         y = (double) x2[j];
//          f[offset + j] = (x + y);
         f[offset + j] = (x + y)*(x - y);
//          f[offset + j] = cosh(M_PI*(x + y)/180.0);
      }
   }

   return 0;
/* ---------------------------------------------  END( dfuncEval ) */
}

/*******************************************************************/
/*                          ldfuncEval                             */
/*                                                                 */
/*!    Set a given array f in long double precision, as a
       function of the x1 and x2 tick marks, i.e.
       f(x, y) = (x + y)*(x - y).

       \param[in]     x1  Array of the tick marks values along
                          x-direction.
       \param[in]     x2  Array of the tick marks values along
                          y-direction.
       \param[out]     f  Array of nx times ny with the values
                          of the function.
       \param[in]      m  Number of points along x-direction.
       \param[in]      n  Number of points along y-direction.

       \return An integer value with 0, if success and; otherwise,
               if there is an error.

*/
/*******************************************************************/
int ldfuncEval(double *x1, double *x2, long double *f,
              int m, int n )
{
/* ---------------------------------------  Variables Declaration  */
   register int i,j, offset;
   long double x,y;

/* ------------------------------------------  BEGIN( ldfuncEval ) */
   if ( (n==0) || (m==0) || (x1==(double *)NULL) || 
        (x2==(double *)NULL) || (f == (long double *)NULL) ) return 1;
   
   for(i=0; i<m; i++){ 
      x = (long double) x1[i];
      offset = i*n;
      for(j=0; j<n; j++){
         y = (long double) x2[j];
//          f[offset + j] = (x + y);
         f[offset + j] = (x + y)*(x - y);
//          f[offset + j] = cosh(M_PI*(x + y)/180.0);
      }
   }

   return 0;
/* --------------------------------------------  END( ldfuncEval ) */
}

/*******************************************************************/
/*                          ffuncEval3D                            */
/*                                                                 */
/*!    Set a given array f in float precision, as a function of
       the x1, x2 and x3 tick marks, i.e.
       f(x, y, z) = z*(x + y)*(x - y).

       \param[in]     x1  Array of the tick marks values along
                          x-direction.
       \param[in]     x2  Array of the tick marks values along
                          y-direction.
       \param[in]     x2  Array of the tick marks values along
                          z-direction.
       \param[out]     f  Array of nx times ny with the values
                          of the function.
       \param[in]      m  Number of points along x-direction.
       \param[in]      n  Number of points along y-direction.
       \param[in]      p  Number of points along y-direction.

       \return An integer value with 0, if success and; otherwise,
               if there is an error.

*/
/*******************************************************************/
int ffuncEval3D(double *x1, double *x2, double *x3, float *f,
                int m, int n, int p )
{
/* ---------------------------------------  Variables Declaration  */
   register int i, j, k, offset;
   float x, y, z;

/* -----------------------------------------  BEGIN( ffuncEval3D ) */
   if ( (n==0) || (m==0) || (p==0) || (x1==(double *)NULL) || 
        (x2==(double *)NULL) || (x3==(double *)NULL) || 
        (f == (float *)NULL) ) return 1;

   offset=0;

   for(i=0; i<m; i++){ 
      x = (float) x1[i];
      /* offset = i*n*m; */
      for(j=0; j<n; j++){
         y = (float) x2[j];
         /* offset2 = j*m + offset;*/
         for(k=0; k<p; k++){
            z = (float) x3[k];
//            f[offset2 + k] = (x + y + z);
            f[offset++] = z*(x + y)*(x - y);
//            f[offset + k] = cosh(M_PI*(x + y)/180.0);
         }
      }
   }

   return 0;
/* -------------------------------------------  END( ffuncEval3D ) */
}

/*******************************************************************/
/*                          dfuncEval3D                            */
/*                                                                 */
/*!    Set a given array f in double precision, as a function of
       the x1, x2 and x3 tick marks, i.e.
       f(x, y, z) = x + y + z.

       \param[in]     x1  Array of the tick marks values along
                          x-direction.
       \param[in]     x2  Array of the tick marks values along
                          y-direction.
       \param[in]     x2  Array of the tick marks values along
                          z-direction.
       \param[out]     f  Array of nx times ny with the values
                          of the function.
       \param[in]      m  Number of points along x-direction.
       \param[in]      n  Number of points along y-direction.
       \param[in]      p  Number of points along y-direction.

       \return An integer value with 0, if success and; otherwise,
               if there is an error.

*/
/*******************************************************************/
int dfuncEval3D(double *x1, double *x2, double *x3, double *f,
                int m, int n, int p )
{
/* ---------------------------------------  Variables Declaration  */
   register int i, j, k, offset;
   double x, y, z;

/* -----------------------------------------  BEGIN( dfuncEval3D ) */
   if ( (n==0) || (m==0) || (p==0) || (x1==(double *)NULL) || 
        (x2==(double *)NULL) || (x3==(double *)NULL) || 
        (f == (double *)NULL) ) return 1;

   offset=0;

   for(i=0; i<m; i++){ 
      x = (double) x1[i];
      /* offset = i*n*m; */
      for(j=0; j<n; j++) {
         y = (double) x2[j];
         /* offset2 = j*m + offset;*/
         for(k=0; k<p; k++){
            z = (double) x3[k];
            f[offset++] = (x + y + z)/10.0;
//             f[offset++] = z*(x + y)*(x - y);
//            f[offset + k] = cosh(M_PI*(x + y)/180.0);
         }
      }
   }

   return 0;
/* -------------------------------------------  END( dfuncEval3D ) */
}

/*******************************************************************/
/*                         ldfuncEval3D                            */
/*                                                                 */
/*!    Set a given array f in long double precision, as a
       function of the x1, x2 and x3 tick marks, i.e.
       f(x, y, z) = z*(x + y)*(x - y).

       \param[in]     x1  Array of the tick marks values along
                          x-direction.
       \param[in]     x2  Array of the tick marks values along
                          y-direction.
       \param[in]     x2  Array of the tick marks values along
                          z-direction.
       \param[out]     f  Array of nx times ny with the values
                          of the function.
       \param[in]      m  Number of points along x-direction.
       \param[in]      n  Number of points along y-direction.
       \param[in]      p  Number of points along y-direction.

       \return An integer value with 0, if success and; otherwise,
               if there is an error.

*/
/*******************************************************************/
int ldfuncEval3D(double *x1, double *x2, double *x3, long double *f,
                int m, int n, int p )
{
/* ---------------------------------------  Variables Declaration  */
   register int i, j, k, offset;
   long double x, y, z;

/* ----------------------------------------  BEGIN( ldfuncEval3D ) */
   if ( (n==0) || (m==0) || (p==0) || (x1==(double *)NULL) || 
        (x2==(double *)NULL) || (x3==(double *)NULL) || 
        (f == (long double *)NULL) ) return 1;

   offset=0;

   for(i=0; i<m; i++){ 
      x = (long double) x1[i];
      /* offset = i*n*m; */
      for(j=0; j<n; j++){
         y = (long double) x2[j];
         /* offset2 = j*m + offset;*/
         for(k=0; k<p; k++){
            z = (long double) x3[k];
//            f[offset2 + k] = (x + y + z);
            f[offset++] = z*(x + y)*(x - y);
//            f[offset + k] = cosh(M_PI*(x + y)/180.0);
         }
      }
   }

   return 0;
/* ------------------------------------------  END( ldfuncEval3D ) */
}

/*******************************************************************/
/*                      dlaplacian_matrix                          */
/*                                                                 */
/*!    Set a linearized matrix MM as a laplacian operator over
       a linearized 2D domain operator, in double precision.
       The matrix is built in such a way that if the 2D domain
       is vectorized the operation matrix vector results in a
       five-point stencil for laplacian finite difference
       operator.

       \param[out]    MM  Array of the values of the matrix.
       \param[in]      n  Number of points along x-direction.
       \param[in]     mn  Number of points along y-direction.

       \return An integer value with 0, if success and; otherwise,
               if there is an error.

*/
/*******************************************************************/
int dlaplacian_matrix( double *MM, int m, int mn )
{
/* ---------------------------------------  Variables Declaration  */
   register int ii, end;

/* -----------------------------------  BEGIN( dlaplacian_matrix ) */
   if ( (mn==0) || (m==0) || (MM == (double *)NULL) ) return 1;
   end = m*m;
   for( ii = 0; ii < end; ii++ ) MM[ ii ] = (double)0.0;
   
   for( ii = 0; ii < m; ii++ ) MM[ ii*m + ii ] = (double)4.0;
   for( ii = 1; ii < m; ii++ ) {
      MM[ (ii-1)*m + ii ] = (double)-1.0;
      MM[ ii*m + ii - 1 ] = (double)-1.0;
   }
   end = m - mn;
   for( ii = 0; ii < end; ii++ ) {
      MM[ ii*m + ii + mn ] = (double)-1.0;
      MM[ (ii+mn)*m + ii ] = (double)-1.0;
   }

   return 0;
/* -------------------------------------  END( dlaplacian_matrix ) */
}

int dlaplacian_bandmatrix( double *MM, int m, int mn )
{
/* ---------------------------------------  Variables Declaration  */
   register int ii, jj, ind, end;

/* -----------------------------------  BEGIN( dlaplacian_matrix ) */
   if ( (mn==0) || (m==0) || (MM == (double *)NULL) ) return 1;
   end = mn-3;
   ind=0;
   for( ii = 0; ii < m; ii++ ) {
      MM[ ind++ ] = (double)-1.0;
      for( jj = 0; jj < end; jj++ ) MM[ ind++ ] = (double)0.0;
      MM[ ind++ ] = (double)-1.0;
      MM[ ind++ ] = (double)-4.0;
   }
//    for( jj = 0; jj < m; jj++ ) MM[ ind++ ] = (double)-1.0;
//    for( ii = 0; ii < end; ii++ ) 
//       for( jj = 0; jj < m; jj++ ) MM[ ind++ ] = (double)0.0;
//    for( jj = 0; jj < m; jj++ ) MM[ ind++ ] = (double)-1.0;
//    for( jj = 0; jj < m; jj++ ) MM[ ind++ ] = (double)-4.0;
   
   return 0;
/* -------------------------------------  END( dlaplacian_matrix ) */
}
