/*******************************************************************/
/*                 Distributed Coupling Toolkit (DCT)              */
/*                                                                 */
/*!
   \file test_dct_perform_3d.c

      \brief This program is used in the tutorial to learn how
      to use the DCT. Main program.
      
      The file implements a test consists of defining two dummy
      models with different resolutions and perform the forcing
      of one variable from one model into the other.

      This corresponds the implementation of the main program
      that drives the calls of models A and B.

    \date Created on May 08, 2018
    
    \author Dany De Cecchis: dcecchis@gmail.com

    \copyright GNU Public License.

*/
/*******************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <mpi.h>

#define ROOT   0
#define PRB_DIM 3

/******************************************************************/
/*                              MAIN                              */
/*                                                                */
/*   This is the main routine for the performance test of DCT     */
/*                                                                */
/******************************************************************/
int main(int  argc, char *argv[])
{

/* ---------------------------------------  Variables Declaration  */
   int numtasks, color, rank, rc, split;
   
   int  Anp[PRB_DIM], Bnp[PRB_DIM], Apts[PRB_DIM], Bpts[PRB_DIM];
   
   MPI_Comm dom_comm;

/* ------------------------------------------------  BEGIN( main ) */


   /****************************************************************
    *****              the MPI system is initialized           *****
    ****************************************************************/
   rc = MPI_Init(&argc, &argv);
   if (rc != MPI_SUCCESS) {
      printf("Error starting MPI program. Terminating.\n");
      MPI_Abort(MPI_COMM_WORLD,rc);
   }

   rc  = MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
   rc |= MPI_Comm_rank(MPI_COMM_WORLD, &rank);
   if (rc != MPI_SUCCESS) {
      printf("Error asking rank and comm size. Terminating.\n");
      MPI_Abort(MPI_COMM_WORLD,rc);
   }
   // printf("This is the process %d of %d\n", rank, numtasks);  
   
   /* The number of parameter is checked */
   if (argc != 13) {
      if (rank == ROOT){
         printf("[ERROR %s]: %s anpx anpy anpz anptsx anptsy anptsz  bnpx bnpy bnpz bnptsx bnptsy bnptsz\n", argv[0], argv[0] );
         printf("where anpx, anpy, anpz, anptsx and anptsy are the number of processor in,\n");
         printf("x, y and z directions and the corresponding number of points in each\n");
         printf("direction for dummy Model A; and where bnpx, bnpy, bnpz, bnptsx, bnptsy, and\n");
         printf("bnptsz are the number of processor in x, y and z directions, and number of\n");
         printf("points in each direction for dummy model B.\n");
         printf("ERROR using the command\n");
      }
      MPI_Abort(MPI_COMM_WORLD,-1);
   }

   /* Discretization and parallel layout Model A */
   Anp[0] = atoi(argv[1]);
   Anp[1] = atoi(argv[2]);
   Anp[2] = atoi(argv[3]);
   Apts[0] = atoi(argv[4]);
   Apts[1] = atoi(argv[5]);
   Apts[2] = atoi(argv[6]);
   /* Discretization and parallel layout Model B */
   Bnp[0] = atoi(argv[7]);
   Bnp[1] = atoi(argv[8]);
   Bnp[2] = atoi(argv[9]);
   Bpts[0] = atoi(argv[10]);
   Bpts[1] = atoi(argv[11]);
   Bpts[2] = atoi(argv[12]);
   
   split = Anp[0]*Anp[1]*Anp[2];

   /***** Checking that everything is correct about number of processors *****/
   if ( numtasks != (split + Bnp[0]*Bnp[1]*Bnp[2]) ) {
      printf("The number of processors does not agree. Terminating.\n");
      MPI_Abort( MPI_COMM_WORLD, -2 );
   }
   
   /*** Phase 1 NO INTERPOLATION OPERATION ***/
   /* 
if ( (Apts[0] != Bpts[0]) || (Apts[1] != Bpts[1]) || (Apts[2] != Bpts[2]) ){
      printf("PHASE 1: In this phase only the same dimension is allowed.\n");
      MPI_Abort( MPI_COMM_WORLD, -3 );
   }
 */

   
   if ( rank < split ) color = 0;
   else            color = 1;
   
   rc  = MPI_Comm_split( MPI_COMM_WORLD, color, 0, &dom_comm );
   if ( rank < split )
      rc = test_dct_perform_a( Anp, Apts, dom_comm, MPI_COMM_WORLD );
   else 
      rc = test_dct_perform_b( Bnp, Bpts, split, dom_comm, MPI_COMM_WORLD );
   if ( rc ) {
      printf("Error calling sub domain function in process %d.\n", rank );
      MPI_Abort( MPI_COMM_WORLD, rc );
   }

   /****************************************************************
    *****              the MPI system is finalized             *****
    ****************************************************************/
   MPI_Finalize();
   return 0;

/* --------------------------------------------------  END( main ) */
}
