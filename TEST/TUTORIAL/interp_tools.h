/*******************************************************************/
/*                 Distributed Coupling Toolkit (DCT)              */
/*                                                                 */
/*!
    \file interp_tools.h                       
                                                            
    \brief   Header file for the tools to check interpolation      
    results                                               

    \date Created on Aug 10, 2011                
                                                            
    \author Dany De Cecchis: dcecchis@gmail.com
    \author Tony Drummond: LADrummond@lbl.gov

    \copyright GNU Public License.

*/
/*******************************************************************/
/*
      $Id: interp_tools.h,v 1.3 2011/10/09 00:51:01 dcecchis Exp $
*/

# ifndef INTERP_TOOLS_H
#   define INTERP_TOOLS_H


int fprtdat( int, int, double *, double *, float *,
              char *, char *, char *, char * );

int dprtdat( int, int, double *, double *, double *,
              char *, char *, char *, char *);

int ldprtdat( int, int, double *, double *, long double *,
               char *, char *, char *, char *);
            
int dprtdatMatrix( int, int, double *, char *, char * );

/* Value initialization function */
int ffuncEval(double *, double *, float *, int, int );
int dfuncEval(double *, double *, double *, int, int );
int ldfuncEval(double *, double *, long double *, int, int );
int ffuncEval3D(double *, double *, double *, float *, int,
                int, int );
int dfuncEval3D(double *, double *, double *, double *, int,
                int, int );
int ldfuncEval3D(double *, double *, double *, long double *, int,
                int, int );

/* Laplacian operator matrices */
int dlaplacian_matrix( double *, int, int );
int dlaplacian_bandmatrix( double *, int, int );
# endif /* INTERP_TOOLS_H */
