/*******************************************************************/
/*                 Distributed Coupling Toolkit (DCT)              */
/*                                                                 */
/*!
   \file test_dct_perfor_loops.c

      \brief This program is to test the DCT performance of all
      the phases and interpoaltion operations with timestepping
      and fully 2 way coupling. Main program.
      
      These tests consist of defining two dummy models with
      different resolutions and perform simulated two-way forcing
      of model variables. In a way that one model forces one
      variable into the other at a given interval, and in turn
      the counterpart model forces back a different variable into
      the model.

      This corresponds the implementation of the main program
      that drives the calls of models A and B.

    \date Created on Sep 30, 2011
    
    \author Dany De Cecchis: dcecchis@gmail.com
    \author Tony Drummond: LADrummond@lbl.gov

    \copyright GNU Public License.
*/
/*******************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <mpi.h>
#ifdef MEMTRK
  #include <TAU.h>
#endif

#define ROOT   0

/******************************************************************/
/*                              MAIN                              */
/*                                                                */
/*   This is the main routine for the performance test of DCT     */
/*                                                                */
/******************************************************************/
int main(int  argc, char *argv[])
{

/* ---------------------------------------  Variables Declaration  */
   int numtasks, color, rank, rc, split;
   
   int  Anp[2], Bnp[2], Apts[2], Bpts[2];
   
   MPI_Comm dom_comm;

/* ------------------------------------------------  BEGIN( main ) */


   /****************************************************************
    *****              the MPI system is initialized           *****
    ****************************************************************/
   rc = MPI_Init(&argc, &argv);
   if (rc != MPI_SUCCESS) {
      printf("Error starting MPI program. Terminating.\n");
      MPI_Abort(MPI_COMM_WORLD,rc);
   }

   rc  = MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
   rc |= MPI_Comm_rank(MPI_COMM_WORLD, &rank);
   if (rc != MPI_SUCCESS) {
      printf("Error asking rank and comm size. Terminating.\n");
      MPI_Abort(MPI_COMM_WORLD,rc);
   }
   // printf("This is the process %d of %d\n", rank, numtasks);  
   
   /* The number of parameter is checked */
   if (argc != 9) {
      if (rank == ROOT){
         printf("[ERROR %s]: %s anpx anpy anptsx anptsy  bnpx bnpy bnptsx bnptsy\n", argv[0], argv[0] );
         printf("where anpx anpy anptsx anptsy are the number of processor in x and y directions,\n");
         printf(" and number of points in each direction for dummy model A; and\n");
         printf("where bnpx bnpy bnptsx bnptsy are the number of processor in x and y directions,\n");
         printf(" and number of points in each direction for dummy model B.\n");
         printf("ERROR using the command\n");
      }
      MPI_Abort(MPI_COMM_WORLD,-1);
   }

   Anp[0] = atoi(argv[1]);
   Anp[1] = atoi(argv[2]);
   Apts[0] = atoi(argv[3]);
   Apts[1] = atoi(argv[4]);
   Bnp[0] = atoi(argv[5]);
   Bnp[1] = atoi(argv[6]);
   Bpts[0] = atoi(argv[7]);
   Bpts[1] = atoi(argv[8]);
   
   split = Anp[0]*Anp[1];
   
   /***** Checking that everything is correct about number of processors *****/
   if ( numtasks != (split + Bnp[0]*Bnp[1]) ) {
      printf("The number of processors does not agree. Terminating.\n");
      MPI_Abort( MPI_COMM_WORLD, -2 );   
   }

   
   if ( rank < split ) color = 0;
   else            color = 1;
   
   rc  = MPI_Comm_split( MPI_COMM_WORLD, color, 0, &dom_comm );
   if ( rank < split )
      rc = test_dct_perfor_loops_a( Anp, Apts, 0, dom_comm, MPI_COMM_WORLD );
   else 
      rc = test_dct_perfor_loops_b( Bnp, Bpts, split, dom_comm, MPI_COMM_WORLD );
   if ( rc ) {
      printf("Error calling model function in process %d.\n", rank );
      MPI_Abort( MPI_COMM_WORLD, rc );
   }

   /****************************************************************
    *****              the MPI system is finalized             *****
    ****************************************************************/
   MPI_Finalize();
   return 0;

/* --------------------------------------------------  END( main ) */

}
