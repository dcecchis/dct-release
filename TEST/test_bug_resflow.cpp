// libMesh pressure & flux calculation
// Jonathan L Matthews

#include <iostream>
#include <algorithm>
#include <math.h>
#include <ctime>
#include <set>
#include <cstring>
#include <fstream>

#include <ctype.h>
#include <mpi.h>
#ifdef MEMTRK
  #include <TAU.h>
#endif

extern "C" {
#include "dct.h"
}

// #include "libmesh/libmesh.h"
// #include "libmesh/mesh.h"
// #include "libmesh/mesh_generation.h"
// #include "libmesh/exodusII_io.h"
// #include "libmesh/equation_systems.h"
// #include "libmesh/fe.h"
// #include "libmesh/quadrature_gauss.h"
// #include "libmesh/dof_map.h"
// #include "libmesh/sparse_matrix.h"
// #include "libmesh/numeric_vector.h"
// #include "libmesh/dense_matrix.h"
// #include "libmesh/dense_vector.h"
// #include "libmesh/linear_implicit_system.h"
// #include "libmesh/transient_system.h"
// #include "libmesh/dense_submatrix.h"
// #include "libmesh/dense_subvector.h"
// #include "libmesh/elem.h"
// #include "libmesh/analytic_function.h"
// #include "libmesh/zero_function.h"
// #include "libmesh/dirichlet_boundaries.h"
// #include "libmesh/tensor_value.h"
// #include "libmesh/vector_value.h"
// #include "libmesh/edge_edge3.h"
/*
#include "h_define.h"
#include "h_global.h"
#include "h_boundary.h"
#include "h_sediment.h"
*/
// using namespace libMesh;
// using std::cout;
// using std::endl;
// using std::vector;
// using std::setw;
// using std::string;

extern "C"
{
  double effective_porosity (double phi0, double alpha, double M, double div_u, double epsilon0, double p, double p0 );
  RealTensor strain_calc(RealTensor du, int dim);
  RealTensor stress_calc(RealTensor strain, double div_u, double lambda, double G, int dim);
  void assemble_res_flow (EquationSystems& es, const std::string& system_name);
  int libMesh_setup(int argc, char *argv[], MPI_Comm mcomm, unsigned int dim, double x1, double y1, double z1, double x2, double y2, double z2, int nx, int ny, int nz);
  int libMesh_solve(double dt);
  void water_property_density ();
}

void init_res_flow(EquationSystems& es, const std::string& system_name);
void init_initial_values (EquationSystems & es, const std::string & system_name);
void get_dct_data(double **p_dct, string varname,int nx,int ny, int nz);
int dct_setup(MPI_Comm dom_comm, MPI_Comm globalcomm, DCT_Couple couple, DCT_Model model, DCT_Field dctpress, int nx, int ny, int nz);
void dct_destroy(MPI_Comm dom_comm, DCT_Couple couple, DCT_Model model, DCT_Field dctpress);

double *p_dct, *u_dct, *v_dct, *w_dct, *xticks, *yticks, *zticks;
static int dimension;
static double injection_width_y, injection_width_z, perforation_interval_z_top, perforation_interval_z_bottom, miny, maxy, well_diameter;
static int FEM_output_interval = 3;
static TransientLinearImplicitSystem * systemp;
static TransientLinearImplicitSystem * systemold;
static EquationSystems * eqsystems;
static ExodusII_IO * ExodusObj;
static double resBottom;

#define min(u, w) (u < w ? u : w)      /*  find lesser of the two values.  */
#define max(u, w) (u > w ? u : w)      /*  find greater of the two values.  */
#define bartocgs(u)  (u * 1019.716213)
#define pastocgs(u)  (u * 0.01019716213)

double pressure_initial;
double massFlow;
double viscosity_dynamic;
double perm;
double porosity;
Elem ****elementMap;


int resflow (int argc, char *argv[], MPI_Comm lcomm, MPI_Comm gcomm)
{
  unsigned int dim = 2;
  int nx, ny, nz, nsteps;
  nx = nz = 20;
  ny = 1;
  nsteps = 100;
  
  double x1, y1, z1, x2, y2, z2, dt;
  x1 = y1 = z2 = 0;
  y2 = 1;
  x2 = 100000; // 1000m wide
  z1 = -50000; // 500m deep
  dt = 10;
cout << "Hey " << endl;
  //libMesh_setup(argc, argv, lcomm, dim, x1, y1, z1, x2, y2, z2, nx, ny, nz);
cout << "Jude " << endl;
  DCT_Couple  couple;
  DCT_Model   model;
  DCT_Field   dctpress;
  DCT_Error   dcterr;

  dct_setup(lcomm, gcomm, couple, model, dctpress, nx, ny, nz);
cout << "them " << endl;   
  for (int i=1; i<=nsteps; i++)
  {
  cout << "under " << endl;   

    get_dct_data(&p_dct,"p",nx,ny,nz);
    get_dct_data(&u_dct,"u_res",nx,ny,nz);
    get_dct_data(&v_dct,"v_res",nx,ny,nz);
    cout << "your " << endl;   
	
	dcterr = DCT_Update_Model_Time(&model);
    DCTCHKERR(dcterr);
	
	dcterr = DCT_Send_Field(&dctpress);
	DCTCHKERR(dcterr);
	
    //get_dct_data(&w_dct,"w_res",nx,ny,nz);
    //libMesh_solve(dt);
    cout << "Do something! " << endl;   

  }
  cout << "Final Iteration! " << endl;   

  // Get final timestep:
  get_dct_data(&p_dct,"p",nx,ny,nz);
  get_dct_data(&u_dct,"u_res",nx,ny,nz);
  get_dct_data(&v_dct,"v_res",nx,ny,nz);
  
  dcterr = DCT_Update_Model_Time(&model);
    DCTCHKERR(dcterr);
	
	dcterr = DCT_Send_Field(&dctpress);
	DCTCHKERR(dcterr);
  
  dct_destroy(gcomm, couple, model, dctpress);
  
  return(0);
}

void set_parameters(double  z1, double z2)
{
  pressure_initial = 140.0; // bars
  massFlow = 16000; // g/s
  viscosity_dynamic = 4.76207471471e-06;
  perm = 1.7194626365158137e-10;
  porosity = 0.36;
  perforation_interval_z_top = .25*(z2-z1) + z1;
  perforation_interval_z_bottom = 0.5*(z2-z1) + z1;
  well_diameter = 10;
}

void   dct_destroy(MPI_Comm dom_comm, DCT_Couple couple, DCT_Model model, DCT_Field dctpress)
{
/*******************************************************************
 ****************   Destroying the DCT structures   ****************
 *******************************************************************/
   int rank;
   MPI_Comm_rank(dom_comm, &rank);
   DCT_Error   dcterr = { DCT_SUCCESS, (DCT_String)NULL};
   
   if (rank == 0 ) printf("Finalizing...\n" );
   dcterr = DCT_Destroy_Couple( &couple );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Destroy_Field( &dctpress );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Destroy_Model( &model );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Finalized(  );
   DCTCHKERR( dcterr );
   
}// end dct_destroy
int dct_setup(MPI_Comm dom_comm, MPI_Comm globalcomm, DCT_Couple couple, DCT_Model model, DCT_Field dctpress, int nx, int ny, int nz)
{
/* ---------------------------------------  Variables Declaration  */
cout << "now " << endl;
   DCT_Error   dcterr = { DCT_SUCCESS, (DCT_String)NULL};

   int         rank, numtasks, rc;

   DCT_Scalar  *xmarks;
   DCT_Scalar  *ymarks;
   DCT_Integer  xnpts = nx+1, ynpts = nz+1;
   DCT_Integer  npx, npy;

   DCT_Integer *iniind;
   DCT_Integer *endind;
   DCT_Integer  nnx, nny;
   DCT_Rank    *mranks;
   DCT_Integer  xintdiv, yintdiv, xintres, yintres;
   
   double      tini, dt;

   int         ii, jj, indi, indj, inix, iniy;
   int		   np[3];
   
   np[0]=1;
   np[1]=1;
   np[2]=1;

/* ----------------------------------  BEGIN( test_dct_perform_a ) */
cout << "don't " << endl;
   rc  = MPI_Comm_size(globalcomm, &numtasks);
   rc |= MPI_Comm_rank(globalcomm, &rank);
   if ( rc != MPI_SUCCESS ) {
         fprintf( stderr, "\nERROR: Error asking model rank and comm size.\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }
cout << "get " << endl;   
   mranks = (DCT_Rank *)malloc( sizeof(DCT_Rank)*(size_t)numtasks );
   if ( mranks == (DCT_Rank *)NULL ) {
         fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }
   iniind = (DCT_Integer *)malloc( sizeof(DCT_Integer)*2*(size_t)numtasks );
   if ( iniind == (DCT_Integer *)NULL ) {
         fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }
   endind = (DCT_Integer *)malloc( sizeof(DCT_Integer)*2*(size_t)numtasks );
   if ( endind == (DCT_Integer *)NULL ) {
         fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }
    
cout << "mad. " << endl;  
 tini = 0;
 dt = 10; 
 
 /****  Determine the subdomain distribution  ****/
   npx = np[0];
   npy = np[1];
   xintdiv = xnpts/npx;
   xintres = xnpts%npx;
   yintdiv = ynpts/npy;
   yintres = ynpts%npy;

   for ( ii=0; ii < numtasks; ii++ ) {
      jj = 2*ii;
      mranks[ii] = (DCT_Rank)rank;
      indi = ii%npx;
      indj = ii/npx;

      /* Calculating correspondig size in X direction for each node */
      /* how many elements a block has in x-direction */
      nnx = xintdiv + (indi < xintres? 1: 0);
      /* Where index each block begins */
      inix = indi*xintdiv + (indi < xintres? indi: xintres);
      iniind[jj] = inix;
      endind[jj] = inix + nnx - 1;

      jj++;
      /* Calculating correspondig size in Y direction for each node */
      /* how many elements a block has in y-direction */
      nny = yintdiv + (indj < yintres? 1: 0);
      /* Where index each block begins */
      iniy = indj*yintdiv + (indj < yintres? indj: yintres);
      iniind[jj] = iniy;
      endind[jj] = iniy + nny - 1;
   }
   
   /** Creating the model variable **/
   jj = 2*rank;
   inix = iniind[jj];
   nnx = endind[jj] - inix + 1;
   jj++;
   iniy = iniind[jj];
   nny = endind[jj] - iniy + 1;
    
/*******************************************************************
 *****************   DCT_BeginRegistration Call   ******************
 *******************************************************************/
   if (rank == 0 ) printf("Registering...\n" );
#ifdef IPM
   MPI_Pcontrol( 1,"Registration");
#endif
   dcterr = DCT_BeginRegistration( globalcomm );
   DCTCHKERR( dcterr );
cout << "Take " << endl;
/*******************************************************************
 *******************   Creating the DCT_Model  *********************
 *******************************************************************/
   dcterr = DCT_Create_Model( &model, "resflow", "Reservoir flow Model", numtasks );
   DCTCHKERR( dcterr );

   dcterr = DCT_Set_Model_Time( &model, tini, dt, DCT_TIME_NO_UNIT );
   DCTCHKERR( dcterr );
cout << "a " << endl;   
   dcterr = DCT_Set_Model_Dom( &model, 2, 1, DCT_UNSTRUCTURED, xticks, xnpts );
   DCTCHKERR( dcterr );
   dcterr = DCT_Set_Model_Dom( &model, 2, 2, DCT_UNSTRUCTURED, zticks, ynpts );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Model_ParLayout( &model, DCT_DIST_RECTANGULAR, np, NULL );
   DCTCHKERR( dcterr );
cout << "sad " << endl;   
   dcterr = DCT_Set_Model_SubDom( &model, mranks, iniind, endind );
   DCTCHKERR( dcterr );

/*******************************************************************
 *******************   Creating the Pressure  *******************
 *******************************************************************/
   dcterr = DCT_Create_Field( &dctpress, "p_res", "Pressure from resflow model",
                            DCT_NO_UNITS, DCT_PRODUCE);
   DCTCHKERR( dcterr );
cout << "song " << endl;
cout << "(nnx,nny) = (" << nnx << "," << nny << ")" << endl;
   dcterr = DCT_Set_Field_Dims( &dctpress, xnpts, ynpts );
   DCTCHKERR( dcterr );

   dcterr = DCT_Set_Field_Labels( &dctpress, DCT_UNSTRUCTURED, xticks, xnpts,
                                  DCT_UNSTRUCTURED, zticks, ynpts );
   DCTCHKERR( dcterr );
cout << "and " << endl;   
   dcterr = DCT_Set_Field_Val_Location( &dctpress, DCT_LOC_CENTERED );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Time( &dctpress, DCT_TIME_NO_UNIT, tini );
   DCTCHKERR( dcterr );
cout << "make " << endl;      
   dcterr = DCT_Set_Field_Freq_Production( &dctpress, dt );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Values( &dctpress, DCT_DOUBLE, p_dct );
   DCTCHKERR( dcterr );
cout << "it " << endl;   
/*******************************************************************
 *****************  Linking Pressure to Model  ******************
 *******************************************************************/
   dcterr =  DCT_Set_Model_Var( &model, &dctpress, DCT_FIELD_TYPE );
   DCTCHKERR( dcterr );

cout << "better. " << endl;   
/*******************************************************************
 **********************   Creating the Couple  *********************
 *******************************************************************/
   dcterr =  DCT_Create_Couple( &couple, "cpl_sub_res",
                        "Coupling between subflow and resflow models", &model, "subflow" );
   DCTCHKERR( dcterr );

/*******************************************************************
 **************   Linking Pressure to the Couple  ***************
 *******************************************************************/
   dcterr =  DCT_Set_Coupling_Vars( &couple, &dctpress, "p_sub", DCT_FIELD_TYPE,
                                    DCT_LINEAR_INTERPOLATION );
   DCTCHKERR( dcterr );
cout << "Remember " << endl;      
/*******************************************************************
 ******************   DCT_EndRegistration Call   *******************
 *******************************************************************/
   dcterr = DCT_EndRegistration(  );
   DCTCHKERR( dcterr );
   cout << "don't " << endl;
#ifdef IPM
   MPI_Pcontrol( -1,"Registration");
#endif
   cout << "let " << endl;
   free ( mranks );
   free ( iniind );
   free ( endind );

}// end dct_setup

void get_dct_data(double **grid, string varname, int nx,int ny, int nz)
{
	TransientLinearImplicitSystem &system = *systemp;
	Point p;
	Elem *elem;
	int gridvar = system.variable_number (varname);
	if (dimension==2) ny=0;
	double *data = *grid;
	for (int i = 0; i<nx+1; i++)
		for (int j = 0; j<ny+1; j++)
			for (int k = 0; k<nz+1; k++)
			{
				elem = elementMap[i][j][k];
				if (dimension==2) 
					p = Point(xticks[i+k*(nx+1)],zticks[i+k*(nx+1)],0);
					else
						p = Point(xticks[i+j*(nx+1)+k*(nx+1)*(ny+1)],yticks[i+j*(nx+1)+k*(nx+1)*(ny+1)],zticks[i+j*(nx+1)+k*(nx+1)*(ny+1)]);
				data[i+j*(nx+1)+k*(nx+1)*(ny+1)] = system.point_value(gridvar, p, elem);
			}
} // end get_dct_data

void zero_flux_x(DenseVector<Number>& output, const Point& p __attribute__((unused)), const Real )
{
  output(0) = 0;
}

void zero_flux_y(DenseVector<Number>& output, const Point& p __attribute__((unused)), const Real )
{
  output(1) = 0;
}

void zero_flux_z(DenseVector<Number>& output, const Point& p __attribute__((unused)), const Real )
{
  if (dimension==2)
    output(1) = 0;
  else if (dimension==3)
    output(2) = 0;
}

void alloc_elem_map(Elem *****Map, int nx,int ny,int nz)
{
	if (dimension==2) ny=0; 	// We only want 1 node layer in y direction if in 2D. 
						// ny is number of elements, one less than number of nodes.
	Elem ****newMap;
	newMap = new Elem***[nx+1];
	for(int i=0; i<=nx; i++)
	{
		newMap[i] = new Elem**[ny+1];
		for(int j=0; j<ny+1; j++)
		{
			newMap[i][j] = new Elem*[nz+1];
		}
	}
	*Map = newMap;			
} // end alloc_elem_map

double* alloc_3d_double( int nx, int ny, int nz)
{
  /*--------------Allocate a 3d array to hold DCT output -----------------*/
  if (dimension==2) ny=0; 	// We only want 1 node layer in y direction if in 2D. 
						// ny is number of elements, one less than number of nodes.
  // double *newgrid;
//   newgrid = new double[(nx+1)*(ny+1)*(nz+1)];
//   grid = newgrid;
double* grid = new double[(nx+1)*(ny+1)*(nz+1)];
return grid;
}// end alloc_3d_double


void alloc_3d_double_pointers(double ****grid, int nx, int ny, int nz)
{
  /*--------------Allocate a 3d array to hold DCT output -----------------*/
  if (dimension==2) ny=0; 	// We only want 1 node layer in y direction if in 2D. 
						// ny is number of elements, one less than number of nodes.
  double ***newgrid;
  newgrid = new double **[nx+1]();
  for (int i=0; i<nx+1; i++)
  {
    newgrid[i] = new double *[ny+1]();
    for (int j=0; j<ny+1; j++)
    {
      newgrid[i][j] = new double[nz+1]();
      for (int k=0; k<nz+1; k++)
        newgrid[i][j][k]=0;
    }
  }
  *grid = newgrid;
}// end alloc_3d_double_pointers

void get_grid_structure(Mesh mesh,Elem *****elementMap, unsigned int dim, double x1, double y1, double z1, double x2, double y2, double z2, int nx, int ny, int nz)
{
  double xinit = x2>x1 ? x1 : x2;
  double yinit = y2>y1 ? y1 : y2;
  double zinit = z2>z1 ? z1 : z2;
  
  Elem ****Map = elementMap[0];
  	
  if (dim==2)
  {
    double xstep = (x2-x1)/nx;
    double zstep = (z2-z1)/nz;
    for (int i=0; i<=nx; i++)
      for (int j = 0; j<=nz; j++)
    {
        xticks[i+j*(nx+1)] = xinit+i*xstep;
        zticks[i+j*(nx+1)] = zinit+j*zstep;
        
        Point p(xticks[i+j*(nx+1)],zticks[i+j*(nx+1)],0);
        
        MeshBase::const_element_iterator el = mesh.active_local_elements_begin ();
        const MeshBase::const_element_iterator end_el = mesh.active_local_elements_end ();
        bool elem_found = false;
        
        for( ; el != end_el; ++el)
        {
          Elem* elem = *el;
          if(elem->contains_point(p))
            {
              Map[i][0][j] = elem;
              elem_found = true;
            }
		}
		if (!elem_found)
          {
            cout  << "Point has no corresponding element." << endl;
            exit(1);
          }
	}
  } else if (dim==3)
  {
  	yinit = (double)ny; // to prevent 'unused' error on compile
    cout << "3D not ready yet." << endl;
    exit(0);
  }
} // end get_grid_structure

int libMesh_setup(int argc, char *argv[], MPI_Comm mcomm, unsigned int dim, double x1, double y1, double z1, double x2, double y2, double z2, int nx, int ny, int nz)
{
  dimension = dim;
  set_parameters( z1, z2);
	p_dct = alloc_3d_double( nx, ny, nz);
	u_dct = alloc_3d_double( nx, ny, nz);
	v_dct = alloc_3d_double( nx, ny, nz);
	xticks = alloc_3d_double( nx, ny, nz);
	yticks = alloc_3d_double( nx, ny, nz);
	zticks = alloc_3d_double( nx, ny, nz);
	w_dct = alloc_3d_double( nx, ny, nz);
	
  
  resBottom = min(z1,z2);
  injection_width_y = abs(y2-y1) < (well_diameter*M_PI*0.5) ? abs(y2-y1) : (well_diameter*M_PI*0.5);
  miny = y1<y2 ? y1 : y2;
  maxy = y2>y1 ? y2 : y1;
  injection_width_z = perforation_interval_z_top - perforation_interval_z_bottom; // centimeters

  static LibMeshInit init(argc, argv, mcomm);
  
  libmesh_example_requires(dim <= LIBMESH_DIM, "3D support");
  
  libmesh_example_requires(libMesh::default_solver_package() != EIGEN_SOLVERS   , "--enable-petsc or --enable-laspack");
  libmesh_example_requires(libMesh::default_solver_package() != TRILINOS_SOLVERS, "--enable-petsc or --enable-laspack");
  
  const Order order = SECOND;
  const Order p_order = FIRST;
  std::string family = "LAGRANGE";

  static Mesh mesh(init.comm());

  if (dim<=2) {
    MeshTools::Generation::build_square (mesh,
                                       nx, nz,
                                       x1, x2,
                                       z1, z2,
                                         QUAD9);
    cout << "libMesh mesh information: (x1,z1) = (" << x1 << "," << z1 << "), (x2,z2) = (" << x2 << "," << z2 << ")" << endl;
  }
  else {
    MeshTools::Generation::build_cube (mesh,
                                         nx, ny, nz,
                                         x1, x2,
                                         y1, y2,
                                         z1, z2,
                                         HEX27);
    cout << "libMesh mesh information: (x1,y1,z1) = (" << x1 << "," << y1 << "," << z1 << "), (x2,y2,z2) = (" << x2 << "," << y2 << "," << z2 << ")" << endl;
  }

//  mesh.print_info();

  alloc_elem_map(&elementMap, nx, ny, nz);

// memory leak
  get_grid_structure(mesh, &elementMap, dim, x1, y1, z1, x2, y2, z2, nx, ny, nz);

  static EquationSystems equation_systems (mesh);
  static TransientLinearImplicitSystem & system =
  equation_systems.add_system<TransientLinearImplicitSystem> ("Reservoir Flow");
  system.time = 0;
  
  static TransientLinearImplicitSystem & system_old =
  equation_systems.add_system<TransientLinearImplicitSystem> ("Initial Values");
  system_old.time = 0;
  
  // Note: u_res, and v_res and w_res are x, y, and z components of the reservoir velocity vector.
  system.add_variable ("u_res", order);
  system.add_variable ("v_res", order);
  if (dim==3) system.add_variable ("w_res", order);
  system.add_variable ("p", p_order);
  system_old.add_variable ("p initial", p_order);
  const unsigned int p_var = system.variable_number ("p");
  const unsigned int u_res_var = system.variable_number ("u_res");
  const unsigned int v_res_var = system.variable_number ("v_res");
  unsigned int w_res_var;
  if (dim==3) w_res_var = system.variable_number ("w_res");
  
  const unsigned int p_init_var = system_old.variable_number ("p initial");
  
  // Define initial conditions for pressure, P(x,y,z,t=0)
  equation_systems.parameters.set<Real>("Pt0") = pressure_initial;
  
  equation_systems.parameters.set<Real>("dt") = 0;
  
  system.attach_assemble_function (assemble_res_flow);
  system.attach_init_function (init_res_flow);
  system_old.attach_init_function (init_initial_values);
  
  std::set<boundary_id_type> boundary_left_right;
  std::set<boundary_id_type> boundary_right;
  std::set<boundary_id_type> boundary_left;
  std::set<boundary_id_type> boundary_bottom_top;
  std::set<boundary_id_type> boundary_front_back;
  
  // Boundary conditions
  //
  if (dimension==2){
    boundary_right.insert(1); // right
    boundary_left.insert(3); // left
    boundary_left_right.insert(1); // right
    boundary_left_right.insert(3); // left
    boundary_bottom_top.insert(0);
    boundary_bottom_top.insert(2);
  }
  else if (dimension==3)
  {
    boundary_right.insert(2); // right
    boundary_left.insert(4); // left
    boundary_left_right.insert(2); // right
    boundary_left_right.insert(4); // left
    boundary_bottom_top.insert(0); // bottom
    boundary_bottom_top.insert(5); // top
    boundary_front_back.insert(1); // front
    boundary_front_back.insert(3); // back
  }
  std::vector<unsigned int> u_velocityVariable(1);
  std::vector<unsigned int> v_velocityVariable(1);
  std::vector<unsigned int> w_velocityVariable(1);
  std::vector<unsigned int> p_variable(1);
  
  u_velocityVariable[0] = u_res_var;
  v_velocityVariable[0] = v_res_var;
  if (dim==3) w_velocityVariable[0] = w_res_var;
  p_variable[0] = p_var;
  
  AnalyticFunction<> zf_x_object(zero_flux_x);
  AnalyticFunction<> zf_z_object(zero_flux_z);
  AnalyticFunction<> zf_y_object(zero_flux_y);
  
  DirichletBoundary right_u_velocityDirichletBC(boundary_right, u_velocityVariable, &zf_x_object);
  DirichletBoundary v_velocityDirichletBC_2D(boundary_bottom_top, v_velocityVariable, &zf_z_object);
  DirichletBoundary v_velocityDirichletBC_3D(boundary_front_back, v_velocityVariable, &zf_y_object);
  DirichletBoundary w_velocityDirichletBC(boundary_bottom_top, w_velocityVariable, &zf_z_object);
  
  system.get_dof_map().add_dirichlet_boundary(right_u_velocityDirichletBC);
  if (dimension==2)
  system.get_dof_map().add_dirichlet_boundary(v_velocityDirichletBC_2D);
  if (dimension==3){
    system.get_dof_map().add_dirichlet_boundary(v_velocityDirichletBC_3D);
    system.get_dof_map().add_dirichlet_boundary(w_velocityDirichletBC);
  }

  /*---------------------------------------------------------------------------
   Initialize systems and add pointers to FEM cells.
   --------------------------------------------------------------------------------*/
  
  equation_systems.init ();
  equation_systems.parameters.set<unsigned int>("linear solver maximum iterations") = 1e5;
  equation_systems.parameters.set<Real>        ("linear solver tolerance") = 1e-12;
  equation_systems.parameters.set<Real>        ("top boundary")     = (z1>z2 ? z1 : z2);
  equation_systems.parameters.set<Real>        ("bottom boundary")  = (z1>z2 ? z2 : z1);
  equation_systems.parameters.set<Real>        ("right boundary")   = (x2>x1 ? x2 : x1);
  equation_systems.parameters.set<Real>        ("left boundary")    = (x2>x1 ? x1 : x2);
  equation_systems.parameters.set<Real>        ("back boundary")    = (y2>y1 ? y2 : y1);
  equation_systems.parameters.set<Real>        ("front boundary")   = (y2>y1 ? y1 : y2);
  
  cout << "libMesh system information:" << endl;
  system.print_info();
    
  systemp = &system;
  systemold = &system_old;
  eqsystems = &equation_systems;

#ifdef LIBMESH_HAVE_EXODUS_API
  static ExodusII_IO exo(mesh);
  std::string out_base = "_FEM_";
  std::string name_base = "DCT_test";
  out_base = (dim==3) ? ((name_base + out_base) + "out_3D.e") : ((name_base + out_base) + "out_2D.e");
  cout << "---------------- Outputing : " << out_base << " initial timestep ----------------" << endl;
  
  // Write a new equation systems output to exodus file. Time in years.
  //ExodusII_IO(mesh).write_equation_systems(out_base, equation_systems);
  exo.write_timestep (out_base, equation_systems, 1, system.time);
  exo.append(true);
  ExodusObj = &exo;
#endif //LIBMESH_HAVE_EXODUS_API

  return(0);
} // end libmesh setup

int libMesh_solve(double dt)
{
  /* pressure_time seconds tracks pressure time progression separate from main loop. */
  // Static variable will retain its value on subsequent iterations and is only
  // initialized on the initial call.
  static double pressure_time_seconds = 0.0;
  static int ntimesteps = 0;
  
  if (dt>0) //only take pressure time step if incremental time is significant
    {
      pressure_time_seconds += dt;
      cout << "skin. " << endl;   

      TransientLinearImplicitSystem &system = *systemp;
      TransientLinearImplicitSystem &system_old = *systemold;
      MeshBase& mesh = system.get_mesh();
      ExodusII_IO &ExObj = *ExodusObj;
  cout << "Then " << endl;   

      system_old.time = pressure_time_seconds;
      
      const double ReservoirFlowTolerance = 1e-2;
      const unsigned int dim = mesh.mesh_dimension();
      const EquationSystems &equation_systems = *eqsystems;
      cout << "you'll " << endl;   

      eqsystems->parameters.set<Real>("dt") = dt;

      clock_t begin = clock();
      cout << "libmesh reservoir flow solve: start" << endl;
      system.time = pressure_time_seconds;
      cout << "begin " << endl;   

      system.solve();
      cout << "to " << endl;   

      clock_t end = clock();
      double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
      if( system.final_linear_residual() > ReservoirFlowTolerance )
	{
	  cout << "warning: libMesh reservoir flow linear system residual norm is " << system.final_linear_residual() << endl;
	}
      else
	{
	  cout << "libmesh reservoir flow solve: end, residual norm is " << system.final_linear_residual() << ", elapsed time " << elapsed_secs << " seconds" << endl;
	}
 
cout << "make " << endl;   

#ifdef LIBMESH_HAVE_EXODUS_API
      static int ex_time_step = 2;
      std::string name_base = "DCT_test";
 cout << "it " << endl;   
     
      if (ntimesteps == 0)
      {
        std::string out_base = "_FEM_";
        out_base = (dim==3) ? ((name_base + out_base) + "out_3D.e") : ((name_base + out_base) + "out_2D.e");
        cout << "---------------- Outputing : " << out_base << " first timestep ----------------" << ex_time_step << endl;
        
        ExObj.write_timestep (out_base, equation_systems, ex_time_step, system.time);
        ex_time_step++;
      }
      else if (ex_time_step % FEM_output_interval == 0)
      {
        std::string out_base = "_FEM_";
        out_base = (dim==3) ? ((name_base + out_base) + "out_3D.e") : ((name_base + out_base) + "out_2D.e");

        cout << "---------------- Outputing : " << out_base << " for timestep " << ex_time_step << " at time " << pressure_time_seconds << " seconds. ----------------" << endl;
        
        ExObj.append(true);
        ExObj.write_timestep(out_base, equation_systems, ex_time_step, system.time);

        ex_time_step++;
      }
      else ex_time_step++;
#endif // #ifdef LIBMESH_HAVE_EXODUS_API
cout << "better. " << endl;   

    } // end if (dt>0)
  ntimesteps++;
  
  return(0);
} // end libMesh_solve

void assemble_res_flow (EquationSystems& es,
                        const std::string& system_name)
{
  /*--------------------------------------------------------------------------------
   Assembles a finite element system for reservoir pressure and velocity according
   to the setup described in Ganis, et al.
   --------------------------------------------------------------------------------*/
  TransientLinearImplicitSystem &system = *systemp;
  TransientLinearImplicitSystem &systeminit = *systemold;

  libmesh_assert_equal_to (system_name, "Reservoir Flow");

  const MeshBase& mesh = es.get_mesh();
  const unsigned int dim = mesh.mesh_dimension();

  const unsigned int u_res_var = system.variable_number ("u_res");
  const unsigned int v_res_var = system.variable_number ("v_res");
  unsigned int w_res_var;
  if (dim==3) w_res_var = system.variable_number ("w_res");
  const unsigned int p_var     = system.variable_number ("p");
  
  /* --------------------------poroelastic parameter block-----------------*/
  //TODO: read in compressibility ratio
  double c = 5.8e-10 * 1/pastocgs( 1 ); // Ganis et al., assumes typo: should read kPa^{-1}, not Pa^{-1}
  
  /* --------------------------end poroelastic parameter block-------------*/
  
  /*---------------------------other hard-code parm block------------------*/
  
  double g=1; // in gram-force per sq. cm, the pressure gradient is equal to density
  
  /*---------------------------end other hard-code parm block--------------*/

  double dt = es.parameters.get<Real>("dt"); // timestep, seconds

  
  FEType fe_vel_type = system.variable_type(u_res_var);
  FEType fe_pres_type = system.variable_type(p_var);
  
  AutoPtr<FEBase> fe_vel  (FEBase::build(dim, fe_vel_type));
  AutoPtr<FEBase> fe_pres (FEBase::build(dim, fe_pres_type));
  AutoPtr<FEBase> fe_face (FEBase::build(dim, fe_vel_type));
  
  QGauss qrule (dim  , fe_vel_type.default_quadrature_order());
  QGauss qface (dim-1, fe_vel_type.default_quadrature_order());
  
  fe_vel->attach_quadrature_rule (&qrule);
  fe_pres->attach_quadrature_rule (&qrule);
  fe_face->attach_quadrature_rule (&qface);
  
  const std::vector<Real>& JxW = fe_vel->get_JxW();
  const std::vector<Point>& xyz = fe_pres->get_xyz();
  const std::vector<Real>& JxW_face = fe_face->get_JxW();
  const std::vector<Point>& xyz_face = fe_face->get_xyz();
  
  const std::vector<std::vector<RealGradient> >& dpsi = fe_vel->get_dphi();
  const std::vector<std::vector<Real> >& psi = fe_vel->get_phi();
  const std::vector<std::vector<Real> >& chi = fe_pres->get_phi();
  const std::vector<std::vector<Real> >& psi_face = fe_face->get_phi();
  
  DenseMatrix<Number> Ke;
  DenseVector<Number> Fe;
  
  DenseSubMatrix<Number>
  Kuu(Ke), Kuv(Ke), Kuw(Ke), Kup(Ke),
  Kvu(Ke), Kvv(Ke), Kvw(Ke), Kvp(Ke),
  Kwu(Ke), Kwv(Ke), Kww(Ke), Kwp(Ke),
  Kpu(Ke), Kpv(Ke), Kpw(Ke), Kpp(Ke);
  
  DenseSubVector<Number>
  Fu(Fe),
  Fv(Fe),
  Fw(Fe),
  Fp(Fe);
  
  std::vector<dof_id_type> dof_indices;
  std::vector<dof_id_type> dof_indices_u_res;
  std::vector<dof_id_type> dof_indices_v_res;
  std::vector<dof_id_type> dof_indices_w_res;
  std::vector<dof_id_type> dof_indices_p;
  
  MeshBase::const_element_iterator       el     = mesh.active_local_elements_begin();
  const MeshBase::const_element_iterator end_el = mesh.active_local_elements_end();
  
  const DofMap & dof_map = system.get_dof_map();
  
  for ( ; el != end_el; ++el)
  {
    const Elem* elem = *el;
    Point centroid = elem->centroid();
    double Pf = system.point_value(p_var, centroid, elem);
    //double Pf_init = systeminit.point_value(p_var, centroid);
    double rho0 = 1;
    double rho_prev = rho0*(1+c*Pf); // previous time step density
    double mu = viscosity_dynamic; // dynamic viscosity, gfsc-sec
    double K = perm; // permeability, cm^2
    double phi_star = porosity;
  
    dof_map.dof_indices (elem, dof_indices);
    dof_map.dof_indices (elem, dof_indices_u_res, u_res_var);
    dof_map.dof_indices (elem, dof_indices_v_res, v_res_var);
    if (dim==3) dof_map.dof_indices (elem, dof_indices_w_res, w_res_var);
    dof_map.dof_indices (elem, dof_indices_p, p_var);
    
    const unsigned int n_dofs   = dof_indices.size();
    const unsigned int n_u_res_dofs = dof_indices_u_res.size();
    const unsigned int n_v_res_dofs = dof_indices_v_res.size();
    unsigned int n_w_res_dofs;
    if (dim==3) n_w_res_dofs = dof_indices_w_res.size();
    const unsigned int n_p_dofs = dof_indices_p.size();
    
    fe_vel->reinit  (elem);
    fe_pres->reinit (elem);
    
    Ke.resize (n_dofs, n_dofs);
    Fe.resize (n_dofs);
    
    Kuu.reposition (u_res_var*n_u_res_dofs, u_res_var*n_u_res_dofs, n_u_res_dofs, n_u_res_dofs);
    Kuv.reposition (u_res_var*n_u_res_dofs, v_res_var*n_u_res_dofs, n_u_res_dofs, n_v_res_dofs);
    if (dim==3) Kuw.reposition (u_res_var*n_u_res_dofs, w_res_var*n_u_res_dofs, n_u_res_dofs, n_w_res_dofs);
    Kup.reposition (u_res_var*n_u_res_dofs, p_var    *n_u_res_dofs, n_u_res_dofs, n_p_dofs);
    
    Kvu.reposition (v_res_var*n_u_res_dofs, u_res_var*n_u_res_dofs, n_v_res_dofs, n_u_res_dofs);
    Kvv.reposition (v_res_var*n_u_res_dofs, v_res_var*n_u_res_dofs, n_v_res_dofs, n_v_res_dofs);
    if (dim==3) Kvw.reposition (v_res_var*n_u_res_dofs, w_res_var*n_u_res_dofs, n_v_res_dofs, n_w_res_dofs);
    Kvp.reposition (v_res_var*n_u_res_dofs, p_var    *n_u_res_dofs, n_v_res_dofs, n_p_dofs);
    if (dim==3) {
    Kwu.reposition (w_res_var*n_u_res_dofs, u_res_var*n_u_res_dofs, n_w_res_dofs, n_u_res_dofs);
    Kwv.reposition (w_res_var*n_u_res_dofs, v_res_var*n_u_res_dofs, n_w_res_dofs, n_v_res_dofs);
    Kww.reposition (w_res_var*n_u_res_dofs, w_res_var*n_u_res_dofs, n_w_res_dofs, n_w_res_dofs);
    Kwp.reposition (w_res_var*n_u_res_dofs, p_var    *n_u_res_dofs, n_w_res_dofs, n_p_dofs);
    }
    Kpu.reposition (p_var    *n_u_res_dofs, u_res_var*n_u_res_dofs, n_p_dofs,     n_u_res_dofs);
    Kpv.reposition (p_var    *n_u_res_dofs, v_res_var*n_u_res_dofs, n_p_dofs,     n_v_res_dofs);
    if (dim==3) Kpw.reposition (p_var    *n_u_res_dofs, w_res_var*n_u_res_dofs, n_p_dofs,     n_w_res_dofs);
    Kpp.reposition (p_var    *n_u_res_dofs, p_var    *n_u_res_dofs, n_p_dofs,     n_p_dofs);

    Fu.reposition (u_res_var*n_u_res_dofs, n_u_res_dofs);
    Fv.reposition (v_res_var*n_u_res_dofs, n_v_res_dofs);
    if (dim==3) Fw.reposition (w_res_var*n_u_res_dofs, n_w_res_dofs);
    Fp.reposition (p_var    *n_u_res_dofs, n_p_dofs);
    
    for (unsigned int qp=0; qp<qrule.n_points(); qp++)
    {
      for (unsigned int i=0; i<n_u_res_dofs; i++)
        for (unsigned int j=0; j<n_u_res_dofs; j++)
        {
          Kuu(i,j)    += mu/rho0/K*JxW[qp]*(psi[i][qp]*psi[j][qp]);
        } // end Kuu
      
      // Nothing in Kuv
      // Nothing in Kuw
      
      for (unsigned int i=0; i<n_u_res_dofs; i++)
      {
        for (unsigned int j=0; j<n_p_dofs; j++)
        {
          Kup(i,j)    += -JxW[qp]*chi[j][qp]*dpsi[i][qp](0);
          // Kup(i,j)  += -rho0*c*g*0*JxW[qp]*chi[j][qp]*psi[i][qp];
          
        }
        // Fu initialized to 0 since there is no gravitational component in the x direction
        // Fu(i)     += rho0*g*0*JxW[qp]*psi[i][qp]; // u is the x direction here, grad(z) dot u = 0
      }// end Kup
      
      // Nothing in Kvu
      
      for (unsigned int i=0; i<n_v_res_dofs; i++)
        for (unsigned int j=0; j<n_v_res_dofs; j++)
        {
          Kvv(i,j) += mu/rho0/K*JxW[qp]*(psi[i][qp]*psi[j][qp]);
        } // end Kvv
      
      // Nothing in Kvw
      
      for (unsigned int i=0; i<n_v_res_dofs; i++)
      {
        for (unsigned int j=0; j<n_p_dofs; j++)
        {
            Kvp(i,j)    += -JxW[qp]*chi[j][qp]*dpsi[i][qp](1);
          if (dimension==2)
          {
            Kvp(i,j)    += -rho0*c*g*(-1)*JxW[qp]*chi[j][qp]*psi[i][qp];
          }
        } //end Kvp
        if (dimension==2)
        {
          Fv(i) += rho0*g*(-1)*JxW[qp]*psi[i][qp]; // v is the z direction here, grad(z) dot v = -1
        } // End Fv
      }
      
      // Nothing in Kwu
      // Nothing in Kwv
      if (dim==3) {
      for (unsigned int i=0; i<n_w_res_dofs; i++)
        for (unsigned int j=0; j<n_w_res_dofs; j++)
        {
          Kww(i,j)    += mu/rho0/K*JxW[qp]*(psi[i][qp]*psi[j][qp]);
        } // end Kww
      
      for (unsigned int i=0; i<n_w_res_dofs; i++)
      {
        for (unsigned int j=0; j<n_p_dofs; j++)
        {
          Kwp(i,j)    += -JxW[qp]*chi[j][qp]*dpsi[i][qp](2);
          if (dimension==3)
          {
            Kwp(i,j)    += -rho0*c*g*(-1)*JxW[qp]*chi[j][qp]*psi[i][qp];
          }
        } //end Kwp
        {
          Fw(i) += rho0*g*(-1)*JxW[qp]*psi[i][qp]; // w is the z direction here, grad(z) dot w = -1
        }
      }
      }
      for (unsigned int i=0; i<n_p_dofs; i++)
        for (unsigned int j=0; j<n_u_res_dofs; j++)
        {
          Kpu(i,j)    += -dt*JxW[qp]*dpsi[j][qp](0)*chi[i][qp];
        }// end Kpu
      
      for (unsigned int i=0; i<n_p_dofs; i++)
        for (unsigned int j=0; j<n_v_res_dofs; j++)
        {
          Kpv(i,j)    += -dt*JxW[qp]*dpsi[j][qp](1)*chi[i][qp];
        } // end Kpv
      if (dim==3) {
      for (unsigned int i=0; i<n_p_dofs; i++)
        for (unsigned int j=0; j<n_w_res_dofs; j++)
        {
          Kpw(i,j)    += -dt*JxW[qp]*dpsi[j][qp](2)*chi[i][qp];
        } // end Kpw
      }
      for (unsigned int i=0; i<n_p_dofs; i++)
      {
        for (unsigned int j=0; j<n_p_dofs; j++)
        {
          Kpp(i,j)    += -phi_star*rho0*c*JxW[qp]*chi[i][qp]*chi[j][qp];
        }
        Fp(i) += phi_star*(rho0-rho_prev)*JxW[qp]*chi[i][qp];
      } // end Kpp
    } // end of the quadrature point qp-loop
    
    {// Add injection boundary condition component to the RHS vector and penalty to the matrix
      // The following loops over the sides of the element.
      // If the element has no neighbor on a side then that
      // side MUST live on a boundary of the domain.
      const double x_left   = es.parameters.get<Real>("left boundary");
      
      double dy = injection_width_y; // cm
      double dz = perforation_interval_z_top - perforation_interval_z_bottom; // centimeters
      double inletArea = dy * dz; // cm^2
      double x_right = es.parameters.get<Real>("right boundary");
      double C = (well_diameter + x_right) * M_PI; // circumference of wellbore in injection zone, cm
      if (dimension==2)
        C = (well_diameter + x_right) * M_PI; // Inject the total amount of fluid that would exit through the 1 cm wide boundary on the right boundary
      else if (dimension==3)
        C = well_diameter * M_PI; //Assumes half of injection goes into the modeled half plane.
      double f = dy/C; // fraction of total mass flow through inlet
      
      //                                      massFlow in kg/s
      double inletFlux = massFlow * f / inletArea; // g/(cm^2 s)
      
      // The penalty value.  \frac{1}{\epsilon}
      const Real penalty = 1.e7;
      
      for (unsigned int s=0; s<elem->n_sides(); s++)
        if (elem->neighbor(s) == NULL)
        {
          fe_face->reinit (elem,s);
          const Real xf = elem->side(s)->centroid()(0);
          
          Real yf;
          Real zf;
          if(dim==2)
            zf = elem->side(s)->centroid()(1);
          else if(dim==3)
          {
            yf = elem->side(s)->centroid()(1);
            zf = elem->side(s)->centroid()(2);
          }
          bool isleft = abs(xf-x_left) < 1e-14;
          bool isright = abs(xf-x_right) < 1e-14;
          bool isperforation = false; // Checks if in perforation range
          
          if (isleft)
          {
            for (unsigned int qp=0; qp<qface.n_points(); qp++)
            {
              
              for (unsigned int i=0; i<psi_face.size(); i++)
              {
                Point mypoint = xyz_face[qp];
                if(dim==2)
                {
                  isperforation = (mypoint(1)<=perforation_interval_z_top)&&(mypoint(1)>=perforation_interval_z_bottom);
                  if  (isperforation) // if in perforation interval
                  {
                    for (unsigned int j=0; j<psi_face.size(); j++)
                      Kuu(i,j) += JxW_face[qp]*penalty*psi_face[i][qp]*psi_face[j][qp];
                    Fu(i) += JxW_face[qp] * penalty * inletFlux * psi_face[i][qp]; // RHS
                  }
                  else // not in perforation interval
                  {
                    for (unsigned int j=0; j<psi_face.size(); j++)
                      Kuu(i,j) += JxW_face[qp]*penalty*psi_face[i][qp]*psi_face[j][qp];
                    // Fu(i) += JxW_face[qp] * penalty * 0 * fe_face[i][qp]; // RHS, flux is zero
                  }
                }
                else if(dim==3)
                {
                  isperforation = (mypoint(2)<=perforation_interval_z_top);
                  isperforation = isperforation&&(mypoint(2)>=perforation_interval_z_bottom);
                  isperforation = isperforation&&(mypoint(1)<=((miny+maxy)*0.5 + injection_width_y*0.5));
                  isperforation = isperforation&&(mypoint(1)>=((miny+maxy)*0.5 - injection_width_y*0.5));
                  
                  if  (isperforation) // if in perforation interval
                  {
                    for (unsigned int j=0; j<psi_face.size(); j++)
                      Kuu(i,j) += JxW_face[qp]*penalty*psi_face[i][qp]*psi_face[j][qp];
                    Fu(i) += JxW_face[qp] * penalty * inletFlux * psi_face[i][qp]; // RHS
                  }
                  else // not in perforation interval
                  {
                    for (unsigned int j=0; j<psi_face.size(); j++)
                      Kuu(i,j) += JxW_face[qp]*penalty*psi_face[i][qp]*psi_face[j][qp];
                    // Fu(i) += JxW_face[qp] * penalty * 0 * fe_face[i][qp]; // RHS, flux is zero
                  }
                  
                }
                else
                {
                  cout << "not updated for 1D or 3D." << endl;
                  exit(1);
                }
                
              }// end i loop
            }// end quadrature point loop
          }// end if(isleft)
        } // end if (elem->neighbor(side) == libmesh_nullptr)
    }
    
    // Add Dirichlet constraints
    dof_map.constrain_element_matrix_and_vector (Ke, Fe, dof_indices);
    
    system.matrix->add_matrix (Ke, dof_indices);
    system.rhs->add_vector    (Fe, dof_indices);
  } // end of element loop
// debugging ouput:
//  system.matrix->print_matlab("SystemMatrix.m");
//  system.rhs->print_matlab("SystemVector.m");
  return;
} // end assemble_res_flow

double effective_porosity (double phi0, double alpha, double M, double div_u, double epsilon0, double p, double p0 )
{
  return phi0 + alpha * ( div_u - epsilon0 ) + ( p - p0 )/M;
} // end effective_porosity

RealTensor strain_calc( RealTensor du, int dim=2)
{
  int ii,jj;
  RealTensor strain(dim);
  for (ii=0; ii<dim; ii++)
    for (jj=0; jj<dim; jj++)
    {
      strain(ii,jj) = 0.5 * ( du(ii,jj) + du(jj,ii) );
    }
  return strain;
} // end strain_calc

RealTensor stress_calc(RealTensor strain, double div_u, double lambda, double G, int dim = 2)
{
  RealTensor stress(dim);
  int ii,jj;
  for (ii=0; ii<dim; ii++)
  {
    for (jj=0; jj<dim; jj++)
      stress(ii,jj) = 2*G*strain(ii,jj);
    stress(ii,ii) += lambda*div_u;
  }
  return stress;
}

Number init_res_flow_value (const Point & p,
                            const Parameters & parameters,
                            const std::string &system_name,
                            const std::string &variable_name)
{
  double density_mass = 1; // g/cc
  double init_press;
  // return initial reservoir pressure
  //
  if( !variable_name.compare("p") || !variable_name.compare("p initial") )
  {
    if (dimension==2)
      init_press = (bartocgs(parameters.get<Real>("Pt0"))-(p(1)-resBottom)*density_mass);
    else if (dimension==3)
      init_press = (bartocgs(parameters.get<Real>("Pt0"))-(p(2)-resBottom)*density_mass);
    return(init_press);
  }
  else
    return(0.0);
}

void init_res_flow (EquationSystems & es, const std::string & system_name)
{
  // It is a good idea to make sure we are initializing
  // the proper system.
  libmesh_assert_equal_to (system_name, "Reservoir Flow");
  
  // Get a reference to the Convection-Diffusion system object.
  TransientLinearImplicitSystem & system =
  es.get_system<TransientLinearImplicitSystem>("Reservoir Flow");
  
  system.project_solution(init_res_flow_value, NULL, es.parameters);
}

void init_initial_values (EquationSystems & es, const std::string & system_name)
{
  // It is a good idea to make sure we are initializing
  // the proper system.
  libmesh_assert_equal_to (system_name, "Initial Values");
  
  // Get a reference to the Convection-Diffusion system object.
  TransientLinearImplicitSystem & system =
  es.get_system<TransientLinearImplicitSystem>("Initial Values");
  
  system.project_solution(init_res_flow_value, NULL, es.parameters);
}
