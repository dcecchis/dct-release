/*******************************************************************/
/*                 Distributed Coupling Toolkit (DCT)              */
/*                                                                 */
/*!
   \file test_dct_perform.h

      \brief This program is to test the DCT performance of all
      the phases and interpolation operations of one way and
      one variable. Header file.
      
      The file implements a test consists of defining two dummy
      models with different resolutions and perform the forcing
      of one variable from one model into the other.

      Header file where the domain limits and other common
      utils are defined for test_dct_perfom tests.

    \date Created on Sep 30, 2011
    
    \author Dany De Cecchis: dcecchis@gmail.com
    \author Tony Drummond: LADrummond@lbl.gov

    \copyright GNU Public License.

*/
/*******************************************************************/

# ifndef TEST_DCT_PERFORM_H
#   define TEST_DCT_PERFORM_H

// # define INIX       10.0
// # define ENDX      100.0
// # define INIY      -50.0
// # define ENDY       50.0
// # define INIZ      -10.0
// # define ENDZ       30.0

/*** Values to test the interpolation **/
# define INIX      -1.0
# define ENDX       3.0
# define INIY       0.0
# define ENDY       6.0
# define INIZ      -2.0
# define ENDZ       2.0

/* Determine the direction on the virtual MPI cartesian topology */
# define LEFT     0
# define RIGHT    1
# define UP       2
# define DOWN     3

# define NGBRS    4

# define HORTAG   1
# define VERTAG   2


/*** Maximum function inline ***/
static inline double dmax(double a, double b) {
  return a > b ? a : b;
}


void update( int, int, double *, double * );

void inidat( int, int, int, int, int, int, double * );

# endif /* TEST_DCT_PERFORM_H */
