#include <stdio.h>
#include <ctype.h>
#include <mpi.h>
#ifdef MEMTRK
  #include <TAU.h>
#endif
#include "dct.h"
#include "test_sd_comm_intp_tool.h"

#define     XRES    73
#define     YRES    41

int test_sd_comm_b( int rank, int numtasks, MPI_Comm dom_comm, MPI_Comm globalcomm )
{
/* ---------------------------------------  Variables Declaration  */
   int         ierr = 0;
   DCT_Error   dcterr = { DCT_SUCCESS, (DCT_String)NULL};
   
   DCT_Couple  couple;
   DCT_Model   model;
   DCT_Field   dcttemp;
   DCT_Field   dctwsx;
   DCT_Field   dctwsy;

   DCT_Scalar  xmarks[XRES];
   DCT_Scalar  ymarks[YRES];

   int         playdim[2] = { 1, 1 };
   DCT_Integer iniind[2] = { 0, 0 };
   DCT_Integer endind[2] = { (XRES-1), (YRES-1)};

   float       tini,dt, tend, tt;
   float      *temp;
   float      *windx;
   float      *windy;

   int         ii, jj, maxdim;

/* --------------------------------------  BEGIN( test_sd_comm_b ) */

   printf("This is the process %d of %d\n", rank, numtasks);

   tini =  0.0;
   dt   =  1.0;
   tend = 11.0;
   
   /****************************************************************************
    ***********************   DCT_BeginRegistration Call   *********************
    ****************************************************************************/
   dcterr = DCT_BeginRegistration( globalcomm );
   DCTCHKERR( dcterr );

   /****************************************************************************
    ************************   Registering the DCT_Model  **********************
    ****************************************************************************/
   dcterr = DCT_Create_Model( &model, "reg_model", "Test regional model", 1 );
   DCTCHKERR( dcterr );

   dcterr = DCT_Set_Model_Time( &model, tini, dt, DCT_TIME_NO_UNIT );
   DCTCHKERR( dcterr );
   
   /* GEnerating the ticks marks */
   xmarks[0] = -180.0;
   for ( ii=1; ii < XRES; ii++ ) xmarks[ ii ] = xmarks[ ii-1 ]+ 2.5;
   ymarks[0] = -50.0;
   for ( ii=1; ii < YRES; ii++ ) ymarks[ ii ] = ymarks[ ii-1 ]+ 2.5;
   
   dcterr = DCT_Set_Model_Dom( &model, 2, 1, DCT_RECTILINEAR, xmarks, XRES );
   DCTCHKERR( dcterr );
   dcterr = DCT_Set_Model_Dom( &model, 2, 2, DCT_RECTILINEAR, ymarks, YRES );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Model_ParLayout( &model, DCT_DIST_RECTANGULAR, playdim); /*, NULL );*/
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Model_SubDom( &model, &rank, iniind, endind );
   DCTCHKERR( dcterr );

   /****************************************************************************
    *************************   Creating the Temperature  **********************
    ****************************************************************************/
   temp = (float *)malloc(sizeof(float)*(size_t)((XRES-1)*(YRES-1)));
   if ( temp == (float *)NULL ) {
         fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }  
   dcterr = DCT_Create_Field( &dcttemp, "reg_sst", "Temperature at Sea Surface",
                            CELSIUS, DCT_CONSUME);
   DCTCHKERR( dcterr );

   dcterr = DCT_Set_Field_Dims( &dcttemp, (XRES-1), (YRES-1) );
   DCTCHKERR( dcterr );

   /* GEnerating the ticks marks */
   xmarks[0] = -178.75;
   for ( ii=1; ii < (XRES-1); ii++ ) xmarks[ ii ] = xmarks[ ii-1 ]+ 2.5;
   ymarks[0] = -48.75;
   for ( ii=1; ii < (YRES-1); ii++ ) ymarks[ ii ] = ymarks[ ii-1 ]+ 2.5;
   dcterr = DCT_Set_Field_Labels( &dcttemp, DCT_RECTILINEAR, xmarks, (XRES-1),
                                  DCT_RECTILINEAR, ymarks, (YRES-1) );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Val_Location( &dcttemp, DCT_LOC_CENTERED );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Time( &dcttemp, DCT_TIME_NO_UNIT, tini );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Freq_Consumption( &dcttemp, dt );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Values( &dcttemp, DCT_FLOAT, temp );
   DCTCHKERR( dcterr );

   /****************************************************************************
    ***********************   Linking Temperature to Model *********************
    ****************************************************************************/
   dcterr =  DCT_Set_Model_Var( &model, &dcttemp, DCT_FIELD_TYPE );
   DCTCHKERR( dcterr );


   /****************************************************************************
    *************************   Creating the x-WindStress  *********************
    ****************************************************************************/
   windx = (float *)malloc(sizeof(float)*(size_t)((XRES-1)*(YRES)));
   if ( windx == (float *)NULL ) {
         fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }  
   dcterr = DCT_Create_Field( &dctwsx, "reg_windsx", "X-coordinate wind stress at Sea Surface",
                              DCT_NO_UNITS, DCT_CONSUME);
   DCTCHKERR( dcterr );

   dcterr = DCT_Set_Field_Dims( &dctwsx, (XRES-1), YRES );
   DCTCHKERR( dcterr );

   /* Generating the temperature ticks marks */
   xmarks[0] = -178.5;
   for ( ii=1; ii < (XRES-1); ii++ ) xmarks[ ii ] = xmarks[ ii-1 ]+ 2.5;
   ymarks[0] = -50.0;
   for ( ii=1; ii < YRES; ii++ ) ymarks[ ii ] = ymarks[ ii-1 ]+ 2.5;
   dcterr = DCT_Set_Field_Labels( &dctwsx, DCT_RECTILINEAR, xmarks, (XRES-1),
                                  DCT_RECTILINEAR, ymarks, YRES );
   DCTCHKERR( dcterr );
   
//    dcterr = DCT_Set_Field_Val_Location( &dctwsx, DCT_LOC_CENTERED );
//    DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Time( &dctwsx, DCT_TIME_NO_UNIT, tini );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Freq_Consumption( &dctwsx, dt );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Values( &dctwsx, DCT_FLOAT, windx );
   DCTCHKERR( dcterr );

   /****************************************************************************
    ***********************   Linking x-WindStress to Model  *******************
    ****************************************************************************/
   dcterr =  DCT_Set_Model_Var( &model, &dctwsx, DCT_FIELD_TYPE );
   DCTCHKERR( dcterr );


   /****************************************************************************
    *************************   Creating the y-WindStress  *********************
    ****************************************************************************/
   windy = (float *)malloc(sizeof(float)*(size_t)(XRES*(YRES-1)));
   if ( windy == (float *)NULL ) {
         fprintf( stderr, "\nERROR[memory]: Error allocating memory\n" );
         fprintf( stderr, "ERROR: %s (%d)\n", __FILE__ , __LINE__ );
         return( 1 );
   }  
   dcterr = DCT_Create_Field( &dctwsy, "reg_windsy", "Y-coordinate wind stress at Sea Surface",
                              DCT_NO_UNITS, DCT_CONSUME);
   DCTCHKERR( dcterr );

   dcterr = DCT_Set_Field_Dims( &dctwsy, XRES, (YRES-1) );
   DCTCHKERR( dcterr );

   /* Generating the temperature ticks marks */
   xmarks[0] = -180.0;
   for ( ii=1; ii < XRES; ii++ ) xmarks[ ii ] = xmarks[ ii-1 ]+ 2.5;
   ymarks[0] = -48.75;
   for ( ii=1; ii < (YRES-1); ii++ ) ymarks[ ii ] = ymarks[ ii-1 ]+ 2.5;
   dcterr = DCT_Set_Field_Labels( &dctwsy, DCT_RECTILINEAR, xmarks, XRES,
                                  DCT_RECTILINEAR, ymarks, (YRES-1) );
   DCTCHKERR( dcterr );
   
//    dcterr = DCT_Set_Field_Val_Location( &dctwsy, DCT_LOC_CENTERED );
//    DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Time( &dctwsy, DCT_TIME_NO_UNIT, tini );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Freq_Consumption( &dctwsy, dt );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Set_Field_Values( &dctwsy, DCT_FLOAT, windy );
   DCTCHKERR( dcterr );

   /****************************************************************************
    ***********************   Linking y-WindStress to Model  *******************
    ****************************************************************************/
   dcterr =  DCT_Set_Model_Var( &model, &dctwsy, DCT_FIELD_TYPE );
   DCTCHKERR( dcterr );


   /****************************************************************************
    ***************************   Creating the Couple  *************************
    ****************************************************************************/
   dcterr =  DCT_Create_Couple( &couple, "GlbReg_clp",
               "Coupling beteween regional and global model", &model, "glb_model" );
   DCTCHKERR( dcterr );

   /****************************************************************************
    ********************   Linking Temperature to the Couple  ******************
    ****************************************************************************/
   dcterr =  DCT_Set_Coupling_Vars( &couple, &dcttemp, "glb_sst", DCT_FIELD_TYPE,
                                    DCT_LINEAR_INTERPOLATION );
   DCTCHKERR( dcterr );
   
   /****************************************************************************
    ********************  Linking x-WindStress to the Couple  ******************
    ****************************************************************************/
   dcterr =  DCT_Set_Coupling_Vars( &couple, &dctwsx, "glb_windsx", DCT_FIELD_TYPE,
                                    DCT_LINEAR_INTERPOLATION );
   DCTCHKERR( dcterr );
   
   /****************************************************************************
    ********************  Linking y-WindStress to the Couple  ******************
    ****************************************************************************/
   dcterr =  DCT_Set_Coupling_Vars( &couple, &dctwsy, "glb_windsy", DCT_FIELD_TYPE,
                                    DCT_LINEAR_INTERPOLATION );
   DCTCHKERR( dcterr );
   
   /****************************************************************************
    ************************   DCT_EndRegistration Call   **********************
    ****************************************************************************/
   dcterr = DCT_EndRegistration(  );
   DCTCHKERR( dcterr );
   
   /****************************************************************************
    ****************************************************************************
    ************************   Model Timesteping begins   **********************
    ****************************************************************************/
   /*** Initialized the variable values for control ***/
   maxdim = (XRES-1)*(YRES-1);
   for ( ii=0; ii < maxdim; ii++ )  *(temp + ii) = -1.0;

   maxdim = (XRES-1)*YRES;
   for ( ii=0; ii < maxdim; ii++ )  *(windx + ii) = -1.0;

   maxdim = XRES*(YRES-1);
   for ( ii=0; ii < maxdim; ii++ )  *(windy + ii) = -1.0;
   
   tt = tini;
   while ( tt <=  tend ) {
      printf("\t****** Model running time %g\n\t******\n", tt );
      dcterr =  DCT_Update_Model_Time( &model );
      DCTCHKERR( dcterr );
      
      printf("Receiving temperature\n" );
      dcterr =  DCT_Recv_Field( &dcttemp );
      DCTCHKERR( dcterr );
   
      printf("Receiving x-wind stress\n" );
      dcterr =  DCT_Recv_Field( &dctwsx );
      DCTCHKERR( dcterr );
   
      printf("Receiving y-wind stress\n" );
      dcterr =  DCT_Recv_Field( &dctwsy );
      DCTCHKERR( dcterr );
   
      tt += dt;
   }
   
   /****************************************************************************
    *************************   Model Timesteping ends   ***********************
    ****************************************************************************
    ****************************************************************************/
   
   /****************************************************************************
    **********************   Destroying the DCT structures   *******************
    ****************************************************************************/
   dcterr = DCT_Destroy_Couple( &couple );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Destroy_Field( &dcttemp );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Destroy_Model( &model );
   DCTCHKERR( dcterr );
   
   dcterr = DCT_Finalized(  );
   DCTCHKERR( dcterr );

   free ( temp );
   free ( windx );
   free ( windy );
   
   return ( ierr );
/* ----------------------------------------  END( test_sd_comm_b ) */

}