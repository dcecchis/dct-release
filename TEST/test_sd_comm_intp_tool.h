/**************************************************************/
/*              test_sd_comm_intp_tool.h                      */
/*                                                            */
/*      This is the header file used for the example test     */
/*      test_subdom_subdom_comm_intp.c where all the          */
/*      functions headers and definition about the mesh       */
/*      sizes are made                                        */
/**************************************************************/
/*  Created on Jun 20, 2011                */
/*                                                            */
/**************************************************************/

/*
      $Id: test_sd_comm_intp_tool.h,v 1.2 2011/07/13 02:42:08 dcecchis Exp $
*/
/*****************************************************************/
/*
      $Log: test_sd_comm_intp_tool.h,v $
      Revision 1.2  2011/07/13 02:42:08  dcecchis
      Fixing the code to perform the tests

      Revision 1.1  2011/07/12 22:40:17  dcecchis
      Tests files using only C code. This use data from files about temperature
      and wind stress.

*/
/*****************************************************************/


/*** Longitudinal and latitudinal discretization for temperature ***/
#define    TNLONG   72
#define    TNLATI   36

/*** Longitudinal and latitudinal discretization for wind stress ***/
#define    WSNLONG  360
#define    WSNLATI  180

#define    TEMPDIR  "../DATA/"    /** Temperature directory location **/
#define    WSDIR    "../DATA/WINDSTRESS/"    /** wind stress directory location **/

#define    TEMPFILE "hadcrut3v.dat" /** Temperature file name  **/



/***** Function prototypes *****/
int read_temperature( int, int, float ** );

int read_wind_stress( float **,  float ** );

int feed_temperature( float *, float **, int );

int feed_xwinds( float *, float **, int );

int feed_ywinds( float *, float **, int );
