/**************************************************************/
/*              Distributed Coupling Toolkit (DCT)            */
/*!
   \file dct_model.c

   \brief Contains the functions implementation of the data structure
          DCT_Model

    This file contains the functions to handle the DCT data type
    structure DCT_Model; which represents a computational model,
    consisting in a domain and a number of \DCT_Vars.

    \date Date of Creation: Oct 11, 2005

    \author Dany De Cecchis: dcecchis@gmail.com
    \author Tony Drummond: LADrummond@lbl.gov

    \copyright GNU Public License.
 */
/**************************************************************/


/* -----------------------------------------------  Include Files  */
#include <stdio.h>
#include <string.h>
#include "dct.h"
/* --------------------------------------------  Global Variables  */
#include "dctsys.h"
#include "dct_comm.h"


/******************************************************************/
/*                       DCT_Create_Model                         */
/*                                                                */
/*!   This routine when called by the user it initializes a global
      model and be registered in the global DCT structure; unless
      is called during the end of the DCT Registration Phase by
      the Registration Broker.

   \param [out]   mod  DCT_Model model to be setup in this routine
   \param [in]   name  Internal name of the model (use during
                       coupling)
   \param [in]   desc  Description of the model 120 char max
   \param [in] nprocs  Number of procesors used by the model

   \return A DCT_Error error handler variable.

*/
/******************************************************************/
DCT_Error DCT_Create_Model( DCT_Model *mod, const DCT_Name name, const DCT_String desc,
                            const DCT_Integer nprocs )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error            ierr;
  int                  nfound, len;

  DCT_Model           *ModelA;
  DCT_List            *current;

/* ----------------------------------  BEGIN( DCT_Create_Model ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  /* Or when the broker is calling the function during the registration */
  if ( (DCT_Step_Check != DCT_BEGIN_STATE) && (DCT_Step_Check != DCT_BROKER_STATE) ) {
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)100);
    sprintf(ierr.Error_msg,
       "[DCT_Create_Model] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }

  /** get the name length **/
  len = strlen(name);
  if ( DCT_Step_Check == DCT_BEGIN_STATE ) { /** Checking if it is during user declaration **/
     
     /* Check if the model was not previously created */
     if (mod->ModTag==DCT_TAG_UNDEF) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)100);
         sprintf(ierr.Error_msg,
          "[DCT_Create_Model] Invalid Model passed as 1st argument. The DCT_Model was already created\n");
         return(ierr);
     }
     
     /* Check if the model can be modified, before to call DCT_Create_Couple function */
     if (mod->ModCommit == DCT_TRUE) {
        /* Sweep its own DCT_Model list */
        nfound = 1;
        current = DCT_Reg_Model;
        while ((current != (DCT_List *) NULL) && (nfound)) {
            DCT_List_Ext (&current, (DCT_VoidPointer *) &ModelA);
            nfound = (ModelA == mod? 0: 1);
        }
        if (nfound == 0) {
           ierr.Error_code  = DCT_INVALID_OPERATION;
           ierr.Error_msg   = (DCT_String) malloc((size_t)180);
           sprintf(ierr.Error_msg,
              "[DCT_Create_Model] Invalid Operation. The model %s is already commited\n", name);
           return(ierr);
        }
     }
     /* Name is a mandatory field to be filled */
     /*len = strlen(name); it was moved to avoid segmentation fault when a null argument is passed*/
     if ( (name == (DCT_Name)NULL) || (len == 0) ) {
       ierr.Error_code  = DCT_INVALID_ARGUMENT;
       ierr.Error_msg   = (DCT_String) malloc((size_t)180);
       sprintf(ierr.Error_msg,
          "[DCT_Create_Model] Invalid Name for Model. A name should be given\n");
       return(ierr);
     }

     if ( nprocs <= 0 ) {
       ierr.Error_code  = DCT_INVALID_MODEL_TAG;
       ierr.Error_msg   = (DCT_String) malloc((size_t)180);
       sprintf(ierr.Error_msg,
       "[DCT_Create_Model] The number of processors to the Model <%s> must be a positive integer\n",
               name);
       return(ierr);
     }
  }

  /*   Set the model name */
  mod->ModName = (DCT_Name) malloc(sizeof(char)*(size_t)(len+1));
  DCTERROR( mod->ModName == (DCT_Name)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
  strcpy(mod->ModName, name);

  /*   Set the model description */
  if (desc != (DCT_String)NULL){
    len = strlen(desc);
    mod->ModDescription = (DCT_String)malloc(sizeof(char)*(size_t)(len+1));
    DCTERROR( mod->ModDescription == (DCT_String) NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr));
    strcpy(mod->ModDescription, desc);
  } else mod->ModDescription = (DCT_String)NULL;

  /*  Control structure initialize */
  mod->ModCommit = DCT_FALSE;
  /* The tag with DCT_TAG_UNDEF distinguish a structure that is created */
  mod->ModTag    = DCT_TAG_UNDEF;

  /* Comm initialization */
  mod->ModNumProcs      = nprocs;
  mod->ModLeadRank      = DCT_UNDEFINED_RANK;
  mod->ModGroupComm     = DCT_COMM_NULL;

  /* timestepping initialization */
  mod->ModTimeIni        = DCT_TIME_UNSET;
  mod->ModTime           = DCT_TIME_UNSET;
  mod->ModCurrentTime    = DCT_TIME_UNSET;
  mod->ModTimeUnits      = DCT_TIME_UNKNOWN;


  /* Set Default Values for other Meta-Data model */
  /* Subdomain structure initialized */
  mod->ModDomain = (DCT_Model_Dom *)NULL;

  /* Model's variables values initialization */
  mod->ModTotVarProduced = 0;
  mod->ModTotVarConsumed = 0;
  mod->ModConsumeVars    = (DCT_List *)NULL;
  mod->ModProduceVars    = (DCT_List *)NULL;

  if ( DCT_Step_Check == DCT_BEGIN_STATE ) { /* Post processing during user
                                                declaration                 */

     /* The Model is added to the DCT system local variable list */
     if ( DCT_List_CHKAdd( &DCT_Reg_Model, mod, DCT_MODEL_TYPE ) != (DCT_List *)NULL ) {
       ierr.Error_code   = DCT_OBJECT_DUPLICATED;
       ierr.Error_msg    = (DCT_String) malloc(115);
       sprintf(ierr.Error_msg,
         "[DCT_Create_Model] DCT_Model name <%s> is already used.\n", mod->ModName );
       return(ierr);
     }
     DCT_LocalModel++;
  }

  ierr.Error_code = DCT_SUCCESS;
  ierr.Error_msg  = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* ------------------------------------  END( DCT_Create_Model ) */

}


/***********************************************************************/
/*                         DCT_Set_Model_Time                          */
/*                                                                     */
/*!  Sets the DCT_Models all related with time stepin; Time Units
     in accordance to DCT_Time_Types, initial time, and time step.

     \param [in, out]   mod  DCT_Model model to be setup in this
                             routine.
     \param [in]  time_init  Initial time of the model
     \param [in] time_inter  Integration time of the model  (time step)
     \param [in] time_units  Time Units of the DCT_Model (i.e., weeks,
                             hours, etc)

     \return A DCT_Error error handler variable.

*/
/***********************************************************************/

DCT_Error DCT_Set_Model_Time( DCT_Model *mod, const DCT_Scalar time_init,
                              const DCT_Scalar time_inter,
                              const DCT_Time_Types time_units)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error ierr;

/* ----------------------------------  BEGIN( DCT_Set_Model_Time ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  /* Or when the broker is calling the function during the registration (Value 2) */
  if ( (DCT_Step_Check != DCT_BEGIN_STATE) && (DCT_Step_Check != DCT_BROKER_STATE) ) {
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)100);
    sprintf(ierr.Error_msg,
       "[DCT_Set_Model_Time] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }

  if ( DCT_Step_Check == DCT_BEGIN_STATE ) { /** Checking if it is during user declaration **/
      /*  Check the model was created */
      if (mod->ModTag!=DCT_TAG_UNDEF) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)115);
         sprintf(ierr.Error_msg,
          "[DCT_Set_Model_Time] Invalid Model passed as 1st argument. Create DCT_Model first by calling DCT_Create_Model\n");
         return(ierr);
      }
      /* Check if the model can be modified, before to call DCT_Create_Couple function */
      if (mod->ModCommit == DCT_TRUE) {
       ierr.Error_code  = DCT_INVALID_OPERATION;
       ierr.Error_msg   = (DCT_String) malloc((size_t)80);
       sprintf(ierr.Error_msg,
          "[DCT_Set_Model_Time] Invalid Operation. The model %s is already commited\n", mod->ModName);
       return(ierr);
      }
      if ( (time_units <= DCT_TIME_UNKNOWN) || (time_units > DCT_DEFINED_TIME_UNITS) ){
        ierr.Error_code  = DCT_INVALID_UNITS;
        ierr.Error_msg   = (DCT_String) malloc((size_t)80);
        sprintf(ierr.Error_msg,
                "[DCT_Set_Model_Time] Invalid time unit type for mod <%s>\n",
                 mod->ModName);
        return(ierr);
      }
      if (time_init < 0.0)  {
       ierr.Error_code  = DCT_INVALID_ARGUMENT;
       ierr.Error_msg   = (DCT_String) malloc((size_t)100);
       sprintf(ierr.Error_msg,
       "[DCT_Set_Model_Time] Initial time for model <%s> must be >= 0.0\n", mod->ModName);
       return(ierr);
      }
      if (time_inter <= 0.0)  {
       ierr.Error_code  = DCT_INVALID_ARGUMENT;
       ierr.Error_msg   = (DCT_String) malloc((size_t)100);
       sprintf(ierr.Error_msg,
       "[DCT_Set_Model_Time] Integration time (time step) for model <%s> must be > 0.0\n", mod->ModName);
       return(ierr);
      }
  }
  mod->ModTimeIni       = (DCT_Scalar) time_init;
  mod->ModTime          = (DCT_Scalar) time_inter;
  mod->ModTimeUnits     = (DCT_Time_Types) time_units;

  ierr.Error_code       = DCT_SUCCESS;
  ierr.Error_msg        = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* ------------------------------------  END( DCT_Set_Model_Time ) */

}

/*********************************************************************/
/*                   DCT_Set_Model_RSpaced_Dom                       */
/*                                                                   */
/*! This routine fixes the scope of the model domain discretized
    equally spaced in all of i ts dimensions (a Cartesian grid,
    DCT_CARTESIAN).

   \param [in, out] mod  DCT_Model model to be setup in this routine.
   \param [in]      dim  set the dimension of the domain, must be 2,3
                         or 4.
   \param [in]       xo  Array with the initial values in each
                         dimension.
   \param [in]       xf  Array with the final values in each
                         dimension.
   \param [in]     npts  Array with the number of points in each
                         dimension.

   \return A DCT_Error error handler variable.

*/
/*********************************************************************/

DCT_Error DCT_Set_Model_RSpaced_Dom( DCT_Model *mod, const DCT_Integer dim,
                                     const DCT_Scalar xo[], const DCT_Scalar xf[],
                                     const DCT_Integer npts[])
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error             ierr;
  DCT_Model_Dom        *tmod;
  DCT_Scalar           *Values;
  register int          i;

/* -----------------------------  BEGIN( DCT_Set_Model_EqSpc_Dom ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  /* Or when the broker is calling the function during the registration (Value 2) */
  if ( (DCT_Step_Check != DCT_BEGIN_STATE) && (DCT_Step_Check != DCT_BROKER_STATE) ) {
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)100);
    sprintf(ierr.Error_msg,
       "[DCT_Set_Model_RSpaced_Dom] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }
  if ( DCT_Step_Check == DCT_BEGIN_STATE ) { /** Checking if it is during user declaration **/
     /*  Check the model was created */
      if (mod->ModTag!=DCT_TAG_UNDEF) {
       ierr.Error_code   = DCT_INVALID_ARGUMENT;
       ierr.Error_msg    = (DCT_String) malloc((size_t)115);
       sprintf(ierr.Error_msg,
         "[DCT_Set_Model_RSpaced_Dom] Invalid Model passed. Create DCT_Model first by calling DCT_Create_Model \n");
       return(ierr);
     }

     /* Check if the model can be modified, before to call DCT_Create_Couple function */
     if (mod->ModCommit == DCT_TRUE){
       ierr.Error_code  = DCT_INVALID_OPERATION;
       ierr.Error_msg   = (DCT_String) malloc((size_t)80);
       sprintf(ierr.Error_msg,
          "[DCT_Set_Model_RSpaced_Dom] Invalid Operation. The model %s is already commited\n",
           mod->ModName);
       return(ierr);
     }

     if ( (dim < MIN_DOMAIN_DIM) || (dim > MAX_DOMAIN_DIM) ){
       ierr.Error_code   = DCT_INVALID_ARGUMENT;
       ierr.Error_msg    = (DCT_String) malloc((size_t)110);
       sprintf(ierr.Error_msg,
          "[DCT_Set_Model_RSpaced_Dom] Domain Dimension must be 2, 3, or 4 for Model <%s>\n", mod->ModName);
       return(ierr);
     }

     for ( i=0; i < dim; i++){
       if ( xf[i] <= xo[i] ) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)110);
         sprintf(ierr.Error_msg,
            "[DCT_Set_Model_RSpaced_Dom] initial value greater than final one for Model <%s>\n",
             mod->ModName);
         return(ierr);
       }
       if ( npts[i] <= 0 ) {
         ierr.Error_code   = DCT_INVALID_UNITS;
         ierr.Error_msg    = (DCT_String) malloc((size_t)110);
         sprintf(ierr.Error_msg,
            "[DCT_Set_Model_RSpaced_Dom] Number of points of Domain should be a positive integer in Model <%s>\n",
           mod->ModName);
         return(ierr);
       }
     }

     if (mod->ModDomain != (DCT_Model_Dom *)NULL) {
       ierr.Error_code   = DCT_INVALID_ARGUMENT;
       ierr.Error_msg    = (DCT_String) malloc((size_t)115);
       sprintf(ierr.Error_msg,
           "[DCT_Set_Model_RSpaced_Dom] The Domain is already set in model <%s>\n",
           mod->ModName);
       return(ierr);
     }
  }

  /* Allocate for Model Domain structure */
  tmod  = (DCT_Model_Dom *)malloc(sizeof(DCT_Model_Dom));
  DCTERROR( tmod == (DCT_Model_Dom *) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr));

  tmod->ModSubDom = (DCT_Data_SubDom *)NULL;
  tmod->ModDomDim = dim;

  /* Parallel Layout structure initialized */
  tmod->ModSubDomParLayout = DCT_DIST_NULL;
  tmod->ModSubDomLayoutDim = (DCT_Integer *)NULL;

  /* Model Domain structure is set */
  tmod->ModDomType  = (DCT_Domain_Type *) malloc(sizeof(DCT_Domain_Type)*(size_t)dim);
  DCTERROR( tmod->ModDomType == (DCT_Domain_Type *) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );

  tmod->ModNumPts  = (DCT_Integer *) malloc(sizeof(DCT_Integer)*(size_t)dim);
  DCTERROR( tmod->ModNumPts == (DCT_Integer *) NULL,
            "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );

  tmod->ModValues  = (DCT_Scalar **) malloc(sizeof(DCT_Scalar *) *(size_t)dim);
  DCTERROR( tmod->ModValues == (DCT_Scalar **) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr));

  /* Filling up the values of the domain structure */
  switch (dim){
  case 4:
    tmod->ModDomType[3] = DCT_CARTESIAN;
    tmod->ModNumPts[3] = npts[3];

    Values = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)3);
    DCTERROR( Values == (DCT_Scalar *) NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
    Values[0] = (DCT_Scalar) xo[3];
    Values[1] = (DCT_Scalar) xf[3];
    Values[2] = (Values[1]-Values[0])/(DCT_Scalar)(npts[3]-1);

    tmod->ModValues[3] = Values;
  case 3:
    tmod->ModDomType[2] = DCT_CARTESIAN;
    tmod->ModNumPts[2] = npts[2];

    Values = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)3);
    DCTERROR( Values == (DCT_Scalar *) NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr));
    Values[0] = (DCT_Scalar) xo[2];
    Values[1] = (DCT_Scalar) xf[2];
    Values[2] = (Values[1]-Values[0])/(DCT_Scalar)(npts[2]-1);

    tmod->ModValues[2] = Values;
  case 2:
    tmod->ModDomType[1] = DCT_CARTESIAN;
    tmod->ModNumPts[1] = npts[1];

    Values = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)3);
    DCTERROR( Values == (DCT_Scalar *) NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr));
    Values[0] = (DCT_Scalar) xo[1];
    Values[1] = (DCT_Scalar) xf[1];
    Values[2] = (Values[1]-Values[0])/(DCT_Scalar)(npts[1]-1);

    tmod->ModValues[1] = Values;
  /* ---------------------------- Dim 1 -------------------------- */
    tmod->ModDomType[0] = DCT_CARTESIAN;
    tmod->ModNumPts[0] = npts[0];

    Values = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)3);
    DCTERROR( Values == (DCT_Scalar *) NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr));
    Values[0] = (DCT_Scalar) xo[0];
    Values[1] = (DCT_Scalar) xf[0];
    Values[2] = (Values[1]-Values[0])/(DCT_Scalar)(npts[0]-1);

    tmod->ModValues[0] = Values;
  }

  mod->ModDomain = tmod;

  ierr.Error_code       = DCT_SUCCESS;
  ierr.Error_msg        = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* -------------------------------  END( DCT_Set_Model_EqSpc_Dom ) */

}

/*********************************************************************/
/*                        DCT_Set_Model_Dom                          */
/*                                                                   */
/*! This routine set the type of the domain in the given dimension
    if it is equally spaced (DCT_CARTESIAN), or un-equally spaced
    (DCT_RECTANGULAR).

   \param [in, out] mod  DCT_Model model to be setup in this routine.
   \param [in]      dim  Set the dimension of the domain, must be
                         2, 3 or 4.
   \param [in]      dir  Direction in one of the dimension to set
                         the domain.
   \param [in]     type  DCT_Domain_Type value to indicate if it is
                         structured, unstructured or general
                         curvilinear.
   \param [in]      val  Values of the labels, if type is
                         DCT_RECTILINEAR; the initial and  final
                         value if type is DCT_CARTESIAN. In case of
                         DCT_GENERAL_CURV type the
                         DCT_Set_Model_GenCur_Dom function should be
                         used instead.
   \param [in]     npts  Array with the number of points along dir.

   \return  A DCT_Error error handler variable.
*/
/*********************************************************************/
DCT_Error DCT_Set_Model_Dom( DCT_Model *mod, const DCT_Integer dim,
                             const DCT_Integer dir, const DCT_Domain_Type type,
                             const DCT_Scalar val[], const DCT_Integer npts )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error             ierr;
  DCT_Model_Dom        *tmod;
  DCT_Scalar           *Values;
  register int          i, ind;

/* ------------------------------------  BEGIN( DCT_Set_Model_Dom ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  /* Or when the broker is calling the function during the registration (Value 2) */
  if ( (DCT_Step_Check != DCT_BEGIN_STATE) && (DCT_Step_Check != DCT_BROKER_STATE) ) {
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)100);
    sprintf(ierr.Error_msg,
       "[DCT_Set_Model_Dom] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }
  if ( DCT_Step_Check == DCT_BEGIN_STATE ) { /** Checking if it is during user declaration **/
     /*  Check the model was created */
      if (mod->ModTag!=DCT_TAG_UNDEF) {
       ierr.Error_code   = DCT_INVALID_ARGUMENT;
       ierr.Error_msg    = (DCT_String) malloc((size_t)115);
       sprintf(ierr.Error_msg,
          "[DCT_Set_Model_Dom] Invalid Model passed as 1st argument. Create DCT_Model first by calling DCT_Create_Model \n");
       return(ierr);
     }
     /* Check if the model can be modified, before to call DCT_Create_Couple function */
     if (mod->ModCommit == DCT_TRUE){
       ierr.Error_code  = DCT_INVALID_OPERATION;
       ierr.Error_msg   = (DCT_String) malloc((size_t)80);
       sprintf(ierr.Error_msg,
          "[DCT_Set_Model_Dom] Invalid Operation. The model %s is already commited\n",
           mod->ModName);
       return(ierr);
     }

     if ( (dim < MIN_DOMAIN_DIM) || (dim > MAX_DOMAIN_DIM) ){
       ierr.Error_code   = DCT_INVALID_ARGUMENT;
       ierr.Error_msg    = (DCT_String) malloc((size_t)110);
       sprintf(ierr.Error_msg,
          "[DCT_Set_Model_Dom] Domain Dimension must be 2, 3, or 4 for Model <%s>\n",
          mod->ModName);
       return(ierr);
     }

     if ( (type <= DCT_UNKNOWN_TYPE) || (type > DCT_DEFINED_DOMTYPE) ){
       ierr.Error_code   = DCT_INVALID_ARGUMENT;
       ierr.Error_msg    = (DCT_String) malloc((size_t)110);
       sprintf(ierr.Error_msg,
               "[DCT_Set_Model_Dom] Domain type is set incorrectly in Model <%s>\n",
               mod->ModName);
       return(ierr);
     } else if (type == DCT_GENERAL_CURV ) {
       ierr.Error_code   = DCT_INVALID_ARGUMENT;
       ierr.Error_msg    = (DCT_String) malloc((size_t)250);
       sprintf(ierr.Error_msg,
               "[DCT_Set_Model_Dom] Domain type direction set as DCT_GENERAL_CURV in Model <%s>.\n Function DCT_Set_Model_GenCur_Dom must be called instead.\n",
               mod->ModName);
       return(ierr);
     }

     if ( npts <= 0 ) {
       ierr.Error_code   = DCT_INVALID_ARGUMENT;
       ierr.Error_msg    = (DCT_String) malloc((size_t)110);
       sprintf(ierr.Error_msg,
          "[DCT_Set_Model_Dom] Number of points must be greater than zero for Model <%s>\n",
           mod->ModName);
       return(ierr);
     }
  }

  /* The index corresponding to the dimension is set */
  ind = dir-1;

  /* The domain was set before */
  if (mod->ModDomain != (DCT_Model_Dom *)NULL) {
    tmod = mod->ModDomain;
    if ( DCT_Step_Check == DCT_BEGIN_STATE ) { /** Checking if it is during user declaration **/
       if (tmod->ModDomDim != dim) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)110);
         sprintf(ierr.Error_msg,
           "[DCT_Set_Model_Dom] Domain dimension passed as %d does not agree with that set in Model <%s>\n",
           dir, mod->ModName);
         return(ierr);
       }
       /* Check if the same direction is not going to be again */
       if ( (tmod->ModValues[ind] != (DCT_Scalar *) NULL) ||
         (tmod->ModNumPts[ind] > 0) ) {
         ierr.Error_code   = DCT_INVALID_LABELING;
         ierr.Error_msg    = (DCT_String) malloc((size_t)110);
         sprintf(ierr.Error_msg,
           "[DCT_Set_Model_Dom] Direction %d is already set to the Domain in Model <%s>\n",
               dir, mod->ModName);
         return(ierr);
       }
    }

  } else {
    /* Allocate for Model Domain structure */
    tmod = (DCT_Model_Dom *)malloc(sizeof(DCT_Model_Dom));
    DCTERROR( tmod == (DCT_Model_Dom *) NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr));

    tmod->ModSubDom = (DCT_Data_SubDom *)NULL;
    tmod->ModDomDim = dim;

    /* Parallel Layout structure initialized */
    tmod->ModSubDomParLayout = DCT_DIST_NULL;
    tmod->ModSubDomLayoutDim = (DCT_Integer *)NULL;

    /* Model Domain structure is set */
    tmod->ModDomType  = (DCT_Domain_Type *) malloc(sizeof(DCT_Boolean)*(size_t)dim);
    DCTERROR( tmod->ModDomType == (DCT_Domain_Type *) NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr));

    tmod->ModNumPts  = (DCT_Integer *)malloc(sizeof(DCT_Integer)*(size_t)dim);
    DCTERROR( tmod->ModNumPts == (DCT_Integer *) NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr));

    tmod->ModValues  = (DCT_Scalar **)malloc(sizeof(DCT_Scalar *)*(size_t)dim);
    DCTERROR( tmod->ModValues == (DCT_Scalar **) NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr));

    /* Initialization of the pointers */
    for (i=0; i < dim; i++ ) {
      tmod->ModValues[i] = (DCT_Scalar *) NULL;
      tmod->ModNumPts[i] = 0;
      tmod->ModDomType[i] = DCT_UNKNOWN_TYPE;
    }

  }

  /* Filling up the values of the domain structure */
  if (type == DCT_RECTILINEAR) {
    tmod->ModDomType[ind] = type;
    tmod->ModNumPts[ind] = npts;

    Values = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)npts);
    DCTERROR( Values == (DCT_Scalar *) NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr));
    for (i=0; i < npts; i++ )
      Values[i] = (DCT_Scalar) val[i];

    tmod->ModValues[ind] = Values;
  } else if (type == DCT_CARTESIAN) {
    tmod->ModDomType[ind] = type;
    tmod->ModNumPts[ind] = npts;

    Values = (DCT_Scalar *)malloc(sizeof(DCT_Scalar)*(size_t)3);
    DCTERROR( Values == (DCT_Scalar *) NULL, "Memory Allocation Failed",
              ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr));
    Values[0] = (DCT_Scalar) val[0];
    Values[1] = (DCT_Scalar) val[1];
    Values[2] = (Values[1] - Values[0])/(DCT_Scalar)(npts-1);

    tmod->ModValues[ind] = Values;
  }

  mod->ModDomain = tmod;
  ierr.Error_code       = DCT_SUCCESS;
  ierr.Error_msg        = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* --------------------------------------  END( DCT_Set_Model_Dom ) */

}

/*********************************************************************/
/*                    DCT_Set_Model_GenCur_Dom                       */
/*                                                                   */
/*!  This routine when called by the user to set the model domain
     when is a general curvilinear and the actual coordinate of all
     the points are needed.

     \attention  To safe memory the structure
     point to the used array of points. So, changes made in the user
     arrays affect the Model representtation.

     \param [in, out] mod  DCT_Model model to be setup in this routine.
     \param [in]      dim  Set the dimension of the domain, must
                           be 2 or 3.
     \param [in]  x, y, z  Arrays of values of the corresponding coordinates for
                           point. The length is the product of npts values in the
                           dim positions
     \param [in]     npts  Array length dim with the number of points along each
                           direction

     \return  A DCT_Error error handler variable.
*/
/*********************************************************************/

DCT_Error DCT_Set_Model_GenCur_Dom( DCT_Model *mod, const DCT_Integer dim,
                                    const DCT_Scalar *x, const DCT_Scalar *y,
                                    const DCT_Scalar *z, const DCT_Integer npts[] )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error             ierr;
  DCT_Scalar           *l1;
  DCT_Scalar           *l2;
  DCT_Scalar           *l3;
  DCT_Model_Dom        *tmod;
  int                   prod;
  register int          i, ind;

/* ----------------------------  BEGIN( DCT_Set_Model_GenCur_Dom ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  /* Or when the broker is calling the function during the registration (Value 2) */
  if ( (DCT_Step_Check != DCT_BEGIN_STATE) && (DCT_Step_Check != DCT_BROKER_STATE) ) {
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)100);
    sprintf(ierr.Error_msg,
      "[DCT_Set_Model_GenCur_Dom] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }
  if ( DCT_Step_Check == DCT_BEGIN_STATE ) { /** Checking if it is during user declaration **/
     /*  Check the model was created */
      if (mod->ModTag!=DCT_TAG_UNDEF) {
       ierr.Error_code   = DCT_INVALID_ARGUMENT;
       ierr.Error_msg    = (DCT_String) malloc((size_t)115);
       sprintf(ierr.Error_msg,
         "[DCT_Set_Model_GenCur_Dom] Invalid Model passed as 1st argument. Create DCT_Model first by calling DCT_Create_Model \n");
       return(ierr);
     }
     /* Check if the model can be modified, before to call DCT_Create_Couple function */
     if (mod->ModCommit == DCT_TRUE){
       ierr.Error_code  = DCT_INVALID_OPERATION;
       ierr.Error_msg   = (DCT_String) malloc((size_t)80);
       sprintf(ierr.Error_msg,
          "[DCT_Set_Model_GenCur_Dom] Invalid Operation. The model %s is already commited\n",
           mod->ModName);
       return(ierr);
     }

     if ( (dim < MIN_DOMAIN_DIM) || ( dim > (MAX_DOMAIN_DIM-1) ) ){
       ierr.Error_code   = DCT_INVALID_ARGUMENT;
       ierr.Error_msg    = (DCT_String) malloc((size_t)110);
       sprintf(ierr.Error_msg,
           "[DCT_Set_Model_GenCur_Dom] Domain Dimension must be 2, 3 for Model <%s>\n",
          mod->ModName);
       return(ierr);
     }

     /*** Check if number of points in each direction is ok ***/
     for(i=0, ind=0; i < dim; i++) ind += (*(npts+i) <= 0 ? 0 : 1 );
     if ( ind != dim ) {
       ierr.Error_code   = DCT_INVALID_ARGUMENT;
       ierr.Error_msg    = (DCT_String) malloc((size_t)110);
       sprintf(ierr.Error_msg,
           "[DCT_Set_Model_GenCur_Dom] Number of points must be greater than zero for Model <%s>\n",
           mod->ModName);
       return(ierr);
     }

     if (mod->ModDomain != (DCT_Model_Dom *)NULL) {
       ierr.Error_code   = DCT_INVALID_ARGUMENT;
       ierr.Error_msg    = (DCT_String) malloc((size_t)110);
       sprintf(ierr.Error_msg,
         "[DCT_Set_Model_GenCur_Dom] Domain has been set, it must be empty to set General Curvilinear domain, in Model <%s>\n",
             mod->ModName);
       return(ierr);
     }
  }

  /* Allocate for Model Domain structure */
  tmod  = (DCT_Model_Dom *)malloc(sizeof(DCT_Model_Dom));
  DCTERROR( tmod == (DCT_Model_Dom *) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );

  tmod->ModSubDom = (DCT_Data_SubDom *)NULL;
  tmod->ModDomDim = dim;

  /* Parallel Layout structure initialized */
  tmod->ModSubDomParLayout = DCT_DIST_NULL;
  tmod->ModSubDomLayoutDim = (DCT_Integer *)NULL;

  /* Filling up the values of the domain structure */
  tmod->ModDomType  = (DCT_Domain_Type *) malloc(sizeof(DCT_Boolean)*(size_t)dim);
  DCTERROR( tmod->ModDomType == (DCT_Domain_Type *) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr));
  /* set in all direction as general curvilinear domain */
  for(i=0, ind=0; i < dim; i++) tmod->ModDomType[i]= DCT_GENERAL_CURV;

  /* set the number of points */
  tmod->ModNumPts  = (DCT_Integer *)malloc(sizeof(DCT_Integer)*(size_t)dim);
  DCTERROR( tmod->ModNumPts == (DCT_Integer *) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr));
  prod = 1;
  for (i=0; i < dim; i++ ) {
     tmod->ModNumPts[i] = npts[i];
     prod *= npts[i];
  }

  /* Set a pointer for each dimension coordinate */
  tmod->ModValues  = (DCT_Scalar **)malloc(sizeof(DCT_Scalar *)*(size_t)dim);
  DCTERROR( tmod->ModValues == (DCT_Scalar **) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr));

  /* Plug the array with the coordinates */
  switch(dim)
  {
    case 3:
      l3 = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)prod );
      DCTERROR( l3 == (DCT_Scalar *)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr));

      for(i=0; i < prod; i++) l3[i] = (DCT_Scalar) z[i];
      tmod->ModValues[2] = l3;

    case 2:
      l2 = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)prod );
      DCTERROR( l2 == (DCT_Scalar *)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr));
  /* ---------------------------- Dim 1 -------------------------- */
      l1 = (DCT_Scalar *) malloc(sizeof(DCT_Scalar)*(size_t)prod );
      DCTERROR( l1 == (DCT_Scalar *)NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr));
      for(i=0; i < prod; i++) {
         l2[i] = (DCT_Scalar) y[i];
         l1[i] = (DCT_Scalar) x[i];
      }

      tmod->ModValues[1] = l2;
      tmod->ModValues[0] = l1;
  }

  mod->ModDomain = tmod;

  ierr.Error_code       = DCT_SUCCESS;
  ierr.Error_msg        = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* ------------------------------  END( DCT_Set_Model_GenCur_Dom ) */

}

/**********************************************************************/
/*                  DCT_Set_Model_ParLayout                           */
/*                                                                    */
/*! Sets the Processor Layout associated with a DCT_Model and assign
    the processors ranks.

    \param [in,out] mod  Is DCT_Model modiable to be setup in
                         this routine.
    \param [in]    dist  Valid distribution of processors related to
                         DCT_distribution defined in the file dct.h.
    \param [in] laydims  Array with the amount of procs in each
                         dimension of the layout.

    \deprecated The following parameter is not used anymore.
    
    \param [in]   ranks  Pointer to array with the processors ranks
                         arranged accordingly with the values inlaydim
                         description row-wise.

   \return  A DCT_Error error handler variable.

   \todo In this function, probably the last parameter (ranks) might be
         removed, because is not used.
*/
/**********************************************************************/
DCT_Error DCT_Set_Model_ParLayout( DCT_Model *mod, const DCT_Distribution dist,
                                   const DCT_Integer *laydims)
                                   /* , const DCT_Rank *ranks)*/
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error             ierr;
  DCT_Integer           totalp;

/* ------------------------------  BEGIN( DCT_Set_Model_ParLayout ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  /* Or when the broker is calling the function during the registration (Value 2) */
  if ( (DCT_Step_Check != DCT_BEGIN_STATE) && (DCT_Step_Check != DCT_BROKER_STATE) ) {
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)100);
    sprintf(ierr.Error_msg,
       "[DCT_Set_Model_Dist] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }

  if ( DCT_Step_Check == DCT_BEGIN_STATE ) { /** Checking if it is during user declaration **/
     /*  Check the model was created */
     if (mod->ModTag!=DCT_TAG_UNDEF) {
       ierr.Error_code   = DCT_INVALID_ARGUMENT;
       ierr.Error_msg    = (DCT_String) malloc((size_t)115);
       sprintf(ierr.Error_msg,
         "[DCT_Set_Model_Dist] Invalid Model passed as 1st argument. Create DCT_Model first by calling DCT_Create_Model\n");
       return(ierr);
     }
     /* Check if the model can be modified, before to call DCT_Create_Couple function */
     if (mod->ModCommit == DCT_TRUE){
       ierr.Error_code  = DCT_INVALID_OPERATION;
       ierr.Error_msg   = (DCT_String) malloc((size_t)80);
       sprintf(ierr.Error_msg,
          "[DCT_Set_Model_Dist] Invalid Operation. The model %s is already commited\n",
           mod->ModName);
       return(ierr);
     }
     if ((dist <= DCT_DIST_NULL) || (dist >  DCT_MAX_DISTRIBUTION_TYPES)) {
       ierr.Error_code    = DCT_INVALID_ARGUMENT;
       ierr.Error_msg     = (DCT_String) malloc((size_t)100);
       sprintf(ierr.Error_msg,
       "[DCT_Set_Model_Dist] The distribution type entered for Model <%s> is not valid\n",
        mod->ModName);
       return(ierr);
     }

     if (mod->ModDomain == (DCT_Model_Dom *)NULL) {
       ierr.Error_code   = DCT_INVALID_ARGUMENT;
       ierr.Error_msg    = (DCT_String) malloc((size_t)115);
       sprintf(ierr.Error_msg,
          "[DCT_Set_Model_Dist] The Domain must be set in model <%s> before to define Subdomains\n",
               mod->ModName);
       return(ierr);
     }
  }

  /* Set the parallel distribution type */
  mod->ModDomain->ModSubDomParLayout = dist;
  if (!Set_Distribution( dist, laydims, &mod->ModDomain->ModSubDomLayoutDim, &totalp)) {
    ierr.Error_code    = DCT_INVALID_DISTRIBUTION;
    ierr.Error_msg     = (DCT_String) malloc((size_t)100);
    sprintf(ierr.Error_msg,
      "[DCT_Set_Model_Dist] the distribution does not agree with the process distribution entered\n");
    return(ierr);
  }
  /* Check consistency between initial creation and parallel setup */
  if (totalp != mod->ModNumProcs) {
    ierr.Error_code    = DCT_INVALID_ARGUMENT;
    ierr.Error_msg     = (DCT_String) malloc((size_t)120);
    sprintf(ierr.Error_msg,
      "[DCT_Set_Model_Dist] the distribution does not agree with the number of processes set when DCT_Create_Model was called\n");
    return(ierr);
  }

  ierr.Error_code    = DCT_SUCCESS;
  ierr.Error_msg     = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* --------------------------------  END( DCT_Set_Model_ParLayout ) */

}

/**********************************************************************/
/*                      DCT_Set_Model_SubDom                          */
/*                                                                    */
/*!   Sets the Subdomains and the processors assigned to DCT_Model

     \param [in, out] mod  DCT_Model model to be setup in this routine.
     \param [in]    procs  Array with dimension agreed with
                           DCT_distribution and the lenght in each one
                           agrees with the entries in the variable
                           ModDistProcCounts.
     \param [in]   inipos  Array indicating the start index in each
                           dimension for each subdomain (same quantity
                           as procs times dims).
     \param [in]   endpos: Array indicating the ending index in each
                           dimension for each subdomain (same quantity
                           as procs times dims).

     \return  A DCT_Error error handler variable.

        \todo Figure out when the user want an automatic
              generation (split) of subdomains (maybe as a util function).

        \todo Develop a function that set the parallel layout and
              subdomain definition in one call.

        \todo Verify when a 3D domain is used but a 2d rectangular
              parallel lay out is chosen.

*/
/**********************************************************************/
DCT_Error DCT_Set_Model_SubDom( DCT_Model *mod, const DCT_Rank *procs,
                                const DCT_Integer *inipos, const DCT_Integer *endpos )
{
/* ---------------------------------------------  Local Variables  */
   DCT_Error             ierr;

   DCT_Model_Dom        *tmod = (DCT_Model_Dom *)NULL;
   DCT_Data_SubDom      *SubDom;
   DCT_Integer          *offsini, *offsend;

   int                   ii, dimtotal;
   DCT_Integer           dim;
   DCT_String            Msg;

/* --------------------------------  BEGIN( DCT_Set_Model_SubDom ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  /* Or when the broker is calling the function during the registration (Value 2) */
  if ( (DCT_Step_Check != DCT_BEGIN_STATE) && (DCT_Step_Check != DCT_BROKER_STATE) ) {
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)100);
    sprintf(ierr.Error_msg,
       "[DCT_Set_Model_SubDom] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }

  /* Extract the model's domain data structure  */
  tmod = mod->ModDomain;

   if ( DCT_Step_Check == DCT_BEGIN_STATE ) { /** Checking if it is during user declaration **/
      /*  Check the model was created */
      if (mod->ModTag!=DCT_TAG_UNDEF) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)115);
         sprintf(ierr.Error_msg,
         "[DCT_Set_Model_SubDom] Invalid Model passed as 1st argument. Create DCT_Model first by calling DCT_Create_Model\n");
         return(ierr);
      }
   /* Check if the model can be modified, before to call DCT_Create_Couple function */
      if (mod->ModCommit == DCT_TRUE){
         ierr.Error_code  = DCT_INVALID_OPERATION;
         ierr.Error_msg   = (DCT_String) malloc((size_t)80);
         sprintf(ierr.Error_msg,
          "[DCT_Set_Model_SubDom] Invalid Operation. The model %s is already commited\n",
           mod->ModName);
         return(ierr);
      }

      if (tmod == (DCT_Model_Dom *)NULL) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)115);
         sprintf(ierr.Error_msg,
          "[DCT_Set_Model_SubDom] The Domain must be set in model <%s> before to define Subdomains\n",
               mod->ModName);
         return(ierr);
      }

      if ( ( tmod->ModSubDomParLayout == DCT_DIST_NULL ) ||
       ( tmod->ModSubDomLayoutDim == (DCT_Integer *)NULL ) ) {
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)115);
         sprintf(ierr.Error_msg,
         "[DCT_Set_Model_SubDom] The distribution layout must be set in model <%s> before to define Subdomains\n",
               mod->ModName);
         return(ierr);
      }
   }

   dim = tmod->ModDomDim;
   /************************** Probably delete this part *****************************/
   /* This is not necessary as long as was checked when the parallel layout was set
   switch(tmod->ModSubDomParLayout)
   {
      case DCT_DIST_SEQUENTIAL:
         dimtotal = 1;
         break;
      case DCT_DIST_RECTANGULAR:
         dimtotal = *(tmod->ModSubDomLayoutDim) * *(tmod->ModSubDomLayoutDim+1);
         break;
      case DCT_DIST_3D_RECTANGULAR:
         dimtotal = *(tmod->ModSubDomLayoutDim) * *(tmod->ModSubDomLayoutDim+1) *
                    *(tmod->ModSubDomLayoutDim+2);
         break;
      default:
         ierr.Error_code   = DCT_INVALID_ARGUMENT;
         ierr.Error_msg    = (DCT_String) malloc((size_t)115);
         sprintf(ierr.Error_msg,
                 "[DCT_Set_Model_SubDom] Unknown distribution layaout in model <%s>\n",
                 mod->ModName);
         return(ierr);
   } */
   /* Getting the total amount of processes */
   dimtotal = mod->ModNumProcs;
   /* Memory allocation for the SubDomain structure DCT_Data_SubDom */
   SubDom = (DCT_Data_SubDom *) malloc(sizeof(DCT_Data_SubDom)*(size_t)dimtotal);
   DCTERROR( SubDom == (DCT_Data_SubDom *) NULL, "Memory Allocation Failed",
            ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr));
   for ( ii=0; ii < dimtotal; ii++ ){
      /* The memory is allocated for each subdomain representation */
      (SubDom+ii)->IniPos = (DCT_Integer *) malloc(sizeof(DCT_Integer)*(size_t)dim);
      DCTERROR( (SubDom+ii)->IniPos == (DCT_Integer *)NULL,
              "Memory Allocation Failed", ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
      (SubDom+ii)->EndPos = (DCT_Integer *) malloc(sizeof(DCT_Integer)*(size_t)dim);
      DCTERROR( (SubDom+ii)->EndPos == (DCT_Integer *) NULL,
              "Memory Allocation Failed",ierr.Error_code  =  DCT_FAILED_MALLOC; return(ierr) );
   }


   offsini = (DCT_Integer *)inipos;
   offsend = (DCT_Integer *)endpos;
   switch ( dim ) {
      case 4:
         for ( ii=0; ii < dimtotal; ii++ ){
            (SubDom+ii)->IniPos[0] = (DCT_Integer) *(offsini++);
            (SubDom+ii)->IniPos[1] = (DCT_Integer) *(offsini++);
            (SubDom+ii)->IniPos[2] = (DCT_Integer) *(offsini++);
            (SubDom+ii)->IniPos[3] = (DCT_Integer) *(offsini++);
            (SubDom+ii)->EndPos[0] = (DCT_Integer) *(offsend++);
            (SubDom+ii)->EndPos[1] = (DCT_Integer) *(offsend++);
            (SubDom+ii)->EndPos[2] = (DCT_Integer) *(offsend++);
            (SubDom+ii)->EndPos[3] = (DCT_Integer) *(offsend++);

            (SubDom+ii)->RankProc = *(procs+ii);
         }
      break;
      case 3:
         for ( ii=0; ii < dimtotal; ii++ ){
            (SubDom+ii)->IniPos[0] = (DCT_Integer) *(offsini++);
            (SubDom+ii)->IniPos[1] = (DCT_Integer) *(offsini++);
            (SubDom+ii)->IniPos[2] = (DCT_Integer) *(offsini++);
            (SubDom+ii)->EndPos[0] = (DCT_Integer) *(offsend++);
            (SubDom+ii)->EndPos[1] = (DCT_Integer) *(offsend++);
            (SubDom+ii)->EndPos[2] = (DCT_Integer) *(offsend++);

            (SubDom+ii)->RankProc = *(procs+ii);
         }
      break;
      case 2:
         for ( ii=0; ii < dimtotal; ii++ ){
            (SubDom+ii)->IniPos[0] = (DCT_Integer) *(offsini++);
            (SubDom+ii)->IniPos[1] = (DCT_Integer) *(offsini++);
            (SubDom+ii)->EndPos[0] = (DCT_Integer) *(offsend++);
            (SubDom+ii)->EndPos[1] = (DCT_Integer) *(offsend++);

            (SubDom+ii)->RankProc = *(procs+ii);
         }
   } /* End of switch ( dim ) */
   tmod->ModSubDom = SubDom;

   if ( DCT_Step_Check == DCT_BEGIN_STATE ) /* The order is only during DCT_BEGIN_STATE */
      DCT_SubDom_ListOrder( SubDom, dimtotal, dim );

   /* Check if the subdomains intersect each other */
   /* Be carefull, define how handle them */
   if ( ( DCT_Check_Intersec( dimtotal, dim, tmod->ModSubDom) ) &&
       ( DCT_Step_Check == DCT_BEGIN_STATE ) ){
    Msg = (DCT_String) malloc((size_t)115);
    sprintf(Msg,
      "[DCT_Set_Model_SubDom] Two or more subdomains intersect among them, in model <%s>\n",
       mod->ModName);
    DCTWARN( Msg );
   }

   ierr.Error_code    = DCT_SUCCESS;
   ierr.Error_msg     = (DCT_String) DCT_NULL_STRING;
   return(ierr);

/* ----------------------------------  END( DCT_Set_Model_SubDom ) */

}

/********************************************************************/
/*                  DCT_Set_Model_Production                        */
/*                                                                  */
/*!    Sets the variables to be Produced as well as their
       frequencies and the initial time of Production.

    \param [in,out]    mod  Is DCT_Model modiable to be setup in
                            this routine.
    \param [in]       pvar  Pointer to the variable to be added
                            to the model.
    \param [in]        var  Name of the variable to be added to
                            the model.
    \param [in]   var_type  Type of the DCT_Vars to be added. It
                            must be one among DCT_Field, DCT_3d_Var
                            or DCT_4d_Var.
    \param [in]          u  Units of the DCT_Vars (i.e., CELSIUS,
                            Km/h, etc).
    \param [in] time_units  Units of time must be belongs to
                            DCT_Time_Types.
    \param [in]       freq  Frequency of Production.
    \param [in]   time_ini  Initial time when the variable must be
                            Produced

    \return  A DCT_Error error handler variable.

*/
/******************************************************************/
DCT_Error DCT_Set_Model_Production( DCT_Model *mod, DCT_VoidPointer pvar,
                                    const DCT_Name var, const DCT_Object_Types var_type,
                                    const DCT_Units u, const DCT_Time_Types time_units,
                                    const DCT_Scalar freq, const DCT_Scalar time_ini)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Field         *field;
  DCT_3d_Var        *var3d;
  DCT_4d_Var        *var4d;

  DCT_Error             ierr;

/* ----------------------------  BEGIN( DCT_Set_Model_Production ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  if (DCT_Step_Check != DCT_BEGIN_STATE){
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)100);
    sprintf(ierr.Error_msg,
     "[DCT_Set_Model_Production] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }
  /*  Check the model was created */
      if (mod->ModTag!=DCT_TAG_UNDEF) {
    ierr.Error_code   = DCT_INVALID_ARGUMENT;
    ierr.Error_msg    = (DCT_String) malloc((size_t)115);
    sprintf(ierr.Error_msg,
      "[DCT_Set_Model_Production] Invalid Model passed as 1st argument. Create DCT_Model first by calling DCT_Create_Model\n");
    return(ierr);
  }
  /* Check if the model can be modified, before to call DCT_Create_Couple function */
  if (mod->ModCommit == DCT_TRUE){
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)80);
    sprintf(ierr.Error_msg,
       "[DCT_Set_Model_Production] Invalid Operation. The model %s is already commited\n",
        mod->ModName);
    return(ierr);
  }
  if ((var_type<=DCT_TYPE_UNKNOWN) || (var_type> DCT_DEFINED_VAR_OBJECT)) {
    ierr.Error_code     = DCT_INVALID_ARGUMENT;
    ierr.Error_msg      = (DCT_String) malloc((size_t)90);
    sprintf(ierr.Error_msg,
       "[DCT_Set_Model_Production] Invalid variable type for Model <%s>\n",
        mod->ModName);
    return(ierr);
  }
  if (freq<= 0.0) {
    ierr.Error_code     = DCT_INVALID_ARGUMENT;
    ierr.Error_msg      = (DCT_String) malloc((size_t)90);
    sprintf(ierr.Error_msg,
       "[DCT_Set_Model_Production] Invalid frequency for Model <%s> it must be > 0.0\n",
        mod->ModName);
    return(ierr);
  }
  if (time_ini< 0.0) {
    ierr.Error_code     = DCT_INVALID_ARGUMENT;
    ierr.Error_msg      = (DCT_String) malloc((size_t)90);
    sprintf(ierr.Error_msg,
       "[DCT_Set_Model_Production] Invalid initial time for Model <%s> it must be >= 0.0\n",
        mod->ModName);
    return(ierr);
  }
  if ( (time_units <= DCT_TIME_UNKNOWN) || (time_units > DCT_DEFINED_TIME_UNITS) ) {
    ierr.Error_code  = DCT_INVALID_UNITS;
    ierr.Error_msg   = (DCT_String) malloc((size_t)80);
    sprintf(ierr.Error_msg,
      "[DCT_Set_Model_Production] invalid time unit type for mod <%s>\n",
        mod->ModName);
    return(ierr);
  }

  switch (var_type)
  {
    case DCT_FIELD_TYPE:
      field = (DCT_Field *)pvar;
      ierr = DCT_Create_Field( field, var,"Variable defined internally", u, DCT_PRODUCE);
      ierr = DCT_Set_Field_Time( field, time_units, time_ini);
      ierr = DCT_Set_Field_Freq_Production( field, freq);
      /* The variable is locked */
      field->VarCommit = DCT_TRUE;
      /* Set the pointer to indicate the model that belongs to */
      field->VarModel = mod;

      break;
    case DCT_3D_VAR_TYPE:
      var3d = (DCT_3d_Var *)pvar;
      ierr = DCT_Create_3d_Var( var3d, var,"Variable defined internally", u, DCT_PRODUCE);
      ierr = DCT_Set_3d_Var_Time( var3d, time_units, time_ini);
      ierr = DCT_Set_3d_Var_Freq_Production( var3d, freq);
      /* The variable is locked */
      var3d->VarCommit = DCT_TRUE;
      /* Set the pointer to indicate the model that belongs to */
      var3d->VarModel = mod;

      break;
    case DCT_4D_VAR_TYPE:
      var4d = (DCT_4d_Var *)pvar;
      ierr = DCT_Create_4d_Var( var4d, var,"Variable defined internally", u, DCT_PRODUCE);
      ierr = DCT_Set_4d_Var_Time( var4d, time_units, time_ini);
      ierr = DCT_Set_4d_Var_Freq_Production( var4d, freq);
      /* The variable is locked */
      var4d->VarCommit = DCT_TRUE;
      /* Set the pointer to indicate the model that belongs to */
      var4d->VarModel = mod;

      break;
    default:
      ierr.Error_code     = DCT_INVALID_ARGUMENT;
      ierr.Error_msg      = (DCT_String) malloc((size_t)90);
      sprintf(ierr.Error_msg,
       "[DCT_Set_Model_Production] Invalid variable type for Model <%s>\n",
                        mod->ModName);
      return(ierr);
  }

  mod->ModProduceVars = (DCT_List *) DCT_List_Add ( &mod->ModProduceVars, pvar, var_type);
  mod->ModTotVarProduced++;

  ierr.Error_code       = DCT_SUCCESS;
  ierr.Error_msg        = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* ------------------------------  END( DCT_Set_Model_Production ) */

}

/******************************************************************/
/*                DCT_Set_Model_Consumption                       */
/*                                                                */
/*!    Sets the variables to be consumed as well as their
       frequencies and the initial time of production

    \param [in,out]    mod  Is DCT_Model modiable to be setup in
                            this routine.
    \param [in]       pvar  Pointer to the variable to be added
                            to the model.
    \param [in]        var  Name of the variable to be added to
                            the model.
    \param [in]   var_type  Type of the DCT_Vars to be added. It
                            must be one among DCT_Field, DCT_3d_Var
                            or DCT_4d_Var.
    \param [in]          u  Units of the DCT_Vars (i.e., CELSIUS,
                            Km/h, etc).
    \param [in] time_units  Units of time must be belongs to
                            DCT_Time_Types.
    \param [in]       freq  Frequency of Consumption.
    \param [in]   time_ini  Initial time when the variable must be
                            consumed

    \return  A DCT_Error error handler variable.

*/
/******************************************************************/
DCT_Error DCT_Set_Model_Consumption( DCT_Model *mod, DCT_VoidPointer pvar,
                                     const DCT_Name var, const DCT_Object_Types var_type,
                                     const DCT_Units u, const DCT_Time_Types time_units,
                                     const DCT_Scalar freq, const DCT_Scalar time_ini)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error             ierr;

  DCT_Field         *field;
  DCT_3d_Var        *var3d;
  DCT_4d_Var        *var4d;


/* ---------------------------  BEGIN( DCT_Set_Model_Consumption ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  if (DCT_Step_Check != DCT_BEGIN_STATE){
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)100);
    sprintf(ierr.Error_msg,
     "[DCT_Set_Model_Consumption] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }
  /*  Check the model was created */
  if (mod->ModTag!=DCT_TAG_UNDEF) {
    ierr.Error_code   = DCT_INVALID_ARGUMENT;
    ierr.Error_msg    = (DCT_String) malloc((size_t)115);
    sprintf(ierr.Error_msg,
      "[DCT_Set_Model_Consumption] Invalid Model passed as 1st argument. Create DCT_Model first by calling DCT_Create_Model\n");
    return(ierr);
  }
  /* Check if the model can be modified, before to call DCT_Create_Couple function */
  if (mod->ModCommit == DCT_TRUE){
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)80);
    sprintf(ierr.Error_msg,
       "[DCT_Set_Model_Consumption] Invalid Operation. The model %s is already commited\n",
        mod->ModName);
    return(ierr);
  }
  if ((var_type<=DCT_TYPE_UNKNOWN) || (var_type> DCT_DEFINED_VAR_OBJECT)) {
    ierr.Error_code     = DCT_INVALID_ARGUMENT;
    ierr.Error_msg      = (DCT_String) malloc((size_t)90);
    sprintf(ierr.Error_msg,
       "[DCT_Set_Model_Consumption], Invalid variable type for Model <%s>\n",
        mod->ModName);
    return(ierr);
  }
  if (freq<= 0.0) {
    ierr.Error_code     = DCT_INVALID_ARGUMENT;
    ierr.Error_msg      = (DCT_String) malloc((size_t)90);
    sprintf(ierr.Error_msg,
       "[DCT_Set_Model_Consumption], Invalid frequency for Model <%s> it must be > 0.0\n",
       mod->ModName);
    return(ierr);
  }
  if (time_ini< 0.0) {
    ierr.Error_code     = DCT_INVALID_ARGUMENT;
    ierr.Error_msg      = (DCT_String) malloc((size_t)90);
    sprintf(ierr.Error_msg,
       "[DCT_Set_Model_Consumption], Invalid initial time for Model <%s> it must be >= 0.0\n",
        mod->ModName);
    return(ierr);
  }
  if ( (time_units <= DCT_TIME_UNKNOWN) || (time_units > DCT_DEFINED_TIME_UNITS) ) {
    ierr.Error_code  = DCT_INVALID_UNITS;
    ierr.Error_msg   = (DCT_String) malloc((size_t)80);
    sprintf(ierr.Error_msg,
      "[DCT_Set_Model_Consumption], invalid time unit type for mod <%s>\n",
        mod->ModName);
    return(ierr);
  }

  switch (var_type)
  {
    case DCT_FIELD_TYPE:
      field = (DCT_Field *)pvar;
      ierr = DCT_Create_Field( field, var,"Variable defined internally", u, DCT_CONSUME);
      ierr = DCT_Set_Field_Time( field, time_units, time_ini);
      ierr = DCT_Set_Field_Freq_Consumption( field, freq);
      /* The variable is locked */
      field->VarCommit = DCT_TRUE;
      /* Set the pointer to indicate the model that belongs to */
      field->VarModel = mod;

      break;
    case DCT_3D_VAR_TYPE:
      var3d = (DCT_3d_Var *)pvar;
      ierr = DCT_Create_3d_Var( var3d, var,"Variable defined internally",
                                u, DCT_CONSUME);
      ierr = DCT_Set_3d_Var_Time( var3d, time_units, time_ini);
      ierr = DCT_Set_3d_Var_Freq_Consumption( var3d, freq);
      /* The variable is locked */
      var3d->VarCommit = DCT_TRUE;
      /* Set the pointer to indicate the model that belongs to */
      var3d->VarModel = mod;

      break;
    case DCT_4D_VAR_TYPE:
      var4d = (DCT_4d_Var *)pvar;
      ierr = DCT_Create_4d_Var( var4d, var,"Variable defined internally",
                                u, DCT_CONSUME);
      ierr = DCT_Set_4d_Var_Time( var4d, time_units, time_ini);
      ierr = DCT_Set_4d_Var_Freq_Consumption( var4d, freq);
      /* The variable is locked */
      var4d->VarCommit = DCT_TRUE;
      /* Set the pointer to indicate the model that belongs to */
      var4d->VarModel = mod;

      break;
    default:
      ierr.Error_code     = DCT_INVALID_ARGUMENT;
      ierr.Error_msg      = (DCT_String) malloc((size_t)90);
      sprintf(ierr.Error_msg,
         "[DCT_Set_Model_Consumption], Invalid variable type for Model <%s>\n",
          mod->ModName);
      return(ierr);
  }

  mod->ModConsumeVars = (DCT_List *) DCT_List_Add ( &mod->ModConsumeVars, pvar, var_type);
  mod->ModTotVarConsumed++;

  ierr.Error_code       = DCT_SUCCESS;
  ierr.Error_msg        = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* -----------------------------  END( DCT_Set_Model_Consumption ) */

}

/******************************************************************/
/*                      DCT_Set_Model_Var                         */
/*                                                                */
/*!   Sets a DCT_Vars structure to be part of a DCT_Model in a
      way to be produced or consumed. A DCT_Vars should be added
      (set) to a determined DCT_Model, in addition, some checks
      for consistency are performed in this function.

     \param [in,out]   mod  Is DCT_Model modiable to be setup in
                            this routine.
     \param [in,out]   var  Pointer to the variable to be added
                            to the model.
     \param [in]  var_type  Type of the variable to be added. It
                            must be one among DCT_Field, DCT_3d_Var
                            or DCT_4d_Var.

    \return  A DCT_Error error handler variable.

*/
/******************************************************************/
DCT_Error DCT_Set_Model_Var( DCT_Model *mod, DCT_VoidPointer pvar,
                             const DCT_Object_Types var_type )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error          ierr;

  DCT_Field         *field= NULL;
  DCT_3d_Var        *var3d= NULL;
  DCT_4d_Var        *var4d= NULL;

  DCT_Domain_Type   *DomType;
  DCT_Model_Dom     *tmod;

  DCT_ProdCons       pvarpc;

/* -----------------------------------  BEGIN( DCT_Set_Model_Var ) */

  /* Check if the the function DCT_BeginRegistration() was called */
  if (DCT_Step_Check != DCT_BEGIN_STATE){
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)100);
    sprintf(ierr.Error_msg,
      "[DCT_Set_Model_Var] Invalid Operation. The function DCT_BeginRegistration() must be called fist.\n");
    return(ierr);
  }
  /*  Check the model was created */
  if (mod->ModTag!=DCT_TAG_UNDEF) {
    ierr.Error_code   = DCT_INVALID_ARGUMENT;
    ierr.Error_msg    = (DCT_String) malloc((size_t)115);
    sprintf(ierr.Error_msg,
      "[DCT_Set_Model_Var] Invalid Model passed as 1st argument. Create DCT_Model first by calling DCT_Create_Model\n");
    return(ierr);
  }
  /* Check if the model can be modified, before to call DCT_Create_Couple function */
  if (mod->ModCommit == DCT_TRUE){
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)80);
    sprintf(ierr.Error_msg,
      "[DCT_Set_Model_Var] Invalid Operation. The model %s is already commited\n",
        mod->ModName);
    return(ierr);
  }
  if ((var_type<=DCT_TYPE_UNKNOWN) || (var_type> DCT_DEFINED_VAR_OBJECT)) {
    ierr.Error_code     = DCT_INVALID_ARGUMENT;
    ierr.Error_msg      = (DCT_String) malloc((size_t)90);
    sprintf(ierr.Error_msg,
      "[DCT_Set_Model_Var], Invalid variable type for Model <%s>\n",
        mod->ModName);
    return(ierr);
  }

  tmod = mod->ModDomain;

  switch (var_type)
  {
    /**************     DCT_FIELD_TYPE     **************/
    case DCT_FIELD_TYPE:
      field = (DCT_Field *) pvar;
      if (field->VarFrequency == DCT_TIME_UNSET) {
        ierr.Error_code     = DCT_INVALID_ARGUMENT;
        ierr.Error_msg      = (DCT_String) malloc((size_t)90);
        sprintf(ierr.Error_msg,
          "[DCT_Set_Model_Var], Frequency for DCT_Field <%s> must be set\n",
                field->VarName );
        return(ierr);
      }
      if (field->VarTimeUnits == DCT_TIME_UNKNOWN) {
        ierr.Error_code  = DCT_INVALID_UNITS;
        ierr.Error_msg   = (DCT_String) malloc((size_t)80);
        sprintf(ierr.Error_msg,
                "[DCT_Set_Model_Var], The type unit for DCT_Field <%s> must be set\n",
                field->VarName);
        return(ierr);
      }
      DomType = field->VarDomType;
      if ( (DomType[0] == DCT_CARTESIAN) ||
           (DomType[1] == DCT_CARTESIAN) ) {
         if ( DCT_Model_And_Var_DomCheck( tmod, DomType, field->VarLabels,
                                                            field->VarDim, 2 ) ) {
            ierr.Error_code  = DCT_INVALID_UNITS;
            ierr.Error_msg   = (DCT_String) malloc((size_t)180);
            sprintf(ierr.Error_msg,
               "[DCT_Set_Model_Var], The Discretization of DCT_Field <%s> does not comply with the model domain\n",
               field->VarName);
            return(ierr);
         }
      }
      /* The variable is locked */
      field->VarCommit = DCT_TRUE;
      /* Set the pointer to indicate the model that belongs to */
      field->VarModel = mod;
      pvarpc = field->VarProduced;
      break;
    /**************     DCT_3D_VAR_TYPE     **************/
    case DCT_3D_VAR_TYPE:
      var3d = (DCT_3d_Var *) pvar;
      if (var3d->VarFrequency == DCT_TIME_UNSET) {
        ierr.Error_code     = DCT_INVALID_ARGUMENT;
        ierr.Error_msg      = (DCT_String) malloc((size_t)90);
        sprintf(ierr.Error_msg,
                "[DCT_Set_Model_Var], Frequency for DCT_3d_Var <%s> must be set\n",
                var3d->VarName );
                return(ierr);
      }
      if (var3d->VarTimeUnits == DCT_TIME_UNKNOWN) {
        ierr.Error_code  = DCT_INVALID_UNITS;
        ierr.Error_msg   = (DCT_String) malloc((size_t)80);
        sprintf(ierr.Error_msg,
                "[DCT_Set_Model_Var], The type unit for DCT_3d_Var <%s> must be set\n",
                var3d->VarName);
                return(ierr);
      }

      DomType = var3d->VarDomType;
      if ( (DomType[0] == DCT_CARTESIAN) ||
           (DomType[1] == DCT_CARTESIAN) ||
           (DomType[2] == DCT_CARTESIAN) ) {
         if ( DCT_Model_And_Var_DomCheck( tmod, DomType, var3d->VarLabels,
                                                            var3d->VarDim, 3 ) ) {
            ierr.Error_code  = DCT_INVALID_UNITS;
            ierr.Error_msg   = (DCT_String) malloc((size_t)180);
            sprintf(ierr.Error_msg,
               "[DCT_Set_Model_Var], The Discretization for DCT_Field <%s> does not comply with the model domain\n",
               field->VarName);
            return(ierr);
         }
      }
      /* The variable is locked */
      var3d->VarCommit = DCT_TRUE;
      /* Set the pointer to indicate the model that belongs to */
      var3d->VarModel = mod;

      pvarpc = var3d->VarProduced;
      break;
    /**************     DCT_4D_VAR_TYPE     **************/
    case DCT_4D_VAR_TYPE:
      var4d = (DCT_4d_Var *) pvar;
      if (var4d->VarFrequency == DCT_TIME_UNSET) {
        ierr.Error_code     = DCT_INVALID_ARGUMENT;
        ierr.Error_msg      = (DCT_String) malloc((size_t)90);
        sprintf(ierr.Error_msg,
                "[DCT_Set_Model_Var], Frequency for DCT_4d_Var <%s> must be set\n",
                var4d->VarName );
                return(ierr);
      }
      if (var4d->VarTimeUnits == DCT_TIME_UNKNOWN) {
        ierr.Error_code  = DCT_INVALID_UNITS;
        ierr.Error_msg   = (DCT_String) malloc((size_t)80);
        sprintf(ierr.Error_msg,
                "[DCT_Set_Model_Var], The type unit for DCT_4d_Var <%s> must be set\n",
                var4d->VarName);
                return(ierr);
      }

      DomType = var4d->VarDomType;
      if ( (DomType[0] == DCT_CARTESIAN) ||
           (DomType[1] == DCT_CARTESIAN) ||
           (DomType[2] == DCT_CARTESIAN) ||
           (DomType[3] == DCT_CARTESIAN) ) {
         if ( DCT_Model_And_Var_DomCheck( tmod, DomType, var4d->VarLabels,
                                                            var4d->VarDim, 4 ) ) {
            ierr.Error_code  = DCT_INVALID_UNITS;
            ierr.Error_msg   = (DCT_String) malloc((size_t)180);
            sprintf(ierr.Error_msg,
               "[DCT_Set_Model_Var], The Discretization for DCT_Field <%s> does not comply with the model domain\n",
               field->VarName);
            return(ierr);
         }
      }
      /* The variable is locked */
      var4d->VarCommit = DCT_TRUE;
      /* Set the pointer to indicate the model that belongs to */
      var4d->VarModel = mod;

      pvarpc = var4d->VarProduced;
      break;
    default:
      ierr.Error_code     = DCT_INVALID_ARGUMENT;
      ierr.Error_msg      = (DCT_String) malloc((size_t)90);
      sprintf(ierr.Error_msg,
              "[DCT_Set_Model_Var], Invalid variable type for Model <%s>\n",
          mod->ModName);
      return(ierr);
  }

  if (pvarpc==DCT_PRODUCE) {
    mod->ModProduceVars = (DCT_List *) DCT_List_Add ( &mod->ModProduceVars, pvar, var_type);
    mod->ModTotVarProduced++;
  } else {
    mod->ModConsumeVars = (DCT_List *) DCT_List_Add ( &mod->ModConsumeVars, pvar, var_type);
    mod->ModTotVarConsumed++;
  }

  ierr.Error_code       = DCT_SUCCESS;
  ierr.Error_msg        = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* -------------------------------------  END( DCT_Set_Model_Var ) */

}

/******************************************************************/
/*                    DCT_Update_Model_Time                       */
/*                                                                */
/* Updates the current time in the DCT_Model structure, using the */
/* initial time and the time step set before                      */
/*                                                                */
/*   mod:  DCT_Model model to be affected                         */
/******************************************************************/
DCT_Error DCT_Update_Model_Time( DCT_Model *mod )
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error             ierr;

/* -------------------------------  BEGIN( DCT_Update_Model_Time ) */

  if (mod->ModCurrentTime == DCT_TIME_UNSET)
      mod->ModCurrentTime = mod->ModTimeIni;
  else
      mod->ModCurrentTime += mod->ModTime;

  ierr.Error_code       = DCT_SUCCESS;
  ierr.Error_msg        = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* ---------------------------------  END( DCT_Update_Model_Time ) */

}


/******************************************************************/
/*                          DCT_Destroy_Model                     */
/*                                                                */
/******************************************************************/

DCT_Error DCT_Destroy_Model( DCT_Model *mod)
{
/* ---------------------------------------------  Local Variables  */
  DCT_Error        ierr;

  DCT_Model_Dom   *tmod;
  DCT_Data_SubDom *SubDom;

  register int     i, dim, dimtotal;

/* -----------------------------------  BEGIN( DCT_Destroy_Model ) */

  /* Check if the the function DCT_EndRegistration() was called */
  if (DCT_Step_Check < DCT_BROKER_STATE) {  /* All states before the broker starts to work */
    ierr.Error_code  = DCT_INVALID_OPERATION;
    ierr.Error_msg   = (DCT_String) malloc((size_t)100);
    sprintf(ierr.Error_msg,
        "[DCT_Destroy_Model] Invalid Operation. The function DCT_EndRegistration() must be called  before to destroy a Model.\n");
    return(ierr);
  }
  /* Check if the model can be modified, before to call DCT_Create_Couple function */
  if ( DCT_Step_Check == DCT_END_STATE ) {
     /*  Check the model was created before to be destroyed */
     if ((mod->ModTag<DCT_TAG_UNDEF) || (mod->ModTag>=DCT_MAX_MODEL) ) {
       ierr.Error_code   = DCT_INVALID_ARGUMENT;
       ierr.Error_msg    = (DCT_String) malloc((size_t)115);
       sprintf(ierr.Error_msg,
         "[DCT_Destroy_Model] Invalid Model passed as 1st argument. DCT_Model was not created by calling DCT_Create_Model\n");
       return(ierr);
     }
     if (mod->ModCommit == DCT_TRUE) {
       ierr.Error_code  = DCT_INVALID_OPERATION;
       ierr.Error_msg   = (DCT_String) malloc((size_t)80);
       sprintf(ierr.Error_msg,
          "[DCT_Destroy_Model] Invalid Operation. The model %s is already commited\n", mod->ModName);
       return(ierr);
     }
  }

  free(mod->ModName);
  mod->ModName = (DCT_Name)NULL;

  if (mod->ModDescription != (DCT_String)NULL) {
    free(mod->ModDescription);
    mod->ModDescription = (DCT_String)NULL;
  }

  mod->ModCommit = DCT_FALSE;

  mod->ModNumProcs      = 0;
  mod->ModLeadRank      = DCT_UNDEFINED_RANK;
  if (mod->ModGroupComm != DCT_COMM_NULL) DCT_Free_Comm ( &(mod->ModGroupComm) );

  mod->ModTimeUnits      = DCT_TIME_UNKNOWN;
  mod->ModTimeIni        = DCT_TIME_UNSET;
  mod->ModTime           = DCT_TIME_UNSET;
  mod->ModCurrentTime    = DCT_TIME_UNSET;


  tmod = mod->ModDomain;
  if (tmod != (DCT_Model_Dom *)NULL) {
    dim = tmod->ModDomDim;
    for (i=0; i < dim; i++) {
       free( tmod->ModValues[i] );
    }
    free( tmod->ModValues );
    free( tmod->ModDomType );
    free( tmod->ModNumPts );

    if (tmod->ModSubDomLayoutDim != (DCT_Integer *)NULL) {
       switch(tmod->ModSubDomParLayout)
       {
         case DCT_DIST_SEQUENTIAL:
            dimtotal = 1;
            break;
         case DCT_DIST_RECTANGULAR:
            dimtotal = *(tmod->ModSubDomLayoutDim) * *(tmod->ModSubDomLayoutDim+1);
            break;
         case DCT_DIST_3D_RECTANGULAR:
            dimtotal = *(tmod->ModSubDomLayoutDim) * *(tmod->ModSubDomLayoutDim+1) *
                       *(tmod->ModSubDomLayoutDim+2);
            break;
         default:
            ierr.Error_code   = DCT_INVALID_ARGUMENT;
            ierr.Error_msg    = (DCT_String) malloc((size_t)115);
            sprintf(ierr.Error_msg,
                    "[DCT_Destroy_Model] Unknown distribution layaout in model <%s>\n",
                    mod->ModName);
            return(ierr);
       }
       free(tmod->ModSubDomLayoutDim);
    }
    SubDom = tmod->ModSubDom;
    if(SubDom != (DCT_Data_SubDom *)NULL) {
       for (i=0; i < dimtotal; i++) {
          free( (SubDom+i)->IniPos );
          free( (SubDom+i)->EndPos );
       }
       free(SubDom);
    }

    free( tmod );
    mod->ModDomain = (DCT_Model_Dom *)NULL;
  }

  mod->ModTotVarProduced = 0;
  mod->ModTotVarProduced = 0;

  DCT_List_Clear (&mod->ModConsumeVars);
  DCT_List_Clear (&mod->ModProduceVars);

  mod->ModTag    = DCT_TAG_UNDEF;

  ierr.Error_code       = DCT_SUCCESS;
  ierr.Error_msg        = (DCT_String) DCT_NULL_STRING;
  return(ierr);

/* -------------------------------------  END( DCT_Destroy_Model ) */

}
