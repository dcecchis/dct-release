/**************************************************************/
/*              Distributed Coupling Toolkit (DCT)            */
/*!
   \file dct_mpi_comm.c
  
   \brief Contains the implementation of functions that use
          directly the MPI communication library.

    This file contains the implementation functions that handle
    the calls to the communication library. These functions interface
    the communication library; and should be, together with 
    \c dct_commdat.h file, the places to make changes when a different
    communication library would be used.

    \note In here, there are not the functions used during the
          the DCT Data Exchange Phase, for that see \c dct_comm_data.c.

    \attention The implementation is based on MPI.
    
    \todo Review DCT_Comm_SetBrokerRank(), because when the broker
          is designated using this, its rank corresponds
          a different communicator when the DCT_Comm is created.

    \todo Check the packing/unpacking operation with new
          implementations from most recent standar
          regarding performance improvement.

    \date Date of Creation: Sep 12, 2008: MPI version.  

    \author Dany De Cecchis: dcecchis@gmail.com
    \author Tony Drummond: LADrummond@lbl.gov

    \copyright GNU Public License.
 */
/**************************************************************/


/* -----------------------------------------------  Include Files  */
#include <mpi.h>
#include <string.h>
#include <math.h>
#include "dct.h"
/* --------------------------------------------  Global Variables  */
#include "dctsys.h"
//#include "macros.h"
#include "dct_comm.h"

/* --------------------------------------------  Variables  */
   /** This variables are used by the Broker to during
       different registration steps and by all processes
       during the exchange steps                          **/
   int               dct_glb_buffsize, dct_glb_prvsize;
   char             *dct_glb_sndbuff;
   char             *dct_glb_prvbuff;
   int              *dct_glb_indice;
   int              *dct_glb_lenbuff;
   char            **dct_glb_recvbuff;
   DCT_Request      *dct_glb_recreqst;

/* This routine declare a sychronization point */
int DCT_Sync( DCT_Comm comm )
{
/* ---------------------------------------------  Local Variables  */
  int err;

/* ----------------------------------------  BEGIN( DCT_Set_Comm ) */

  err = MPI_Barrier ( comm );

  return (err);

/* ------------------------------------------  END( DCT_Set_Comm ) */
}

int DCT_Check_CommIni(  )
{
/* ---------------------------------------------  Local Variables  */
  int flag, err;

/* -----------------------------------  BEGIN( DCT_Check_CommIni ) */

   err = MPI_Initialized( &flag );
   if ( !flag ) {
      fflush( NULL );
      fprintf( stderr, "DCT_ERROR[ %d ]: Communication library is not initialized",
               DCT_FAILED_LIB_COMM);
      return(1);
   }

  return (err);

/* -------------------------------------  END( DCT_Check_CommIni ) */
}

/******************************************************************/
/*                     DCT_Create_Comm                           */
/*                                                               */
/*!  This routing create the DCT Communicator among all the
     processes that will be envolved in the model coupling.
     If a previous function DCT_Assign_Broker was called,
     DCT_Create_Comm check that the same DCT_Comm was passed.
     If comm is not a valid DCT_Comm, MPI_COMM_WORLD is taken
     as reference to be duplicated.

 \param [in, out] comm  Is the communicator to be created.

 \return Integer value with 0 if success, otherwise broadcast
         the MPI error code.
*/
int DCT_Create_Comm( DCT_Comm comm )
{
/* ---------------------------------------------  Local Variables  */
  int  err;

/* -------------------------------------  BEGIN( DCT_Create_Comm ) */

   if ( DCT_Comm_World == DCT_COMM_NULL ) {
      /* In orden to translate the broker rank the previous comm is stored */
      if ( comm == DCT_COMM_NULL )
         DCT_User_Comm = Get_Global_Communicator();
      else
         DCT_User_Comm = comm;

      err = MPI_Comm_dup ( DCT_User_Comm, &DCT_Comm_World );
      err =  err | MPI_Comm_group( DCT_Comm_World, &DCT_Group_World );
   }
   else { /* DCT_Assign_Broker was called first and the same comm should be provided */
      DCTERROR( comm != DCT_Comm_World, "Different communicator was assigned before", return(1) );

   }

   return(err);

/* ---------------------------------------  END( DCT_Create_Comm ) */
}

int DCT_Comm_GetRank( )
{
/* ---------------------------------------------  Local Variables  */
  int err;

/* ------------------------------------  BEGIN( DCT_Comm_GetRank ) */


    err = MPI_Comm_size( DCT_Comm_World, &DCT_procinfo.ProcCommSize ) |
          MPI_Comm_rank( DCT_Comm_World, &DCT_procinfo.ProcDCTRank );

  return(err);

/* --------------------------------------  END( DCT_Comm_GetRank ) */
}

int DCT_Comm_SetBrokerRank ( DCT_Rank broker )
{
/* ---------------------------------------------  Local Variables  */
  
  DCT_Group       usrgrp;
  int             err=0;
  int             size;
  
/* ------------------------------  BEGIN( DCT_Comm_SetBrokerRank ) */
   if (DCT_procinfo.ProcDCTBroker == DCT_UNDEFINED_RANK){

     err = MPI_Comm_size( DCT_Comm_World, &size );
     if ( err ) return err;

     if ( ( broker >= 0 ) && ( broker < size ) ) {
           err = MPI_Comm_group( DCT_User_Comm, &usrgrp );
           if (err) return err;
           err = MPI_Group_translate_ranks( usrgrp, 1, &(broker),
                                            DCT_Group_World, &(DCT_procinfo.ProcDCTBroker) );
           if (err) return err;
     }
     else err = 1;
   }
   return(err);

/* ---------------------------------  END( DCT_Comm_SetBrokerRank ) */
}

int DCT_Free_Comm (  )
{
/* ---------------------------------------------  Local Variables  */
  int err;

/* ---------------------------------------  BEGIN( DCT_Free_Comm ) */

   err = MPI_Comm_free( &DCT_Comm_World );
   err =  err | MPI_Group_free( &DCT_Group_World );

   return(err);
/* -----------------------------------------  END( DCT_Free_Comm ) */
}

int DCT_Free_DType ( DCT_Comm_Datatype *DType, DCT_Integer Nmsg )
{
/* ---------------------------------------------  Local Variables  */
   int err=0;
   register int ii;

/* --------------------------------------  BEGIN( DCT_Free_DType ) */

   for ( ii=0; ii < Nmsg; ii++ )
      err = err | MPI_Type_free ( (DType + ii) );

   return(err);
/* ----------------------------------------  END( DCT_Free_DType ) */
}

int DCT_Free_Requests ( DCT_Request  *reqs, DCT_Integer Nmsg )
{
/* ---------------------------------------------  Local Variables  */
   int err=0;
   register int ii;

/* -----------------------------------  BEGIN( DCT_Free_Requests ) */

   for ( ii=0; ii < Nmsg; ii++ )
      err = err |  MPI_Request_free ( (reqs + ii) );

   return(err);
/* -------------------------------------  END( DCT_Free_Requests ) */
}

int DCT_Create_Model_Comm ( DCT_Model *mod )
{
/* ---------------------------------------------  Local Variables  */
   int err=0;
   
   DCT_Rank    *ranks;
   DCT_Group    modgrp;
   DCT_Rank     ldrrank;
   
   int          nprocs;

/* -------------------------------  BEGIN( DCT_Create_Model_Comm ) */

   nprocs = mod->ModNumProcs;
   err = DCT_Extract_SubDom_Ranks( mod->ModDomain, &ranks, nprocs );
   if (err) return err;
   
   err = MPI_Group_incl( DCT_Group_World, nprocs, ranks, &modgrp );
   if (err) return err;

   /** The model communicator is built **/
   err = MPI_Group_translate_ranks( DCT_Group_World, 1, &(mod->ModLeadRank),
                                                           modgrp, &ldrrank );
   if (err) return(err);

   mod->ModLeadRank = ldrrank;

   err = MPI_Comm_create( DCT_Comm_World, modgrp, &(mod->ModGroupComm) );
   if (err) return err;
   
   err = MPI_Group_free( &modgrp );
   if (err) return err;
   
   free ( ranks );
   
   return(err);
/* ---------------------------------  END( DCT_Create_Model_Comm ) */
}

int DCT_Destroy_Model_Comm ( DCT_Model *mod )
{
/* ---------------------------------------------  Local Variables  */
   int err=0;

/* ------------------------------  BEGIN( DCT_Destroy_Model_Comm ) */

   err = MPI_Comm_free( &(mod->ModGroupComm) );
   if (err) return err;
   
   return(err);
/* --------------------------------  END( DCT_Destroy_Model_Comm ) */
}

int DCT_Send_Couple( DCT_Couple **lcpl )
{
/* ---------------------------------------------  Local Variables  */
  
   DCT_List     *current;
   DCT_Couple   *couple;

   int           err=0;
   int           pos, pckcnt;
   
   char         *sbuf;
   
   DCT_Integer   nproc, numvar;
   
   DCT_Tag       MsgTag;
   int         **nlen;
   int           cplpck;
   register int  ii;

/* -------------------------------------  BEGIN( DCT_Send_Couple ) */

   nlen = (int **) malloc( sizeof(int *)*(size_t)DCT_LocalCouple );
   DCTERROR( nlen == (int **)NULL, "Memory Allocation Failed", return(1) );
   for ( ii = 0; ii < DCT_LocalCouple; ii++ ) {
      nlen[ii] = (int *) malloc( sizeof(int)*3 );
      DCTERROR( nlen[ii] == (int *)NULL, "Memory Allocation Failed", return(1) );
   }
   
   /* Counting the size of the package */
   /* Lenght of Coupler and the two model names for all coupler + # of couple */
   MPI_Pack_size( 1 + 3*DCT_LocalCouple, DCT_INT, DCT_Comm_World, &pckcnt); 
   cplpck = pckcnt;
   /* Sweep the DCT_Couple list */
   ii = 0;
   current = DCT_Reg_Couple;
   while (current != (DCT_List *) NULL) {
      DCT_List_Ext (&current, (DCT_VoidPointer *) &couple);
      /* For each DCT_Couple send the structure and get the
         message tag back to know if the process has to send
         the rest of information                             */
      lcpl[ii] =  couple;
      pckcnt = strlen( couple->CoupleName );
      nlen[ii][0] = ( DCT_MAX_NAME > pckcnt ? pckcnt: DCT_MAX_NAME);
      MPI_Pack_size( nlen[ii][0], DCT_CHAR, DCT_Comm_World, &pckcnt);
      cplpck += pckcnt;

      pckcnt = strlen( couple->CoupleModelA->ModName );
      nlen[ii][1] = ( DCT_MAX_NAME > pckcnt ? pckcnt: DCT_MAX_NAME);
      MPI_Pack_size( nlen[ii][1], DCT_CHAR, DCT_Comm_World, &pckcnt);
      cplpck += pckcnt;
      
      pckcnt = strlen( couple->CoupleHModelB->ModelName );
      nlen[ii][2] = ( DCT_MAX_NAME > pckcnt ? pckcnt: DCT_MAX_NAME);
      MPI_Pack_size( nlen[ii][2], DCT_CHAR, DCT_Comm_World, &pckcnt);
      cplpck += pckcnt;


      ii++;
   }
   /** number of process and vars **/
   MPI_Pack_size( 2*DCT_LocalCouple, DCT_INT, DCT_Comm_World, &pckcnt);
   cplpck += pckcnt;
   
   /* Allocating memory for the package buffer */
   sbuf = (char *) malloc( (size_t)cplpck );
   DCTERROR( sbuf == (char *) NULL, "Memory Allocation Failed", return(1) );
/*                                                   BEGIN DEBUG */
//    printf("[DCT_Send_Couple]: Sender %d, Couple Lenght: %d\n\n", DCT_procinfo.ProcDCTRank, cplpck );
/*                                                     END DEBUG */
   /* Packing */
   pos =0;
   MPI_Pack( &DCT_LocalCouple, 1, DCT_INT, sbuf, cplpck, &pos, DCT_Comm_World );
   for ( ii = 0; ii < DCT_LocalCouple; ii++ ) {
      MPI_Pack( nlen[ii], 3, DCT_INT, sbuf, cplpck, &pos, DCT_Comm_World ); 
      MPI_Pack( lcpl[ii]->CoupleName, nlen[ii][0], DCT_CHAR, sbuf, cplpck,
                &pos, DCT_Comm_World );
      MPI_Pack( lcpl[ii]->CoupleModelA->ModName, nlen[ii][1], DCT_CHAR, sbuf, cplpck,
                &pos, DCT_Comm_World );
      MPI_Pack( lcpl[ii]->CoupleHModelB->ModelName, nlen[ii][2], DCT_CHAR, sbuf, cplpck,
                &pos, DCT_Comm_World );
      numvar = (int) lcpl[ii]->CoupleTotNumVars;
      MPI_Pack( &numvar, 1, DCT_INT, sbuf, cplpck, &pos, DCT_Comm_World );
      nproc = lcpl[ii]->CoupleModelA->ModNumProcs;
      MPI_Pack( &nproc, 1, DCT_INT, sbuf, cplpck, &pos, DCT_Comm_World );
   }
   
/*                                                   BEGIN DEBUG */
//    printf("[DCT_Send_Couple]: Sender %d, Pack Lenght: %d\n\n", DCT_procinfo.ProcDCTRank, pos );
/*                                                     END DEBUG */
   
   MsgTag = DCT_Msg_Tags_COUPLE_REG;
   err = MPI_Send ( sbuf, pos, MPI_PACKED, DCT_procinfo.ProcDCTBroker,
                          MsgTag, DCT_Comm_World);

   free ( sbuf );
   for ( ii = 0; ii < DCT_LocalCouple; ii++ ) free ( nlen[ii] );
   free ( nlen );

   return(err);
/* ---------------------------------------  END( DCT_Send_Couple ) */
}

/** This function is to post in advance the registration
    of the DCT_Couple from all processes.                **/
int DCT_Post_RecvCouple(  )
{
/* ---------------------------------------------  Local Variables  */
   int       err = 0;
   DCT_Tag   mtag;
   int       nproc, pckcnt;
   int       ii;
   char     *rbuff;

/* ---------------------------------  BEGIN( DCT_Post_RecvCouple ) */

   mtag = DCT_Msg_Tags_COUPLE_REG;
   nproc = DCT_procinfo.ProcCommSize-1;

   /* At the most, for each process it can be sent the number of DCT_Couple,
      and 5*DCT_MAX_COUPLE integer numbers corresponding to the lengths of its
      DCT_Couple name and DCT_models' (the names, with 256 chars max), plus
      the number of of precesses per local DCT_Model and the number of DCT_Vars
      included in the Couple with the local model */  
   MPI_Pack_size( 1 + 5*DCT_MAX_COUPLE, DCT_INT, DCT_Comm_World, &pckcnt);
   dct_glb_buffsize = pckcnt;
   MPI_Pack_size( 3*DCT_MAX_COUPLE*DCT_MAX_NAME, DCT_CHAR, DCT_Comm_World, &pckcnt);
   dct_glb_buffsize += pckcnt;

   /** These variables are GLOBAL. See the beginning of the file for their declaration **/
   dct_glb_recvbuff = (char **) malloc( sizeof(char *)*(size_t)nproc );
   DCTERROR( dct_glb_recvbuff == (char **) NULL, "Memory Allocation Failed", return(1) );
   dct_glb_recreqst = (DCT_Request *) malloc( sizeof(DCT_Request)*(size_t)nproc );
   DCTERROR( dct_glb_recreqst == (DCT_Request *)NULL, "Memory Allocation Failed", return(1) );

   for ( ii=0; ii < nproc; ii++ ) {
      rbuff = (char *) malloc( (size_t)dct_glb_buffsize );
      DCTERROR( rbuff == (char *) NULL, "Memory Allocation Failed", return(1) );
      MPI_Irecv( rbuff, dct_glb_buffsize, MPI_PACKED, MPI_ANY_SOURCE, mtag, DCT_Comm_World, dct_glb_recreqst+ii );
      dct_glb_recvbuff[ii] = rbuff;
   }
   
   return(err);
/* -----------------------------------  END( DCT_Post_RecvCouple ) */
}

int DCT_Rels_RecvCouple(  )
{
/* ---------------------------------------------  Local Variables  */
   int       err = 0;

/* ---------------------------------  BEGIN( DCT_Rels_RecvCouple ) */


   /** This variables are GLOBAL. See the beginning of the file for their declaration **/
   // free ( dct_glb_lenbuff );
   free ( dct_glb_recvbuff );
   free ( dct_glb_recreqst );
   
   return(err);
/* -----------------------------------  END( DCT_Rels_RecvCouple ) */
}

int DCT_Get_Couple( DCT_Couple **lcpl, int *nclp, DCT_Rank *rcvr )
{
/* ---------------------------------------------  Local Variables  */
   int err=0;
   
   DCT_Couple  *couple;
   DCT_Model   *ModelA;
   
   // DCT_Tag      MsgTag;
   DCT_Status   status;
   
   char         *rbuf;
   DCT_Name      Maname;
   DCT_Name      Mbname;
   DCT_Name      Cname;

   DCT_Integer   nproc, numvar;
   int           nlen[3];
   int           cplpck, pos, index;
   register int  ii;

/* --------------------------------------  BEGIN( DCT_Get_Couple ) */

   nproc = DCT_procinfo.ProcCommSize-1;
   MPI_Waitany(nproc, dct_glb_recreqst, &index, &status);
   *rcvr = status.MPI_SOURCE;
   /* Assign the package buffer */
   rbuf = dct_glb_recvbuff[index];
   cplpck = dct_glb_buffsize;
/*                                                   BEGIN DEBUG */
//    printf("[DCT_Get_Couple]: Receiver %d, Mssg Lenght: %d\n\n", DCT_procinfo.ProcDCTRank, cplpck );
/*                                                     END DEBUG */
   
   /* UnPacking */
   pos =0;
   /* Number of DCT_Couple structures */
   MPI_Unpack(rbuf, cplpck, &pos, nclp, 1, DCT_INT, DCT_Comm_World);

   for ( ii = 0; ii < *nclp; ii++ ) {
      /* Unpacking length of couple and models names */
      MPI_Unpack(rbuf, cplpck, &pos, nlen, 3, DCT_INT, DCT_Comm_World);
      
      Cname = (DCT_Name) malloc( sizeof(char)*(size_t)(nlen[0]+1) );
      DCTERROR( Cname == (DCT_Name)NULL, "Memory Allocation Failed", return(1) );
      Maname = (DCT_Name) malloc( sizeof(char)*(size_t)(nlen[1]+1) );
      DCTERROR( Maname == (DCT_Name)NULL, "Memory Allocation Failed", return(1) );
      Mbname  = (DCT_Name) malloc( sizeof(char)*(size_t)(nlen[2]+1) );
      DCTERROR( Mbname == (DCT_Name)NULL, "Memory Allocation Failed", return(1) );
      
      /** The info in the message is unpacked **/
      MPI_Unpack( rbuf, cplpck, &pos, Cname, nlen[0], DCT_CHAR, DCT_Comm_World );
      Cname[ nlen[0] ] = '\0';
      MPI_Unpack( rbuf, cplpck, &pos, Maname, nlen[1], DCT_CHAR, DCT_Comm_World);
      Maname[ nlen[1] ] = '\0';
      MPI_Unpack( rbuf, cplpck, &pos, Mbname, nlen[2], DCT_CHAR, DCT_Comm_World);
      Mbname[ nlen[2] ] = '\0';
   
      MPI_Unpack( rbuf, cplpck, &pos, &numvar, 1, DCT_INT, DCT_Comm_World);
      MPI_Unpack( rbuf, cplpck, &pos, &nproc, 1, DCT_INT, DCT_Comm_World);
   
/*                                                   BEGIN DEBUG */
//       printf( "[DCT_Get_Couple]: Receiver %d, UnPack Lenght: %d\n\n", 
//               DCT_procinfo.ProcDCTRank, pos );
/*                                                     END DEBUG */
      
      /* Creating the DCT structures */
      /* Always is a memory allocated availably to avoid unnecessary allocation calls */
      ModelA = lcpl[ii]->CoupleModelA;
      DCT_Create_Model( ModelA, Maname, (DCT_String)NULL, nproc );
      couple = lcpl[ii];
      DCT_Create_Couple( couple, Cname, (DCT_String)NULL, ModelA, Mbname );
      /**** The internal DCT_Couple array is allocated ****/
      couple->CoupleTotNumVars = numvar;
      couple->CouplingTable = (DCT_VoidPointer)NULL;
      
/*                                                   BEGIN DEBUG */
//       printf("[DCT_Get_Couple]: Receiver %d, Checking Strings after create:\n Couple Name: %s\n ModelA Name: %s\n Hollow ModelB Name: %s\n", DCT_procinfo.ProcDCTRank, couple->CoupleName,
//       couple->CoupleModelA->ModName, couple->CoupleHModelB->ModelName );
/*                                                     END DEBUG */
  
   
      /* Clean temporary allocations */
      free( Cname );
      free( Maname );
      free( Mbname );
   } /** End of for ( ii = 0; ii < DCT_LocalCouple; ii++ ) **/
   free( rbuf );
   
   return(err);
/* ----------------------------------------  END( DCT_Get_Couple ) */
}

int DCT_Send_Tag( const DCT_Rank rcvr, const int len, DCT_Tag *cplTag,
                  const DCT_Msg_Tags_Type msgtag )
{
/* ---------------------------------------------  Local Variables  */
   int err;
   DCT_Tag mtag;

/* ----------------------------------------  BEGIN( DCT_Send_Tag ) */

   mtag = msgtag; /* The message tag is built */
   err = MPI_Send ( cplTag, len, DCT_TAG, rcvr, mtag, DCT_Comm_World);
/*                                                   BEGIN DEBUG */
//    printf("[DCT_Send_Tag] Sender %d, MSG TAG sent: %d\n\n", DCT_procinfo.ProcDCTRank, mtag );
//    printf("[DCT_Send_Tag] Sender %d, CPL TAG sent: %d\n\n", DCT_procinfo.ProcDCTRank, cplTag );
//    printf("[DCT_Send_Tag] Sender %d, MOD TAG sent: %d\n\n", DCT_procinfo.ProcDCTRank, modTag );
/*                                                     END DEBUG */
   
   return(err);
/* ------------------------------------------  END( DCT_Send_Tag ) */
}

int DCT_Get_Tag( DCT_Couple **lcpl, int *ncplmsg )
{
/* ---------------------------------------------  Local Variables  */
   int err=0;
   DCT_Couple  *couple;
   DCT_Tag     *tags;
   DCT_Model   *model;
   
   DCT_Tag      msgtag;
   DCT_Rank     myrank;
   
   DCT_Status   status;
   
   register int ii, jj;
   
/* -----------------------------------------  BEGIN( DCT_Get_Tag ) */
   tags = (DCT_Tag *)malloc(sizeof(DCT_Tag)*(size_t)(4*DCT_LocalCouple));
   DCTERROR( tags == (DCT_Tag *)NULL, "Memory Allocation Failed", return(1) );
   myrank = DCT_procinfo.ProcDCTRank;
   
   err = MPI_Recv( tags, 4*DCT_LocalCouple, DCT_TAG, DCT_procinfo.ProcDCTBroker, MPI_ANY_TAG,
             DCT_Comm_World, &status );
   if (err) return(err);

   msgtag = status.MPI_TAG; // - DCT_procinfo.ProcDCTRank*10000);
/*                                                   BEGIN DEBUG */
//    printf("[DCT_Get_Tag] Receiver %d, MSG TAG received: %d\n\n",
//                                        DCT_procinfo.ProcDCTRank, *msgtag );
/*                                                     END DEBUG */
   /* Rank of the counter model leader is contained */
   if ( msgtag == DCT_Msg_Tags_CPLTAG_MOD ) {
      *ncplmsg = 1;
      for ( ii = 0; ii < DCT_LocalCouple; ii++ ) {
         jj = 4*ii;
         couple = lcpl[ii];
         couple->CoupleTag = tags[jj];
         /* Set the model tag and check the uniqueness */
         model = couple->CoupleModelA;
         if ( model->ModTag == DCT_TAG_UNDEF ) {
            model->ModTag = tags[jj+1];
            model->ModLeadRank = tags[jj+2];
            if ( tags[jj+2] == myrank )
               couple->CoupleHModelB->ModLeadRank = tags[jj+3];
         }
         else {
            DCTERROR ( model->ModTag != tags[jj+1],
                      "Internal Error. Assigning different tag to a DCT_Model", return(1) );
            DCTERROR ( model->ModLeadRank != tags[jj+2],
                      "Internal Error. Assigning different tag to a DCT_Model", return(1) );
            couple->CoupleHModelB->ModLeadRank = tags[jj+3];
         }
   
      }
   }
   else if ( msgtag == DCT_Msg_Tags_COUPLE_TAG ) {
      *ncplmsg = 0;
      for ( ii = 0; ii < DCT_LocalCouple; ii++ ) {
         jj = 4*ii;
         couple = lcpl[ii];
         couple->CoupleTag = tags[jj];
         /* Set the model tag and check the uniqueness */
         model = couple->CoupleModelA;
         if ( model->ModTag == DCT_TAG_UNDEF ) {
            model->ModTag = tags[jj+1];
            model->ModLeadRank = tags[jj+2];
         }
         else {
            DCTERROR ( model->ModTag != tags[jj+1],
                      "Internal Error. Assigning different tag at same DCT_Model", return(1) );
            DCTERROR ( model->ModLeadRank != tags[jj+2],
                      "Internal Error. Assigning different tag at same DCT_Model", return(1) );
         }
   
      }
   }
   else {
      DCTERROR( DCT_TRUE, "Internal Error. Invalid tag value in DCT_Get_Tag", return(1) );
   }
   
   
   free ( tags );
   return(err);
/* -------------------------------------------  END( DCT_Get_Tag ) */
}

int DCT_PckSize_Model( DCT_Model *Model, int *cpck, DCT_Comm DCT_Comm_World )
{
/* ---------------------------------------------  Local Variables  */

   DCT_Model_Dom     *tmod;
   DCT_Integer        dim, tpts;
   
   DCT_Distribution   dist;

   int                err=0;
   
   int                sw, pckcnt;
   
   int                nldim, nlen, dimtotal;

   register int       ii;

/* -----------------------------------  BEGIN( DCT_PckSize_Model ) */
   MPI_Pack_size( 1, DCT_INT, DCT_Comm_World, &pckcnt); /* len */
   *cpck = pckcnt;
   nlen = strlen( Model->ModName );
   MPI_Pack_size( nlen, DCT_CHAR, DCT_Comm_World, &pckcnt); /* Model Name */
   *cpck += pckcnt;
   MPI_Pack_size( 1, DCT_INT, DCT_Comm_World, &pckcnt); /* Num procs ModNumProcs */
   *cpck += pckcnt;
   MPI_Pack_size( 2, DCT_SCALAR, DCT_Comm_World, &pckcnt); /* ModTimeIni & ModTime */
   *cpck += pckcnt;
   MPI_Pack_size( 1, DCT_INT, DCT_Comm_World, &pckcnt); /* ModTimeUnit */
   *cpck += pckcnt;   
   
   tmod = Model->ModDomain;

   /* Counting Packing the domain */
   MPI_Pack_size( 1, DCT_INT, DCT_Comm_World, &pckcnt); /* Dim */
   *cpck += pckcnt;
   dim = tmod->ModDomDim;
   MPI_Pack_size( (int)dim, DCT_INT, DCT_Comm_World, &pckcnt); /* type */
   *cpck += pckcnt;
   MPI_Pack_size( (int)dim, DCT_INT, DCT_Comm_World, &pckcnt); /* npts */
   *cpck += pckcnt;
   
   
   /****  The values or tick marks ***/
   sw = 0;  /* Used to check is equally sapced in all dimension */
   for (ii=0; ii < dim; ii++ ) 
       sw += ( tmod->ModDomType[ii] == DCT_CARTESIAN ? 1: 0);
   
   if ( sw == dim ) { /*** All directions are equally spaced  ***/
      MPI_Pack_size( 2*(int)dim, DCT_SCALAR, DCT_Comm_World, &pckcnt);
      *cpck += pckcnt;
   }
   else if ( tmod->ModDomType[0] == DCT_GENERAL_CURV ) { /*** All directions are
                                                               general curvinlinear ***/
      tpts = 1;
      for (ii=0; ii < dim; ii++ ) tpts *= tmod->ModNumPts[ii];
      MPI_Pack_size( (int)tpts*dim, DCT_SCALAR, DCT_Comm_World, &pckcnt);
      *cpck += pckcnt;
   
   } else {  /*** Some equally or unequally spaced ***/
      for (ii=0; ii < dim; ii++ ) {
         if ( tmod->ModDomType[ii] == DCT_CARTESIAN ) {
            MPI_Pack_size( 2, DCT_SCALAR, DCT_Comm_World, &pckcnt);
            *cpck += pckcnt;
         } 
         else if ( tmod->ModDomType[ii] == DCT_RECTILINEAR ) {
            MPI_Pack_size( (int)tmod->ModNumPts[ii], DCT_SCALAR, DCT_Comm_World,
                           &pckcnt);
            *cpck += pckcnt;
         } else {
            DCTERROR( DCT_TRUE, "Internal error, bad model domain", return(1) );
         }
      } /* End of for */
      
   } /** End of clasifying the model domain type **/

   /* Counting Packing the subdomain */
   dist = tmod->ModSubDomParLayout;
   MPI_Pack_size( 1, DCT_INT, DCT_Comm_World, &pckcnt); /* Parlayout */
   *cpck += pckcnt;
   dimtotal = Model->ModNumProcs;
   switch(dist)
   {
    case DCT_DIST_NULL:
       DCTERROR( DCT_TRUE, "Internal error, wrong model parallel layout", return(1) );
    break;
    case DCT_DIST_SEQUENTIAL:
       nldim = 1;
       
    break;
    case DCT_DIST_RECTANGULAR:
       nldim = 2;
       
    break;
    case DCT_DIST_3D_RECTANGULAR:
       nldim = 3;
       
    break;
    default:
       DCTERROR( DCT_TRUE, "Internal error, wrong model parallel layout", return(1) );
   }
   MPI_Pack_size( nldim, DCT_INT, DCT_Comm_World, &pckcnt); /* Parlayout Dimension */
   *cpck += pckcnt;
   MPI_Pack_size( dimtotal, DCT_RANK, DCT_Comm_World, &pckcnt); /* Rank processes list */
   *cpck += pckcnt;
   MPI_Pack_size( 2*dimtotal*(int)dim, DCT_INT, DCT_Comm_World, &pckcnt); /* ini and end pos, order */
   *cpck += pckcnt;
   
   return(err);
/* -------------------------------------  END( DCT_PckSize_Model ) */
}

int DCT_Packing_Model( DCT_Model *Model, void *sbuf, int lbuf, int *pos,
                       DCT_Comm DCT_Comm_World )
{
/* ---------------------------------------------  Local Variables  */

   DCT_Model_Dom     *tmod;
   DCT_Integer        dim, tpts;

   DCT_Integer       *inipos;
   DCT_Integer       *endpos;
   DCT_Integer       *offsini, *offsend;
   
   DCT_Rank          *procs;
   
   DCT_Distribution   dist;
   DCT_Data_SubDom   *SubDom;

   int                err=0;
   
   int                sw;
   
   int                nldim, nlen, dimtotal;

   register int       ii;

/* -----------------------------------  BEGIN( DCT_Packing_Model ) */
   /* Packing the model */
   nlen = strlen( Model->ModName );
   MPI_Pack( &nlen, 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World); 
   MPI_Pack( Model->ModName, nlen, DCT_CHAR, sbuf, lbuf, pos, DCT_Comm_World);
   MPI_Pack( &(Model->ModNumProcs), 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);
   MPI_Pack( &(Model->ModTimeIni), 1, DCT_SCALAR, sbuf, lbuf, pos, DCT_Comm_World);
   MPI_Pack( &(Model->ModTime), 1, DCT_SCALAR, sbuf, lbuf, pos, DCT_Comm_World);
   MPI_Pack( &(Model->ModTimeUnits), 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);
   
   tmod = Model->ModDomain;
   dim = tmod->ModDomDim;
   /* Packing the domain */
   MPI_Pack( &dim, 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);
   MPI_Pack( tmod->ModDomType, dim, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);
   MPI_Pack( tmod->ModNumPts , dim, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);


   /****  The values or tick marks ***/
   sw = 0;  /* Used to check is equally sapced in all dimension */
   for (ii=0; ii < dim; ii++ ) 
       sw += ( tmod->ModDomType[ii] == DCT_CARTESIAN ? 1: 0);
   
   if ( sw == dim ) { /*** All directions are equally spaced  ***/
      /* All the initial values */
      for (ii=0; ii < dim; ii++ ) {
         MPI_Pack( tmod->ModValues[ii], 1, DCT_SCALAR, sbuf, lbuf,
                   pos, DCT_Comm_World );
      }
      /* All the final values */
      for (ii=0; ii < dim; ii++ ) {
         MPI_Pack( &(tmod->ModValues[ii][1]), 1, DCT_SCALAR, sbuf, lbuf,
                   pos, DCT_Comm_World );
      }
   }
   else if ( tmod->ModDomType[0] == DCT_GENERAL_CURV ) { /*** All directions are
                                                               general curvinlinear ***/
      tpts = 1;
      for (ii=0; ii < dim; ii++ ) tpts *= tmod->ModNumPts[ii];
      
      for (ii=0; ii < dim; ii++ ) {
         MPI_Pack( tmod->ModValues[ii], tpts, DCT_SCALAR, sbuf, lbuf,
                   pos, DCT_Comm_World);
      }
   } else {  /*** Some equally or unequally spaced ***/
      for (ii=0; ii < dim; ii++ ) {
         if ( tmod->ModDomType[ii] == DCT_CARTESIAN ) {
            MPI_Pack( tmod->ModValues[ii], 2, DCT_SCALAR, sbuf, lbuf,
                   pos, DCT_Comm_World);
         } 
         else if ( tmod->ModDomType[ii] == DCT_RECTILINEAR ) {
            MPI_Pack( tmod->ModValues[ii], (int)tmod->ModNumPts[ii], DCT_SCALAR,
                      sbuf, lbuf, pos, DCT_Comm_World);
         }
      } /* End of for */
      
   } /** End of clasifying the model domain type **/

   /* Packing the subdomain */
   dist = tmod->ModSubDomParLayout;
   MPI_Pack( &dist, 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);
   dimtotal = Model->ModNumProcs;
   switch(dist)
   {
    case DCT_DIST_NULL:
       DCTERROR( DCT_TRUE, "Internal error, wrong model parallel layout", return(1) );
    break;
    case DCT_DIST_SEQUENTIAL:
       nldim = 1;
       
    break;
    case DCT_DIST_RECTANGULAR:
       nldim = 2;
       
    break;
    case DCT_DIST_3D_RECTANGULAR:
       nldim = 3;
       
    break;
    default:
       DCTERROR( DCT_TRUE, "Internal error, wrong model parallel layout", return(1) );
   }
   
   MPI_Pack( tmod->ModSubDomLayoutDim, nldim, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);

   procs = (DCT_Rank *) malloc( sizeof(DCT_Rank) * (size_t)dimtotal );
   DCTERROR( procs == (DCT_Rank *) NULL, "Memory Allocation Failed", return(1) );

   inipos = (DCT_Integer *) malloc( sizeof(DCT_Integer) * (size_t)dimtotal*(size_t)dim );
   DCTERROR( inipos == (DCT_Integer *)NULL, "Memory Allocation Failed", return(1) );
   endpos = (DCT_Integer *) malloc( sizeof(DCT_Integer) * (size_t)dimtotal*(size_t)dim );
   DCTERROR( endpos == (DCT_Integer *)NULL, "Memory Allocation Failed", return(1) );
   
   SubDom = tmod->ModSubDom;
   offsini = inipos;
   offsend = endpos;
   
   switch (dim){
   case 4:
      for ( ii=0; ii < dimtotal; ii++ ){
         *(offsini++) = (SubDom + ii)->IniPos[0];
         *(offsini++) = (SubDom + ii)->IniPos[1];
         *(offsini++) = (SubDom + ii)->IniPos[2];
         *(offsini++) = (SubDom + ii)->IniPos[3];
         
         *(offsend++) = (SubDom + ii)->EndPos[0];
         *(offsend++) = (SubDom + ii)->EndPos[1];
         *(offsend++) = (SubDom + ii)->EndPos[2];
         *(offsend++) = (SubDom + ii)->EndPos[3];
         
         *(procs + ii) = (SubDom + ii)->RankProc;
      }
   break;
   case 3:
      for ( ii=0; ii < dimtotal; ii++ ){
         *(offsini++) = (SubDom + ii)->IniPos[0];
         *(offsini++) = (SubDom + ii)->IniPos[1];
         *(offsini++) = (SubDom + ii)->IniPos[2];
         
         *(offsend++) = (SubDom + ii)->EndPos[0];
         *(offsend++) = (SubDom + ii)->EndPos[1];
         *(offsend++) = (SubDom + ii)->EndPos[2];
         
         *(procs + ii) = (SubDom + ii)->RankProc;
      }
   break;
   case 2:
      for ( ii=0; ii < dimtotal; ii++ ){
         *(offsend++) = (SubDom + ii)->EndPos[0];
         *(offsend++) = (SubDom + ii)->EndPos[1];
         
         *(offsini++) = (SubDom + ii)->IniPos[0];
         *(offsini++) = (SubDom + ii)->IniPos[1];
         
         *(procs + ii) = (SubDom + ii)->RankProc;
      }
   }

   MPI_Pack( procs, dimtotal, DCT_RANK, sbuf, lbuf, pos, DCT_Comm_World);
   MPI_Pack( inipos, dimtotal*(int)dim, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);
   MPI_Pack( endpos, dimtotal*(int)dim, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World); 

   free ( procs );
   free ( inipos );
   free ( endpos );
   
   return(err);
/* -------------------------------------  END( DCT_Packing_Model ) */
}


int DCT_UnPacking_Model( DCT_Couple *couple, DCT_Boolean *modela, int broker,
                         void *rbuf, int lbuf, int *pos, DCT_Comm DCT_Comm_World )
{
/* ---------------------------------------------  Local Variables  */
   int err=0;
   
   DCT_Model      *Model;
   DCT_Name        ModelName;
   
   DCT_Scalar      TimeIni, Time;
   DCT_Scalar    **DomVal;

//    DCT_Integer     TotVCon, TotVPro;
   DCT_Integer     dim, dim2;
   DCT_Integer    *npts;

   DCT_Integer    *laydims;
   
   DCT_Integer    *inipos;
   DCT_Integer    *endpos;
   
   DCT_Rank       *procs;

   int             sw, nlen, ttype, parlout;
   int             tpts, nldim, dimtotal;
   int             domt = 0;
   int            *domtype;

   register int    ii;

/* ---------------------------------  BEGIN( DCT_UnPacking_Model ) */
 
   /************ UnPacking ************/
   /* UnPacking the model */
   /* Unpacking length of couple and models names */
   MPI_Unpack(rbuf, lbuf, pos, &nlen, 1, DCT_INT, DCT_Comm_World);
   
   ModelName = (DCT_Name) malloc( sizeof(char)*(size_t)(nlen+1) );
   DCTERROR( ModelName == (DCT_Name)NULL, "Memory Allocation Failed", return(1) );
   
   /** The info in the message is unpacked **/
   MPI_Unpack( rbuf, lbuf, pos, ModelName, nlen, DCT_CHAR, DCT_Comm_World );
   ModelName[nlen]='\0';
   /**  Check which model is received  **/
   if ( strcmp( couple->CoupleModelA->ModName, ModelName )==0 ) {
      Model = couple->CoupleModelA;
      *modela = DCT_TRUE;
   }
   else if ( strcmp( couple->CoupleHModelB->ModelName, ModelName )==0 ) {
      Model = couple->CoupleModelB;
      *modela = DCT_FALSE;
   }
   else {
      DCTERROR( DCT_TRUE, "Internal error, bad model name in coupler", return(1) );
   }
   MPI_Unpack( rbuf, lbuf, pos, &dimtotal, 1, DCT_INT, DCT_Comm_World);
   
   /** If it is not the broker Create the DCT_Model structures */
   if (!broker)
     DCT_Create_Model( Model, ModelName, (DCT_String)NULL, dimtotal );

   MPI_Unpack( rbuf, lbuf, pos, &TimeIni, 1, DCT_SCALAR, DCT_Comm_World);
   MPI_Unpack( rbuf, lbuf, pos, &Time, 1, DCT_SCALAR, DCT_Comm_World);
   MPI_Unpack( rbuf, lbuf, pos, &ttype, 1, DCT_INT, DCT_Comm_World);
   
   /* Set the time */
   DCT_Set_Model_Time( Model, TimeIni, Time, (DCT_Time_Types) ttype);

   /* UnPacking the domain */
   
   /* Checking if the models dimensions agree */
   MPI_Unpack( rbuf, lbuf, pos, &dim, 1, DCT_INT, DCT_Comm_World);
   
   domtype =  (int *) malloc( sizeof(char)*(size_t)dim );
   DCTERROR( domtype == (int *)NULL, "Memory Allocation Failed", return(1) );

   npts =  (DCT_Integer *) malloc( sizeof(char)*(size_t)dim );
   DCTERROR( npts == (DCT_Integer *)NULL, "Memory Allocation Failed", return(1) );
   
   MPI_Unpack( rbuf, lbuf, pos, domtype, dim, DCT_INT, DCT_Comm_World);
   MPI_Unpack( rbuf, lbuf, pos, npts, dim, DCT_INT, DCT_Comm_World);

   sw = 0;  /* Used to check is equally sapced in all dimension */
   for (ii=0; ii < dim; ii++ )
       sw += ( domtype[ii] == (int)DCT_CARTESIAN ? 1: 0);
   
   /************************    BEGIN OF FIX    ***************************/
   /* THIS PIECE OF CODE LOOKS THAT IS OLD LEFTOVERS BECAUSE THIS IS NOT 
      THE PLACE WHERE COMPATIBILITY OF MODEL DOMAIN HAS TO BE DONE        */
   dim2 = 0; /* This to know that an existing counter model was already registered */ 
   if ( (*modela==DCT_TRUE) &&
        ( couple->CoupleModelB->ModDomain != (DCT_Model_Dom *)NULL ) ) {
      dim2 = couple->CoupleModelB->ModDomain->ModDomDim;
      domt = (int) couple->CoupleModelB->ModDomain->ModDomType[0];
   } else if ( couple->CoupleModelA->ModDomain != (DCT_Model_Dom *)NULL ) {
      dim2 = couple->CoupleModelA->ModDomain->ModDomDim;
      domt = (int) couple->CoupleModelA->ModDomain->ModDomType[0];
   }
   DCTERROR( ( dim2 != 0) && ( dim2 != dim ), "Error, Models dimensions are different",
             return(1) );

   /************ ATTENTION THIS CONDITION SHOULD BE REMOVED IF
                 DIFFERENT DOMAIN TYPE COULD BE OVERLAPPED     ************/
   DCTERROR( ( domt == DCT_GENERAL_CURV) && ( domt != domtype[0] ), 
             "Error, Models domain type are incompatibles", return(1) );
   /*************************    END OF FIX    ****************************/


   /****  The values or tick marks ***/

   /* As long as it is necessary call the General Curvilinear
      function to set the domain with 3 vector */
   DomVal =  (DCT_Scalar **) malloc( sizeof(DCT_Scalar *)*(size_t)(MAX_DOMAIN_DIM-1) );
   DCTERROR( DomVal == (DCT_Scalar **)NULL, "Memory Allocation Failed", return(1) );
   if ( sw == dim ) { /*** All directions are equally spaced  ***/

      DomVal[0] =  (DCT_Scalar *) malloc( sizeof(DCT_Scalar)*(size_t)dim );
      DCTERROR( DomVal[0] == (DCT_Scalar *)NULL, "Memory Allocation Failed", return(1) );
      DomVal[1] =  (DCT_Scalar *) malloc( sizeof(DCT_Scalar)*(size_t)dim );
      DCTERROR( DomVal[1] == (DCT_Scalar *)NULL, "Memory Allocation Failed", return(1) );
         
      MPI_Unpack( rbuf, lbuf, pos, DomVal[0], dim, DCT_SCALAR, DCT_Comm_World);
      MPI_Unpack( rbuf, lbuf, pos, DomVal[1], dim, DCT_SCALAR, DCT_Comm_World);

      /* Setting the Domain in the DCT structures */
      DCT_Set_Model_RSpaced_Dom( Model, dim, DomVal[0], DomVal[1], npts );
      
      /* Cleaning the structure */
      free ( DomVal[0] );
      free ( DomVal[1] );
   }
   else if (domtype[0] == (int) DCT_GENERAL_CURV) { /*** All directions are
                                                        general curvilinear ***/
      tpts = 1;
      for (ii=0; ii < dim; ii++ ) tpts *= npts[ii];
      
      for (ii=0; ii < dim; ii++ ) {
         DomVal[ii] =  (DCT_Scalar *) malloc( sizeof(DCT_Scalar)*(size_t)tpts );
         DCTERROR( DomVal[ii] == (DCT_Scalar *)NULL, "Memory Allocation Failed",
                   return(1) );
         MPI_Unpack( rbuf, lbuf, pos, DomVal[ii], tpts, DCT_SCALAR, DCT_Comm_World);
      }
      /* Setting the Domain in the DCT structures */
      DCT_Set_Model_GenCur_Dom( Model, dim, DomVal[0], DomVal[1], DomVal[2], npts );

      /* Freeing the structure */
      for (ii=0; ii<dim; ii++) free ( DomVal[ii ] );
      
   } else {  /*** Some equally or unequally spaced ***/
      for (ii=0; ii < dim; ii++ ) {
         if ( domtype[ii] == (int)DCT_CARTESIAN ) {
            DomVal[ii] =  (DCT_Scalar *) malloc( sizeof(DCT_Scalar)*(size_t)2 );
            DCTERROR( DomVal[ii] == (DCT_Scalar *)NULL, "Memory Allocation Failed",
                      return(1) );
            MPI_Unpack( rbuf, lbuf, pos, DomVal[ii], 2, DCT_SCALAR, DCT_Comm_World);
            /* Setting the Domain in the DCT structures */
            DCT_Set_Model_Dom( Model, dim, ii+1, DCT_CARTESIAN, DomVal[ii], npts[ii] );
         } 
         else if ( domtype[ii] == (int)DCT_RECTILINEAR ) {
            DomVal[ii] =  (DCT_Scalar *) malloc( sizeof(DCT_Scalar)*(size_t)npts[ii] );
            DCTERROR( DomVal[ii] == (DCT_Scalar *)NULL, "Memory Allocation Failed",
                      return(1) );
            MPI_Unpack( rbuf, lbuf, pos, DomVal[ii], npts[ii], DCT_SCALAR, 
                        DCT_Comm_World);
            /* Setting the Domain in the DCT structures */
            DCT_Set_Model_Dom( Model, dim, ii+1, DCT_RECTILINEAR, DomVal[ii],
                               npts[ii] );
         }
      } /* End of for */
      
      for ( ii=0; ii< dim; ii++ ) free( DomVal[ii] );
      
   } /** End of clasifying the model domain type **/
   
   /* Cleaning the Model Structured not used  anymore */
   free( DomVal );
   free( domtype );
   free( npts );

   /* UnPacking the subdomain */
   MPI_Unpack( rbuf, lbuf, pos, &parlout, 1, DCT_INT, DCT_Comm_World);
   switch(parlout)
   {
    case DCT_DIST_NULL:
       DCTERROR( DCT_TRUE, "Internal error, wrong model parallel layout", return(1) );
    break;
    case DCT_DIST_SEQUENTIAL:
       nldim = 1;
    break;
    case DCT_DIST_RECTANGULAR:
       nldim = 2;
    break;
    case DCT_DIST_3D_RECTANGULAR:
       nldim = 3;
    break;
    default:
       DCTERROR( DCT_TRUE, "Internal error, wrong model parallel layout", return(1) );
   }
   
   laydims = (DCT_Integer *) malloc( sizeof(DCT_Integer) * (size_t)nldim );
   DCTERROR( laydims == (DCT_Integer *)NULL, "Memory Allocation Failed", return(1) );
   MPI_Unpack( rbuf, lbuf, pos, laydims, nldim, DCT_INT, DCT_Comm_World);
   /*switch(parlout)
   {
    case DCT_DIST_NULL:
       DCTERROR( DCT_TRUE, "Internal error, wrong model parallel layout", return(1) );
    break;
    case DCT_DIST_SEQUENTIAL:
       DCTERROR( DCT_TRUE, "Internal error, wrong model parallel layout", return(1) );
    break;
    case DCT_DIST_RECTANGULAR:
       dimtotal = (int) *(laydims) * *(laydims+1);
    break;
    case DCT_DIST_3D_RECTANGULAR:
       dimtotal = (int) *(laydims) * *(laydims+1) * *(laydims+2);
    break;
    default:
       DCTERROR( DCT_TRUE, "Internal error, wrong model parallel layout", return(1) );
   }*/

   procs = (DCT_Rank *) malloc( sizeof(DCT_Rank) * (size_t)dimtotal );
   DCTERROR( procs == (DCT_Rank *) NULL, "Memory Allocation Failed", return(1) );

   inipos = (DCT_Integer *) malloc( sizeof(DCT_Integer) * (size_t)dimtotal*(size_t)dim );
   DCTERROR( inipos == (DCT_Integer *)NULL, "Memory Allocation Failed", return(1) );
   endpos = (DCT_Integer *) malloc( sizeof(DCT_Integer) * (size_t)dimtotal*(size_t)dim );
   DCTERROR( endpos == (DCT_Integer *)NULL, "Memory Allocation Failed", return(1) );

   MPI_Unpack( rbuf, lbuf, pos, procs, dimtotal, DCT_RANK, DCT_Comm_World);
   MPI_Unpack( rbuf, lbuf, pos, inipos, dimtotal*(int)dim, DCT_INT, DCT_Comm_World);
   MPI_Unpack( rbuf, lbuf, pos, endpos, dimtotal*(int)dim, DCT_INT, DCT_Comm_World);

   /* Setting the DCT paralell layout */
   DCT_Set_Model_ParLayout( Model, parlout, laydims); /*, NULL );*/
   DCT_Set_Model_SubDom( Model, procs, inipos, endpos );

   /* Clean temporary allocations */
   free( ModelName );
   free ( procs );
   free ( inipos );
   free ( endpos );
   free ( laydims );
   
   return(err);
/* -----------------------------------  END( DCT_UnPacking_Model ) */
}

int DCT_PckSize_Vars( DCT_Object_Types CVType, DCT_VoidPointer CVar, DCT_Name nCVar2,
                      int *cpck, DCT_Comm DCT_Comm_World )
{
/* ---------------------------------------------  Local Variables  */

   DCT_Field         *field;
   DCT_3d_Var        *var3d;
   DCT_4d_Var        *var4d;
  
   int                err=0;
   
   int                pckcnt;
   
//   int                tpts, nlen;
   int                nlen;

/* ------------------------------------  BEGIN( DCT_PckSize_Vars ) */

   MPI_Pack_size( 1, DCT_INT, DCT_Comm_World, &pckcnt); /* CVType */
   *cpck = pckcnt;
   MPI_Pack_size( 1, DCT_INT, DCT_Comm_World, &pckcnt); /* CVarTrans */
   *cpck += pckcnt;   
   nlen = strlen( nCVar2 );
   MPI_Pack_size( 1, DCT_INT, DCT_Comm_World, &pckcnt); /* nCVar2 Name nlen */
   *cpck += pckcnt;
   MPI_Pack_size( nlen, DCT_CHAR, DCT_Comm_World, &pckcnt); /* nCVar2 */
   *cpck += pckcnt;

   switch (CVType)
   {
    case DCT_FIELD_TYPE:
      field  = (DCT_Field *) CVar;
      nlen = strlen( field->VarName );

      break;
    case DCT_3D_VAR_TYPE:
      var3d  = (DCT_3d_Var *) CVar;
      nlen = strlen( var3d->VarName );

      break;
    case DCT_4D_VAR_TYPE:
      var4d  = (DCT_4d_Var *) CVar;
      nlen = strlen( var4d->VarName );

      break;
    default:
      DCTERROR( DCT_TRUE, "Internal error, bad variable type", return(1) );
   }
   MPI_Pack_size( 1, DCT_INT, DCT_Comm_World, &pckcnt); /* nlen */
   *cpck += pckcnt;
   MPI_Pack_size( nlen, DCT_CHAR, DCT_Comm_World, &pckcnt); /* VarName */
   *cpck += pckcnt;
   MPI_Pack_size( 2, DCT_SCALAR, DCT_Comm_World, &pckcnt); /* VarTimeIni & VarFreq */
   *cpck += pckcnt;
   MPI_Pack_size( 2, DCT_INT, DCT_Comm_World, &pckcnt); /* VarTimeUnit & VarUnits */
   *cpck += pckcnt;
   MPI_Pack_size( 2, DCT_INT, DCT_Comm_World, &pckcnt); /* VarProduce & VarUserDataType */
   *cpck += pckcnt;
   
   return(err);
/* --------------------------------------  END( DCT_PckSize_Vars ) */
}

int DCT_Packing_Vars( DCT_Object_Types *CVType, DCT_VoidPointer CVar, DCT_Name nCVar2,
                      DCT_Data_Transformation CVarTrans, void *sbuf, int lbuf, int *pos,
                      DCT_Comm DCT_Comm_World )
{
/* ---------------------------------------------  Local Variables  */

   DCT_Field         *field;
   DCT_3d_Var        *var3d;
   DCT_4d_Var        *var4d;

   int                err=0;
   
   int                nlen, nlen2;   //, tpts

/* ------------------------------------  BEGIN( DCT_Packing_Vars ) */
   /* Packing the Var */
   MPI_Pack( CVType, 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World); 
   MPI_Pack( &CVarTrans, 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);
   nlen2 = strlen( nCVar2 );
   MPI_Pack( &nlen2, 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World); 
   MPI_Pack( nCVar2, nlen2, DCT_CHAR, sbuf, lbuf, pos, DCT_Comm_World);

   switch (*CVType)
   {
    case DCT_FIELD_TYPE:
      field  = (DCT_Field *) CVar;
      nlen = strlen( field->VarName );
      MPI_Pack( &nlen, 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World); 
      MPI_Pack( field->VarName, nlen, DCT_CHAR, sbuf, lbuf, pos, DCT_Comm_World);
      MPI_Pack( &(field->VarUnits), 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);
      MPI_Pack( &(field->VarProduced), 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);
      MPI_Pack( &(field->VarTimeIni), 1, DCT_SCALAR, sbuf, lbuf, pos, DCT_Comm_World);
      MPI_Pack( &(field->VarFrequency), 1, DCT_SCALAR, sbuf, lbuf, pos, DCT_Comm_World);
      MPI_Pack( &(field->VarTimeUnits), 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);
      MPI_Pack( &(field->VarUserDataType), 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);

      break;
    case DCT_3D_VAR_TYPE:
      var3d  = (DCT_3d_Var *) CVar;
      nlen = strlen( var3d->VarName );
      MPI_Pack( &nlen, 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World); 
      MPI_Pack( var3d->VarName, nlen, DCT_CHAR, sbuf, lbuf, pos, DCT_Comm_World);
      MPI_Pack( &(var3d->VarUnits), 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);
      MPI_Pack( &(var3d->VarProduced), 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);
      MPI_Pack( &(var3d->VarTimeIni), 1, DCT_SCALAR, sbuf, lbuf, pos, DCT_Comm_World);
      MPI_Pack( &(var3d->VarFrequency), 1, DCT_SCALAR, sbuf, lbuf, pos, DCT_Comm_World);
      MPI_Pack( &(var3d->VarTimeUnits), 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);
      MPI_Pack( &(var3d->VarUserDataType), 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);

      break;
    case DCT_4D_VAR_TYPE:
      var4d  = (DCT_4d_Var *) CVar;
      nlen = strlen( var4d->VarName );
      MPI_Pack( &nlen, 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World); 
      MPI_Pack( var4d->VarName, nlen, DCT_CHAR, sbuf, lbuf, pos, DCT_Comm_World);
      MPI_Pack( &(var4d->VarUnits), 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);
      MPI_Pack( &(var4d->VarProduced), 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);
      MPI_Pack( &(var4d->VarTimeIni), 1, DCT_SCALAR, sbuf, lbuf, pos, DCT_Comm_World);
      MPI_Pack( &(var4d->VarFrequency), 1, DCT_SCALAR, sbuf, lbuf, pos, DCT_Comm_World);
      MPI_Pack( &(var4d->VarTimeUnits), 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);
      MPI_Pack( &(var4d->VarUserDataType), 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);

      break;
    default:
      DCTERROR( DCT_TRUE, "Internal error, bad variable type", return(1) );

   }
   
   return(err);
/* --------------------------------------  END( DCT_Packing_Vars ) */
}


int DCT_UnPacking_Vars( DCT_Couple *couple, DCT_Boolean *modela, DCT_Tag *vartag,
                            void *rbuf, int lbuf, int *pos, DCT_Comm DCT_Comm_World )
{
/* ---------------------------------------------  Local Variables  */
   int err=0;
   
//    DCT_VoidPointer          cplvar = (DCT_VoidPointer)NULL;
   
   DCT_Object_Types         CVType;
   DCT_Data_Transformation  CVarTrans;
   DCT_Name                 nCvar2;
   
   DCT_List                *varexist;
   
   DCT_Model               *model;
   
   DCT_CPL_Node            *cplnode;
   DCT_Hollow_CPLVar       *DCTVar1;
   DCT_Hollow_CPLVar       *DCTVar2;

   DCT_List                *current;
   DCT_Hollow_Var          *HCVar2;
   DCT_Field               *field;
   DCT_3d_Var              *var3d;
   DCT_4d_Var              *var4d;
   DCT_Name                 Name;


   DCT_Scalar               TimeIni, Time;
   
   DCT_Integer              cnvar;
   
   DCT_ProdCons             tproc2;
   int                      nlen, nlen2, ttype, tproc, uvtype, tunit;
   int                      nfound;

//    int register             ii;

/* ----------------------------------  BEGIN( DCT_UnPacking_Vars ) */
 
   /*** Determining the Model attached ***/
   model = ( *modela ? couple->CoupleModelA : couple->CoupleModelB );
   
   /************ UnPacking ************/
   /* UnPacking the Var Header*/
   /* Unpacking the DCT Data Type */
   MPI_Unpack(rbuf, lbuf, pos, &CVType, 1, DCT_INT, DCT_Comm_World);
   MPI_Unpack(rbuf, lbuf, pos, &CVarTrans, 1, DCT_INT, DCT_Comm_World);
   MPI_Unpack(rbuf, lbuf, pos, &nlen2, 1, DCT_INT, DCT_Comm_World);
   nCvar2 = (DCT_Name) malloc( sizeof(char)*(size_t)(nlen2+1) );
   DCTERROR( nCvar2 == (DCT_Name)NULL, "Memory Allocation Failed", return(1) );
   
   MPI_Unpack( rbuf, lbuf, pos, nCvar2, nlen2, DCT_CHAR, DCT_Comm_World );
   nCvar2[nlen2] = '\0';

   /*** The Hollow cpl variable is allocated **/
   DCTVar1 = (DCT_Hollow_CPLVar *) malloc( sizeof(DCT_Hollow_CPLVar) );
   DCTERROR( DCTVar1 == (DCT_Hollow_CPLVar *)NULL, "Memory Allocation Failed", return(1) );

   /*** UnPacking the varible ***/
   MPI_Unpack(rbuf, lbuf, pos, &nlen, 1, DCT_INT, DCT_Comm_World);
   Name = (DCT_Name) malloc( sizeof(char)*(size_t)(nlen+1) );
   DCTERROR( Name == (DCT_Name)NULL, "Memory Allocation Failed", return(1) );
   
   MPI_Unpack( rbuf, lbuf, pos, Name, nlen, DCT_CHAR, DCT_Comm_World );
   Name[nlen] = '\0';

   MPI_Unpack( rbuf, lbuf, pos, &tunit, 1, DCT_INT, DCT_Comm_World );
   MPI_Unpack( rbuf, lbuf, pos, &tproc, 1, DCT_INT, DCT_Comm_World );
   MPI_Unpack( rbuf, lbuf, pos, &TimeIni, 1, DCT_SCALAR, DCT_Comm_World);
   MPI_Unpack( rbuf, lbuf, pos, &Time, 1, DCT_SCALAR, DCT_Comm_World);
   MPI_Unpack( rbuf, lbuf, pos, &ttype, 1, DCT_INT, DCT_Comm_World);
   MPI_Unpack( rbuf, lbuf, pos, &uvtype, 1, DCT_INT, DCT_Comm_World);

   /* The DCT structure is created */
   DCTVar1->VarName = Name;
   DCTVar1->CoupleVarTypes = CVType;
   /* The var is added if it does not exist or continue if it does */
   varexist = DCT_List_CHKAdd_VarsOnModel( &DCT_Reg_Vars, DCTVar1, DCT_HCPLVAR_TYPE, model );
   if ( varexist == (DCT_List *)NULL ) {
      (*vartag)++;
      DCTERROR( *vartag > DCT_MAX_VARS, "Number of variable tag exhausted", return(1) );
      DCTVar1->VarTag = *vartag;
      DCTVar1->VarModel = model;
      DCTVar1->VarUnits = (DCT_Units) tunit;
      DCTVar1->VarTimeUnits = (DCT_Time_Types) ttype;
      DCTVar1->VarTimeIni = TimeIni;
      DCTVar1->VarFrequency = Time;
      DCTVar1->VarProduced = (DCT_ProdCons) tproc;
      DCTVar1->VarUserDataType = (DCT_Data_Types) uvtype;

   }
   else {
      DCTVar2 = (DCT_Hollow_CPLVar *) varexist->AddressData;
      if ( DCTVar2->CoupleVarTypes == CVType ) {
         free( Name );
         free( DCTVar1 );
         DCTVar1 = DCTVar2;
         Name = DCTVar1->VarName;
         DCTVar2 = (DCT_Hollow_CPLVar *)NULL;
      } else {
         DCTERROR( DCTVar2->CoupleVarTypes != CVType,
                  "Variable received does not have the same type as a varible registered",
                  return(1) );
      }
   }
/*                                                   BEGIN DEBUG */
//          printf("[DCT_UnPacking_Vars] DCTVar1->VarName %s, VAR TAG: %d\n\n",
//                                                            DCTVar1->VarName, *vartag );
/*                                                     END DEBUG */

   
   if ( *modela == DCT_TRUE ) { /* The incoming model is ModelA */
      cnvar = couple->CoupleNumVars;
      DCTERROR( cnvar == couple->CoupleTotNumVars, "Internal error, Couple vars full",
                return(1) );
      /* No other model vars registered */
      if ( couple->CoupleNumVars2 == 0) {
         /*** The ModelB's Hollow cpl variable is allocated **/
         DCTVar2 = (DCT_Hollow_CPLVar *) malloc( sizeof(DCT_Hollow_CPLVar) );
         DCTERROR( DCTVar2 == (DCT_Hollow_CPLVar *)NULL, "Memory Allocation Failed", return(1) );
         /* The Name is set */
         DCTVar2->VarName = nCvar2;

         /* Couple node is created */
         cplnode = (DCT_CPL_Node *) malloc(sizeof(DCT_CPL_Node));
         DCTERROR( cplnode == (DCT_CPL_Node *)NULL, "Memory Allocation Failed", return(1) );
      
         cplnode->CoupleVars1 = DCTVar1; /* Store de Var structure */
         cplnode->CoupleVars2 = DCTVar2; /* Store the couterpart structure */
         cplnode->CVarType = DCT_HCPLVAR_TYPE; /* Store the var type */
         cplnode->CVarTrans = CVarTrans; /* Store the transformation */
         
         current = (DCT_List *) couple->CouplingTable;
         current = DCT_List_Add ( &current, cplnode, DCT_CPL_NODE_TYPE);
         couple->CouplingTable = (DCT_VoidPointer) current;
         couple->CoupleNumVars = ++cnvar; /* The variable is counted */
      } else { /* ModelB's varibles are already loaded. They will be paired */
        current = (DCT_List *) couple->CouplingTable;
        nfound = 1;
        while ( nfound && (current != (DCT_List *) NULL) ) {
           DCT_List_Ext ( &current, (DCT_VoidPointer *) &cplnode );
           
           DCTVar2 = (DCT_Hollow_CPLVar *) cplnode->CoupleVars1;
           if ( strcmp( DCTVar2->VarName, Name )==0 ) {
              DCTVar2 = (DCT_Hollow_CPLVar *) cplnode->CoupleVars2;
              if ( strcmp( DCTVar2->VarName, nCvar2 )==0 ) nfound = 0;
           }
        }
        
        DCTERROR( nfound, "Error, Couple vars name does not match", return(1) );
        DCTERROR( DCTVar2->CoupleVarTypes != CVType,
                 "Error, Couple vars type does not match", return(1) );
        if ( tproc == DCT_CONSUME ) cplnode->CVarTrans = CVarTrans;

        DCTERROR( DCTVar2->VarProduced == (DCT_ProdCons)tproc,
                       "Error, Couple vars must pair Produce/Consume", return(1) );
        /* The previous temporary Variable is deleted */
        DCTVar2 = (DCT_Hollow_CPLVar *) cplnode->CoupleVars1;
        free( DCTVar2->VarName );
        free ( DCTVar2 ); /* The previous Hollow_DCT_Var is freed */
        
        /** The counter variable name is freed **/
        free( nCvar2 );

        cplnode->CoupleVars1 = DCTVar1; /* The actual structure is plugged */
        couple->CoupleNumVars = ++cnvar; /* The variable is counted */
      }
   
   } else {  /* The incoming model is ModelB */
      cnvar = couple->CoupleNumVars2;
      DCTERROR( cnvar == couple->CoupleTotNumVars, "Internal error, Couple vars full",
                return(1) );
      /* No other model vars registered */
      if ( couple->CoupleNumVars == 0) {
         /*** The ModelA's Hollow cpl variable is allocated **/
         DCTVar2 = (DCT_Hollow_CPLVar *) malloc( sizeof(DCT_Hollow_CPLVar) );
         DCTERROR( DCTVar2 == (DCT_Hollow_CPLVar *)NULL, "Memory Allocation Failed", return(1) );
         /* The Name is set */
         DCTVar2->VarName = nCvar2;

         /* Couple node is created */
         cplnode = (DCT_CPL_Node *) malloc(sizeof(DCT_CPL_Node));
         DCTERROR( cplnode == (DCT_CPL_Node *)NULL, "Memory Allocation Failed", return(1) );
      
         cplnode->CoupleVars1 = DCTVar2; /* Store de Var structure */
         cplnode->CoupleVars2 = DCTVar1; /* Store the couterpart structure */
         cplnode->CVarType = DCT_HCPLVAR_TYPE; /* Store the var type */
         cplnode->CVarTrans = CVarTrans; /* Store the transformation */

         current = (DCT_List *) couple->CouplingTable;
         current = DCT_List_Add ( &current, cplnode, DCT_CPL_NODE_TYPE);
         couple->CouplingTable = (DCT_VoidPointer) current;
         couple->CoupleNumVars2 = ++cnvar; /* The variable is counted */
      } else { /* ModelA's varibles are already loaded. They will be paired */
        current = (DCT_List *) couple->CouplingTable;
        nfound = 1;
        while ( nfound && (current != (DCT_List *) NULL) ) {
           DCT_List_Ext ( &current, (DCT_VoidPointer *) &cplnode );
           switch (cplnode->CVarType) {
             case DCT_FIELD_TYPE:
               field  = (DCT_Field *) cplnode->CoupleVars1;
               HCVar2 = (DCT_Hollow_Var *) cplnode->CoupleVars2;
               if ( strcmp( HCVar2->VarName, Name )==0 ) {
                  if ( strcmp( field->VarName, nCvar2 )==0 ) {
                     nfound = 0;
                     tproc2 = field->VarProduced;
                  }
               }
               break;
             case DCT_3D_VAR_TYPE:
               var3d  = (DCT_3d_Var *) cplnode->CoupleVars1;
               HCVar2 = (DCT_Hollow_Var *) cplnode->CoupleVars2;
               if ( strcmp( HCVar2->VarName, Name )==0 ) {
                  if ( strcmp( var3d->VarName, nCvar2 )==0 ) {
                     nfound = 0;
                     tproc2 = var3d->VarProduced;
                  }
               }
               break;
             case DCT_4D_VAR_TYPE:
               var4d  = (DCT_4d_Var *) cplnode->CoupleVars1;
               HCVar2 = (DCT_Hollow_Var *) cplnode->CoupleVars2;
               if ( strcmp( HCVar2->VarName, Name )==0 ) {
                  if ( strcmp( var4d->VarName, nCvar2 )==0 ) {
                     nfound = 0;
                     tproc2 = var4d->VarProduced;
                  }
               }
               break;
             case DCT_HCPLVAR_TYPE:
               DCTVar2 = (DCT_Hollow_CPLVar *) cplnode->CoupleVars2;
               if ( strcmp( DCTVar2->VarName, Name )==0 ) {
                  DCTVar2 = (DCT_Hollow_CPLVar *) cplnode->CoupleVars1;
                  if ( strcmp( DCTVar2->VarName, nCvar2 )==0 ) {
                     nfound = 0;
                     tproc2 = DCTVar2->VarProduced;
                  }
               }
               break;
             default:
               DCTERROR( DCT_TRUE, "Error, cplnode->CVarType wrong type", return(1) );
           }
        }
        
        DCTERROR( nfound, "Error, Couple vars name does not match", return(1) );
        if ( cplnode->CVarType == DCT_HCPLVAR_TYPE ) {
           DCTERROR( DCTVar2->CoupleVarTypes != CVType,
                    "Error, Coupled vars type does not match", return(1) );
           DCTERROR( tproc2 == (DCT_ProdCons)tproc,
                          "Error, Coupled vars must pair Produce/Consume", return(1) );
           if ( tproc == DCT_CONSUME ) cplnode->CVarTrans = CVarTrans;
   
           /* The previous temporary Variable is deleted */
           DCTVar2 = (DCT_Hollow_CPLVar *) cplnode->CoupleVars2;
           free( DCTVar2->VarName );
           free ( DCTVar2 ); /* The previous Hollow_DCT_Var is freed */
        } else {   /*** Local couple structure ***/
           DCTERROR( cplnode->CVarType != CVType,
                    "Error, Coupled vars type does not match", return(1) );
           DCTERROR( tproc2 == (DCT_ProdCons)tproc,
                          "Error, Coupled vars must pair Produce/Consume", return(1) );
           if ( tproc == DCT_CONSUME ) cplnode->CVarTrans = CVarTrans;
   
           /* The previous temporary Variable is deleted */
           free( HCVar2->VarName );
           free ( HCVar2 ); /* The previous Hollow_DCT_Var is freed */
        
        }
        
        /** The counter variable name is freed **/
        free( nCvar2 );

        cplnode->CoupleVars2 = DCTVar1; /* The actual structure is plugged */
        couple->CoupleNumVars2 = ++cnvar; /* The variable is counted */
      }
   
   } /* End of if ( *modela == DCT_TRUE ) */
   
   return(err);
/* ------------------------------------  END( DCT_UnPacking_Vars ) */
}

int DCT_Mutual_ModReg( DCT_Couple **lcpl )
{
/* ---------------------------------------------  Local Variables  */

   DCT_Model               *ModelA;
   DCT_Couple              *couple;
   DCT_Boolean              modela;
   int                      err=0;
   
   int                      pos, lpck, pckcnt, broker;
   
   char                    *sbuf, *rbuf;
   DCT_Rank                 cntrrank, ldrrank;
   DCT_Tag                  msgtag;
   
   DCT_Status               status;
   DCT_Request              req;
   DCT_Comm                 grpcomm;

   register int             icpl;

/* -----------------------------------  BEGIN( DCT_Mutual_ModReg ) */
   broker = DCT_procinfo.ProcDCTBroker == DCT_procinfo.ProcDCTRank;

   /** For every couple, the mutual model is registered **/
   for ( icpl = 0; icpl < DCT_LocalCouple; icpl++ ) {
      couple = lcpl[icpl];

      /* Building the message tag */
      msgtag = DCT_Msg_Tags_CNTR_MOD_DTLS + 10000*couple->CoupleTag;
      cntrrank = couple->CoupleHModelB->ModLeadRank;

      ModelA = couple->CoupleModelA;
      
      /* Counting Packing the model */
      err = DCT_PckSize_Model( ModelA, &pckcnt, DCT_Comm_World );
      if (err) return(err);
      lpck = pckcnt;
   
      /********** Allocating memory for the package buffer ************/
      sbuf = (char *) malloc( (size_t)lpck );
      DCTERROR( sbuf == (char *) NULL, "Memory Allocation Failed", return(1) );
         
      /************ Packing ************/
      pos = 0;
      err = DCT_Packing_Model( ModelA, sbuf, lpck, &pos, DCT_Comm_World );
      if (err) return(err);
      
/*                                                   BEGIN DEBUG */
//       printf("[DCT_Mutual_ModReg]: Sender %d, Pack Model: %d\n\n", DCT_procinfo.ProcDCTRank, pos );
//       printf("[DCT_Mutual_ModReg]: Sender %d, Sent MSG tag: %d\n\n",
//                                         DCT_procinfo.ProcDCTRank, msgtag );
/*                                                     END DEBUG */
      
      /************ The Package is sent ************/
      err = MPI_Isend ( sbuf, pos, MPI_PACKED, cntrrank, msgtag, DCT_Comm_World, &req );
      if (err) return(err);


      ldrrank = ModelA->ModLeadRank;
      grpcomm = ModelA->ModGroupComm;
      
      err = MPI_Probe( cntrrank, msgtag, DCT_Comm_World, &status);
      if (err) return(err);
      MPI_Get_count( &status, MPI_PACKED, &lpck );
      
      /** The model package size is sent **/
      MPI_Bcast( &lpck, 1, DCT_INT, ldrrank, grpcomm );

/*                                                   BEGIN DEBUG */
//       printf("[DCT_Mutual_ModReg]: Receiver %d from %d, Model Mssg Lenght: %d\n\n", DCT_procinfo.ProcDCTRank, rcvr, lpck );
/*                                                     END DEBUG */
      /* Allocating memory for the package buffer */
      rbuf = (char *) malloc( (size_t)lpck );
      DCTERROR( rbuf == (char *) NULL, "Memory Allocation Failed", return(1) );
      
      err = MPI_Recv( rbuf, lpck, MPI_PACKED, cntrrank, msgtag, DCT_Comm_World, &status );
      if (err) return(err);
      
      /** Broadcast to the rest model process **/
      err = MPI_Bcast( rbuf, lpck, MPI_PACKED, ldrrank, grpcomm );
      if (err) return (err);

      /************ UnPacking ************/
      if ( !broker ) {
         ModelA = (DCT_Model *) malloc(sizeof(DCT_Model));
         DCTERROR( ModelA == (DCT_Model *)NULL, "Memory Allocation Failed", return(1) );
         couple->CoupleModelB = ModelA;

      }
      pos = 0;
      err = DCT_UnPacking_Model( couple, &modela, broker, rbuf, lpck, &pos, DCT_Comm_World );
      if (err) return(err);
      DCTERROR( modela == DCT_TRUE, "Received wrong model", return(1) );

      /** Be sure the message was delivered **/
      MPI_Wait(&req, MPI_STATUS_IGNORE);

      free ( sbuf );
      free ( rbuf );
   } /* End of for ( icpl = 0; icpl < DCT_LocalCouple; icpl++ ) */
   
   
   return(err);
/* -------------------------------------  END( DCT_Mutual_ModReg ) */
}

int DCT_Recv_ModReg ( DCT_Couple **lcpl )
{
/* ---------------------------------------------  Local Variables  */

   DCT_Model               *ModelB, *ModelA;
   DCT_Couple              *couple;
   DCT_Boolean              modela;
   int                      err=0;
   int                      nobroker = 0;
   int                      pos, lpck;
   
   char                    *rbuf;
   DCT_Rank                 ldrrank;

   DCT_Comm                 grpcomm;

   register int             icpl;

/* -------------------------------------  BEGIN( DCT_Recv_ModReg ) */

   /** For ecery couple, the mutual model is registered **/
   for ( icpl = 0; icpl < DCT_LocalCouple; icpl++ ){
      couple = lcpl[icpl];

      ModelA = couple->CoupleModelA;
      
      ldrrank = ModelA->ModLeadRank;
      grpcomm = ModelA->ModGroupComm;

      /** The model package size is received **/
      MPI_Bcast( &lpck, 1, DCT_INT, ldrrank, grpcomm );

/*                                                   BEGIN DEBUG */
//       printf("[DCT_Mutual_ModReg]: Receiver %d from %d, Model Mssg Lenght: %d\n\n", DCT_procinfo.ProcDCTRank, rcvr, lpck );
/*                                                     END DEBUG */
      /* Allocating memory for the package buffer */
      rbuf = (char *) malloc( (size_t)lpck );
      DCTERROR( rbuf == (char *) NULL, "Memory Allocation Failed", return(1) );
      
      /** Broadcast to the rest model process **/
      err = MPI_Bcast( rbuf, lpck, MPI_PACKED, ldrrank, grpcomm );
      if (err) return(err);
      
      /************ UnPacking ************/
      ModelB = (DCT_Model *) malloc(sizeof(DCT_Model));
      DCTERROR( ModelB == (DCT_Model *)NULL, "Memory Allocation Failed",return(1) );
      couple->CoupleModelB = ModelB;
      
      pos = 0;
      err = DCT_UnPacking_Model( couple, &modela, nobroker, rbuf, lpck, &pos, DCT_Comm_World );
      if (err) return(err);
      DCTERROR( modela == DCT_TRUE, "Received wrong model", return(1) );

      /** Check the message was sent **/
      free ( rbuf );
      
   }
   
   return(err);
/* ---------------------------------------  END( DCT_Recv_ModReg ) */
}


/******************************************************************/
/*                         DCT_Send_Var                           */
/*                                                                */
/*!    This routine is called by the model designated leaders to
       send the variable information of the DCT_Vars to the DCT
       broker.

    \param [in] ncplmsg  Array of pointer to DCT_Couple declared
                         locally en the process.

    \todo According with the structure of the DCT library and the
          same level of the broker corresponding function, a
          interface function, or part of the functionality from
          here must to move to the dct_broker.c file.
    
    \return An error handler variable.

*/
/******************************************************************/
int DCT_Send_Var( DCT_Couple **lcpl )
{
/* ---------------------------------------------  Local Variables  */

   DCT_Model               *ModelA;
   DCT_Couple              *couple;
   DCT_CPL_Node            *cplnode;
   DCT_Hollow_Var          *HCVar2;

   DCT_List                *current;

   int                      err=0;
   
   int                      slim, pos, lpck, pckcnt, nlen;
   
   char                    *sbuf;
   DCT_Tag                  msgtag;

   register int             icpl, ii;

/* ----------------------------------------  BEGIN( DCT_Send_Var ) */

   /* Building the message tag */
   msgtag = DCT_Msg_Tags_MODVAR_REG;
   /** Number of couple declared locally **/
   MPI_Pack_size( 1, DCT_INT, DCT_Comm_World, &pckcnt);
   lpck = pckcnt;
   /** All couple tags **/
   MPI_Pack_size( DCT_LocalCouple, DCT_TAG, DCT_Comm_World, &pckcnt);
   lpck += pckcnt;
   for ( icpl = 0; icpl < DCT_LocalCouple; icpl++ ){
      couple = lcpl[icpl];

      ModelA = couple->CoupleModelA;
   
      /* Counting the size of the package */
      
      /* Counting Packing the model header */
      MPI_Pack_size( 1, DCT_INT, DCT_Comm_World, &pckcnt); /* len */
      lpck += pckcnt;
      nlen = strlen( ModelA->ModName );
      MPI_Pack_size( nlen, DCT_CHAR, DCT_Comm_World, &pckcnt); /* Model Name */
      lpck += pckcnt;

      /* Counting Packing Variables */
      /* The variables to be transmited are those actually used IN the couple structure */
      /* So, a variable in a Model can be registered to be used in another couple */
      slim = (int) couple->CoupleTotNumVars;
   
      current = (DCT_List *)couple->CouplingTable;
      for ( ii=0; (ii < slim) && (!err); ii++ ) {
         DCT_List_Ext ( &current, (DCT_VoidPointer *) &cplnode );
         HCVar2 = (DCT_Hollow_Var *) cplnode->CoupleVars2;
         err = DCT_PckSize_Vars( cplnode->CVarType, cplnode->CoupleVars1,
                                                HCVar2->VarName, &pckcnt, DCT_Comm_World );
         if (err) return(err);
         lpck += pckcnt;
      }
   }

   /********** Allocating memory for the package buffer ************/
   sbuf = (char *) malloc( (size_t)lpck );
   DCTERROR( sbuf == (char *) NULL, "Memory Allocation Failed", return(1) );
   
/*                                                   BEGIN DEBUG */
//    printf("[DCT_Send_ModVar]: Sender %d, Couple Lenght: %d\n\n", DCT_procinfo.ProcDCTRank, lpck );
/*                                                     END DEBUG */
      
   /************ Packing ************/
   pos = 0;
   err = MPI_Pack( &DCT_LocalCouple, 1, DCT_INT, sbuf, lpck, &pos, DCT_Comm_World);
   if (err) return(err);
   for ( icpl = 0; icpl < DCT_LocalCouple; icpl++ ){
      couple = lcpl[icpl];
      err = MPI_Pack( &(couple->CoupleTag), 1, DCT_TAG, sbuf, lpck, &pos, DCT_Comm_World);
      if (err) return(err);
      ModelA = couple->CoupleModelA;
   
      nlen = strlen( ModelA->ModName );
      MPI_Pack( &nlen, 1, DCT_INT, sbuf, lpck, &pos, DCT_Comm_World); 
      MPI_Pack( ModelA->ModName, nlen, DCT_CHAR, sbuf, lpck, &pos, DCT_Comm_World);
   
      /* Counting Packing Variables */
      /* The variables to be transmited are those actually used IN the couple structure */
      /* So, a variable in a Model can be registered to be used in another couple */
      slim = (int) couple->CoupleTotNumVars;

      current = (DCT_List *)couple->CouplingTable;
      for ( ii=0; (ii < slim) && (!err); ii++ ) {
         DCT_List_Ext ( &current, (DCT_VoidPointer *)&cplnode );
         HCVar2 = (DCT_Hollow_Var *) cplnode->CoupleVars2;
         err = DCT_Packing_Vars( &(cplnode->CVarType), cplnode->CoupleVars1, HCVar2->VarName,
                                  cplnode->CVarTrans, sbuf, lpck, &pos, DCT_Comm_World );         
         if (err) return(err);
      }
   }
/*                                                   BEGIN DEBUG */
//    printf("[DCT_Send_ModVar]: Sender %d, Pack Model & Vars Lenght: %d\n\n", DCT_procinfo.ProcDCTRank, pos );
//    printf("[DCT_Send_ModVar]: Sender %d, Sent MSG tag: %d\n\n",
//                                      DCT_procinfo.ProcDCTRank, msgtag );
/*                                                     END DEBUG */
   
   /************ The Package is sent ************/
   err = MPI_Send ( sbuf, pos, MPI_PACKED, DCT_procinfo.ProcDCTBroker, msgtag,
                    DCT_Comm_World);

   free ( sbuf );

   return(err);
/* ------------------------------------------  END( DCT_Send_Var ) */
}


int DCT_Get_Var( DCT_Tag *vartag )
{
/* ---------------------------------------------  Local Variables  */
   int err=0;
   
   DCT_List       *current;
   DCT_Couple     *couple;
   
   DCT_Name        ModelName;
   
   DCT_Tag         cpltag;
   char           *rbuf;
   DCT_Status      status;
   DCT_Boolean     modela;
   
   int             slim, pos, lpck, nclp, nfound, nlen;

   int             rcvr;

   register int    ii, icpl;

/* -----------------------------------------  BEGIN( DCT_Get_Var ) */

   err = MPI_Probe(MPI_ANY_SOURCE, DCT_Msg_Tags_MODVAR_REG, DCT_Comm_World, &status);
   if (err) return(err);
   rcvr = status.MPI_SOURCE;
   MPI_Get_count( &status, MPI_PACKED, &lpck );
   
/*                                                   BEGIN DEBUG */
//    printf("[DCT_Get_ModVar]: Receiver %d from %d, Model Mssg Lenght: %d\n\n", DCT_procinfo.ProcDCTRank, rcvr, lpck );
/*                                                     END DEBUG */
   
   /* Allocating memory for the package buffer */
   rbuf = (char *) malloc( (size_t)lpck );
   DCTERROR( rbuf == (char *) NULL, "Memory Allocation Failed", return(1) );
   
   err = MPI_Recv( rbuf, lpck, MPI_PACKED, rcvr, status.MPI_TAG, DCT_Comm_World, &status );
   if (err) return(err);
 
   /************ UnPacking ************/
   pos = 0;
   MPI_Unpack(rbuf, lpck, &pos, &nclp, 1, DCT_INT, DCT_Comm_World);
   for ( icpl = 0; icpl < nclp; icpl++ ){
      
      MPI_Unpack(rbuf, lpck, &pos, &cpltag, 1, DCT_TAG, DCT_Comm_World);
      /* Serching the DCT_Couple */
      nfound = 1;
      current = DCT_Reg_Couple; /* Restart the list */
      /* serching in the list */
      while ( nfound && (current != (DCT_List *) NULL) ) {
         DCT_List_Ext ( &current, (DCT_VoidPointer *) &couple );
         nfound = ( couple->CoupleTag != cpltag );
      }
      DCTERROR( nfound, "Error receiving Model. Couple Tag is not registered", return(1) );
      
      /************ UnPacking ************/
      /* UnPacking the model */
      /* Unpacking length of couple and models names */
      /** Why the couple's and model's names are required sice they have
          valid tags? It would be more efficient using the tag            **/
      MPI_Unpack(rbuf, lpck, &pos, &nlen, 1, DCT_INT, DCT_Comm_World);
      
      ModelName = (DCT_Name) malloc( sizeof(char)*(size_t)(nlen+1) );
      DCTERROR( ModelName == (DCT_Name)NULL, "Memory Allocation Failed", return(1) );
      /*! \todo Why don't use the model tag in this stage rather than model's name */
      /** The info in the message is unpacked **/
      MPI_Unpack( rbuf, lpck, &pos, ModelName, nlen, DCT_CHAR, DCT_Comm_World );
      ModelName[nlen]='\0';
      /**  Check which model is received  **/
      if ( strcmp( couple->CoupleModelA->ModName, ModelName )==0 ) {
         modela = DCT_TRUE;
      }
      else if ( strcmp( couple->CoupleModelB->ModName, ModelName )==0 ) {
         modela = DCT_FALSE;
      }
      else {
         DCTERROR( DCT_TRUE, "Internal error, bad model name in coupler", return(1) );
      }
   
      slim = (int) couple->CoupleTotNumVars;
      for ( ii=0; (ii < slim) && (!err); ii++ ) {
         err = DCT_UnPacking_Vars( couple, &modela, vartag, rbuf, lpck, &pos, DCT_Comm_World );
         if (err) return(err);
      }
      
      free( ModelName );
   }

/*                                                   BEGIN DEBUG */
//    printf("[DCT_Get_ModVar]: Receiver %d, UnPack Model & Vars Lenght: %d\n\n", DCT_procinfo.ProcDCTRank,
//           pos );
/*                                                     END DEBUG */
      
   free( rbuf );

   return(err);
/* -------------------------------------------  END( DCT_Get_Var ) */
}

int DCT_Broker_Var_Plan( DCT_Tag *vtag, DCT_Couple *couple, int nvar )
{
/* ---------------------------------------------  Local Variables  */
   int                      err=0;

   DCT_Object_Types         CVarTypes;

   DCT_Hollow_CPLVar       *DCTVar1;

   DCT_List               *current;
   DCT_CPL_Node           *cplnode;
   DCT_CPL_Vars           *CPLMasterT;
   
   DCT_ProdCons            vprod;
   
   DCT_Field               *field;
   DCT_3d_Var              *var3d;
   DCT_4d_Var              *var4d;
   
   DCT_Integer              vind;
   
   int                      dim;
   
   register int             ii, jj;

/* ---------------------------------  BEGIN( DCT_Broker_Var_Plan ) */

   /* The domain dimension */
   dim = couple->CoupleModelA->ModDomain->ModDomDim;

   /* Last master table index */
   jj = DCT_CPL_Table.TotNumCPLVars;
   couple->CoupleFirstVarCPL = jj;
   
   CPLMasterT = &(DCT_CPL_Table.CPLMasterTable);
   current = (DCT_List *) couple->CouplingTable;
   /* The coupled var list is sweep */
   for ( ii=0; (ii < nvar) && (current != (DCT_List *) NULL ); ii++ ) {
      /* The information is stored in the CPL Master table */
      DCT_List_Ext ( &current, (DCT_VoidPointer *) &cplnode );
      CVarTypes = cplnode->CVarType;
      CPLMasterT->CPLVarLocal[jj] =  cplnode->CoupleVars1;
      // CPLMasterT->CPLVarRemote[jj] = cplnode->CoupleVars2;/* Make this later at the end */
      CPLMasterT->CPLVarTypes[jj] = CVarTypes;
      switch ( CVarTypes )
      {
         case DCT_FIELD_TYPE:
            field = (DCT_Field *) cplnode->CoupleVars1;
            vprod = field->VarProduced;
            vind = (field->VarNumCpl)++; /* Assing and increment later */
            field->VarCplIndx[vind] = jj;
            
            break;
         case DCT_3D_VAR_TYPE:
            var3d = (DCT_3d_Var *) cplnode->CoupleVars1;
            vprod = var3d->VarProduced;
            vind = (var3d->VarNumCpl)++; /* Assing and increment later */
            var3d->VarCplIndx[vind] = jj;
            
            break;
         case DCT_4D_VAR_TYPE:
            var4d = (DCT_4d_Var *) cplnode->CoupleVars1;
            vprod = var4d->VarProduced;
            vind = (var4d->VarNumCpl)++; /* Assing and increment later */
            var4d->VarCplIndx[vind] = jj;
            
      }  /* End of  switch ( CVarTypes ) */
      CPLMasterT->CPLVarProd[jj] = vprod;
      if ( vprod == DCT_PRODUCE ) {
         CPLMasterT->CPLDataComm[jj] = DCT_ProdPlan_Init( vtag[ii], DCT_NO_MSG );
         DCTERROR( CPLMasterT->CPLDataComm[jj] == (DCT_Produce_Plan *)NULL, 
                      "Memory Allocation Failed", return(1) );
      }
      else {
         DCTVar1 = (DCT_Hollow_CPLVar *) cplnode->CoupleVars2;
         CPLMasterT->CPLDataComm[jj] = DCT_ConsPlan_Init( dim, vtag[ii], cplnode->CVarTrans,
                                    DCTVar1->VarUnits, DCTVar1->VarUserDataType, DCT_NO_MSG );
         DCTERROR( CPLMasterT->CPLDataComm[jj] == (DCT_Consume_Plan *)NULL, 
                      "Memory Allocation Failed", return(1) );
      }
    
      jj++;  /* The variable en the master table is counted */
   } /* End of while ( current != (DCT_List *) NULL ) */
   DCT_CPL_Table.TotNumCPLVars = jj;


   return(err);
/* -----------------------------------  END( DCT_Broker_Var_Plan ) */
}

int DCT_PckSize_Var_Plan( DCT_Boolean modela, int *llvname, int *lrvname, DCT_ProdCons *vprod,
                             int nvar, int *cpck, DCT_Comm DCT_Comm_World )
                            
                            
{
/* ---------------------------------------------  Local Variables  */
   int                      err=0;

   int                      pckcnt;

   register int             ii;

/* -----------------------------  BEGIN( DCT_PckSize_Var_Plan ) */

   /* Variables Tags */
   MPI_Pack_size( 1, DCT_INT, DCT_Comm_World, &pckcnt); /* Number of vars */
   *cpck = pckcnt;
   MPI_Pack_size( 2*nvar, DCT_TAG, DCT_Comm_World, &pckcnt); /* VarTags and MsgTags */
   *cpck += pckcnt;
   
   /* The coupled var list is sweep */
   for ( ii=0; ii < nvar; ii++ ) {
      MPI_Pack_size( 2, DCT_INT, DCT_Comm_World, &pckcnt); /* Local and Remote Name length */
      *cpck += pckcnt;
      MPI_Pack_size( llvname[ii], DCT_CHAR, DCT_Comm_World, &pckcnt);/* Local var name length */
      *cpck += pckcnt;
      MPI_Pack_size( lrvname[ii], DCT_CHAR, DCT_Comm_World, &pckcnt);/* Remote var name length */
      *cpck += pckcnt;
      MPI_Pack_size( 2, DCT_SCALAR, DCT_Comm_World, &pckcnt); /* VarInitTime & VarUpdateInter */
      *cpck += pckcnt;
      if ( ( (vprod[ii] == DCT_CONSUME) && ( modela == DCT_TRUE  ) ) ||
           ( (vprod[ii] == DCT_PRODUCE) && ( modela == DCT_FALSE ) )    ){
         MPI_Pack_size( 2, DCT_INT, DCT_Comm_World, &pckcnt); /* VarUnit & VarUserDataType */
         *cpck += pckcnt;      
      }
   }

   return(err);
/* ----------------------------------  END( DCT_PckSize_Var_Plan ) */
}

int DCT_Packing_Var_Plan ( const DCT_Couple *couple, DCT_Boolean modela, DCT_Tag *vtag,
                              int *llvname, int *lrvname, DCT_ProdCons *vprod, int nvar,
                              void *sbuf, int lbuf, int *pos, DCT_Comm DCT_Comm_World )
{
/* ---------------------------------------------  Local Variables  */
   int                      err=0;
   
   DCT_Name                 name;
   
   DCT_List                *current;
   DCT_CPL_Node            *cplnode;
   
   DCT_Field               *field;
   DCT_3d_Var              *var3d;
   DCT_4d_Var              *var4d;

   DCT_Hollow_CPLVar       *DCTVar1;
   DCT_Hollow_CPLVar       *DCTVar2;

   DCT_Scalar               chtime, initime, timefreq;
   DCT_Time_Types           timeu;
   DCT_Units                vunit;
   DCT_Data_Types           vudata;
   
   DCT_Tag                  tag;
   
   register int             ii;

/* --------------------------------  BEGIN( DCT_Packing_Var_Plan ) */

   /* Variables Tags are packed */
   /* Number of vars */
   MPI_Pack( &nvar, 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);
   /* Msgs Tags */
   MPI_Pack( vtag, nvar, DCT_TAG, sbuf, lbuf, pos, DCT_Comm_World);

   /* The varcpl table is pointed */
   current = (DCT_List *) couple->CouplingTable;
   if ( modela == DCT_TRUE ) {
      for ( ii=0; (ii < nvar) && (current != (DCT_List *) NULL ); ii++ ) {
         /* The information is stored in the CPL Master table */
         DCT_List_Ext ( &current, (DCT_VoidPointer *) &cplnode );
         switch ( cplnode->CVarType )
         {
            case DCT_FIELD_TYPE:
               field = (DCT_Field *) cplnode->CoupleVars1;
               name = field->VarName;
               timeu = field->VarTimeUnits;
               tag  = field->VarTag;
   
               break;
            case DCT_3D_VAR_TYPE:
               var3d = (DCT_3d_Var *) cplnode->CoupleVars1;
               name = var3d->VarName;
               timeu = var3d->VarTimeUnits;
               tag  = var3d->VarTag;
   
               break;
            case DCT_4D_VAR_TYPE:
               var4d = (DCT_4d_Var *) cplnode->CoupleVars1;
               name = var4d->VarName;
               timeu = var4d->VarTimeUnits;
               tag  = var4d->VarTag;
   
               break;
            case DCT_HCPLVAR_TYPE:
               DCTVar1 = (DCT_Hollow_CPLVar *) cplnode->CoupleVars1;
               name = DCTVar1->VarName;
               timeu = DCTVar1->VarTimeUnits;
               tag  = DCTVar1->VarTag;
   
         }  /* End of  switch ( CVarTypes ) */
   
         DCTVar2 = (DCT_Hollow_CPLVar *) cplnode->CoupleVars2; 

         /* The local variable length */
         MPI_Pack( (llvname +ii), 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);
         /* The local variable name */
         MPI_Pack( name, llvname[ii], DCT_CHAR, sbuf, lbuf, pos, DCT_Comm_World);

         /* The remote variable length */
         MPI_Pack( (lrvname +ii), 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);
         /* The remote variable name */
         MPI_Pack( DCTVar2->VarName, lrvname[ii], DCT_CHAR, sbuf, lbuf, pos, DCT_Comm_World);

         /* Model A Actual VarTag */
         MPI_Pack( &tag, 1, DCT_TAG, sbuf, lbuf, pos, DCT_Comm_World);

         chtime = DCT_Change_TimeUnit( DCTVar2->VarTimeIni, DCTVar2->VarTimeUnits, timeu );
         MPI_Pack( &chtime, 1, DCT_SCALAR, sbuf, lbuf, pos, DCT_Comm_World);
         chtime = DCT_Change_TimeUnit( DCTVar2->VarFrequency, DCTVar2->VarTimeUnits, timeu );
         MPI_Pack( &chtime, 1, DCT_SCALAR, sbuf, lbuf, pos, DCT_Comm_World);

         if ( vprod[ii] == DCT_CONSUME ) {
            MPI_Pack( &(DCTVar2->VarUnits), 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);
            MPI_Pack( &(DCTVar2->VarUserDataType), 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);
         }
      } /* End of for ( ii=0; (ii < nvar) && (current != (DCT_List *) NULL ); ii++ ) */
   }
   else {  /* modela == DCT_FALSE  */
      for ( ii=0; (ii < nvar) && (current != (DCT_List *) NULL ); ii++ ) {
         /* The information is stored in the CPL Master table */
         DCT_List_Ext ( &current, (DCT_VoidPointer *) &cplnode );
         switch ( cplnode->CVarType )
         {
            case DCT_FIELD_TYPE:
               field = (DCT_Field *) cplnode->CoupleVars1;
               name = field->VarName;
               initime = field->VarTimeIni;
               timefreq = field->VarFrequency;
               timeu = field->VarTimeUnits;
               vunit = field->VarUnits;
               vudata = field->VarUserDataType;
   
               break;
            case DCT_3D_VAR_TYPE:
               var3d = (DCT_3d_Var *) cplnode->CoupleVars1;
               name = var3d->VarName;
               initime = var3d->VarTimeIni;
               timefreq = var3d->VarFrequency;
               timeu = var3d->VarTimeUnits;
               vunit = var3d->VarUnits;
               vudata = var3d->VarUserDataType;
   
               break;
            case DCT_4D_VAR_TYPE:
               var4d = (DCT_4d_Var *) cplnode->CoupleVars1;
               name = var4d->VarName;
               initime = var4d->VarTimeIni;
               timefreq = var4d->VarFrequency;
               timeu = var4d->VarTimeUnits;
               vunit = var4d->VarUnits;
               vudata = var4d->VarUserDataType;
   
               break;
            case DCT_HCPLVAR_TYPE:
               DCTVar1 = (DCT_Hollow_CPLVar *) cplnode->CoupleVars1;
               name = DCTVar1->VarName;
               initime = DCTVar1->VarTimeIni;
               timefreq = DCTVar1->VarFrequency;
               timeu = DCTVar1->VarTimeUnits;
               vunit = DCTVar1->VarUnits;
               vudata = DCTVar1->VarUserDataType;
   
         }  /* End of  switch ( CVarTypes ) */
   
         DCTVar2 = (DCT_Hollow_CPLVar *) cplnode->CoupleVars2; 

         /* The local variable length */
         MPI_Pack( (llvname +ii), 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);
         /* The local variable name */
         MPI_Pack( DCTVar2->VarName, llvname[ii], DCT_CHAR, sbuf, lbuf, pos, DCT_Comm_World);

         /* The remote variable length */
         MPI_Pack( (lrvname +ii), 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);
         /* The remote variable name */
         MPI_Pack( name, lrvname[ii], DCT_CHAR, sbuf, lbuf, pos, DCT_Comm_World);

         /* Model B Actual VarTag */
         MPI_Pack( &(DCTVar2->VarTag), 1, DCT_TAG, sbuf, lbuf, pos, DCT_Comm_World);

         chtime = DCT_Change_TimeUnit( initime, timeu, DCTVar2->VarTimeUnits );
         MPI_Pack( &chtime, 1, DCT_SCALAR, sbuf, lbuf, pos, DCT_Comm_World);
         chtime = DCT_Change_TimeUnit( timefreq, timeu, DCTVar2->VarTimeUnits );
         MPI_Pack( &chtime, 1, DCT_SCALAR, sbuf, lbuf, pos, DCT_Comm_World);

         if ( vprod[ii] == DCT_PRODUCE ) {
            MPI_Pack( &vunit, 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);
            MPI_Pack( &vudata, 1, DCT_INT, sbuf, lbuf, pos, DCT_Comm_World);
         }
      } /* End of for ( ii=0; (ii < nvar) && (current != (DCT_List *) NULL ); ii++ ) */
   }  /*  End of if ( modela == DCT_TRUE )  */

   return(err);
/* ----------------------------------  END( DCT_Packing_Var_Plan ) */
}

int DCT_UnPacking_Var_Plan( DCT_Couple *couple, void *rbuf, int lbuf, int *pos,
                               DCT_Comm DCT_Comm_World )
{
/* ---------------------------------------------  Local Variables  */
   int                      err=0;

   DCT_CPL_Vars            *CPLMasterT;
   DCT_List                *current;
   DCT_Hollow_Var          *HCVar2;
   
   DCT_CPL_Node            *cplnode;
   DCT_Field               *field;
   DCT_3d_Var              *var3d;
   DCT_4d_Var              *var4d;
   
   DCT_Name                 RName;
   DCT_Name                 LName;

   int                      nlen, dim, nvar;
   
   DCT_Tag                 *vtag;
   DCT_Tag                  vartag;
   DCT_ProdCons             vprod;
   DCT_Units                vunit;
   DCT_Data_Types           vudtype;

   register int             ii, jj, nfound, vind;

/* ------------------------------  BEGIN( DCT_UnPacking_Var_Plan ) */

   /* The domain dimension */
   dim = couple->CoupleModelA->ModDomain->ModDomDim;

   /* Number of vars */
   MPI_Unpack(rbuf, lbuf, pos, &nvar, 1, DCT_INT, DCT_Comm_World);
   DCTERROR( couple->CoupleNumVars != (DCT_Integer)nvar,
             "Internal Error. Number of variables sent and local do not match",
              return(1) );
   vtag = (DCT_Tag *) malloc( sizeof(DCT_Tag)*(size_t)nvar );
   DCTERROR( vtag == (DCT_Tag *)NULL, "Memory Allocation Failed", return(1) );
   
   /* Tags */
   MPI_Unpack(rbuf, lbuf, pos, vtag, nvar, DCT_TAG, DCT_Comm_World);

   /* Last master table index */
   jj = DCT_CPL_Table.TotNumCPLVars;
   couple->CoupleFirstVarCPL = jj; /* The initial index in the Master Table */
   
   CPLMasterT = &(DCT_CPL_Table.CPLMasterTable);
   /* Etracting the variables */
   for ( ii=0; ii < nvar; ii++ ) {

      /* Local Variable name */
      MPI_Unpack(rbuf, lbuf, pos, &nlen, 1, DCT_INT, DCT_Comm_World);
      LName = (DCT_Name) malloc( sizeof(char)*(size_t)(nlen + 1) );
      DCTERROR( LName == (DCT_Name)NULL, "Memory Allocation Failed", return(1) );
      MPI_Unpack(rbuf, lbuf, pos, LName, nlen, DCT_CHAR, DCT_Comm_World);
      LName[nlen] = '\0';

      /* Remote Variable name */
      MPI_Unpack(rbuf, lbuf, pos, &nlen, 1, DCT_INT, DCT_Comm_World);
      RName = (DCT_Name) malloc( sizeof(char)*(size_t)(nlen + 1) );
      DCTERROR( RName == (DCT_Name)NULL, "Memory Allocation Failed", return(1) );
      MPI_Unpack(rbuf, lbuf, pos, RName, nlen, DCT_CHAR, DCT_Comm_World);
      RName[nlen] = '\0';

      /* The var tag is retrived */
      MPI_Unpack(rbuf, lbuf, pos, &vartag, 1, DCT_TAG, DCT_Comm_World);

      /* The pair variable is searched in the couple list */
      /* The varcpl table is pointed */
      current = (DCT_List *) couple->CouplingTable;
      nfound = 1;
      while ( nfound && (current != (DCT_List *) NULL) ) {

         /* Match the remote name first */
         while ( nfound && (current != (DCT_List *) NULL) ) {
            DCT_List_Ext ( &current, (DCT_VoidPointer *) &cplnode );
            HCVar2 = (DCT_Hollow_Var *) cplnode->CoupleVars2;
            if ( strcmp( HCVar2->VarName, RName )==0 ) nfound = 0;
         }
            
         if ( nfound == 0 ) {
            switch (cplnode->CVarType) {
               case DCT_FIELD_TYPE:
                  field  = (DCT_Field *) cplnode->CoupleVars1;
                  if ( strcmp( field->VarName, LName )==0 ) {
                     vprod = field->VarProduced;
                     vind = (field->VarNumCpl)++; /* Assing and increment later */
                     field->VarCplIndx[vind] = jj;
                     if ( field->VarTag == DCT_TAG_UNDEF ) field->VarTag = vartag;
                  }
                  else nfound = 1;
               break;
               case DCT_3D_VAR_TYPE:
                  var3d  = (DCT_3d_Var *) cplnode->CoupleVars1;
                  if ( strcmp( var3d->VarName, LName )==0 ) {
                     vprod = var3d->VarProduced;
                     vind = (var3d->VarNumCpl)++; /* Assing and increment later */
                     var3d->VarCplIndx[vind] = jj;
                     if ( var3d->VarTag == DCT_TAG_UNDEF ) var3d->VarTag = vartag;
                  }
                  else nfound = 1;
               break;
               case DCT_4D_VAR_TYPE:
                  var4d  = (DCT_4d_Var *) cplnode->CoupleVars1;
                  if ( strcmp( var4d->VarName, LName )==0 ) {
                     vprod = var4d->VarProduced;
                     vind = (var4d->VarNumCpl)++; /* Assing and increment later */
                     var4d->VarCplIndx[vind] = jj;
                     if ( var4d->VarTag == DCT_TAG_UNDEF ) var4d->VarTag = vartag;
                  }
                  else nfound = 1;
               break;
               default:
               DCTERROR( DCT_TRUE, "Error, cplnode->CVarType wrong type", return(1) );
            } /* End of switch (cplnode->CVarType) */
         } /* End of if ( !nfound )  */
      } /* End of while ( nfound && (current != (DCT_List *) NULL) ) */
      DCTERROR( nfound, "Error, coupled variables not found", return(1) );
      
      /* The variables address are stored in the master list */
      CPLMasterT->CPLVarTypes[jj] = cplnode->CVarType;
      CPLMasterT->CPLVarLocal[jj] = cplnode->CoupleVars1;
      CPLMasterT->CPLVarRemote[jj] = HCVar2;

      /* The Initial Time and Frequency are set in the Hallow var */
      MPI_Unpack(rbuf, lbuf, pos, &(HCVar2->VarTimeIni), 1, DCT_SCALAR, DCT_Comm_World);
      MPI_Unpack(rbuf, lbuf, pos, &(HCVar2->VarFrequency), 1, DCT_SCALAR, DCT_Comm_World);
      CPLMasterT->CPLVarProd[jj] = vprod;
      if ( vprod == DCT_PRODUCE ) { 
         CPLMasterT->CPLDataComm[jj] = DCT_ProdPlan_Init( vtag[ii], DCT_NO_MSG );
         DCTERROR( CPLMasterT->CPLDataComm[jj] == (DCT_Produce_Plan *)NULL, 
                      "Memory Allocation Failed", return(1) );
      }
      else {
         MPI_Unpack(rbuf, lbuf, pos, &vunit, 1, DCT_INT, DCT_Comm_World);
         MPI_Unpack(rbuf, lbuf, pos, &vudtype, 1, DCT_INT, DCT_Comm_World);
         CPLMasterT->CPLDataComm[jj] = DCT_ConsPlan_Init( dim, vtag[ii], cplnode->CVarTrans,
                                    vunit, vudtype, DCT_NO_MSG );
         DCTERROR( CPLMasterT->CPLDataComm[jj] == (DCT_Consume_Plan *)NULL, 
                      "Memory Allocation Failed", return(1) );
      }
      jj++;  /* The variable en the master table is counted */

      free( LName );
      free( RName );
   } /* End of for ( ii=0; ii < nvar; ii++ ) */
   DCT_CPL_Table.TotNumCPLVars = jj;

   free( vtag );
   
   return(err);
/* --------------------------------  END( DCT_UnPacking_Var_Plan ) */
}

int DCT_Send_Var_Plan( DCT_Couple **lcpl )
{
/* ---------------------------------------------  Local Variables  */
   
   int                      err=0;
   
   DCT_List                *current;
   DCT_Couple              *couple;
   DCT_Model               *ModelA;
   int                      nvar, pos, lpck;
   
   char                   **sbuf;
   DCT_Tag                 *vtag;
   DCT_ProdCons            *vprod;
   int                     *lnameA;
   int                     *lnameB;

   DCT_Rank                 rcvr, ldrrank;
   DCT_Comm                 grpcomm;

   int                      nmsg;
   
   int                      size, stag;
   
   DCT_Request             *reqst;

   register int             icpl, ii, jj;
   

/* -----------------------------------  BEGIN( DCT_Send_Var_Plan ) */

   size = 2*DCT_MAX_COUPLE - DCT_LocalCouple; /* Calculate the maximum of messages */

   /* The maximum allocation of messages buffers */
   sbuf = (char **) malloc ( sizeof(char *)*(size_t)size );
   DCTERROR( sbuf == (char **)NULL, "Memory Allocation Failed", return(1) );
   reqst = (DCT_Request *) malloc( sizeof(DCT_Request)*(size_t)size );
   DCTERROR( reqst == (DCT_Request *)NULL, "Memory Allocation Failed", return(1) );
   
   /* The array of variables tag */
   vtag = (DCT_Tag *) malloc ( sizeof(DCT_Tag)*(size_t)DCT_MAX_VARS );
   DCTERROR( vtag == (DCT_Tag *)NULL, "Memory Allocation Failed", return(1) );

   /* The array of variables porduction type */
   vprod = (DCT_ProdCons *) malloc ( sizeof(DCT_ProdCons)*(size_t)DCT_MAX_VARS );
   DCTERROR( vprod == (DCT_ProdCons *)NULL, "Memory Allocation Failed", return(1) );

   /* Arrays of model's vars name length */
   lnameA = (int *) malloc ( sizeof(int)*(size_t)DCT_MAX_VARS );
   DCTERROR( lnameA == (int *)NULL, "Memory Allocation Failed", return(1) );
   lnameB = (int *) malloc ( sizeof(int)*(size_t)DCT_MAX_VARS );
   DCTERROR( lnameB == (int *)NULL, "Memory Allocation Failed", return(1) );

   MPI_Pack_size( 1, DCT_TAG, DCT_Comm_World, &stag); /* Couple Tag */
   nmsg = 0;    /* Number of messages sent */
   current = DCT_Reg_Couple;
   /** Couples where broker participates **/
   for ( ii=0; ii < DCT_LocalCouple; ii++ ) {
      DCT_List_Ext (&current, (DCT_VoidPointer *) &couple);

      /******** Building the variable messages tags using the vars tags *********/
      nvar = DCT_BuildMsgTag( couple, vtag, lnameA, lnameB, vprod );

      /* Packing to send to Model B leader */
      rcvr = couple->CoupleModelB->ModLeadRank;
      /* Counting the size of the package */
      err = DCT_PckSize_Var_Plan( DCT_FALSE, lnameB, lnameA, vprod, nvar, &lpck,
                                                                 DCT_Comm_World );
      lpck += stag;
      /********** Allocating memory for the package buffer ************/
      sbuf[nmsg] = (char *) malloc( (size_t)lpck );
      DCTERROR( sbuf[nmsg] == (char *) NULL, "Memory Allocation Failed", return(1) );

      /************ Packing ************/
      pos = 0;
      /* Couple Tag */
      MPI_Pack( &(couple->CoupleTag), 1, DCT_TAG, sbuf[nmsg], lpck, &pos, DCT_Comm_World);

      err = DCT_Packing_Var_Plan( couple, DCT_FALSE, vtag, lnameB, lnameA, vprod, nvar,
                                                sbuf[nmsg], lpck, &pos, DCT_Comm_World );
      /************ The Package is sent ************/
      err = MPI_Isend( sbuf[nmsg], pos, MPI_PACKED, rcvr, DCT_Msg_Tags_VARINFO_LEAD,
                                                       DCT_Comm_World, (reqst+nmsg) );
      nmsg++;

   } /** End of while ( current != (DCT_List *) NULL) **/
   /* Sweep other DCT_Couple list */
   while ( current != (DCT_List *) NULL) {
      DCT_List_Ext (&current, (DCT_VoidPointer *) &couple);

      /******** Building the variable messages tags using the vars tags *********/
      nvar = DCT_BuildMsgTag( couple, vtag, lnameA, lnameB, vprod );

      /* Packing to send to Model A leader */
      rcvr = couple->CoupleModelA->ModLeadRank;
      /* Counting the size of the package */
      err = DCT_PckSize_Var_Plan( DCT_TRUE, lnameA, lnameB, vprod, nvar, &lpck,
                                                                DCT_Comm_World );
      lpck += stag;
      /********** Allocating memory for the package buffer ************/
      sbuf[nmsg] = (char *) malloc( (size_t)lpck );
      DCTERROR( sbuf[nmsg] == (char *) NULL, "Memory Allocation Failed", return(1) );
      /************ Packing ************/
      pos = 0;
      /* Couple Tag */
      MPI_Pack( &(couple->CoupleTag), 1, DCT_TAG, sbuf[nmsg], lpck, &pos, DCT_Comm_World);
      err = DCT_Packing_Var_Plan( couple, DCT_TRUE, vtag, lnameA, lnameB, vprod, nvar,
                                               sbuf[nmsg], lpck, &pos, DCT_Comm_World );
      /************ The Package is sent ************/
         err = MPI_Isend( sbuf[nmsg], pos, MPI_PACKED, rcvr, DCT_Msg_Tags_VARINFO_LEAD,
                                                           DCT_Comm_World, (reqst+nmsg) );
      nmsg++;


      /* Packing to send to Model B leader */
      rcvr = couple->CoupleModelB->ModLeadRank;
      /* Counting the size of the package */
      err = DCT_PckSize_Var_Plan( DCT_FALSE, lnameB, lnameA, vprod, nvar, &lpck,
                                                                 DCT_Comm_World );
      lpck += stag;
      /********** Allocating memory for the package buffer ************/
      sbuf[nmsg] = (char *) malloc( (size_t)lpck );
      DCTERROR( sbuf[nmsg] == (char *) NULL, "Memory Allocation Failed", return(1) );
      /************ Packing ************/
      pos = 0;
      /* Couple Tag */
      MPI_Pack( &(couple->CoupleTag), 1, DCT_TAG, sbuf[nmsg], lpck, &pos, DCT_Comm_World);
      err = DCT_Packing_Var_Plan( couple, DCT_FALSE, vtag, lnameB, lnameA, vprod, nvar,
                                                sbuf[nmsg], lpck, &pos, DCT_Comm_World );
      /************ The Package is sent ************/
      err = MPI_Isend( sbuf[nmsg], pos, MPI_PACKED, rcvr, DCT_Msg_Tags_VARINFO_LEAD,
                                                       DCT_Comm_World, (reqst+nmsg) );
      nmsg++;

   } /** End of while ( current != (DCT_List *) NULL) **/


   /** The first step, send to model leader var info **/   
   err = MPI_Waitall( nmsg, reqst, MPI_STATUSES_IGNORE);
   if (err) return(err);
   for ( jj =0; jj < nmsg; jj++ ) {
      free ( sbuf[jj] );
   }
   free ( reqst );

   /***** Second step, send the var info to same model processes ****/   
   for ( icpl = 0; icpl < DCT_LocalCouple; icpl++ ) {
      couple = lcpl[icpl];
      ModelA = couple->CoupleModelA;
      
      ldrrank = ModelA->ModLeadRank;
      grpcomm = ModelA->ModGroupComm;

      /******** Building the variable messages tags using the vars tags *********/
      nvar = DCT_BuildMsgTag( couple, vtag, lnameA, lnameB, vprod );

      /* Packing to send to the rest of the model processes */
      /* Counting the size of the package */
      err = DCT_PckSize_Var_Plan( DCT_TRUE, lnameA, lnameB, vprod, nvar, &lpck,
                                                                DCT_Comm_World );
      lpck += stag; /* The incorporation of the couple tag */


      /** The model package size is received **/
      MPI_Bcast( &lpck, 1, DCT_INT, ldrrank, grpcomm );

      /********** Allocating memory for the package buffer ************/
      sbuf[icpl] = (char *) malloc( (size_t)lpck );
      DCTERROR( sbuf[icpl] == (char *) NULL, "Memory Allocation Failed", return(1) );
      /************ Packing ************/
      pos = 0;
      /* Couple Tag */
      MPI_Pack( &(couple->CoupleTag), 1, DCT_TAG, sbuf[icpl], lpck, &pos, DCT_Comm_World);
      err = DCT_Packing_Var_Plan( couple, DCT_TRUE, vtag, lnameA, lnameB, vprod, nvar,
                                                 sbuf[icpl], lpck, &pos, DCT_Comm_World );

      /** Broadcast to the rest model process **/
      err = MPI_Bcast( sbuf[icpl], lpck, MPI_PACKED, ldrrank, grpcomm );
      if (err) return(err);

      /* Storing in Model A leader (broker) */
      err = DCT_Broker_Var_Plan( vtag, couple, nvar );
      
   } /* End of for ( icpl = 0; icpl < DCT_LocalCouple; icpl++ ) */

   for ( ii =0; ii < DCT_LocalCouple; ii++ ) {
      free ( sbuf[ii] );
   }
   free ( sbuf );
   free ( vtag );
   free ( vprod );
   free ( lnameA );
   free ( lnameB );
   
   return(err);
/* -------------------------------------  END( DCT_Send_Var_Plan ) */
}

int DCT_GetSend_Var_Plan( DCT_Couple **lcpl )
{
/* ---------------------------------------------  Local Variables  */
   int               err=0;
   
   char            **rbuf;
   char             *auxbuf;
   int              *buflen;

   DCT_Couple       *couple;
   DCT_Model        *ModelA;

   DCT_Comm          grpcomm;
   DCT_Rank          broker, ldrrank;
   DCT_Tag           cpltag;
   DCT_Status        status;

   int               pos, lpck, tagpos;

   register int      ind, icpl, ii;
/* --------------------------------  BEGIN( DCT_GetSend_Var_Plan ) */

   broker = DCT_procinfo.ProcDCTBroker;

   /* The maximum allocation of messages buffers */
   rbuf = (char **) malloc ( sizeof(char *)*(size_t)DCT_LocalCouple );
   DCTERROR( rbuf == (char **)NULL, "Memory Allocation Failed", return(1) );   
   buflen = (int *) malloc ( sizeof(int)*(size_t)DCT_LocalCouple );
   DCTERROR( buflen == (int *)NULL, "Memory Allocation Failed", return(1) );   

   /** The coupled var info is received from the broker **/
   for ( icpl = 0; icpl < DCT_LocalCouple; icpl++ ){
      err = MPI_Probe( broker, DCT_Msg_Tags_VARINFO_LEAD, DCT_Comm_World, &status);
      MPI_Get_count( &status, MPI_PACKED, &lpck );
      /* Allocating memory for the package buffer */
      auxbuf = (char *) malloc( (size_t)lpck );
      DCTERROR( auxbuf == (char *) NULL, "Memory Allocation Failed", return(1) );

      err = MPI_Recv( auxbuf, lpck, MPI_PACKED, broker, DCT_Msg_Tags_VARINFO_LEAD,
                                                           DCT_Comm_World, &status );
      if (err) return(err);

      tagpos = 0;
      MPI_Unpack(auxbuf, lpck, &tagpos, &cpltag, 1, DCT_TAG, DCT_Comm_World);

      for ( ind=0; (ind < DCT_LocalCouple)&&( lcpl[ind]->CoupleTag != cpltag ); ind++ );
      DCTERROR( ind == DCT_LocalCouple, "Error receiving coupled var information", return(1) );
      
      /* Buffer is stored */
      rbuf[ind] = auxbuf;
      buflen[ind] = lpck;


   }

   /** The coupled var info is broadcast to all model processes **/
   for ( icpl = 0; icpl < DCT_LocalCouple; icpl++ ) {
      couple = lcpl[icpl];

      ModelA = couple->CoupleModelA;
      /** The leader rank is get **/
      ldrrank = ModelA->ModLeadRank;
      grpcomm = ModelA->ModGroupComm;
      
      /** The message size is registered **/
      lpck = buflen[icpl];

      /** The model package size is sent **/
      MPI_Bcast( &lpck, 1, DCT_INT, ldrrank, grpcomm );

      /** Broadcast to the rest model process **/
      err = MPI_Bcast( rbuf[icpl], lpck, MPI_PACKED, ldrrank, grpcomm );
      if (err) return (err);

//       tmod = couple->CoupleModelA->ModDomain;
//       SubDom = tmod->ModSubDom;
//       dist = tmod->ModSubDomParLayout;
//       switch(dist)
//       {
//        case DCT_DIST_NULL:
//           DCTERROR( DCT_TRUE, "Internal error, wrong model parallel layout", return(1) );
//        break;
//        case DCT_DIST_SEQUENTIAL:
//           DCTERROR( DCT_TRUE, "Internal error, wrong model parallel layout", return(1) );
//        break;
//        case DCT_DIST_RECTANGULAR:
//           nproc = (int) *(tmod->ModSubDomLayoutDim) * *(tmod->ModSubDomLayoutDim+1);
//        break;
//        case DCT_DIST_3D_RECTANGULAR:
//           nproc = (int) *(tmod->ModSubDomLayoutDim) * *(tmod->ModSubDomLayoutDim+1) *
//                      *(tmod->ModSubDomLayoutDim+2);
//        break;
//        default:
//           DCTERROR( DCT_TRUE, "Internal error, wrong model parallel layout", return(1) );
//       }
//    
//       nmsg = 0;    /* Number of messages sent */
//       count = 0;
//       for ( jj=0; jj < nproc; jj++ ) {
//          rcvr = (SubDom + jj)->RankProc;
//          if ( rcvr != broker ) {
// //             DCT_Check_BufferAvail( lpck, nmsg, reqst );
//             psize = lpck + MPI_BSEND_OVERHEAD;
//             count += psize;
//             dct_glb_lenbuff[nmsg] = psize;
//             /** Check there is space in the buffer **/
// //             printf("Pe[%d], count= %d, psize= %d\n", 
// //                 DCT_procinfo.ProcDCTRank, count, psize);
//             while ( count > dct_glb_buffsize ) {
// //                printf("Pe[%d], nmsg= %d, count= %d, buffsize= %d\n",
// //                        DCT_procinfo.ProcDCTRank, nmsg, count, dct_glb_buffsize );
//                err = MPI_Waitsome( nmsg, dct_glb_recreqst, &outcount, dct_glb_indice,
//                                                                  MPI_STATUSES_IGNORE );
//                if (err) return (err);
//                for ( ii = 0; ii < outcount; ii++ ) count -= dct_glb_lenbuff[ dct_glb_indice[ii] ];
// //                printf("Pe[%d], outcount= %d, count= %d, buffsize= %d\n",
// //                        DCT_procinfo.ProcDCTRank, outcount, count, dct_glb_buffsize );
//                DCTERROR( outcount == MPI_UNDEFINED, "Not enough space in the buffer", return(1) );
//             }
// //             printf("Pe[%d][%s, %d]: MPI_Ibsend rank = %d\n", DCT_procinfo.ProcDCTRank,
// //                                                        __FILE__ , __LINE__ + 1, rcvr );
//             err = MPI_Ibsend ( rbuf[icpl], lpck, MPI_PACKED, rcvr, DCT_Msg_Tags_VARINFO_PROCS,
//                                                       DCT_Comm_World, (dct_glb_recreqst+nmsg) );
//             if (err) return (err);
//             nmsg++;
//          }
//       }

      pos = tagpos;
      /* Iterates through the varibles */
      err = DCT_UnPacking_Var_Plan( couple, rbuf[icpl], lpck, &pos, DCT_Comm_World );

//       /** Check the message was delivered **/
//       MPI_Waitall( nmsg, dct_glb_recreqst, MPI_STATUSES_IGNORE );

   } /* End of for ( icpl = 0; icpl < DCT_LocalCouple; icpl++ ) */

   for ( ii =0; ii < DCT_LocalCouple; ii++ ) {
      free ( rbuf[ii] );
   }
   free ( rbuf );
   free ( buflen );

   return(err);
/* ----------------------------------  END( DCT_GetSend_Var_Plan ) */
}


int DCT_Get_Var_Plan( DCT_Couple **lcpl  )
{
/* ---------------------------------------------  Local Variables  */
   int             err=0;
   
   DCT_Couple     *couple;
   DCT_Model      *ModelA;
   
   char           *rbuf;
   
   DCT_Tag         cpltag;
   
   DCT_Rank        ldrrank;
   DCT_Comm        grpcomm;

   int             pos, lpck;

   register int    icpl;

/* ------------------------------------  BEGIN( DCT_Get_Var_Plan ) */

   for ( icpl = 0; icpl < DCT_LocalCouple; icpl++ ){

//       for ( ind=0; (ind < DCT_LocalCouple)&&( lcpl[ind]->CoupleTag != cpltag ); ind++ );
//       DCTERROR( ind == DCT_LocalCouple, "Error receiving coupled var information", return(1) );
      couple = lcpl[icpl];

      ModelA = couple->CoupleModelA;
      /** The leader rank is get **/
      ldrrank = ModelA->ModLeadRank;
      grpcomm = ModelA->ModGroupComm;

//       err = MPI_Probe( MPI_ANY_SOURCE, DCT_Msg_Tags_VARINFO_PROCS, DCT_Comm_World, &status );
//       if (err) return err;
//       sndr = status.MPI_SOURCE;
//       MPI_Get_count( &status, MPI_PACKED, &lpck );

      /** The model package size is received **/
      MPI_Bcast( &lpck, 1, DCT_INT, ldrrank, grpcomm );

   /*                                                   BEGIN DEBUG */
   //    printf("[DCT_Get_SubDom_Comm_Plan]: Model %s: Receiver %d, MSG TAG received: %d\n\n",
   //                      couple->CoupleModelA->ModName, DCT_procinfo.ProcDCTRank, status->MPI_TAG );
   //    printf("[DCT_Get_SubDom_Comm_Plan]: Model %s: Receiver %d, CPL TAG received: %d\n\n",
   //                    couple->CoupleModelA->ModName, DCT_procinfo.ProcDCTRank, couple->CoupleTag );
   /*                                                     END DEBUG */
   

      /* Allocating memory for the package buffer */
      rbuf = (char *) malloc( (size_t)lpck );
      DCTERROR( rbuf == (char *) NULL, "Memory Allocation Failed", return(1) );
      
//       err = MPI_Recv( rbuf, lpck, MPI_PACKED, sndr, DCT_Msg_Tags_VARINFO_PROCS, DCT_Comm_World,
//                                                                                        &status );
//       if (err) return(err);

      /** Broadcast to the rest model process **/
      err = MPI_Bcast( rbuf, lpck, MPI_PACKED, ldrrank, grpcomm );
      if (err) return(err);

      /************ UnPacking ************/
      pos = 0;
      MPI_Unpack( rbuf, lpck, &pos, &cpltag, 1, DCT_TAG, DCT_Comm_World );

      /* Iterates through the varibles */
      err = DCT_UnPacking_Var_Plan( couple, rbuf, lpck, &pos, DCT_Comm_World );
      if (err) return err;

      free( rbuf );
   }
   return(err);
/* --------------------------------------  END( DCT_Get_Var_Plan ) */
}

int DCT_Comm_Datatype_Set ( DCT_Comm_Datatype *DType, DCT_Data_Types UDType,
                            int *IniInd, int *Length,
                            int *Dim, int dim )
{
/* ---------------------------------------------  Local Variables  */
   int err=0;
   
   DCT_Comm_Datatype  olddata;

/* -------------------------------  BEGIN( DCT_Comm_Datatype_Set ) */

   switch (UDType) {
      case DCT_FLOAT:
         olddata = DCT_COMM_FLOAT;
         break;
      case DCT_DOUBLE:
         olddata = DCT_COMM_DOUBLE;
         break;
      case DCT_LONG_DOUBLE:
         olddata = DCT_COMM_LONG_DOUBLE;
         break;
      default:
         return (1);
   }
   
   err = MPI_Type_create_subarray( dim, Dim, Length, IniInd, MPI_ORDER_C, olddata, DType );

   err = MPI_Type_commit( DType );

   return(err);
/* ---------------------------------  END( DCT_Comm_Datatype_Set ) */
}

/*******************************************************************/
/*                       DCT_Post_RecvVarComm                      */
/*                                                                 */
/*!     Post the non-blocking receiving messages for possible
        ACK of all the consumer other model processes that
        intersects with the actual subdomain, and their
        neighbors. This in order to know when all thave
        being set up.

    \param[in] VarMsgTag  The message tag set.
    \param[in]     nproc  Number of processor.
    \param[out]   ackarr  Array of length of nproc the comps.

    \return An integer error control value with 0, if success; 
            and 0 otherwise.

*/
/*******************************************************************/
int DCT_Post_RecvVarComm( DCT_Tag VarMsgTag, int nproc, DCT_Boolean **ackarr )
{
/* ---------------------------------------------  Local Variables  */
   int           err = 0;
   int           ii;
   DCT_Boolean  *localackarr;
   DCT_Tag       msgtagACK;

/* --------------------------------  BEGIN( DCT_Post_RecvVarComm ) */

   /* Each process can send the number of DCT_Couple and DCT_MAX_COUPLE
      lenths of its name and models' name plus the names, with 256 chars max  */  

   /** These variables are GLOBAL. See the beginning of the file for their declaration **/
   localackarr = (DCT_Boolean *) malloc( sizeof(DCT_Boolean)*(size_t)nproc );
   DCTERROR( localackarr == (DCT_Boolean *) NULL, "Memory Allocation Failed", return(1) );
   *ackarr = localackarr;
   dct_glb_lenbuff = (int *) malloc( sizeof(int)*(size_t)nproc );
   DCTERROR( dct_glb_lenbuff == (int *) NULL, "Memory Allocation Failed", return(1) );
   dct_glb_recreqst = (DCT_Request *) malloc( sizeof(DCT_Request)*(size_t)nproc );
   DCTERROR( dct_glb_recreqst == (DCT_Request *)NULL, "Memory Allocation Failed", return(1) );

   /** The ACK message tag corresponding only for this variable **/
   msgtagACK = DCT_Msg_Tags_VAR_ACK + VarMsgTag;

   for ( ii=0; ii < nproc; ii++ ) 
      MPI_Irecv( (localackarr+ii), 1, DCT_BOOL, MPI_ANY_SOURCE, msgtagACK,
                                      DCT_Comm_World, dct_glb_recreqst+ii );

   return(err);
/* ----------------------------------  END( DCT_Post_RecvVarComm ) */
}

int DCT_Rels_RecvVarComm( DCT_Boolean *ackarr )
{
/* ---------------------------------------------  Local Variables  */
   int       err = 0;

/* --------------------------------  BEGIN( DCT_Rels_RecvVarComm ) */


   /** This variables are GLOBAL. See the beginning of the file for their declaration **/
   free ( ackarr );
   free ( dct_glb_lenbuff );
   free ( dct_glb_recreqst );
   
   return(err);
/* ----------------------------------  END( DCT_Rels_RecvVarComm ) */
}



/*******************************************************************/
/*                        DCT_ProduceVar_Comm                      */
/*                                                                 */
/*!     Calculates the definite Producer side communication
        pattern. In this function, it is checked whether is
        required more data or not.

    \param[in,out]  VarList  Node of the a variable coupling
                             in the master couple tabla.

    \return An integer error control value with 0, if success; 
            and 0 otherwise.

*/
/*******************************************************************/
int DCT_ProduceVar_Comm( DCT_Var_Node *VarList )
{
/* ---------------------------------------------  Local Variables  */
   int                  err=0;
   
   DCT_Field           *field;
   DCT_3d_Var          *var3d;
   DCT_4d_Var          *var4d;
   
   DCT_VoidPointer      UData;

   DCT_SDProduce_Plan  *CSnd;
   
   DCT_Integer          nmsg, sndmsg;
   
   DCT_Integer          mtind, NSDCons, nAck;
   DCT_Integer         *Dim;
   DCT_Integer          Inc[MAX_DOMAIN_DIM];
   DCT_Integer         *IniInd;
   DCT_Integer         *Length;
   DCT_Integer          totlen;
   DCT_Scalar          *IniVal;
   DCT_Scalar          *EndVal;
   DCT_Scalar         **MsgSlab;
   DCT_Scalar          *Labels[MAX_DOMAIN_DIM];
   DCT_Scalar           Needit[2*MAX_DOMAIN_DIM];
   
   DCT_Domain_Type     *DomType;
   
   DCT_Data_Types       UserDataType;
   DCT_Data_Types       UnKnowData = DCT_DATA_TYPE_UNKNOWN;
   DCT_Produce_Plan    *CommSched;
   
   DCT_Tag              msgtagP, msgtagC;
   
   DCT_Rank             Rcvr;
   DCT_Rank            *RcvrRnks;
   DCT_Scalar          *SDIniVal;
   DCT_Scalar          *SDEndVal;
   
   DCT_Request         *req;
   DCT_Status          *acksts;
   DCT_Status           status;
   DCT_Boolean          acknow;
   DCT_Boolean         *ackarr;
   char               **buf;

   int                  pck, pckcnt, pos, dim, bufind, dimflag;

   register int         ii, jj, kk, chk4msg, ifnewmsg;

/* ---------------------------------  BEGIN( DCT_ProduceVar_Comm ) */

   /* The  var information (dimension, domain type, labels,
      user data type and the user data pointer )                 */
   switch (VarList->VarType)
   {
      case DCT_FIELD_TYPE:
         field = (DCT_Field *)VarList->VarAddr;
         dim = 2;
         Dim = field->VarDim;

         DomType = field->VarDomType;

         Labels[0] = field->VarLabels[0];
         Labels[1] = field->VarLabels[1];
         
         UserDataType = field->VarUserDataType;
         UData        = field->VarValues;

         break;
      case DCT_3D_VAR_TYPE:
         var3d = (DCT_3d_Var *)VarList->VarAddr;
         dim = 3;
         Dim = var3d->VarDim;

         DomType = var3d->VarDomType;

         Labels[0] = var3d->VarLabels[0];
         Labels[1] = var3d->VarLabels[1];
         Labels[2] = var3d->VarLabels[2];
         
         UserDataType = var3d->VarUserDataType;
         UData        = var3d->VarValues;

         break;
      case DCT_4D_VAR_TYPE:
         var4d = (DCT_4d_Var *)VarList->VarAddr;
         dim = 4;
         Dim = var4d->VarDim;

         DomType = var4d->VarDomType;

         Labels[0] = var4d->VarLabels[0];
         Labels[1] = var4d->VarLabels[1];
         Labels[2] = var4d->VarLabels[2];
         Labels[3] = var4d->VarLabels[3];
         
         UserDataType = var4d->VarUserDataType;
         UData        = var4d->VarValues;

   } /* End of switch (VarList->VarType) */
   /* The Produce Plan is got from the Master table */
   mtind = VarList->CPLMTIndex;
   CommSched = (DCT_Produce_Plan *) DCT_CPL_Table.CPLMasterTable.CPLDataComm[mtind];

   CSnd = (DCT_SDProduce_Plan *)VarList->SDComm;
   /* Number of preset messages */
   nmsg = CSnd->Nmsg;
   /* Number of Consumer's Sub Domains laying on and next to the messages */
   NSDCons = CSnd->RcvrNSD;

   /** The ack receiving message are posted **/
   DCT_Post_RecvVarComm( VarList->VarMsgTag, NSDCons, &ackarr );
   acksts = (DCT_Status *) malloc ( sizeof(DCT_Status)*(size_t)NSDCons );
   DCTERROR( acksts == (DCT_Status *)NULL, "Memory Allocation Failed", return(1) );

   /* The maximum allocation of DCT_Requests */
   req = (DCT_Request *) malloc ( sizeof(DCT_Request)*(size_t)nmsg );
   DCTERROR( req == (DCT_Request *)NULL, "Memory Allocation Failed", return(1) );

   /* Buffer count */
   MPI_Pack_size( 1, DCT_INT, DCT_Comm_World, &pckcnt); /* Value DCT_Data_Types */
   pck = pckcnt;
   MPI_Pack_size( 2*dim, DCT_SCALAR, DCT_Comm_World, &pckcnt); /* Initial and End Values */
   pck += pckcnt;
   MPI_Pack_size( dim, DCT_INT, DCT_Comm_World, &pckcnt); /* Lengths */
   pck += pckcnt;
   
   /* The maximum allocation of messages buffers */
   buf = (char **) malloc ( sizeof(char *)*(size_t)nmsg );
   DCTERROR( buf == (char **)NULL, "Memory Allocation Failed", return(1) );

   IniInd = (DCT_Integer *) malloc( sizeof(DCT_Integer)*(size_t)(NSDCons*dim) );
   DCTERROR( IniInd == (DCT_Integer *)NULL, "Memory Allocation Failed", return(1) );
   RcvrRnks = (DCT_Rank *) malloc( sizeof(DCT_Rank)*(size_t)NSDCons );
   DCTERROR( RcvrRnks == (DCT_Rank *)NULL, "Memory Allocation Failed", return(1) );
   Length = (DCT_Integer *) malloc( sizeof(DCT_Integer)*(size_t)(NSDCons*dim) );
   DCTERROR( Length == (DCT_Integer *)NULL, "Memory Allocation Failed", return(1) );
   /** This array is used to control the producer processes role **/
   /** The value -1 means that the process never intended to produce
       values. Value 0 means that it was planned by the broker but
       no actual data would be transmitted. Greater than zero means
       actual data to transmit. The array is initialized in the first
       dimension component by -1                                       **/
   for ( ii=0; ii < NSDCons; ii++ )
      Length[ii*dim] = -1;
   IniVal = (DCT_Scalar *) malloc( sizeof(DCT_Scalar)*(size_t)dim );
   DCTERROR( IniVal == (DCT_Scalar *)NULL, "Memory Allocation Failed", return(1) );
   EndVal = (DCT_Scalar *) malloc( sizeof(DCT_Scalar)*(size_t)dim );
   DCTERROR( EndVal == (DCT_Scalar *)NULL, "Memory Allocation Failed", return(1) );
   MsgSlab = (DCT_Scalar **) malloc( sizeof(DCT_Scalar *)*(size_t)dim );
   DCTERROR( MsgSlab == (DCT_Scalar **)NULL, "Memory Allocation Failed", return(1) );

   /** Message tag for producer-consumer comm step **/
   msgtagP = DCT_Msg_Tags_VAR_PROD_PLAN + VarList->VarMsgTag;
   /** Message tag for consumer-producer comm step **/
   msgtagC = DCT_Msg_Tags_VAR_CONS_PLAN + VarList->VarMsgTag;

   sndmsg = 0; /* Number of messages to send based on if there is point on the range */

   /**** ATTENTION: The general curvilinear section has not
         incorporated the EndVal, when the subdomain is checked ****/
   if ( DomType[0] == DCT_GENERAL_CURV ) {
      for ( ii=0; ii < nmsg; ii++ ) {
         Rcvr = (CSnd->Msend + ii)->Recvr;
         SDIniVal = (CSnd->Msend + ii)->IniVal;
         SDEndVal = (CSnd->Msend + ii)->EndVal;
         /** The values in between the requested values are determined **/
         /*********** ATTENTION THE VARIABLE K HERE IS UNINITIALIZED **************/
         dimflag = DCT_IncludedValues_GC( dim, &totlen, Labels, Dim, SDIniVal, SDEndVal,
                            IniVal, EndVal, (IniInd /*+ kk*/), (Length /*+ kk*/), MsgSlab );

         if ( dimflag == dim ) {   /* There is something to send */
            sndmsg++;

            /* Buffer pack  */
            pos = 0;
            jj = dim*ii;
            MPI_Pack( &UserDataType, 1, DCT_INT, buf[ii], pck, &pos, DCT_Comm_World); 
            MPI_Pack( (IniVal + jj), dim, DCT_SCALAR, buf[ii], pck, &pos, DCT_Comm_World);
            MPI_Pack( (Length + jj), dim, DCT_INT, buf[ii], pck, &pos, DCT_Comm_World);
            
         }
         else {  /* There is not anything to send */
      
            /* Buffer pack  */
            pos = 0;
            MPI_Pack( &UnKnowData, 1, DCT_INT, buf[ii], pck, &pos, DCT_Comm_World); 
            
            jj = dim*ii;
            Length[jj] = 0; /* Indicating that no package to send */
            
         }
         /* Buffer sent  */
         err = MPI_Isend( buf[ii], pck, MPI_PACKED, Rcvr, msgtagP,
                                                DCT_Comm_World, (req+ii) );
      
      } /* End of for ( ii=0; ii < nmsg; ii+ ) */
   
   } else {    /****************** DCT_CARTESIAN and DCT_RECTILINEAR *****************/

      for ( ii=0; ii < nmsg; ii++ ) {
         Rcvr = (CSnd->Msend + ii)->Recvr;
         RcvrRnks[ii] = Rcvr;
         SDIniVal = (CSnd->Msend + ii)->IniVal;
         SDEndVal = (CSnd->Msend + ii)->EndVal;
         
         kk = ii*dim;
         /** The values in between the requested values are determined **/
         dimflag = DCT_IncludedValues( dim, &totlen, DomType, Labels, Dim, SDIniVal,
                                       SDEndVal, IniVal, EndVal, (IniInd + kk),
                                       (Length + kk), MsgSlab, NULL );
         if ( dimflag == dim ) {   /* There is something to send */
            sndmsg++;
            /* Tick Marks (Labels) */
            MPI_Pack_size( totlen, DCT_SCALAR, DCT_Comm_World, &pckcnt); 
            pckcnt += pck; /*** pck is calculated above and should be constant */

            /********** Allocating memory for the package buffer ************/
            buf[ii] = (char *) malloc( (size_t)pckcnt );
            DCTERROR( buf[ii] == (char *) NULL, "Memory Allocation Failed", return(1) );
            /* Buffer pack  */
            pos = 0;
            
            MPI_Pack( &UserDataType, 1, DCT_INT, buf[ii], pckcnt, &pos, DCT_Comm_World); 
            MPI_Pack( IniVal, dim, DCT_SCALAR, buf[ii], pckcnt, &pos, DCT_Comm_World);
            MPI_Pack( EndVal, dim, DCT_SCALAR, buf[ii], pckcnt, &pos, DCT_Comm_World);
            MPI_Pack( (Length+kk), dim, DCT_INT, buf[ii], pckcnt, &pos, DCT_Comm_World);
            for (jj = 0; jj < dim; jj++ )
               MPI_Pack( MsgSlab[jj], Length[kk + jj], DCT_SCALAR, buf[ii], pckcnt, &pos, DCT_Comm_World);
            
         }/* End if ( dimflag == dim ) */
         else {  /* There is not anything to send */
      
            /********** Allocating memory for the package buffer ************/
            buf[ii] = (char *) malloc( (size_t)pck );
            DCTERROR( buf[ii] == (char *) NULL, "Memory Allocation Failed", return(1) );
            /* Buffer pack  */
            pos = 0;
            MPI_Pack( &UnKnowData, 1, DCT_INT, buf[ii], pck, &pos, DCT_Comm_World);
            
            Length[kk] = 0; /* Indicating that no package to send */

         }/* End else of if ( dimflag == dim ) */
         /* Buffer sent  */
         err = MPI_Isend( buf[ii], pos, MPI_PACKED, Rcvr, msgtagP,
                                                DCT_Comm_World, (req+ii) );
         if (err) return(err);

         for (jj = (dim-1); jj >= (dim-dimflag); jj-- ) {
            if ( DomType[jj] == DCT_CARTESIAN ) {
               free( MsgSlab[jj] );
            }
         }

      } /* End of for ( ii=0; ii < nmsg; ii+ ) */
   
   } /* End of if ( DomType[0] == DCT_GENERAL_CURV )  */
   
   err = MPI_Waitall( nmsg, req, MPI_STATUSES_IGNORE );
   if (err) return(err);
   /* the buf is freed */
   for ( ii=0; ii < nmsg; ii++ ) free ( buf[ii] );
   
   /*** Producer wait for Ack or more data ***/
   chk4msg = 1;
   nAck = 0;
   bufind = 0;

   SDIniVal = (DCT_Scalar *) malloc( sizeof(DCT_Scalar)*(size_t)dim );
   DCTERROR( SDIniVal == (DCT_Scalar *)NULL, "Memory Allocation Failed", return(1) );
   SDEndVal = (DCT_Scalar *) malloc( sizeof(DCT_Scalar)*(size_t)dim );
   DCTERROR( EndVal == (DCT_Scalar *)NULL, "Memory Allocation Failed", return(1) );
   
   while ( chk4msg ) {

      /** Check for the ack received **/
      //if ( DCT_Check4Ack( NSDCons, Length, RcvrRnks, ackarr ) ) continue;
      MPI_Testsome( NSDCons, dct_glb_recreqst, &dimflag, dct_glb_lenbuff, acksts );
      if ( dimflag != 0 ) {
         /** Check that producer and receiver agree if there is something to send **/
         for ( ii=0; ii < dimflag; ii++ ) {
            kk = dct_glb_lenbuff[ii];
            Rcvr = acksts[ ii ].MPI_SOURCE;
            for ( jj=0; (jj < nmsg)&&(Rcvr != RcvrRnks[jj]); jj++ );
            acknow = ( (Length[jj*dim] > 0) && ( jj != nmsg ) ? DCT_TRUE : DCT_FALSE );
            DCTERROR( ackarr[kk] != acknow,
                      "Internal Error. Inconsistency between consumer and producer data",
                      return(1) );
         }
         nAck += dimflag;
         if ( (nAck == NSDCons) || (dimflag == MPI_UNDEFINED) ) chk4msg = 0;
         continue;
      }
   
      MPI_Iprobe( MPI_ANY_SOURCE, msgtagC, DCT_Comm_World, &dimflag, &status );
      if ( !dimflag ) continue;
      Rcvr = status.MPI_SOURCE;
      MPI_Get_count( &status, MPI_PACKED, &pckcnt );
      
      /********** Allocating memory for the package buffer ************/
      buf[bufind] = (char *) malloc( (size_t)pckcnt );
      DCTERROR( buf[bufind] == (char *) NULL, "Memory Allocation Failed", return(1) );

      /* The consumer info is received */
      err = MPI_Recv( buf[bufind] , pckcnt, MPI_PACKED, Rcvr, msgtagC, DCT_Comm_World,
                                                                              &status );
      if (err) return(err);
      /** Obtaining the header **/
      pos = 0;
      MPI_Unpack(buf[bufind], pckcnt, &pos, Inc, dim, DCT_INT, DCT_Comm_World);
      MPI_Unpack(buf[bufind], pckcnt, &pos, Needit, 2*dim, DCT_SCALAR, DCT_Comm_World);
      for ( ii=0; ii < nmsg; ii++ )
         if ( Rcvr == RcvrRnks[ii] ) break;

      /**** If it is new, get the information, register it and send it.
            Otherwise, change it and send it  ****/
      if ( ii == nmsg ) { /* The message is new (it was not scheduled by the Broker)*/
         pos = nmsg;
         nmsg++;
         ifnewmsg = 1;  /* The number of actual messages would need to be increased */
         RcvrRnks[pos] = Rcvr;
      } else { /* The message should be modified */
         pos = ii;

      } /* End of if ( ( ii == nmsg ) || ( ii >= CSnd->Nmsg ) )
                     The message is new */
      dimflag = 2*dim;
      jj = 0;
      for ( ii=0; ii < dimflag; ii+=2 ) {
         /* Getting the new value limits */
         SDIniVal[jj] = Needit[ii];
         SDEndVal[jj++] = Needit[ii+1];
      }
      
      dimflag = 0;
      totlen = 0;
      kk = pos*dim; /* Where the message legths are located */
      ifnewmsg = ( Length[kk] <= 0 ? 1: 0 );  /* The number of actual messages
                                                 would need to be increased */
      /** The values in between the requested values are determined **/
      /** This time, the producer should provide data the could be outside
          of the consumer limits. Because the data sent was not enough     **/
      dimflag = DCT_IncludedValues( dim, &totlen, DomType, Labels, Dim, SDIniVal,
                                    SDEndVal, IniVal, EndVal, (IniInd + kk), (Length + kk),
                                    MsgSlab, Inc );

      free ( buf[bufind] );
      if ( dimflag == dim ) {   /* There is something to send */
         if ( ifnewmsg ) sndmsg++; /* Check if the message wasn't counted before */
         /* Tick Marks (Labels) */
         MPI_Pack_size( totlen, DCT_SCALAR, DCT_Comm_World, &pckcnt); 
         pckcnt += pck; /*** pck is calculated above and should be constant */

         /********** Allocating memory for the package buffer ************/
         buf[bufind] = (char *) malloc( (size_t)pckcnt );
         DCTERROR( buf[bufind] == (char *) NULL, "Memory Allocation Failed", return(1) );
         /* Buffer pack  */
         pos = 0;
         
         MPI_Pack( &UserDataType, 1, DCT_INT, buf[bufind], pckcnt, &pos, DCT_Comm_World); 
         MPI_Pack( IniVal, dim, DCT_SCALAR, buf[bufind], pckcnt, &pos, DCT_Comm_World);
         MPI_Pack( EndVal, dim, DCT_SCALAR, buf[bufind], pckcnt, &pos, DCT_Comm_World);
         MPI_Pack( (Length+kk), dim, DCT_INT, buf[bufind], pckcnt, &pos, DCT_Comm_World);
         for (jj = 0; jj < dim; jj++ ) {
            MPI_Pack( MsgSlab[jj], Length[kk + jj], DCT_SCALAR, buf[bufind], pckcnt, &pos, DCT_Comm_World);
            if ( DomType[jj] == DCT_CARTESIAN ) {
               free( MsgSlab[jj] );
            }
         }
         
      }
      else {  /* There is not anything to send */
   
         /********** Allocating memory for the package buffer ************/
         buf[bufind] = (char *) malloc( (size_t)pck );
         DCTERROR( buf[bufind] == (char *) NULL, "Memory Allocation Failed", return(1) );
         /* Buffer pack  */
         pos = 0;
         MPI_Pack( &UnKnowData, 1, DCT_INT, buf[bufind], pck, &pos, DCT_Comm_World);
         
         Length[kk] = 0; /* Indicating that no package to send */

      }
      /* Buffer sent  */
      err = MPI_Isend( buf[bufind], pos, MPI_PACKED, Rcvr, msgtagP,
                                             DCT_Comm_World, (req+bufind) );
      if (err) return(err);
      bufind++;
   

      /* Check if there is room on buffer */
      if ( bufind == nmsg ) {
         err = MPI_Waitall( bufind, req, MPI_STATUSES_IGNORE );
         if (err) return(err);
         /* the buf is freed */
         for ( ii=0; ii < bufind; ii++ ) {
            free ( buf[ii] );
         }
         bufind = 0;
      }

   } /* End of while ( chk4msg ) */

   /* Check if there is any pending message */
   if ( bufind > 0 ) {
         err = MPI_Waitall( bufind, req, MPI_STATUSES_IGNORE );
         if (err) return(err);
         for ( ii=0; ii < bufind; ii++ ) {
            free ( buf[ii] );
         }
         bufind = 0;
   }
   /****** Communication between producer-consumer finishes ******/
   /**************************************************************/
   DCT_Rels_RecvVarComm( ackarr );
   
   free ( acksts );
   free ( buf );
   free ( req );
   free ( SDIniVal );
   free ( SDEndVal );
   free ( IniVal );
   free ( EndVal );
   free ( MsgSlab );
   
   /***************************************************************************/
   
   /** Changing the definitely var tag **/
   CommSched->VarMsgTag = DCT_Msg_Tags_COMM_EXCH + VarList->VarMsgTag;

   CommSched->Nmsg = sndmsg;
//    CommSched->Msg = (DCT_Msg_Slab *) malloc( sizeof(DCT_Msg_Slab)*(size_t)sndmsg );
//    DCTERROR( CommSched->Msg == (DCT_Msg_Slab *)NULL, "Memory Allocation Failed", return(1) );

   /** The derived data type array is allocated **/
   CommSched->DType = (DCT_Comm_Datatype *)malloc(sizeof(DCT_Comm_Datatype)*(size_t)sndmsg);
   DCTERROR( CommSched->DType == (DCT_Comm_Datatype *)NULL, "Memory Allocation Failed",
             return(1) );
   /** The use of persistent requests suppose to obtain better performance **/
   CommSched->reqsts = (DCT_Request *) malloc( sizeof(DCT_Request)*(size_t)sndmsg );
   DCTERROR( CommSched->reqsts == (DCT_Request *)NULL, "Memory Allocation Failed", return(1) );

   pck = 0;
   for ( ii=0; ii < nmsg; ii++ ) {
      jj = dim*ii;
      if ( Length[jj] > 0 ) { /* Set the variable package */
      
/*                                                   BEGIN DEBUG */
//          printf("Package from %d to %d\n", CSnd->Sendr, RcvrRnks[ii] );
//          printf("Data Type: %d\n", UserDataType );
//          printf("Initial Coordinates: [ %d", IniInd[jj] );
//          for ( kk=1; kk < dim; kk++ ) printf(", %d", IniInd[jj + kk] );
//          printf(" ]\n");
//          printf("Tile with: [ %d", Length[jj] );
//          for ( kk=1; kk < dim; kk++ ) printf(", %d", Length[jj + kk] );
//          printf(" ]\n");
//          
/*                                                     END DEBUG */
         
      
         /*  Set the datatype as the submatrix of the user one */
         err = DCT_Comm_Datatype_Set ( (CommSched->DType + pck), UserDataType,
                                       (IniInd + jj), (Length + jj), Dim, dim );
         if (err) return(err);

         /** Creating the persistent requests **/
         err =  MPI_Send_init ( UData, 1, *(CommSched->DType + pck), RcvrRnks[ii],
                                 CommSched->VarMsgTag, DCT_Comm_World,
                                 (CommSched->reqsts + pck) );
         if (err) return(err);
         pck++; /* Index incement for a message to be sent */
      }
   }
   /** pck is only counting how many messages are being scheduled to send **/
   DCTERROR( pck != sndmsg,
             "Internal Error. Inconsistency between the number of messages to be sent",
             return(1) );
   
   /* Freeing the allocation */
   free ( IniInd );
   free ( RcvrRnks );
   free ( Length );

   
   return(err);
/* -----------------------------------  END( DCT_ProduceVar_Comm ) */
}


/*******************************************************************/
/*                        DCT_ConsumeVar_Comm                      */
/*                                                                 */
/*!     Calculates the definite Consumer side communication
        pattern. In this function, it is checked whether is
        required more data or not.

    \param[in,out]  VarList  Node of the a variable coupling
                             in the master couple tabla.

    \return An integer error control value with 0, if success; 
            and 0 otherwise.

        \todo Verify why this \c dim is not initialized in the
              part coded \"nothing to receive\".
              
        \todo Verify why this \c msgtagACK is no initialized in
              the part \"Nothing to receive from the beggining;
              so, only send Ack to producer\".

*/
/*******************************************************************/
int DCT_ConsumeVar_Comm( DCT_Var_Node *VarList )
{
/* ---------------------------------------------  Local Variables  */
   int                  err=0;
   
   DCT_Field           *field;
   DCT_3d_Var          *var3d;
   DCT_4d_Var          *var4d;

   
   /*DCT_VoidPointer      UData;*/

   DCT_SDConsume_Plan  *CRcv;
   
   DCT_Integer          nmsg, sndmsg, prodSD;
   
   DCT_Integer          IniI, mtind;
   DCT_Integer          inipos[MAX_DOMAIN_DIM], endpos[MAX_DOMAIN_DIM];
   DCT_Integer          GlbIni[MAX_DOMAIN_DIM], GlbEnd[MAX_DOMAIN_DIM];
   DCT_Integer          Inc[MAX_DOMAIN_DIM];
   DCT_Integer         *Dim;
   DCT_Integer         *IniInd[MAX_DOMAIN_DIM];
   DCT_Integer         *EndInd[MAX_DOMAIN_DIM];
   int                 *indices;

   DCT_Integer         *Length;
   DCT_Integer         *size;
   DCT_Scalar          *IniVal;
   DCT_Scalar          *EndVal;
   DCT_Scalar         **MsgSlab;
   DCT_Scalar          *Labels[MAX_DOMAIN_DIM];
   DCT_Scalar          *VarLabels[MAX_DOMAIN_DIM];
   DCT_Scalar           Needit[2*MAX_DOMAIN_DIM];
   DCT_Scalar          *MSlabptr;
   
   DCT_Domain_Type     *DomType;
   
   DCT_Data_Types       UserDataType, RcvDataType;
   DCT_Consume_Plan    *CommSched;
   /* DCT_SD_Consume      *SDSlab; */
   DCT_Data_SubDom     *SndrSDom;
   DCT_Data_SubDom     *TempSDom;

   DCT_Rank             Rcvr;
   DCT_Tag              msgtagP, msgtagC, msgtagACK;
   DCT_Status           status;
   DCT_Request         *req;
   DCT_Boolean          acknow[2];
   char               **buf;

   int                  pck, pckcnt, pos, dim, chkdom;

   register int         ii, dimflag, jj, kk, jj1;

/* ---------------------------------  BEGIN( DCT_ConsumeVar_Comm ) */

   CRcv = (DCT_SDConsume_Plan *)VarList->SDComm;
   nmsg = CRcv->Nmsg;
   /* SDSlab = CRcv->SDSlab; */
   /* The absolute maximum of messages is the number of subdomains */
   prodSD = CRcv->SndrNSD;
   /* The producer structure is get */
   SndrSDom = CRcv->SndrSDom;

   /* The maximum allocation of DCT_Requests */
   req = (DCT_Request *) malloc ( sizeof(DCT_Request)*(size_t)prodSD );
   DCTERROR( req == (DCT_Request *)NULL, "Memory Allocation Failed", return(1) );
   
   /* The maximum allocation of messages buffers */
   buf = (char **) malloc ( sizeof(char *)*(size_t)prodSD );
   DCTERROR( buf == (char **)NULL, "Memory Allocation Failed", return(1) );

   /* The Consume Plan is got from the Master table */
   mtind = VarList->CPLMTIndex;
   CommSched = (DCT_Consume_Plan *) DCT_CPL_Table.CPLMasterTable.CPLDataComm[mtind];

   /* Check if it is anything to do */
   if ( nmsg > 0 ) { /* Broker sent possible messages */

      switch (VarList->VarType)
      {
         case DCT_FIELD_TYPE:
            field = (DCT_Field *)VarList->VarAddr;
            dim = 2;
            Dim = field->VarDim;

            DomType = field->VarDomType;
   
            Labels[0] = field->VarLabels[0];
            Labels[1] = field->VarLabels[1];
            
            UserDataType = field->VarUserDataType;
            /*UData        = field->VarValues;*/
            
            break;
         case DCT_3D_VAR_TYPE:
            var3d = (DCT_3d_Var *)VarList->VarAddr;
            dim = 3;
            Dim = var3d->VarDim;

            DomType = var3d->VarDomType;
   
            Labels[0] = var3d->VarLabels[0];
            Labels[1] = var3d->VarLabels[1];
            Labels[2] = var3d->VarLabels[2];
            
            UserDataType = var3d->VarUserDataType;
            /*UData        = var3d->VarValues;*/
   
            break;
         case DCT_4D_VAR_TYPE:
            var4d = (DCT_4d_Var *)VarList->VarAddr;
            dim = 4;
            Dim = var4d->VarDim;

            DomType = var4d->VarDomType;
   
            Labels[0] = var4d->VarLabels[0];
            Labels[1] = var4d->VarLabels[1];
            Labels[2] = var4d->VarLabels[2];
            Labels[3] = var4d->VarLabels[3];
            
            UserDataType = var4d->VarUserDataType;
            /*UData        = var4d->VarValues;*/
   
      } /* End of switch (VarList->VarType) */
      
      IniVal = (DCT_Scalar *) malloc( sizeof(DCT_Scalar)*(size_t)(dim*prodSD) );
      DCTERROR( IniVal == (DCT_Scalar *)NULL, "Memory Allocation Failed", return(1) );
      EndVal = (DCT_Scalar *) malloc( sizeof(DCT_Scalar)*(size_t)(dim*prodSD) );
      DCTERROR( EndVal == (DCT_Scalar *)NULL, "Memory Allocation Failed", return(1) );
      /** This array is used to control the producer processes role **/
      /** The value -1 means that the process never intended to produce
          values. Value 0 means that it was planned by the broker but
          no actual data would be transmitted. Greater than zero means
          actual data to transmit. The array is initialized in the first
          dimension component by -1                                       **/
      Length = (DCT_Integer *) malloc( sizeof(DCT_Integer)*(size_t)(dim*prodSD) );
      DCTERROR( Length == (DCT_Integer *)NULL, "Memory Allocation Failed", return(1) );
      for ( ii=0; ii < prodSD; ii++ )
         Length[ii*dim] = -1;
      MsgSlab = (DCT_Scalar **) malloc( sizeof(DCT_Scalar *)*(size_t)(dim*prodSD) );
      DCTERROR( MsgSlab == (DCT_Scalar **)NULL, "Memory Allocation Failed", return(1) );
      /* Indices of producers that have something to send */
      indices = (int *) malloc( sizeof(int)*(size_t)prodSD );
      DCTERROR( indices == (int *)NULL, "Memory Allocation Failed", return(1) );
     
      /** Message tag for producer-consumer comm step **/
      msgtagP = DCT_Msg_Tags_VAR_PROD_PLAN + VarList->VarMsgTag;
      /** Message tag for consumer-producer comm step **/
      msgtagC = DCT_Msg_Tags_VAR_CONS_PLAN + VarList->VarMsgTag;
      /** Massage tag for consumer-producer ACK (Ok) **/
      msgtagACK = DCT_Msg_Tags_VAR_ACK + VarList->VarMsgTag;
   
      /*  Obtaining all Tiles */
      sndmsg = 0; /* Number of messages with something to send from producer */
      for ( ii=0; ii < nmsg; ii++ ) {
         err = MPI_Probe(MPI_ANY_SOURCE, msgtagP, DCT_Comm_World, &status);
         if (err) return(err);
   
         Rcvr = status.MPI_SOURCE;
         MPI_Get_count( &status, MPI_PACKED, &pck );
         /* Search the respective receiver subdomain */
         for ( jj1=0; (jj1 < prodSD) && (Rcvr != ( SndrSDom + jj1 )->RankProc); jj1++ );

         /********** Allocating memory for the package buffer ************/
         buf[jj1] = (char *) malloc( (size_t)pck );
         DCTERROR( buf[jj1] == (char *) NULL, "Memory Allocation Failed", return(1) );
         
         /* The producer info is received */
         err = MPI_Recv( buf[jj1] , pck, MPI_PACKED, Rcvr, msgtagP, DCT_Comm_World, &status );
         if (err) return(err);

         /* The message is unpacked */
         pos = 0;      
         MPI_Unpack(buf[jj1], pck, &pos, &RcvDataType, 1, DCT_INT, DCT_Comm_World);
   
         /* The message is stored */
         jj = dim*jj1;
         if ( RcvDataType == CommSched->VarRcvdDType ) {
            indices[sndmsg++] = jj1;   /*** count and register messages with actual data ***/
            MPI_Unpack(buf[jj1], pck, &pos, (IniVal + jj), dim, DCT_SCALAR, DCT_Comm_World);
            MPI_Unpack(buf[jj1], pck, &pos, (EndVal + jj), dim, DCT_SCALAR, DCT_Comm_World);
            MPI_Unpack(buf[jj1], pck, &pos, (Length + jj), dim, DCT_INT, DCT_Comm_World);
            for (kk = 0; kk < dim; kk++ ) {
               MsgSlab[jj+kk] = (DCT_Scalar *) malloc( sizeof(DCT_Scalar)*(size_t)Length[jj+kk] );
               DCTERROR( MsgSlab[jj+kk] == (DCT_Scalar *)NULL,
                         "Memory Allocation Failed", return(1) );
               MPI_Unpack(buf[jj1], pck, &pos, MsgSlab[jj+kk], Length[jj+kk], DCT_SCALAR,
                                                                                DCT_Comm_World );
            }
         }
         else if ( RcvDataType == DCT_DATA_TYPE_UNKNOWN )  {
            Length[jj] = 0; /* Indicating that no package to received */
         }
         else {
            DCTERROR( CommSched->VarRcvdDType != RcvDataType,
                         "Internal Error. Variable data type is not the same among process",
                         return(1) );
         }
         /* The buffer used is freed */
         free ( buf[jj1] );

      } /* End for ( ii=0; ii < nmsg; ii++ ) */
      
      /*** The slabs defined by the broker are arrived ***/
      /*** The checking process starts ***/

      /** No messages are gonna be received, because no
          producer has data in between the required values **/
      if ( sndmsg == 0 ) {
         CommSched->Nmsg = 0;
         acknow[0] = DCT_FALSE;
         
         for ( ii=0; ii < prodSD; ii++ ) {
            jj1 = ii*dim;
            DCTERROR( Length[jj1] > 0, "Internal Error in Length array", return(1) );
            err = MPI_Isend( acknow, 1, DCT_BOOL, ( SndrSDom + ii )->RankProc,
                                                msgtagACK, DCT_Comm_World, (req+ii) );
            if (err) return(err);
         }
         
         /** Nothing to receive **/
         for (ii=0; ii < dim; ii++ ) {
            CommSched->VarRcvdNpts[ii] = 0;
            CommSched->VarNpts[ii] = 0;
            CommSched->VarIniPos[ii] = 0;
         }
         
         /* Freeing the allocation */
         free ( IniVal );
         free ( EndVal );
         free ( Length );
         free ( MsgSlab );
         free ( indices );

      }
      else {    /* of if ( sndmsg == 0 ) */
   
         size = (DCT_Integer *) malloc( sizeof(DCT_Integer)*(size_t)dim );
         DCTERROR( size == (DCT_Integer *)NULL, "Memory Allocation Failed", return(1) );
         for ( ii=0; ii < dim; ii++ ) size[ii] = 0;
   
         for ( ii=0; ii < dim; ii++ ) {
            IniInd[ii] = (int *) malloc( sizeof(int)*prodSD );
            DCTERROR( IniInd[ii] == (int *)NULL, "Memory Allocation Failed", return(1) );
            EndInd[ii] = (int *) malloc( sizeof(int)*prodSD );
            DCTERROR( EndInd[ii] == (int *)NULL, "Memory Allocation Failed", return(1) );

            size[ii] = CRcv->Npts[ii];
            VarLabels[ii] = (DCT_Scalar *) malloc( sizeof(DCT_Scalar *)*(size_t)size[ii] );
            DCTERROR( VarLabels[ii] == (DCT_Scalar *)NULL, "Memory Allocation Failed",
                                                                               return(1) );
            /* The adjustments are depending of each particular variable */
            /* The original model domain coupling values should remain unaltered   */
            GlbIni[ii] = CRcv->GlbInd[ii];
            GlbEnd[ii] = GlbIni[ii] + CRcv->Npts[ii] - 1;
         }
         
         /** Setting the values to be affected inside of the consumer subdomain **/
         for (ii=0; ii < dim; ii++ ) {
            if ( DomType[ii] == DCT_CARTESIAN ) {
               err = DCT_IncludedIndex_Cartes( Labels[ii], 0, Dim[ii], CRcv->IniVal[ii],
                                              CRcv->EndVal[ii], (CommSched->VarIniPos+ii),
                                              (CommSched->VarNpts+ii), NULL, 0 );
               DCTERROR( err == 0 ,
                         "Error: Subdomain obtained by the broker is out of boundary", return(1) );
            }
            else {
               err = DCT_IncludedIndex_Rectil( Labels[ii], 0, Dim[ii], CRcv->IniVal[ii],
                                                CRcv->EndVal[ii], (CommSched->VarIniPos+ii),
                                                (CommSched->VarNpts+ii), NULL, 0 );
               DCTERROR( err == 0 ,
                         "Error: Subdomain obtained by the broker is out of boundary", return(1) );
            }
         }


         /**** Iterates until all the data adjust the requirements ****/
         chkdom = 1;
         while ( chkdom ) {
            /* Tha slabs are ordered with coordinate values precedence */
            for ( ii=1; ii < sndmsg; ii++ ) {
               pos = 1;
               for ( jj=ii; (jj >= 1) && (pos==1); jj-- ) {
                  /**** The function check is the element below is lower  *****/
                  pos = DCT_CheckandSwap( IniVal, (indices + jj-1), (indices + jj), dim );
               }
            }
      
            for (ii=0; ii < dim; ii++ ) {
               inipos[ii] = 0;
               endpos[ii] = 0;
            }
            
            /** The slabs are positioned in the subdomain
                and the tick marks are generated **/
            /** The first slab is registered **/
            jj1 = dim*indices[0];
            for (jj=0; jj < dim; jj++ ) {
               IniI = inipos[jj];
               IniInd[jj][0] = IniI;
               pckcnt = *(Length + jj1 + jj);
               endpos[jj] = IniI + pckcnt - 1;
               EndInd[jj][0] = endpos[jj];
               /**  Copying the labels  **/
               MSlabptr = *(MsgSlab + jj1 + jj);
               if ( endpos[jj] >= size[jj] ) { /* More room is needed */
                  size[jj] = endpos[jj] + 1;
                  VarLabels[jj] = (DCT_Scalar *) realloc( VarLabels[jj],
                                                sizeof(DCT_Scalar *)*(size_t)size[jj] );
                  DCTERROR( VarLabels[jj] == (DCT_Scalar *)NULL, 
                                                "Memory Reallocation Failed", return(1) );
               }
               for (kk = 0; kk < pckcnt; kk++ ) { /* Actual label copy */
                     VarLabels[jj][IniI + kk] = *(MSlabptr + kk);
               }
            }
            /** The rest slabs are registered **/
            for ( ii=1; ii < sndmsg; ii++ ) {
               jj1 = dim*indices[ii];
               for (jj=0; jj < dim; jj++ ) {
                  MSlabptr = *(MsgSlab + jj1 + jj);
                  if ( *MSlabptr < VarLabels[jj][endpos[jj]] ) {
                     IniI = DCT_index_bsearch( VarLabels[jj], 0, endpos[jj], *MSlabptr, 0);
                  } else 
                     IniI = endpos[jj] + 1;
                  
                  IniInd[jj][ii] = IniI;
                  pckcnt = *(Length + jj1 + jj);
                  endpos[jj] = IniI + pckcnt - 1;
                  EndInd[jj][ii] = endpos[jj];
                  if ( endpos[jj] >= size[jj] ) { /* More room is needed */
                     size[jj] = endpos[jj] + 1;
                     VarLabels[jj] = (DCT_Scalar *)  realloc( VarLabels[jj],
                                                            sizeof(DCT_Scalar *)*(size_t)size[jj] );
                     DCTERROR( VarLabels[jj] == (DCT_Scalar *)NULL, 
                                                   "Memory Allocation Failed", return(1) );
                  }
                  /**  Copying the labels  **/
                  /*! \todo Some labels are copied several times
                            A better way will be found to do it  */
                  for (kk = 0; kk < pckcnt; kk++ ) {
                        VarLabels[jj][IniI + kk] = *(MSlabptr + kk);
                  }
               } /* End of for (jj=0; jj < dim; jj++ ) */
            } /* End of for ( ii=1; ii < sndmsg; ii++ ) */
            
            /** The size of the whole subdomain to be received is store **/
            for (ii=0; ii < dim; ii++ ) {
               CommSched->VarRcvdNpts[ii] = endpos[ii] + 1;
            }
            
            /**** Checking if anything else is needed. If so, check whether
                  more data could be obtained or the subdomain is reduced  ****/
            dimflag = DCT_CheckIfNeedMore( dim, DomType, Labels, /*Dim,*/ VarLabels, endpos,
                                           CommSched, CRcv, Inc, Needit, GlbIni, GlbEnd );
         
            /** Everything needed is on the schedule **/
            if ( dimflag == dim ) {
               /** Sending the Ack to everyone **/
               acknow[0] = DCT_TRUE;
               acknow[1] = DCT_FALSE;
               for ( ii=0; ii < prodSD; ii++ ) {
                  jj = ( Length[ii*dim] > 0 ? 0 : 1 );
                  err = MPI_Isend( &acknow[jj], 1, DCT_BOOL, ( SndrSDom + ii )->RankProc,
                                            msgtagACK, DCT_Comm_World, (req+ii) );
                  if (err) return(err);
               }
               
               chkdom = 0;  /*** No more checks are made  READY TO RECEIVE ***/
               
            } else { /* Else of if ( dimflag == dim ) */

               /** The new resquests are sent **/      
               /* Integer to indicate if search above or below */
               MPI_Pack_size( dim, DCT_INT, DCT_Comm_World, &pckcnt);
               pck = pckcnt;
               /* Values between the search is taken */
               MPI_Pack_size( 2*dim, DCT_SCALAR, DCT_Comm_World, &pckcnt); 
               pck += pckcnt;
               nmsg = 0;
               for ( ii=0; ii < prodSD; ii++ ) {
                  /* Check if the subdomain it has the index */
                  TempSDom = SndrSDom + ii;
                  if ( DCT_OnNewBoundary( TempSDom->IniPos, TempSDom->EndPos ,
                                          GlbIni, GlbEnd, Inc, dim ) ) {
                     
                     /********** Allocating memory for the package buffer ************/
                     buf[ii] = (char *) malloc( (size_t)pck );
                     DCTERROR( buf[ii] == (char *) NULL, "Memory Allocation Failed", return(1) );
                     pos = 0;
                     MPI_Pack( Inc, dim, DCT_INT, buf[ii], pck, &pos, DCT_Comm_World);
                     MPI_Pack( Needit, 2*dim, DCT_SCALAR, buf[ii], pck, &pos, DCT_Comm_World);
                     err = MPI_Isend( buf[ii], pck, MPI_PACKED, TempSDom->RankProc,
                                                         msgtagC, DCT_Comm_World, (req+nmsg) );
                     if (err) return(err);
                     nmsg++;
                  }
               }
               err = MPI_Waitall( nmsg, req, MPI_STATUSES_IGNORE );
               if (err) return(err);

               /*  Obtaining back all new requested tiles */
               for ( ii=0; ii < nmsg; ii++ ) {
                  err = MPI_Probe(MPI_ANY_SOURCE, msgtagP, DCT_Comm_World, &status);
                  if (err) return(err);
            
                  Rcvr = status.MPI_SOURCE;
                  MPI_Get_count( &status, MPI_PACKED, &pck );
                  /* Search the respective receiver subdomain */
                  for ( jj1=0; (jj1 < prodSD) && (Rcvr != ( SndrSDom + jj1 )->RankProc); jj1++ );
         
                  /********** Allocating memory for the package buffer ************/
                  /* The buffer used is freed */
                  free ( buf[jj1] ); /* It was used before */
                  buf[jj1] = (char *) malloc( (size_t)pck );
                  DCTERROR( buf[jj1] == (char *) NULL, "Memory Allocation Failed", return(1) );
                  
                  /* The producer info is received */
                  err = MPI_Recv( buf[jj1] , pck, MPI_PACKED, Rcvr, msgtagP,
                                                     DCT_Comm_World, &status );
                  if (err) return(err);
         
                  /* The message is unpacked */
                  pos = 0;      
                  MPI_Unpack(buf[jj1], pck, &pos, &RcvDataType, 1, DCT_INT, DCT_Comm_World);
            
                  /* The message is stored */
                  jj = dim*jj1;
                  if ( RcvDataType == CommSched->VarRcvdDType ) {
                     if ( Length[jj] <= 0 ) /** It was not included before **/
                        indices[sndmsg++] = jj1;/*** count and register  ***/
                     MPI_Unpack(buf[jj1], pck, &pos, (IniVal + jj), dim, DCT_SCALAR, DCT_Comm_World);
                     MPI_Unpack(buf[jj1], pck, &pos, (EndVal + jj), dim, DCT_SCALAR, DCT_Comm_World);
                     /** if existed before, delete the labels **/
                     if ( Length[jj] > 0 ) {
                        for (kk = 0; kk < dim; kk++ ) {
                           free( MsgSlab[jj+kk] );
                        }
                     }
                     MPI_Unpack(buf[jj1], pck, &pos, (Length + jj), dim, DCT_INT, DCT_Comm_World);
                     for (kk = 0; kk < dim; kk++ ) {
                        MsgSlab[jj+kk] = (DCT_Scalar *) malloc( sizeof(DCT_Scalar) *(size_t) Length[jj+kk] );
                        DCTERROR( MsgSlab[jj+kk] == (DCT_Scalar *)NULL, 
                                                     "Memory Allocation Failed", return(1) );
                        MPI_Unpack(buf[jj1], pck, &pos, MsgSlab[jj+kk], Length[jj+kk], 
                                                             DCT_SCALAR, DCT_Comm_World );
                     }
                  }
                  else if ( (RcvDataType == DCT_DATA_TYPE_UNKNOWN) && (Length[jj] >= 0) )  {
                     DCTERROR( CommSched->VarRcvdDType != RcvDataType,
                                  "Internal Error. Ambiguous information from producer",
                                  return(1) );
                  }
                  else {
                     DCTERROR( CommSched->VarRcvdDType != RcvDataType,
                                  "Internal Error. Variable data type is not the same among process",
                                  return(1) );
                  }
                  /* The buffer used is freed */
                  free ( buf[jj1] );
         
               }
   
               
            } /* End of if ( dimflag == dim ) */
            
         } /* End of while ( chkdom ) */
         
      } /** End of if ( sndmsg == 0 ) **/

   } else {   /* Nothing to receive from the beggining;
                 so, only send Ack to producer          */
      CommSched->Nmsg = 0;
      acknow[0] = DCT_FALSE;
      for ( ii=0; ii < prodSD; ii++ ) {
         err = MPI_Isend( acknow, 1, DCT_BOOL, ( SndrSDom + ii )->RankProc,
                                             msgtagACK, DCT_Comm_World, (req+ii) );
         if (err) return(err);
      }

      /** Nothing to receive **/
      for (ii=0; ii < dim; ii++ ) {  /* Attention with this dim */
         CommSched->VarRcvdNpts[ii] = 0;
         CommSched->VarNpts[ii] = 0;
         CommSched->VarIniPos[ii] = 0;
      }
      
   } /* End of if ( nmsg > 0 )  */
   /****** Communication between producer-consumer finishes ******/
   /**************************************************************/
   free ( buf );

   /* Wait until all ack messages finish */
   err = MPI_Waitall( prodSD, req, MPI_STATUSES_IGNORE );
   if (err) return(err);
   free ( req );

   /*** Filling the DCT_Consume_plan ****/

   /** Changing the definitely var tag **/
   CommSched->VarMsgTag = DCT_Msg_Tags_COMM_EXCH + VarList->VarMsgTag;
   /** if any message  **/
   if ( nmsg > 0 ) {
      CommSched->Nmsg = sndmsg;

      /** The derived data type array is allocated **/
      CommSched->DType = (DCT_Comm_Datatype *) 
                          malloc(sizeof(DCT_Comm_Datatype)*(size_t)sndmsg);
      DCTERROR( CommSched->DType == (DCT_Comm_Datatype *)NULL, "Memory Allocation Failed",
                return(1) );
      /** The use of persistent requests suppose to obtain better performance **/
      CommSched->reqsts = (DCT_Request *) malloc( sizeof(DCT_Request)*(size_t)sndmsg );
      DCTERROR( CommSched->reqsts == (DCT_Request *)NULL, "Memory Allocation Failed", return(1) );
   

      /* The memory buffer where tiles will be received is allocated */
      CommSched->VarRcvdVal = DCT_Data_Allocate( CommSched->VarRcvdDType, 
                                                 CommSched->VarRcvdNpts, dim, 0 );
      DCTERROR( CommSched->VarRcvdVal == NULL, "Memory Allocation Failed", return(1) );

      /** Depending of the filter or interpolation operation the weight
          values are calculated. They substitute the label values */
      err = DCT_Calculate_Weight( CommSched, VarLabels, Labels, DomType, Dim, UserDataType,
                                                                                       dim );
      if (err) return(err);      

      for ( ii=0; ii < sndmsg; ii++ ) {
         jj1 = indices[ii];
         jj = dim*jj1;            
/*                                                   BEGIN DEBUG */
//          printf("Package from %d to %d\n", ( SndrSDom + jj1 )->RankProc, CRcv->Recvr );
//          printf("Data Type: %d\n", UserDataType );
//          printf("Initial Coordinates: [ %d", IniInd[0][ii] );
//          int kk;
//          for ( kk=1; kk < dim; kk++ ) printf(", %d", IniInd[kk][ii] );
//          printf(" ]\n");
//          printf("Tile with: [ %d", Length[jj] );
//          for ( kk=1; kk < dim; kk++ ) printf(", %d", Length[jj + kk] );
//          printf(" ]\n");
//          
/*                                                     END DEBUG */

         /*  Set the datatype as the submatrix of the user one */
         for ( kk=0; kk < dim; kk++ ) 
            Inc[kk] = IniInd[kk][ii];
         /* Set the communication data type that describe the hole chunk en the receiving array */
         err = DCT_Comm_Datatype_Set ( (CommSched->DType+ii), CommSched->VarRcvdDType,
                                       Inc, (Length + jj), CommSched->VarRcvdNpts, dim );
         if (err) return(err);

         /** Creating the persistent requests **/
         err = MPI_Recv_init( CommSched->VarRcvdVal, 1, *(CommSched->DType+ii),
                              ( SndrSDom + jj1 )->RankProc, CommSched->VarMsgTag,
                              DCT_Comm_World, (CommSched->reqsts + ii) );
         if (err) return(err);
         
      }

      /* Freeing the allocation */
      free ( IniVal );
      free ( EndVal );
      free ( Length );
      for ( ii = 0; ii < dim; ii++ ) free ( VarLabels[ii] );
      for ( ii=0; ii < sndmsg; ii++ ) {
         jj = dim*indices[ii];
         for ( kk=0; kk < dim; kk++ ) {
            free( MsgSlab[jj+kk] );
         }
      }
      free ( MsgSlab );
      free ( indices );
      free ( size );
      for ( kk=0; kk < dim; kk++ ) {
         free ( IniInd[kk] );
         free ( EndInd[kk] );
      }

   } /* End of if ( nmsg > 0 ) Filling the DCT_Consume_plan */
   
   return(err);
/* -----------------------------------  END( DCT_ConsumeVar_Comm ) */
}
