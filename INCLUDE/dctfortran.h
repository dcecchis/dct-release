/**************************************************************/
/*              Distributed Coupling Toolkit (DCT)            */
/*                                                            */
/*!
   \file dctfortran.h
  
   \brief Contains the DCT function prototypes used in the
          DCT FORTRAN interface.
  
    This header file contains all the function prototypes available
    for the user when DCT is call from a FORTRAN program.
  
    \attention The function name convention used here is the 
    the lowercase and trailing underscore convention.
  
    \date Date of Creation: March 05, 2010.  
  
    \author Dany De Cecchis: dcecchis@gmail.com
    \author Tony Drummond: LADrummond@lbl.gov

    \copyright GNU Public License.
 */
/**************************************************************/


# ifndef DCTFORTRAN_H
#   define DCTFORTRAN_H

/* -----------------------------------------------  Include Files  */
#include <dct.h>
#include <dctsys.h>

/* -------------------------------------------------  Definitions  */

/*!   In order to be consisten in how the arrays are stored in memory,
      two option are given by defining a directive. DCT_F_ORDER, where in
      memory the last index goes first (column wise) */
#define   DCT_F_ORDER    1

/*!   In order to be consisten in how the arrays are stored in memory,
      two option are given by define a directive. DCT_C_ORDER 
      where the first index goes first */
#define   DCT_C_ORDER    0

/* -----------------------------------------  Function Prototypes  */

/*************************************************************/
/*   Functions associated with DCT_Field data structures     */
/*************************************************************/
int dct_create_field_( DCT_Field *, char *, char *, int *, int *, int *,
                       char *, int, int, int );

int dct_set_field_dims_( DCT_Field *, int *, int *, int *, int*, char *,
                         int );

int dct_set_field_val_location_( DCT_Field *, int *, int *, char *, int );

int dct_set_field_strides_(DCT_Field *, int *, int *, int *, char *, int );

int dct_set_field_time_( DCT_Field *, int *, double  *, int *, char *, int );

int dct_set_field_dist_( DCT_Field *, int *, int  *,int *, char *, int );

int dct_set_field_mask_( DCT_Field *, int *, int *, char *, int );

int dct_set_field_freq_consumption_( DCT_Field *, double *, int *, char *, int );

int dct_set_field_freq_production_( DCT_Field *, double *, int *, char *, int );

int dct_set_field_labels_( DCT_Field *, int *, double *, int *, int *, double *,
                           int *, int *, int *, char *, int );

int dct_set_field_units_( DCT_Field *, int *, int *, char *, int );
                           
int dct_set_field_values_( DCT_Field *, int *, void *, int *, char *, int);

int dct_destroy_field_( DCT_Field *, int *, char *, int );

/*************************************************************/
/*           User Support Operations on Data Fields          */
/*************************************************************/


/*************************************************************/
/*   Functions associated with DCT_3d_Var data structures    */
/*************************************************************/
int dct_create_3d_var_( DCT_3d_Var *, char *, char *, int *, int *,
                        int *, char *, int , int , int );

int dct_set_3d_var_dims_( DCT_3d_Var *, int *, int *, int *, int *,
                          int *, char *, int );
int dct_set_3d_var_val_location_( DCT_3d_Var *, int *, int *, char *, int );

int dct_set_3d_var_strides_( DCT_3d_Var *, int *, int *, int *, int *,
                             char *, int );

int dct_set_3d_var_time_( DCT_3d_Var *, int *, double *, int *, char *, int );

int dct_set_3d_var_dist_( DCT_3d_Var *, int *, int *, int *, char *, int );

int dct_set_3d_var_mask_( DCT_3d_Var *, int *, int *, char *, int  );

int dct_set_3d_var_freq_consumption_( DCT_3d_Var *, double *, int *, char *,
                                      int );
int dct_set_3d_var_freq_production_( DCT_3d_Var *, double *, int *, char *,
                                     int );

int dct_set_3d_var_labels_( DCT_3d_Var *, int *, double *, int *, int *,
                            double *, int *, int *, double *, int *, int *,
                            int *, char *, int );

int dct_set_3d_var_units_( DCT_3d_Var *, int *, int *, char *, int );

int dct_set_3d_var_values_( DCT_3d_Var *, int *, void *, int *, char *, int  );

int dct_destroy_3d_var_( DCT_3d_Var *, int *, char *, int );

/*************************************************************/
/*           User Support Operations on Data 3d_Vars          */
/*************************************************************/

/*************************************************************/
/*   Functions associated with DCT_4d_Var data structures    */
/*************************************************************/
// DCT_Error DCT_Create_4d_Var( DCT_4d_Var *, const DCT_Name, const DCT_String,
//                              const DCT_Units, DCT_ProdCons);
// DCT_Error DCT_Set_4d_Var_Dims( DCT_4d_Var *, const DCT_Integer,
//                                const DCT_Integer, const DCT_Integer,
//                                const DCT_Integer);
// DCT_Error DCT_Set_4d_Var_Val_Location( DCT_4d_Var *, const DCT_Val_Loc);
// DCT_Error DCT_Set_4d_Var_Strides( DCT_4d_Var *, const DCT_Integer,
//                                   const DCT_Integer, const DCT_Integer,
//                                   const DCT_Integer);
// DCT_Error DCT_Set_4d_Var_Time( DCT_4d_Var *, const DCT_Time_Types,
//                                const DCT_Scalar);
// DCT_Error DCT_Set_4d_Var_Dist( DCT_4d_Var *, const DCT_Distribution,
//                                const DCT_Integer *);
// DCT_Error DCT_Set_4d_Var_Mask( DCT_4d_Var *, const DCT_Boolean *);
// DCT_Error DCT_Set_4d_Var_Freq_Production( DCT_4d_Var *, const DCT_Scalar );
// DCT_Error DCT_Set_4d_Var_Freq_Consumption( DCT_4d_Var *, const DCT_Scalar );
// DCT_Error DCT_Set_4d_Var_Labels( DCT_4d_Var *, const DCT_Boolean, const DCT_Scalar *,
//                              const DCT_Integer, const DCT_Boolean, const DCT_Scalar *,
//                              const DCT_Integer, const DCT_Boolean, const DCT_Scalar *,
//                              const DCT_Integer, const DCT_Boolean, const DCT_Scalar *,
//                              const DCT_Integer);
// DCT_Error DCT_Set_4d_Var_Units( DCT_4d_Var *, const DCT_Units);
// DCT_Error DCT_Set_4d_Var_Values( DCT_4d_Var *, const DCT_Data_Types,
//                                  const DCT_VoidPointer);
// DCT_Error DCT_Destroy_4d_Var( DCT_4d_Var * );


/*************************************************************/
/*           User Support Operations on Data 4d_Vars          */
/*************************************************************/

/*************************************************************/
/*   Functions associated with DCT_Model data structures     */
/*************************************************************/
int dct_create_model_( DCT_Model *, char *, char *, int *, int *,
                         char *, int , int , int );

int dct_set_model_time_( DCT_Model *, double *, double *, int *, int *,
                         char *, int  );

int dct_set_model_dom_( DCT_Model *, int *, int *, int *, double *, int *,
                        int *, int *, char *, int  );

int dct_set_model_rspaced_dom_( DCT_Model *, int *, double *, double *,
                                int *, int *, int *, char *, int );

int dct_set_model_gencur_dom_( DCT_Model *, int *, double *, double *,
                               double *, int *, int *, int *, char *, int );

int dct_set_model_parlayout_( DCT_Model *, int *, int *, int *,
                              int *, char *, int );

int dct_set_model_subdom_( DCT_Model *, int *, int *, int *, int *, int *,
                           char *, int );

int dct_set_model_production_( DCT_Model *, DCT_VoidPointer, char *, int *,
                               int *, int *, double *, double *, int *,
                               char *, int , int );

int dct_set_model_consumption_( DCT_Model *, DCT_VoidPointer, char *, int *,
                                int *, int *, double *, double *, int *,
                                char *, int , int );

int dct_set_model_var_( DCT_Model *, DCT_VoidPointer, int *, int *, char *,
                        int  );

int dct_update_model_time_( DCT_Model *, int *, char *, int );

int dct_destroy_model_( DCT_Model *, int *, char *, int );

/*************************************************************/
/*           User Support Operations on Data Models          */
/*************************************************************/

/*************************************************************/
/*   Functions associated with DCT_Couple data structures    */
/*************************************************************/

int dct_create_couple_( DCT_Couple *, char *, char *, DCT_Model *, char *,
                        int *, char *, int, int, int, int );

int dct_set_coupling_vars_( DCT_Couple *, DCT_VoidPointer, char *, int *, int *,
                            int *, char *, int, int );

int dct_destroy_couple_( DCT_Couple *, int *, char *, int );

/*************************************************************/
/*            Functions associated with dct_broker.c         */
/*************************************************************/
int dct_assign_broker_( int *, int *, int *, char *, int );

int dct_beginregistration_( int *, int *, char *, int );

int dct_endregistration_( int *, char *, int );

int dct_finalized_( int *, char *, int );

/*************************************************************/
/*   Functions associated with file I/O operations           */
/*                 Defined in dct_fileio.c                   */
/*************************************************************/
int dct_write_field_( DCT_Field *, int *, char *, int );

int dct_write_3d_var_( DCT_3d_Var *, int *, char *, int );

int dct_read_field_( DCT_Field *, int *, char *, int );

int dct_read_3d_var_( DCT_3d_Var *, int *, char *, int );

/*************************************************************/
/*   Functions associated with messages comm operation       */
/*               Defined in dct_comm_data.c                  */
/*************************************************************/
int dct_send_field_( DCT_Field *, int *, char *, int );

int dct_send_3d_var_( DCT_3d_Var *, int *, char *, int );

int dct_recv_field_( DCT_Field *, int *, char *, int );

int dct_recv_3d_var_( DCT_3d_Var *, int *, char *, int );



# endif /* DCTFORTRAN_H */




