# README #

This README document gives you the steps to be familiar with the Distributed Coupling Toolkit (DCT), how to install and use it.

## What is DCT? ##

Computational models allow scientists to simulate and gain intuition about physical phenomena
or a processes being studied. Different scientific fields are being interested in develop
simulations coupling models, specially the climate modeling or earth sciences application,
where the interaction of different resolution models or sea-atmosphere-land interaction is
specially tackle using coupling model approach. On sake of generality, a computational model
could be seen as a black box in which some input data is given to the model and it outputs
back a set of variables and fields. In the most simple approach, two models are *weakly*
coupled, one *consumer model* uses output data (e.g. variables, fields, etc.) from another
*producer model*, at a given time intervals. Lastly, some data transformation is needed during
the coupling process. The data transformation is required to comply with the physics of the
consumer model.

The Distributed Coupling Toolkit (DCT) is a software library that supports *weak* coupling
between pairs of computational models, and its design provides scalability in both at the
model physical complexity and parallel processing levels.  DCT is collectively used for the
formulation of weak coupling without the user relinquishing the model execution control,
sacrificing computational resources or adding intrusive code from one model into the other
model. The weak coupling formulation involves the description of model computational domains,
variables to be coupled, parallel data layout and model and couple time stepping. The DCT
library is written in ANSI C, and a wrapper interface for Fortran 77/90-95 is also provided.

The current version of DCT is 0.9.

## How do I get set up? ##

### Download and set up ###

#### Download ####
  The DCT can be downloaded via BitBucket. You can clone and checkout the DCT-Release repository
  typing, for example, the commands
```
#!bash
git clone https://<bitbucketuser>@bitbucket.org/dcecchis/dct-release.git DCT
```
  The repository comprises of a directory tree that organizes the DCT source files.
  In the root directory, there is a Makefile to compile and build the library, the `SRC/`
  directory contains the C source files that implements the library; in the `INCLUDE/` directory
  there are the C header files; inside `SRC/FINTERF` there is the implementation of the
  Fortran wappers; The directory `TEST/` has some test programs to test the DCT functionalities.
  And the directories `LIB/` and `BIN/` are to store the library files and test executables
  respectively.

#### Installation Requirements ####
 * make
 * C compiler
 * Fortran compiler

#### Libraries ####
 * [Message Passing Interface (MPI)](https://en.wikipedia.org/wiki/Message_Passing_Interface)

#### Set up ####
  The `Makefile`, in the root directory, must be set to compile and build the library. First, the
  directory tree must be set, the default parameters usually work well. However, if the DCT
  is required to be hosted in a different place, for example, the `LIBDIR` variable must be
  set accordingly.
```
#!make
ROOTLIB = $(PWD)
SOURCE  = $(ROOTLIB)/SRC
FINTER  = $(ROOTLIB)/SRC/FINTERF
TESTDIR = $(ROOTLIB)/TEST
BINDIR  = $(ROOTLIB)/BIN
LIBDIR  = $(ROOTLIB)/LIB
LIBINC  = $(ROOTLIB)/INCLUDE
```
  Once the directories are set, the compilers must be given. The DCT requires to be compiled
  and used a [Message Passing Interface (MPI)](https://en.wikipedia.org/wiki/Message_Passing_Interface)
  library installed in the host.
  Thus, the MPI compilers must be settled in the corresponding variables, e.g. `MPICC = mpicc`,
  and make sure that, if the MPI library paramenteres are not in the system, they must be declared
  by hand providing the corresponding directory paths for the include files (using the compiler
  flag `-I`) and the library files (using the compiler option `-I`).

#### Compiling DCT ####

  When the Makefile is ready, to compile the DCT you should type the command:
```
#!bash
$ make library
```
  If it goes without errors, the library is built and installed in the directory given in
  the variable `$LIBDIR`; i.e. `LIB/`, by default. In that directory it is found the file
  `libdct.a` which is the file need to link after to compile the application that uses the DCT
  functionalities. In addition, for the use of the DCT from a model written in Fortran 77/90-95,
  then just type the command
```
#!bash
$ make finterf
```
  And again, if no errors, then in the same directory is the file `libfdct.a`,  that it would
  be used to link the library to the executable.

#### How to run tests ####

  The DCT has a set of test that users can use to check if the library is working in the system.
  The test provided with the system, is only dummy model that produce data to be exchanged between
  concurrently programs as a model coupling should be set. The test that you can compile and run
  there are:     

  Name of Test       |            Description 
  :---------------------- | :---------------------------------
  `TEST_DCT_PERFORM`      | One way coupling using 2D domain and one DCT_Field data structure.
  `TEST_DCT_PERFOR_LOOPS` | Fully two way coupling using 2D domain and two DCT_Field data structure.
  `TEST_SUBDOM`           | Fortran fully two way coupling between 3 model using 2D domain and DCT_Field structures.
  `TEST_DCT_PERF_3D`      | One way coupling using 2D domain and one DCT_3d_Var data structure.

  Each test can be built just typing
```
#!bash
$ make <Name_of_Test>
```
  Then, if no error, the executable is under the directory set in the variable `$LIBDIR`, i.e.
  `BIN/` by default. The executable must be run using the correct paramaeters in each case. Please,
  see the test source code, or try to run the test to get the help message about the paramaters
  that it must be passed. An example of run script where the output for different process are
  stored in different files is given in the file `run_test`. In general case, the command to run
  a test is
```
#!bash
$ mpirun -np <NumProc> ./<Name_of_Test> [test_parameters]
```

## How do I use the library ##

The DCT is a software library that it provides an
[application programming interface (API)](https://en.wikipedia.org/wiki/Application_programming_interface)
to perform *weak* coupling
between models and comprises a high-level user interface to formulate coupling, and also
represents abstractions of model properties, including: variables, domains, units, and
relationships between variables and models.

Two key design abstractions are used in the DCT to address these requirements and to implement
a distributed coupling approach:   

  * Data structures that capture the model coupling formulation.
  * The design of phases that conceptually separates the descriptive coupling

### DCT Data Structures ###

The DCT data structures are able to get the description of the model coupling in an easy and
intuitive manner, users describe the variables and models involved in the coupling process,
as well as the coupling process itself. This includes a description of the variables, their
values, and their context, that will be needed during the coupling process.

The DCT API provides three kinds of abstract data structures to represent variables, models, and
couplings.  

  * DCT_Field and DCT_3d_Var.
  * DCT_Model.
  * DCT_Couple.

The DCT variables include: DCT_Field and DCT_3d_Var, which describe 2- and 3-dimensional
variables, respectively. The DCT_Model structure describes a numerical model and it has associated
a number of variables depending on its domain dimension. In a computational model, conceptually
more than one DCT_Model can be defined, in order to bring more flexibility to describes sub-parts
or sub-dimensions of a model. The DCT_Couple structure describes the coupling between a pair of
DCT_Model ’s. Like DCT_Model, in a computational model several DCT_Couple instances can be
declared. The user has to declare, for a DCT_Couple instance, the local DCT_Model and the remote DCT_Model name that are going to be coupled. In addition of that, the variables, the local structures as well as the remote name of the counterpart variable has to be linked with the DCT_Couple.

### DCT Phases ###

The DCT comprises the definition of the three phases: *Registration*, *Coupling*, and *Finalize*.
All three phases should be present in a coupling application that uses the DCT. The defintion of these phases provide the advantage of organize the user code, and to keep separated the descriptive part of the coupling from the operational or exchanging part of the code.

The DCT’s *Registration phase* is a flexible mechanism for modelers to formulate the coupling between *n* pairs of computational models. During the *Registration phase*, all the DCT’s structures are set up with the information to be used during the coupling operations. This phase is defined between two function calls: DCT_BeginRegistration(), which defines when the registration phase begins, and DCT_EndRegistration(), which indicates the end of the registration phase. All active PEs in the coupling must call these functions to formulate a coherent parallel coupling. After this phase, no changes to DCT data structures is allowed by the DCT API.

The coupling phase is part of the models’ time step, and is where the data exchanges are performed in an automatic manner, through a simple invocation to DCT send or DCT receive functions. This phase consists of the actual coupling operations to be inserted in the iteration steps of the numerical models. If a model produces a field or variable, it must call DCT_Send_Field() or DCT_Send_3d_Var(), respectively. Internally, every process element (PE) will check whether it is time to send the data to the consumer model. When affirmative, the data is sent directly to its corresponding PE in the consumer model. In cases where the field or variable is declared to be consumed, the user must then use DCT_Recv_Field() or DCT_Recv_3d_Var(), respectively; these functions check whether it is time to consume the data. The process of consuming a variable is comprised of: receiving data messages directly from the producer model PEs, assembling the subdomain from individual messages, transforming the data into the consumer grid, and converting the values into the local units. Once this is done, the transformed values are available in the array pointed by the DCT_Field or DCT_3d_Var given by the user.

The finalizing phase is provided to ensure the correct completion of all coupling steps and the deallocation of the DCT’s data structures; this phase is required to ensure robust completion of parallel computations.

### Code Example ###

* Code review
* Other guidelines

## Who do I talk to? ##

* Repo owner or admin: [Dany De Cecchis](mailto:dcecchis@gmail.com)
* Other community or team contact